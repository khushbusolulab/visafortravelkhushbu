<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_v1 extends CI_Controller {

    public $email_config = array();

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();

        $this->load->model('Api_v1_model');
        $this->email_config = $this->config->item('email_config');
        log_message("debug", "REQUEST: " . serialize($_SERVER));
        log_message("debug", "REQUEST: " . serialize($_REQUEST));
    }

    public function index() {
        $this->load->view('welcome_message');
    }

    /* login */

    public function Login() {
        if (!isset($_REQUEST['emailId'])) {
            $Login = array("Failure" => array("message" => "Email address is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['password'])) {
            $Login = array("Failure" => array("message" => "Password is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceId'])) {
            $Login = array("Failure" => array("message" => "deviceId is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceToken'])) {
            $Login = array("Failure" => array("message" => "deviceToken is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceType'])) {
            $Login = array("Failure" => array("message" => "deviceType is required"), "StatusCode" => 400);
        } else {
            $loginArr = array();

            if (!isset($_REQUEST['latitude']))
                $latitude = "";
            else
                $latitude = $_REQUEST['latitude'];

            if (!isset($_REQUEST['longitude']))
                $longitude = "";
            else
                $longitude = $_REQUEST['longitude'];
            
            $emailExistCheck = $this->Api_v1_model->CheckEmailIdExist($_REQUEST['emailId']);
            if(!empty($emailExistCheck)){
                $resDevice = $this->Api_v1_model->checkDeviceIdExist($_REQUEST['deviceId']);
                if (!empty($resDevice)) {
                    $resDevice = $this->Api_v1_model->checkDeviceIdExist($_REQUEST['deviceId']);
                    $userData = $this->Api_v1_model->getUserById($resDevice[0]->userId);
                    //print_R($userData);exit;

                    if ($userData[0]->profilePicture == "")
                        $imagePath = "";
                    else
                        $imagePath = base_url() . "images/users/" . $userData[0]->profilePicture;

                    /*if ($userData[0]->profilePictureThumb == "")
                        $imagePathThumb = "";
                    else
                        $imagePathThumb = base_url() . "images/users/" . $userData[0]->profilePictureThumb;*/

                    $code['userId'] = $userData[0]->userId;
                    $code['userName'] = $userData[0]->fullName;
                    $code['emailId'] = $userData[0]->emailId;
                    $code['accessCode'] = $userData[0]->accessCode;
                    //$code['newUser']=$userData[0]->newUser;
                    $code['mobile'] = $userData[0]->mobile;
                    $code['image'] = $imagePath;
                    //$code['thumbImage'] = $imagePathThumb;
                    $message = $code;

                    $Login = array("Success" => $message, "StatusCode" => 200);
                }else {

                    if ($_REQUEST['password'] != "" && $_REQUEST['emailId'] != "") {
                        $login = $this->Api_v1_model->login($_REQUEST['emailId'], $_REQUEST['password']);
                        if ($login == 'verify') {
                            $Login = array("Failure" => array("message" => "Please verify your Email prior to login."), "StatusCode" => 400);
                        } else if (!empty($login)) {


                            $idres = $this->Api_v1_model->getidByEmail($_REQUEST['emailId']);
                            //print_R($login);exit;
                            $updateLatLong = $this->Api_v1_model->updateLatLong($login[0]->userId, $latitude, $longitude);
                            $updateToken = $this->Api_v1_model->updateToken($_REQUEST['deviceId'], $_REQUEST['deviceToken'], $login[0]->userId, $_REQUEST['deviceType']);
                            if ($login[0]->accessCode != "" && $login[0]->accessCode != NULL && $login[0]->accessCode != 0) {
                                $accessCode = $login[0]->accessCode;
                            } else {
                                $accessCode = $this->Api_v1_model->generateRandomString();
                                $updateAccessCode = $this->Api_v1_model->updateAccessCode($accessCode, $login[0]->userId);
                            }
                            if ($login[0]->profilePicture == "")
                                $imagePath = "";
                            else
                                $imagePath = base_url() . "images/users/" . $login[0]->profilePicture;

                            /*if ($userData[0]->profilePictureThumb == "")
                                $imagePathThumb = "";
                            else
                                $imagePathThumb = base_url() . "images/users/" . $userData[0]->profilePictureThumb;*/


                            $Login = array("Success" => array("userId" => $login[0]->userId, "userName" => $login[0]->fullName, "mobile" => $login[0]->mobile, "image" => $imagePath, "emailId" => $login[0]->emailId, "accessCode" => $accessCode), "StatusCode" => 200);
                        } else {
                            $Login = array("Failure" => array("message" => "Email address and/or password do not match"), "StatusCode" => 400);
                        }
                    } else {
                        $Login = array("Failure" => array("message" => "Email address and/or password do not match"), "StatusCode" => 400);
                    }
                }
            }else {
                $Login = array("Failure" => array("message" => "Email address does not exist"), "StatusCode" => 400);
            }
            
        }
        log_message("debug", "RESPONSE" . serialize($Login));
        echo json_encode($Login);
    }

    /* login */

    /* registration */

    public function Registration() {
        if (!isset($_REQUEST['emailId'])) {
            $Registration = array("Failure" => array("message" => "Email address is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['password'])) {
            $Registration = array("Failure" => array("message" => "Password is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceToken'])) {
            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceId'])) {
            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceType'])) {
            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['latitude'])) {
            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['longitude'])) {
            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['fullName'])) {
            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
        } else {
            $email = $_REQUEST['emailId'];
            $mobile = (isset($_REQUEST['mobile']) && !empty($_REQUEST['mobile'])) ? $_REQUEST['mobile'] : '';
            $profilePicture = '';
            $password = $_REQUEST['password'];
            $deviceToken = $_REQUEST['deviceToken'];
            $deviceId = $_REQUEST['deviceId'];
            $deviceType = $_REQUEST['deviceType'];
            $longitude = $_REQUEST['longitude'];
            $latitude = $_REQUEST['latitude'];
            $userName = $_REQUEST['fullName'];
            $imagePath = "";


            $emailCheck = $this->Api_v1_model->CheckEmailExsist($email);
            if ($emailCheck == 0) {

                $normalRegiToDeviceId = $this->Api_v1_model->normalRegiToDeviceId($_REQUEST['deviceId']);
                $normalRegiToDeviceId = explode(',', $normalRegiToDeviceId[0]->emails);
                //print_R($normalRegiToDeviceId);exit;
                $resDevice = $this->Api_v1_model->checkDeviceIdExist($_REQUEST['deviceId']);
                //print_R($resDevice);exit;	
                if (!empty($resDevice) && !in_array($_REQUEST['emailId'], $normalRegiToDeviceId) && !empty($normalRegiToDeviceId[0])) {
                    $resDevice = $this->Api_v1_model->checkDeviceIdExist($_REQUEST['deviceId']);
                    $userData = $this->Api_v1_model->getUserById($resDevice[0]->userId);
                    //print_R($userData);exit;
                    $code['userId'] = $userData[0]->userId;
                    $code['userName'] = $userData[0]->fullName;
                    $code['email'] = $userData[0]->emailId;
                    $message = $code;
                    $i = 'yes';
                    $Registration = array("Success" => $message, "StatusCode" => 200);
                } else {
                    $verificationCode = $this->generateRandomToken(15, 'small_alpha_numeric');
                    $signUp = $this->Api_v1_model->Registration($email, $mobile, $password, $imagePath, $verificationCode, $longitude, $latitude, $userName);

                    if ($signUp > 0) {
                        $insertUserSignup = $this->Api_v1_model->Signup($signUp, $deviceToken, $deviceId, $deviceType);
                        if ($insertUserSignup > 0) {
                            $data['verificationCode'] = $verificationCode;
                            $data['userId'] = $signUp;
                            $id = $insertUserSignup;
                            $this->load->library('email');
                            $this->email->initialize($this->email_config);
                            $this->email->set_mailtype("html");
                            $this->email->set_newline("\r\n");
                            $this->email->from('admin@solulab.com'); // change it to yours
                            $this->email->to($email); // change it to yours
                            $this->email->subject('Confirm Registration');
                            $message = $this->load->view('verifyRegistration', $data, TRUE);
                            $this->email->message($message);
                            if ($this->email->send()) {
                                $Registration = array("Success" => array("message" => "Please check your mail"), "StatusCode" => 200);
                            } else {
                                $Registration = array("Failure" => array("message" => show_error($this->email->print_debugger())), "StatusCode" => 400);
                            }
                            //$Registration = array("Success" => array("message" => "Please verify your mail for the Email Verification Email"), "StatusCode" => 202);
                        } else {
                            $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
                        }
                    } else {
                        $Registration = array("Failure" => array("message" => "Error occurred during registration. Please try again"), "StatusCode" => 400);
                    }
                }
            } else {
                $Registration = array("Failure" => array("message" => "This email address is already registered. Please choose another email address or login to access your account"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($Registration));
        echo json_encode($Registration);
    }

    /* registration */

    function verifyRegistration() {
        $verificationCode = $this->uri->segment(3);
        $userId = $this->uri->segment(4);
        $userdata = $this->Api_v1_model->getUserById($userId);
        if (!empty($userdata) && count($userdata) > 0) {
            $res = $this->Api_v1_model->checkVerificationCode($verificationCode, $userId);
            if ($res == 1) {
                echo "Congrats, You Email id verified. Now you can login into the App.";
                $deleteRes = $this->Api_v1_model->deleteVerificationCode($verificationCode, $userId);
            } else
                echo "Error occurred while verifying the registration. Please try again";
        } else {
            echo "User does not exist. Please check theh link you have entered.";
        }
    }

    /* Forgot Password */

    function forgotPassword() {
        if (!isset($_REQUEST['emailId'])) {
            $forgotPassword = array("Failure" => array("message" => "Email address is required"), "StatusCode" => 400);
        } else {
            $email = $_REQUEST['emailId'];

            if ($email != "") {
                $getid = $this->Api_v1_model->getUserId($email);
                $res = $this->Api_v1_model->getUserById($getid[0]->userId);
                if (isset($getid[0]->userId)) {
                    $id = $getid[0]->userId;

                    $randomString = $this->generateRandomToken($length = 20, 'alpha_numeric');
                    $this->Api_v1_model->saveUserToken($getid[0]->userId, $randomString);

                    $data['id'] = $randomString;
                    $url = base_url()."index.php/Api_v1/resetPassword/$randomString";
                    $email_temp = $this->Api_v1_model->getEmailTemplate(2);                
                    $this->load->library('email');
                    $this->email->initialize($this->email_config);
                    //$this->email->initialize($config);
                    $this->email->set_mailtype("html");
                    $this->email->set_newline("\r\n");
                    $this->email->from('admin@solulab.com');
                    $this->email->to($email); // change it to yours
                    $this->email->subject($email_temp[0]['subject']);
                    $message = $email_temp[0]['description'];
                    $message = str_replace('%reset_url%', "$url", $message);
                    $this->email->message($message);
                    if ($this->email->send()) {
                        $forgotPassword = array("Success" => array("message" => "We have sent reset password instructions to your registered email. Please check your email address"), "StatusCode" => 200);
                    } else {
                        $forgotPassword = array("Failure" => array("message" => show_error($this->email->print_debugger())), "StatusCode" => 400);
                    }
                } else {
                    $forgotPassword = array("Failure" => array("message" => "Not a registered email address"), "StatusCode" => 400);
                }
            } else {
                $forgotPassword = array("Failure" => array("message" => "Email address is required"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($forgotPassword));
        echo json_encode($forgotPassword);
    }

    /* Forgot Password */

    /* Reset Password */

    function resetPassword() {
        $token = $this->uri->segment(3);
        $result = $this->Api_v1_model->getTokenDetails(urldecode($token));
        if ($result) {
            if ($result[0]->status == 1) {
                die('Verification token is already used.');
            } else {
                $data = $result[0];
                $this->load->view("forgotPassword", $data);
            }
        } else {
            $forgotPassword = array("Failure" => array("message" => ""), "StatusCode" => 400);
            die('Invalid token. Please click on the link given in email to reset your password.');
        }
    }

    /* Reset Password */

    /* Set New Password */

    function setNewPassword() {
        $id = $_REQUEST['id'];
        $password = $_REQUEST['password'];
        $token = $_REQUEST['token'];

        $res = $this->Api_v1_model->setNewPassword($id, $password);
        if ($res > 0) {
            $this->Api_v1_model->updateUserToken($id, $token);
            $data['data'] = $this->Api_v1_model->getUserById($id);

            $email_temp = $this->Api_v1_model->getEmailTemplate(1);

            $this->load->library('email');
            $this->email->initialize($this->email_config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");
            $this->email->from('admin@solulab.com'); // change it to yours
            $this->email->to($data['data'][0]->emailId); // change it to yours
            $this->email->subject($email_temp[0]['subject']);
            $message = $email_temp[0]['description'];
            $message = str_replace('%useremail%', $data['data'][0]->emailId, $message);
            $this->email->message($message);
            if ($this->email->send()) {
                $forgotPassword = array("Success" => array("message" => "Please check your mail"), "StatusCode" => 200);
            } else {
                $forgotPassword = array("Failure" => array("message" => show_error($this->email->print_debugger())), "StatusCode" => 400);
            }
            echo "Congratulations! Your password has been reset successfully.";
        } else {
            echo "Error occurred during setting new password. Please try again";
        }
    }

    /* Set New Password */

    function generateRandomToken($length = 10, $string_type = 'all') {
        $random_string = "";
        $all_chars = "";

        if ($string_type == 'small_alpha') {
            $all_chars = "abcdefghijklmnopqrstuvwxyz";
        } else if ($string_type == 'caps_alpha') {
            $all_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if ($string_type == 'small_alpha_numeric') {
            $all_chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        } else if ($string_type == 'caps_alpha_numeric') {
            $all_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if ($string_type == 'alpha_numeric') {
            $all_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if ($string_type == 'numeric') {
            $all_chars = "0123456789";
        }

        if ($string_type != 'all') {
//loop to generate string
            for ($i = 0; $i < $length; $i++) {
                $random_string .= $all_chars[rand(0, strlen($all_chars) - 1)];
            }
        }

//if none of the type found the consider default type and use all characters
//for password this will be used
        if ($string_type == 'all') {
            $all_part = substr(str_shuffle("*^%#@!0123456789abcdefghijklmno*^%#@!qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length - 3);
            $caps_part = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1);
            $number_part = substr(str_shuffle("0123456789"), 0, 1);
            $special_part = substr(str_shuffle("*^%#@!"), 0, 1);

            $random_string = $number_part . $all_part . $caps_part . $special_part;
        }

        return $random_string;
    }

    function socialLogin() {
        if (!isset($_REQUEST['emailId']) || empty($_REQUEST['emailId']) || $_REQUEST['emailId'] == "" || $_REQUEST['emailId'] == NULL) {
            $socialLogin = array("Failure" => array("message" => "Email address is required to login. Please allow access to use your email address."), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceToken'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceId'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceType'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['latitude'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['longitude'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else {
            
            $mobile = isset($_REQUEST['mobile']) ? $_REQUEST['$mobile'] : '';
            $facebookId = isset($_REQUEST['facebookId']) ? $_REQUEST['facebookId'] : '';
            $googleId = isset($_REQUEST['googleId']) ? $_REQUEST['googleId'] : '';
            
            $emailExistCheck = $this->Api_v1_model->CheckEmailIdExist($_REQUEST['emailId']);
            if(!empty($emailExistCheck)){
                //email exist
                //now check if email is link with this device or not?
                $emailLinkedToDeviceId = $this->Api_v1_model->getEmailLinkedToDeviceId($_REQUEST['deviceId'],$_REQUEST['emailId']);
                if(!empty($emailLinkedToDeviceId)){
                    //email+device id exists already registered user
                    if($facebookId != "" && $googleId == ""){
                        $checkFacebookId = $this->Api_v1_model->checkFacebookId($_REQUEST['emailId'],$_REQUEST['facebookId']);
                        if($checkFacebookId > 0){
                            //email with this fb id exsits
                            //do direct login
                            echo "already";
                            $id = $this->Api_v1_model->getidByEmail($_REQUEST['emailId']);
                            $logincount = $id[0]->loginCounts + 1;
                            //$data['last_login'] = $today;
                            $data['loginCount'] = $logincount;
                            $data['userId'] = $id[0]->userId;
                            $data['verificationCode'] = '';
                            
                            
                            $updateLatLong = $this->Api_v1_model->updateLatLong($id[0]->userId, $_REQUEST['latitude'], $_REQUEST['longitude']);
                            $updateLastLogin = $this->Api_v1_model->updateLastLogin($data['loginCount'], $data['userId'], $data['verificationCode']);
                            //$updateNewUser = $this->Api_v1_model->updateNewUser($user_id);
                            $updateToken = $this->Api_v1_model->updateToken($_REQUEST['deviceId'], $_REQUEST['deviceToken'], $data['userId'], $_REQUEST['deviceType']);
                            $code['accessCode'] = $id[0]->accessCode;
                            $code['userId'] = $id[0]->userId;
                            $code['email'] = $id[0]->emailId;
                            $code['userName'] = ($id[0]->fullName != "") ? $id[0]->fullName : "";
                            $code['mobile'] = $id[0]->mobile;
                            $message = $code;
                            $socialLogin = array("Success" => $message, "StatusCode" => 200);
                        }else{
                            
                        }
                    }else if($facebookId == "" && $googleId != ""){
                        
                    }else{
                        //no id
                        $socialLogin = array("Failure" => array("message" => "No social id provided"), "StatusCode" => 400);
                    }
                }else{
                    //not a registered user with this device add new entry for regi user
                    echo "here";
                    $id = $this->Api_v1_model->getidByEmail($_REQUEST['emailId']);
                    $logincount = $id[0]->loginCounts + 1;
                    $data['loginCount'] = $logincount;
                    $data['userId'] = $id[0]->userId;
                    $data['verificationCode'] = '';
                          
                    $type = isset($_REQUEST['googleId']) ? $_REQUEST['googleId']: (isset($_REQUEST['facebookId']) ? $_REQUEST['facebookId'] : '');
                    $keyId = isset($_REQUEST['googleId']) ? 'googleId' : (isset($_REQUEST['facebookId']) ? 'facebookId' : '');
                    $social['userId'] = $id[0]->userId;
                    //$ans = $this->Api_v1_model->addSocialLogin($social, $keyId, $type, $_REQUEST['emailId']);
                    $updateLatLong = $this->Api_v1_model->updateLatLong($id[0]->userId, $_REQUEST['latitude'], $_REQUEST['longitude']);
                    $updateLastLogin = $this->Api_v1_model->updateLastLogin($data['loginCount'], $data['userId'], $data['verificationCode']);
                    //$updateNewUser = $this->Api_v1_model->updateNewUser($user_id);
                    $updateToken = $this->Api_v1_model->updateToken($_REQUEST['deviceId'], $_REQUEST['deviceToken'], $data['userId'], $_REQUEST['deviceType']);
                    $code['accessCode'] = $id[0]->accessCode;
                    $code['userId'] = $id[0]->userId;
                    $code['email'] = $id[0]->emailId;
                    $code['userName'] = ($id[0]->fullName != "") ? $id[0]->fullName : "";
                    $code['mobile'] = $id[0]->mobile;
                    $message = $code;
                    $socialLogin = array("Success" => $message, "StatusCode" => 200);
                }
            }else{
                
                echo "new email";
                if($facebookId == "" && $googleId == ""){
                    $socialLogin = array("Failure" => array("message" => "No social id provided"), "StatusCode" => 400);
                    log_message("debug", "RESPONSE" . serialize($socialLogin));
                    echo json_encode($socialLogin);exit;
                }
                //new registration
                $userId = $this->Api_v1_model->registersocialLogin($_REQUEST);
                $social['userId'] = $userId;
                $type = isset($_REQUEST['googleId']) ? $_REQUEST['googleId']: (isset($_REQUEST['facebookId']) ? $_REQUEST['facebookId'] : '');
                $keyId = isset($_REQUEST['googleId']) ? 'googleId' : (isset($_REQUEST['facebookId']) ? 'facebookId' : '');
                
                $id = $this->Api_v1_model->getidByEmail($_REQUEST['emailId']);
                $logincount = $id[0]->loginCounts + 1;
                $data['loginCount'] = $logincount;
                $data['userId'] = $id[0]->userId;
                $data['verificationCode'] = '';
                    
                $deleteDevice  = $this->Api_v1_model->deleteDevice($_REQUEST['deviceToken']);
                
                $ans = $this->Api_v1_model->addSocialLogin($social, $keyId, $type, $_REQUEST['emailId']);
                $updateLatLong = $this->Api_v1_model->updateLatLong($id[0]->userId, $_REQUEST['latitude'], $_REQUEST['longitude']);
                $updateLastLogin = $this->Api_v1_model->updateLastLogin($data['loginCount'], $data['userId'], $data['verificationCode']);
                $updateToken = $this->Api_v1_model->updateToken($_REQUEST['deviceId'], $_REQUEST['deviceToken'], $data['userId'], $_REQUEST['deviceType']);
                //print_R($ans);
                $data['email'] = $_REQUEST['emailId'];
                $data['password'] = $this->generateRandomToken(10, 'small_alpha_numeric');
                $updateUserData['password'] = md5($data['password']);

                $email_temp = $this->Api_v1_model->getEmailTemplate(4);
                $this->load->library('email');
                $this->email->initialize($this->email_config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");
                $this->email->from('admin@solulab.com');
                $this->email->to($data['email']); // change it to yours
                $this->email->subject($email_temp[0]['subject']);
                $message = $email_temp[0]['description'];
                $message = str_replace('%useremail%', $data['email'], $message);
                $message = str_replace('%password%', $data['password'], $message);
                $this->email->message($message);
                $this->email->send();

                $user = $this->Api_v1_model->getUserById($userId);
                $accessCode = $this->Api_v1_model->generateRandomString();
                $updateUserData['accessCode'] = $accessCode;
                $updateUserData['loginCount'] = $user[0]->loginCounts + 1;
                //$updateUserData['last_login'] = $today; 
                $updateUserData['isBlocked'] = 0;
                $updateUserData['isConfirmed'] = 1;
                $code['accessCode'] = $user[0]->accessCode;
                $code['userId'] = $user[0]->userId;
                $code['email'] = $user[0]->emailId;
                $code['userName'] = ($user[0]->fullName != "") ? $user[0]->fullName : "";
                $code['mobile'] = $user[0]->mobile;
                $message = $code;
                $socialLogin = array("Success" => $message, "StatusCode" => 200);
            }
        }
        log_message("debug", "RESPONSE" . serialize($socialLogin));
        echo json_encode($socialLogin);
    }

    function testUpload() {


        $target_dir = "images/media/";

        foreach ($_FILES as $val) {
            $microtime = microtime();

            $fileName = $val["name"];
            $extension = explode(".", $fileName);
            $Path = "";
            if ($_REQUEST['flag'] == "image") {
                $flag = "image";
                $Path = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
            } else {
                $flag = "video";
                $Path = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
            }

            $target_dir2 = $target_dir . $Path;
            //move_uploaded_file($val["tmp_name"], $target_dir2);
//$res	=	$this->Api_v1_model->inspiration($_REQUEST['userId'],$_REQUEST['message'],$_REQUEST['flag'],$Path);

            if (move_uploaded_file($val["tmp_name"], $target_dir2)) {
                $inspiration = array("Success" => array("message" => "inspiration added successfully"), "StatusCode" => 200);
            } else {
                $inspiration = array("Failure" => array("message" => "Failed to add inspiration"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($inspiration));
        echo json_encode($inspiration);
    }

    function logOut() {
        if (!isset($_REQUEST['accessCode'])) {
            $logOut = array("Failure" => array("message" => "Access Code not found"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['userId'])) {
            $logOut = array("Failure" => array("message" => "User Id not found"), "StatusCode" => 400);
        } else {
            $status = $this->Api_v1_model->ifExistUserId($_REQUEST['userId']); /* if User id does not exists */
            if ($status == 1) {
                $valid = $this->Api_v1_model->checkAccessCode($_REQUEST['userId'], $_REQUEST['accessCode']);
                if ($valid == 1) {
                    $res = $this->Api_v1_model->logOut($_REQUEST['accessCode'], $_REQUEST['userId'], $_REQUEST['deviceId']);
                    if ($res != 0) {
                        $logOut = array("Success" => array("message" => "Logged out successfully"), "StatusCode" => 200);
                    } else {
                        $logOut = array("Failure" => array("message" => "Failed to logout"), "StatusCode" => 400);
                    }
                } else {
                    $logOut = array("Failure" => array("message" => "Access code does not match"), "StatusCode" => 400);
                }
            } else {
                $logOut = array("Failure" => array("message" => "User does not exists"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($logOut));
        echo json_encode($logOut);
    }

    function profile() {
        /*if (!isset($_REQUEST['age'])) {
            $profile = array("Failure" => array("message" => "Age is required"), "StatusCode" => 400);
        }  else if (!isset($_REQUEST['DOB'])) {
          $profile = array("Failure" => array("message" => "DOB is required"), "StatusCode" => 400);
          }  else */ if (!isset($_REQUEST['userId'])) {
            $profile = array("Failure" => array("message" => "Error occurred while editing profile. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['userName'])) {
            $profile = array("Failure" => array("message" => "Error occurred while editing profile. Please try again"), "StatusCode" => 400);
        } /* else if (!isset($_REQUEST['location'])) {
          $profile = array("Failure" => array("message" => "Location is required"), "StatusCode" => 400);
          } else if (!isset($_REQUEST['affiliation'])) {
          $profile = array("Failure" => array("message" => "Affiliation is required"), "StatusCode" => 400);
          } else if (!isset($_REQUEST['goals'])) {
          $profile = array("Failure" => array("message" => "Goals is required"), "StatusCode" => 400);
          } else if (!isset($_REQUEST['favActivitiy'])) {
          $profile = array("Failure" => array("message" => "Favourite Activity is required"), "StatusCode" => 400);
          } else if (!isset($_REQUEST['favWorkout'])) {
          $profile = array("Failure" => array("message" => "Favourite Workout is required"), "StatusCode" => 400);
          } else if (!isset($_REQUEST['other'])) {
          $profile = array("Failure" => array("message" => "other is required"), "StatusCode" => 400);
          } else if(!isset($_REQUEST['personalWebsite'])){
          $profile	=	array("Failure" => array("message"=>"personalWebsite is required"),"StatusCode"=>400);
          } else if(!isset($_REQUEST['youtubeChannel'])){
          $profile	=	array("Failure" => array("message"=>"youtubeChannel is required"),"StatusCode"=>400);
          } else if(!isset($_REQUEST['twitterPage'])){
          $profile	=	array("Failure" => array("message"=>"twitterPage is required"),"StatusCode"=>400);
          } else if (!isset($_REQUEST['fitnessPro'])) {
          $profile = array("Failure" => array("message" => "Error occurred while editing profile. Please try again"), "StatusCode" => 400);
          } */ else if (!isset($_REQUEST['accessCode'])) {
            $profile = array("Failure" => array("message" => "Error occurred while editing profile. Please try again"), "StatusCode" => 400);
        } else {
            $status = $this->Api_v1_model->ifExistUserId($_REQUEST['userId']); /* if User id does not exists */
            if ($status == 1) {
                $valid = $this->Api_v1_model->checkAccessCode($_REQUEST['userId'], $_REQUEST['accessCode']);
                if ($valid == 1) {
                    $data = $_REQUEST;

                    if (isset($_FILES['profilePicture']) && !empty($_FILES['profilePicture'])) {

                        $microtime = microtime();
                        $imagePath = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . '.jpg';
                        $target_dir2 = './images/users/' . $imagePath;
                        move_uploaded_file($_FILES['profilePicture']["tmp_name"], $target_dir2);
                    } else {
                        $userimageData = $this->Api_v1_model->getUserById($_REQUEST['userId']);
                        $imagePath = $userimageData[0]->profilePicture;
                    }

                    /*if (isset($_FILES['profilePictureThumb']) && !empty($_FILES['profilePictureThumb'])) {

                        $microtime = microtime();
                        $imagePathThumb = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . '_thumb.jpg';
                        $target_dir2 = './images/users/' . $imagePathThumb;
                        move_uploaded_file($_FILES['profilePictureThumb']["tmp_name"], $target_dir2);
                    } else {
                        $userimageData = $this->Api_v1_model->getUserById($_REQUEST['userId']);
                        $imagePathThumb = $userimageData[0]->profilePictureThumb;
                    }*/
                    $userData = $this->Api_v1_model->getUserById($_REQUEST['userId']);
                    

                    $res = $this->Api_v1_model->updateProfile($data, $imagePath);
                    if ($res != 0) {
                        
                        $profile = array("Success" => array("message" => "Profile updated successfully", "data" => array("userName" => $data['userName'], "profilePicture" => base_url() . 'images/users/' . $imagePath)), "StatusCode" => 200);
                    } else {
                        $profile = array("Failure" => array("message" => "Failed to update profile", "data" => array("userName" => $data['userName'], "profilePicture" => base_url() . 'images/users/' . $imagePath)), "StatusCode" => 400);
                    }
                } else {
                    $profile = array("Failure" => array("message" => "Access code does not match"), "StatusCode" => 400);
                }
            } else {
                $profile = array("Failure" => array("message" => "User does not exists"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($profile));
        echo json_encode($profile);
    }

    function viewProfile() {
        if (!isset($_REQUEST['userId'])) {
            $viewProfile = array("Failure" => "Error occurred while fetching profile data from server. Please try again", "StatusCode" => 400);
        } else if (!isset($_REQUEST['accessCode'])) {
            $profile = array("Failure" => array("message" => "Error occurred while fetching profile data from server. Please try again"), "StatusCode" => 400);
        } else {
            if (isset($_REQUEST['limit']))
                $current_index = $_REQUEST['limit'];
            else
                $current_index = 0;

            if (isset($_REQUEST['perPage']))
                $per_page = $_REQUEST['perPage'];
            else
                $per_page = 10;
            /* $valid	=	$this->Api_v1_model->checkAccessCode($_REQUEST['userId'],$_REQUEST['accessCode']);
              if($valid	==	1){ */
            $status = $this->Api_v1_model->ifExistUserId($_REQUEST['userId']); /* if User id does not exists */
            if ($status == 1) {
                $res = $this->Api_v1_model->getProfile($_REQUEST['userId']);
                if (!empty($res)) {
                    $repRating = $this->Api_v1_model->getRepRating($_REQUEST['userId']);
                    $totalHostedActivity = $this->Api_v1_model->totalHostedActivity($_REQUEST['userId']);
                    $totalAttendedActivity = $this->Api_v1_model->totalAttendedActivity($_REQUEST['userId']);
                    if ($res[0]->profilePicture == "")
                        $img = "";
                    else
                        $img = base_url() . "images/users/" . $res[0]->profilePicture;

                    /*if ($res[0]->profilePictureThumb == "")
                        $imgThumb = "";
                    else
                        $imgThumb = base_url() . "images/users/" . $res[0]->profilePictureThumb;*/

                    $countFollowing = $this->Api_v1_model->getAllfollowingList($_REQUEST['userId']);
                    $countFollowing = (is_array($countFollowing) && count($countFollowing) > 0) ? count($countFollowing) : 0;

                    $countFollowers = $this->Api_v1_model->viewFollowers($_REQUEST['userId']);
                    $countFollowers = (is_array($countFollowers) && count($countFollowers) > 0) ? count($countFollowers) : 0;

                    $countActivity = $this->Api_v1_model->getActivityByUser($_REQUEST['userId']);
                    $countActivity = (is_array($countActivity) && count($countActivity) > 0) ? count($countActivity) : 0;
                    $countActivity = $totalAttendedActivity + $totalHostedActivity;

                    $isPremium = !empty($res[0]->isPremium) ? $res[0]->isPremium : "";
                    $premiumToken = !empty($res[0]->premiumToken) ? $res[0]->premiumToken : "";
                    $isProfessional = !empty($res[0]->isProfessional) ? $res[0]->isProfessional : "";
                    $professionalToken = !empty($res[0]->professionalToken) ? $res[0]->professionalToken : "";

                    $viewListAll = array("email" => $res[0]->email, "userName" => $res[0]->userName, "mobile" => $res[0]->mobile, "profilePicture" => $img, "thumbImage" => $imgThumb, "age" => $res[0]->age, "DOB" => $res[0]->DOB, "gender" => $res[0]->gender, "location" => $res[0]->location, "affiliation" => $res[0]->affiliation, "goals" => $res[0]->goals, "favActivitiy" => $res[0]->favActivitiy, "favWorkout" => $res[0]->favWorkout, "bio" => $res[0]->bio, "repRating" => $repRating, "totalHostedActivity" => $totalHostedActivity, "totalAttendedActivity" => $totalAttendedActivity, "fitnessPro" => $res[0]->fitnessPro, "approvedFitnessPro" => $res[0]->approvedFitnessPro, 'totalFollowers' => $countFollowers, 'totalFollowing' => $countFollowing, 'totalActivity' => $countActivity, 'isPremium' => $isPremium, 'premiumToken' => $premiumToken, 'isProfessional' => $isProfessional, 'professionalToken' => $professionalToken, "quickbloxId" => $res[0]->quickBloxId);

                    if (isset($_REQUEST['myUserId']) && !empty($_REQUEST['myUserId']) && ($_REQUEST['myUserId'] != $_REQUEST['userId'])) {
                        $viewListAll['isFollowing'] = $this->Api_v1_model->CheckUserFollowingOrNOt($_REQUEST['myUserId'], $_REQUEST['userId']);

                        $viewListAll['isSelfFollowing'] = $this->Api_v1_model->CheckUserFollowingOrNOt($_REQUEST['userId'], $_REQUEST['myUserId']);

                        $privacyData = $this->Api_v1_model->getUserPrivacy($_REQUEST['userId']);
                        if ($privacyData[0]->hideEmail == 0) {
                            $viewListAll['email'] = '';
                        }
                        if ($privacyData[0]->hideMobile == 0) {
                            $viewListAll['mobile'] = '';
                        }
                        if ($privacyData[0]->hideLocation == 0) {
                            $viewListAll['location'] = '';
                        }
                    }

                    $viewProfile = array("Success" => array("message" => $viewListAll), "StatusCode" => 200);
                } else {
                    $viewProfile = array("Failure" => array("message" => "No data found"), "StatusCode" => 400);
                }
            } else {
                $viewProfile = array("Failure" => array("message" => "User does not exists"), "StatusCode" => 400);
            }
            /* }else{
              $viewProfile	=	array("Failure" => array("message"=>"Access code does not match"),"StatusCode"=>400);
              } */
        }
        log_message("debug", "RESPONSE" . serialize($viewProfile));
        echo json_encode($viewProfile);
    }

    function rateActivity() {
        if (!isset($_REQUEST['userId'])) {
            $rateActivity = array("Failure" => array("message" => "Error occurred during fetching rate Activity. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['accessCode'])) {
            $rateActivity = array("Failure" => array("message" => "Error occurred during fetching rate Activity. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['feedId'])) {
            $rateActivity = array("Failure" => array("message" => "Error occurred during fetching rate Activity. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['rating'])) {
            $rateActivity = array("Failure" => array("message" => "Error occurred during fetching rate Activity. Please try again"), "StatusCode" => 400);
        } else {
            $userId = $_REQUEST['userId']; /* current logged in user id */
            $accessCode = $_REQUEST['accessCode'];
            $feedId = $_REQUEST['feedId'];
            $rating = $_REQUEST['rating'];
            $rateDescription = $_REQUEST['rateDescription'];
            if ($userId != "" && $feedId != "" && $accessCode != "") {
                $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
                if ($checkAccessCode > 0) {
                    $status = $this->Api_v1_model->ifExistUserId($userId); /* if User id does not exists */
                    if ($status == 1) {
                        $res = $this->Api_v1_model->rateActivity($userId, $feedId, $rating, $rateDescription);
                        if ($res > 0) {
                            $rateActivity = array("Success" => array("message" => "Rating added successfully"), "StatusCode" => 200);
                        } else {
                            $rateActivity = array("Failure" => array("message" => "Fail to add rating"), "StatusCode" => 400);
                        }
                    } else {
                        $rateActivity = array("Failure" => array("message" => "User does not exists"), "StatusCode" => 400);
                    }
                } else {
                    $rateActivity = array("Failure" => array("message" => "access Code does not match"), "StatusCode" => 400);
                }
            } else {
                $rateActivity = array("Failure" => array("message" => "Error occurred during rating. Please try again"), "StatusCode" => 400);  /* If user id is passed as blank */
            }
        }
        log_message("debug", "RESPONSE" . serialize($rateActivity));
        echo json_encode($rateActivity);
    }

    function sendFeedback() {
        if (!isset($_REQUEST['userId'])) {
            $sendFeedback = array("Failure" => array("message" => "Error occurred during sending Feedback. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['accessCode'])) {
            $sendFeedback = array("Failure" => array("message" => "Error occurred during sending Feedback. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['feedback'])) {
            $sendFeedback = array("Failure" => array("message" => "Error occurred during sending Feedback. Please try again"), "StatusCode" => 400);
        } else {
            $userId = $_REQUEST['userId']; /* current logged in user id */
            $accessCode = $_REQUEST['accessCode'];
            $feedback = $_REQUEST['feedback'];
            if ($userId != "" && $feedback != "" && $accessCode != "") {
                $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
                if ($checkAccessCode > 0) {
                    $status = $this->Api_v1_model->ifExistUserId($userId); /* if User id does not exists */
                    if ($status == 1) {
                        $res = $this->Api_v1_model->sendFeedback($userId, $feedback);
                        if ($res > 0) {
                            $userData = $this->Api_v1_model->getUserData($userId);
                            $data['userdata'] = $userData[0];
                            $data['feedback'] = $feedback;

                            $email_temp = $this->Api_v1_model->getEmailtemplate(3);
                            $this->load->library('email');
                            $this->email->initialize($this->email_config);
                            $this->email->set_mailtype("html");
                            $this->email->set_newline("\r\n");
                            $this->email->from('support@versafit.co');
                            $this->email->to(FEEDBACK_EMAIL);
                            $this->email->subject($email_temp[0]['subject']);
                            $message = $email_temp[0]['description'];
                            $username = $data['userdata']->email . (!empty($data['userdata']->userName) ? ' (' . $data['userdata']->userName . ') ' : '');
                            $message = str_replace('%username%', $username, $message);
                            $message = str_replace('%feedback%', $data['feedback'], $message);
                            $this->email->message($message);
                            if ($this->email->send()) {
                                $sendFeedback = array("Success" => array("message" => "Feedback send successfully"), "StatusCode" => 200);
                            } else {
                                $sendFeedback = array("Failure" => array("message" => show_error($this->email->print_debugger())), "StatusCode" => 400);
                            }
                        } else {
                            $sendFeedback = array("Failure" => array("message" => "Fail to add feedback"), "StatusCode" => 400);
                        }
                    } else {
                        $sendFeedback = array("Failure" => array("message" => "User does not exists"), "StatusCode" => 400);
                    }
                } else {
                    $sendFeedback = array("Failure" => array("message" => "access Code does not match"), "StatusCode" => 400);
                }
            } else {
                $sendFeedback = array("Failure" => array("message" => "Error occurred during sending Feedback. Please try again"), "StatusCode" => 400);  /* If user id is passed as blank */
            }
        }
        log_message("debug", "RESPONSE" . serialize($sendFeedback));
        echo json_encode($sendFeedback);
    }

    function searchUser() {
        if (!isset($_REQUEST['userId']) || !isset($_REQUEST['accessCode']) || !isset($_REQUEST['searchKeyword'])) {
            $searchUser = array("Failure" => array("message" => "Error occurred while searching user. Please try again"), "StatusCode" => 400);
        }/* else if (empty($_REQUEST['searchKeyword'])) {
          $searchUser = array("Success" => array(), "StatusCode" => 200);
          } */ else {
            if (isset($_REQUEST['limit']))
                $current_index = $_REQUEST['limit'];
            else
                $current_index = 0;

            if (isset($_REQUEST['perPage']))
                $per_page = $_REQUEST['perPage'];
            else
                $per_page = 10;
            $checkAccessCode = $this->Api_v1_model->checkAccessCode($_REQUEST['userId'], $_REQUEST['accessCode']);
            if ($checkAccessCode > 0) {
                $viewUserAll = array();
                /* check comments */
                $resUser = $this->Api_v1_model->searchUser($_REQUEST['searchKeyword'], $current_index, $per_page);

                if (is_array($resUser) && count($resUser) > 0) {
                    $usersArr = array();
                    foreach ($resUser as $val) {
                        $userData = $this->Api_v1_model->getUserById($val->userId);
                        //echo $val->userId;echo "<br>";echo $userData[0]->profilePicture;echo "<br>";
                        if (is_array($userData) && count($userData) > 0) {
                            $checkFollwing = $this->Api_v1_model->CheckUserFollowingOrNOt($_REQUEST['userId'], $val->userId);
                            if ($checkFollwing == 1)
                                $status = 1;
                            else
                                $status = 0;
                            $userImage = "";
                            if ($userData[0]->profilePicture != "")
                                $userImage = base_url() . "images/users/" . $userData[0]->profilePicture;


                            $userIamgeThumb = "";
                            /*if ($userData[0]->profilePictureThumb != "")
                                $userIamgeThumb = base_url() . "images/users/" . $userData[0]->profilePictureThumb;*/

                            // echo $userImage;echo "<br>";
                            $tempArr = array("userId" => $val->userId, "email" => $userData[0]->email, "userName" => $userData[0]->userName, "userImage" => $userImage, "thumbImage" => $userIamgeThumb, "following" => $status);

                            array_push($viewUserAll, $tempArr);
                        } else {
                            continue;
                        }
                    }

                    $searchUser = array("Success" => array("message" => $viewUserAll), "StatusCode" => 200);
                } else {
                    if ($current_index > 0) {
                        $message = "No more data found";
                        $searchUser = array("Failure" => array("message" => $message), "StatusCode" => 400);
                    } else {
                        $message = array();
                        $searchUser = array("Success" => array("message" => $message), "StatusCode" => 200);
                    }
                }
            } else {
                $searchUser = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($searchUser));
        echo json_encode($searchUser);
    }

    function userSocialLogin() {
        if (!isset($_REQUEST['emailId'])) {
            $socialLogin = array("Failure" => array("message" => "Email Id is required"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceToken'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['deviceId'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['latitude'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['longitude'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else if (!isset($_REQUEST['verifyUser'])) {
            $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
        } else {

            /* get data */
            $today = date("Y-m-d H:i:s");
            $_REQUEST['is_confirmed'] = 1;
            $_REQUEST['is_blocked'] = 0;
            $_REQUEST['verification_code'] = '';
            $device_id = isset($_REQUEST['deviceId']) ? $_REQUEST['deviceId'] : '';
            $registration_id = isset($_REQUEST['deviceToken']) ? $_REQUEST['deviceToken'] : '';
            $deviceType = isset($_REQUEST['deviceType']) ? $_REQUEST['deviceType'] : '';
            $data['is_confirmed'] = 1;
            $data['is_blocked'] = 0;
            $data['verification_code'] = '';

            $resDevice = $this->Api_v1_model->checkDeviceIdExist($_REQUEST['deviceId']);
            if (!empty($resDevice)) {
                //print_R($resDevice);exit;
                $userId = $resDevice[0]->userId;
                if ($_REQUEST['verifyUser'] == "true") {
                    $social['userId'] = $userId;
                    $keyId = 'userSocialId';
                    $social[$keyId] = isset($_REQUEST['googleId']) ? $_REQUEST['googleId'] : (isset($_REQUEST['facebookId']) ? $_REQUEST['facebookId'] : (isset($_REQUEST['twitterId']) ? $_REQUEST['twitterId'] : ''));
                    $type = isset($_REQUEST['googleId']) ? 'google' : (isset($_REQUEST['facebookId']) ? 'facebook' : (isset($_REQUEST['twitterId']) ? 'twitter' : ''));


                    $ckLogin = $this->Api_v1_model->checkUserSocialLogin($social, $keyId, $type); /* for duplicate entry prevent */


                    //print_R($ckLogin);exit;
                    if (!empty($ckLogin)) {
                        $Data = $this->Api_v1_model->getUserById($userId);
                        //print_R($Data);exit;
                        $updateLatLong = $this->Api_v1_model->updateLatLong($userId, $_REQUEST['latitude'], $_REQUEST['longitude']);
                        //$updateAccessCode = $this->Api_v1_model->updateAccessCode($accessCode, $userId);
//$updateNewUser	=	$this->Api_v1_model->updateNewUser($user_id);
                        $updateToken = $this->Api_v1_model->updateToken($device_id, $registration_id, $userId, $deviceType);
                        $code['accessCode'] = $Data[0]->accessCode;
                        $code['userId'] = $userId;
                        $code['email'] = $Data[0]->email;
                        $code['newUser'] = 0;
                        $message = $code;
                        $i = 'yes';
                        $socialLogin = array("Success" => $message, "StatusCode" => 200);
                    } else {
                        $ckUser = $this->Api_v1_model->checkUser($social);

                        if ($this->Api_v1_model->addSocialLogin($social, $keyId, $type, $_REQUEST['emailId'])) {

                            $Data = $this->Api_v1_model->getUserById($userId);
                            //print_R($Data);exit;
                            //$updateLatLong = $this->Api_v1_model->updateLatLong($userId, $_REQUEST['latitude'], $_REQUEST['longitude']);
                            //$updateAccessCode = $this->Api_v1_model->updateAccessCode($accessCode, $userId);
                            //$updateNewUser	=	$this->Api_v1_model->updateNewUser($user_id);
                            //$updateToken = $this->Api_v1_model->updateToken($device_id, $registration_id, $userId, $deviceType);
                            $code['accessCode'] = $Data[0]->accessCode;
                            $code['userId'] = $userId;
                            $code['email'] = $Data[0]->email;
                            $code['newUser'] = 1;
                            $message = $code;
                            $i = 'yes';
                            $socialLogin = array("Success" => $message, "StatusCode" => 200);
                        } else {
                            $message = 'Error occurred during login. Please try again';
                            $socialLogin = array("Failure" => array("message" => $message), "StatusCode" => 400);
                        }
                    }
                } else {
                    $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
                }
            } else {
                $socialLogin = array("Failure" => array("message" => "Error occurred during login. Please try again"), "StatusCode" => 400);
            }
        }
        log_message("debug", "RESPONSE" . serialize($socialLogin));
        echo json_encode($socialLogin);
    }

    function homePage() {
        /*
          inputs: pageId,lat,long
         * 
         * for gold Arr
         * get city and country from lat long
         * fetch placement from featurePlacement
         * if no placement is there set all data as near by values
         * if only one of 3 or 2 placement are set set them as first and rest as near by values
         * 
         * 
         * for sponsers Arr
         *    get category wise sponsers array sorted by near by values of silver packages
         *    get category wise details  array sorted by near by values of bronse packages
         *          */

        //countryName,stateName,cityName,latitude,longitude,pageNo
        extract($_REQUEST);
        //echo $userId;
        $getIdsFromName = $this->Api_v1_model->getIdsFromName($cityName, $stateName, $countryName);
        //echo "<pre>";print_r($getIdsFromName);

        if (!empty($getIdsFromName)) {
            $cityId = $getIdsFromName[0]->cityId;
            $stateId = $getIdsFromName[0]->stateId;
            $countryId = $getIdsFromName[0]->countryId;
            $cnt = 0;
            $total = 10;

            //gold banner
            $goldBanner = array();
            $getGoldSpotPosition = $this->Api_v1_model->getGoldSpotPosition($cityId);
            //echo "<pre>";print_r($getGoldSpotPosition);exit;
            if (!empty($getGoldSpotPosition)) {
                $cnt = count($getGoldSpotPosition);

                foreach ($getGoldSpotPosition as $val) {
                    $businessId = $val->businessId;
                    $getBanner = $this->Api_v1_model->getBanner($businessId);
                    //echo "<pre>";print_r($getBanner);exit;
                    if (!empty($getBanner)) {
                        if ($getBanner[0]->images != "") {
                            $img = base_url() . "images/banner/" . $getBanner[0]->images;
                        } else {
                            $img = "";
                        }
                        $tempArr = array("businessId" => $getBanner[0]->businessId, "businessName" => $getBanner[0]->businessName, "businessImage" => $img);
                        array_push($goldBanner, $tempArr);
                    }
                }
            }
            $remaining = $total - $cnt;
            $getRestNearByGoldBanner = $this->Api_v1_model->getRestNearByGoldBanner($remaining, $latitude, $longitude);
            //echo "<pre>";print_r($getRestNearByGoldBanner);
            if (!empty($getRestNearByGoldBanner)) {
                foreach ($getRestNearByGoldBanner as $val) {
                    $businessId = $val->businessId;
                    $getBanner = $this->Api_v1_model->getBanner($businessId);
                    if (!empty($getBanner)) {
                        if ($getBanner[0]->images != "") {
                            $img = base_url() . "images/banner/" . $getBanner[0]->images;
                        } else {
                            $img = "";
                        }
                        $tempArr = array("businessId" => $getBanner[0]->businessId, "businessName" => $getBanner[0]->businessName, "businessImage" => $img);
                        array_push($goldBanner, $tempArr);
                    }
                }
            }
            //echo json_encode($goldBanner);
            //gold banner 
            $homePage = array("Success" => array("message" => $goldBanner), "StatusCode" => 200);
        } else {
            $homePage = array("Failure" => array("message" => "No data found"), "StatusCode" => 400);
        }
        echo json_encode($homePage);
    }

    function getCategoryWiseData() {
        //categoryId,dataCount,latitude,longitude
        extract($_REQUEST);

        $silverSponser = array();
        $bronzeSponser = array();
            
        $category = $this->Api_v1_model->getCategoryById($categoryId);
        //echo "<pre>";print_r($category);exit;
        if (!empty($category)) {
            //sliver
            $getSliverSonsers = $this->Api_v1_model->getSliverSonsers($categoryId, $latitude, $longitude);
            //echo "<pre>dccc";print_r($getSliverSonsers);exit;
            if (!empty($getSliverSonsers)) {
                foreach ($getSliverSonsers as $val2) {
                    $getBanner = $this->Api_v1_model->getBanner($val2->businessId);
                    if (isset($getBanner[0]->images) &&  $getBanner[0]->images != "") {
                        $img = base_url() . "images/banner/" . $getBanner[0]->images;
                    } else {
                        $img = "";
                    }
                    $tempArr = array("businessId" => $val2->businessId, "businessName" => $val2->businessName, "businessAddress" => $val2->businessAddress1, "cityName" => $val2->cityName, "businessImage"=>$img);
                    array_push($silverSponser, $tempArr);
                }
            }
            //silver
            //bronze
            $getBronze = $this->Api_v1_model->getBronze($categoryId, $latitude, $longitude,$dataCount);
            //echo "<pre>bronze here";print_r($getBronze);
            if (!empty($getBronze)) {
                foreach ($getBronze as $val2) {
                    $getBanner = $this->Api_v1_model->getBanner($val2->businessId);
                    if (isset($getBanner[0]->images) &&  $getBanner[0]->images != "") {
                        $img = base_url() . "images/banner/" . $getBanner[0]->images;
                    } else {
                        $img = "";
                    }
                    $tempArr = array("businessId" => $val2->businessId, "businessName" => $val2->businessName, "businessAddress" => $val2->businessAddress1, "cityName" => $val2->cityName,"businessImage"=>$img, "distance" => round($val2->distance,2) . " km");
                    array_push($bronzeSponser, $tempArr);
                }
            }
            //bronze

            $catArr = array("categoryId" => $categoryId,"categoryName"=>$category[0]->categoryName, "Sliver" => $silverSponser, "Bronze" => $bronzeSponser);

            $finalArr = array("Success" => array("message" => $catArr), "StatusCode" => 200);
        } else {
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
        
        echo json_encode($finalArr);
    }
    
    
    function getBronzeSponsrs(){
        //categoryId,latitude,longitude,dataCount
        extract($_REQUEST);
        $bronzeSponser = array();
        //bronze
        $getBronze = $this->Api_v1_model->getBronze($categoryId, $latitude, $longitude,$dataCount);
        //echo "<pre>bronze here";print_r($getBronze);
        if (!empty($getBronze)) {
            foreach ($getBronze as $val2) {
                $getBanner = $this->Api_v1_model->getBanner($val2->businessId);
                if (isset($getBanner[0]->images) &&  $getBanner[0]->images != "") {
                    $img = base_url() . "images/banner/" . $getBanner[0]->images;
                } else {
                    $img = "";
                }
                $tempArr = array("businessId" => $val2->businessId, "businessName" => $val2->businessName, "businessAddress" => $val2->businessAddress1, "cityName" => $val2->cityName,"businessImage"=>$img,);
                array_push($bronzeSponser, $tempArr);
            }
            $finalArr = array("Success" => array("message" => $bronzeSponser), "StatusCode" => 200);
        }else{
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
        //bronze
        
        echo json_encode($finalArr);
    }
    
    
    function getSilverSponsers(){
        //categoryId,latitude,longitude,dataCount
        extract($_REQUEST);
        $silverSponser = array();
        //sliver
        $getSliverSonsers = $this->Api_v1_model->getSliverSonsers($categoryId, $latitude, $longitude);
        //echo "<pre>dccc";print_r($getSliverSonsers);exit;
        if (!empty($getSliverSonsers)) {
            foreach ($getSliverSonsers as $val2) {
                $getBanner = $this->Api_v1_model->getBanner($val2->businessId);
                if (isset($getBanner[0]->images) &&  $getBanner[0]->images != "") {
                    $img = base_url() . "images/banner/" . $getBanner[0]->images;
                } else {
                    $img = "";
                }
                $tempArr = array("businessId" => $val2->businessId, "businessName" => $val2->businessName, "businessAddress" => $val2->businessAddress1, "cityName" => $val2->cityName, "businessImage"=>$img);
                array_push($silverSponser, $tempArr);
            }
            $finalArr = array("Success" => array("message" => $silverSponser), "StatusCode" => 200);
        }else{
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
        echo json_encode($finalArr);
    }
    
    function seeMore(){
        //categoryId,latitude,longitude,limit,perPage,dataCount
        extract($_REQUEST);
        if (isset($_REQUEST['limit']))
            $limit = $_REQUEST['limit'];
        else
            $limit = 0;

        if (isset($_REQUEST['perPage']))
            $perPage = $_REQUEST['perPage'];
        else
            $perPage = 10;

        $getResult  =   $this->Api_v1_model->getLatLongbaseBusiness($categoryId, $latitude, $longitude, $limit, $perPage);
        //echo "<pre>all here";print_r($getResult);
        
        $seemoreArr = array();
        if(!empty($getResult)){
            foreach($getResult as $val){
                $getServiceImages = $this->Api_v1_model->getServiceImages($val->businessId);
                $imgArr = array();
                if(!empty($getServiceImages)){
                   foreach($getServiceImages as $image){
                        if (isset($image->images) &&  $image->images != "") {
                            $img = base_url() . "uploads/service_pictures/" . $image->images;
                        } else {
                            $img = "";
                        }
                        $tempImg = array("serviceImg"=>$img);
                        array_push($imgArr,$tempImg);
                    } 
                }
                
                $day = date("l");
                $getOpeningHours = $this->Api_v1_model->getOpeningHours($val->businessId,$day);
                if(!empty($getOpeningHours))
                    $openingHours = $getOpeningHours[0]->day." ".$getOpeningHours[0]->openingHours." to ".$getOpeningHours[0]->closingHours;
                else
                    $openingHours = "";
                
                $getTags = $this->Api_v1_model->getServiceTags($val->businessId);
                //echo "<pre>";print_r($getTags);exit;
                if($getTags != 0){
                    $serviceTagsArr = array();
                    foreach($getTags as $val2){
                       array_push($serviceTagsArr,$val2->serviceTag);
                    }
                    $serviceTags = implode(",",$serviceTagsArr);   
                }else
                    $serviceTags = "";
                
                $tempArr = array("businessId"=>$val->businessId,"businessName"=>$val->businessName,"businessAddress"=>$val->businessAddress1,"latitude"=>$val->latitude,"longitude"=>$val->longitude,"businessesNoOfViews"=>$val->businessesNoOfViews,"noOfBookmarks"=>$val->noOfBookmarks,"noOfReviews"=>$val->noOfReviews,"avgRating"=>$val->avgRating,"cityName"=>$val->cityName,"distance"=>  round($val->distance,2)." km","serviceImages"=>$imgArr,"openingHours"=>$openingHours,"serviceTags"=>$serviceTags);
                array_push($seemoreArr,$tempArr);
            }
            $finalArr = array("Success" => array("message" => $seemoreArr), "StatusCode" => 200);
        }else{
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
        echo json_encode($finalArr);
    }
    
    
    function categoryWiseSponsersList(){
        //categoryId,dataCount,latitude,longitude
        extract($_REQUEST);
        
        $getBronze = $this->Api_v1_model->getBronze($categoryId, $latitude, $longitude, $dataCount);
        //echo "<pre>bronze here";print_r($getBronze);exit;
        
        $bronzeSponser  =   array();
        if (!empty($getBronze)) {
            foreach ($getBronze as $val2) {
                $getBanner = $this->Api_v1_model->getBanner($val2->businessId);
                if (isset($getBanner[0]->images) &&  $getBanner[0]->images != "") {
                    $img = base_url() . "images/banner/" . $getBanner[0]->images;
                } else {
                    $img = "";
                }
                $tempArr = array("businessId" => $val2->businessId, "businessName" => $val2->businessName, "businessAddress" => $val2->businessAddress1, "cityName" => $val2->cityName,"businessImage"=>$img,);
                array_push($bronzeSponser, $tempArr);
            }
            $finalArr = array("Success" => array("message" => $bronzeSponser), "StatusCode" => 200);
        }else{
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
        echo json_encode($finalArr);
    }
    
    function search(){
        //searchKeyword,latitude,longitude,limit,perPage,categoryId
        extract($_REQUEST);
        
        if (isset($_REQUEST['limit']))
            $limit = $_REQUEST['limit'];
        else
            $limit = 0;

        if (isset($_REQUEST['perPage']))
            $perPage = $_REQUEST['perPage'];
        else
            $perPage = 10;
    
        $searchArr = explode(" ",$searchKeyword);
        $searchVal = "b.businessName LIKE '".$searchKeyword."'";
        $resArr = array();
        foreach($searchArr as $val){
            $searchVal .= " OR b.businessName LIKE '$val%' ";
        }
        
        $getResult = $this->Api_v1_model->getSearchResult($searchVal,$latitude, $longitude, $limit, $perPage);
        //echo "<pre>bronze here";print_r($getResult);exit;
        $seemoreArr = array();
        if(!empty($getResult)){
            foreach($getResult as $val){
                $getServiceImages = $this->Api_v1_model->getServiceImages($val->businessId);
                $imgArr = array();
                if(!empty($getServiceImages)){
                   foreach($getServiceImages as $img){
                        if (isset($img->images) &&  $img->images != "") {
                            $img = base_url() . "images/banner/" . $img->images;
                        } else {
                            $img = "";
                        }
                        $tempImg = array($image->businessesImageId=>$img);
                        array_push($imgArr,$tempImg);
                    } 
                }
                
                $day = date("l");
                $getOpeningHours = $this->Api_v1_model->getOpeningHours($val->businessId,$day);
                if(!empty($getOpeningHours))
                    $openingHours = $getOpeningHours[0]->day." ".$getOpeningHours[0]->openingHours." to ".$getOpeningHours[0]->closingHours;
                else
                    $openingHours = "";
                
                $getTags = $this->Api_v1_model->getServiceTags($val->businessId);
                if(!empty($getOpeningHours)){
                    $serviceTags = implode(",",$getTags);   
                }else
                    $serviceTags = "";
                
                $tempArr = array("businessId"=>$val->businessId,"businessName"=>$val->businessName,"businessAddress"=>$val->businessAddress1,"latitude"=>$val->latitude,"longitude"=>$val->longitude,"businessesNoOfViews"=>$val->businessesNoOfViews,"noOfBookmarks"=>$val->noOfBookmarks,"noOfReviews"=>$val->noOfReviews,"avgRating"=>$val->avgRating,"cityName"=>$val->cityName,"distance"=>  round($val->distance,2)." km","serviceImages"=>$imgArr,"openingHours"=>$openingHours,"serviceTags"=>$serviceTags);
                array_push($seemoreArr,$tempArr);
            }
            $finalArr = array("Success" => array("message" => $seemoreArr), "StatusCode" => 200);
        }else{
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
         echo json_encode($finalArr);
    }

    
    function filter(){
        //available,rated,bookmarked,categoriesId,latitude,longitude
        extract($_REQUEST);
        
       if (isset($_REQUEST['limit']))
            $limit = $_REQUEST['limit'];
        else
            $limit = 0;

        if (isset($_REQUEST['perPage']))
            $perPage = $_REQUEST['perPage'];
        else
            $perPage = 10;
        
        $where = "";
        $cond = "";
        
        if (isset($_REQUEST['userId'])){
            $bookmarked = $_REQUEST['bookmarked'];
            $userId = $_REQUEST['userId'];
            $accessCode = $_REQUEST['accessCode'];
            $cond .= "INNER JOIN bookmarks as bm ON bm.businessId = b.businessId";
            $where .=  "and bm.userId = $userId";
        }
        
        
        if($rated == 1){
            $where .=  "and b.avgRating >= 3.5";
        }
        
        if($available == 1){
            $day = date("l");
            $time = date("h");
            
            $where = "and oh.day = '$day' and '$time' between openingHours AND closingHours";
        }
        
        
        $catArr = explode(",",$categoriesId);
        $catVal = "";
        $resArr = array();
        $count = count($catArr);$i =1;
        foreach($catArr as $val){
            if($count == $i){
                $catVal .= " bc.categoryId = $val ";
            }else
                $catVal .= " bc.categoryId = $val OR ";
            $i++;
        }
        $results = $this->Api_v1_model->getFilters($where,$cond,$catVal,$latitude, $longitude, $limit, $perPage);
        //echo "<pre>bronze here";print_r($results);exit;
        $seemoreArr = array();
        if(!empty($results)){
            foreach($results as $val){
                $getServiceImages = $this->Api_v1_model->getServiceImages($val->businessId);
                $imgArr = array();
                if(!empty($getServiceImages)){
                   foreach($getServiceImages as $img){
                        if (isset($img->images) &&  $img->images != "") {
                            $img2 = base_url() . "images/banner/" . $img->images;
                        } else {
                            $img2 = "";
                        }
                        $tempImg = array($img->businessesImageId=>$img2);
                        array_push($imgArr,$tempImg);
                    } 
                }
                
                $day = date("l");
                $getOpeningHours = $this->Api_v1_model->getOpeningHours($val->businessId,$day);
                if(!empty($getOpeningHours))
                    $openingHours = $getOpeningHours[0]->day." ".$getOpeningHours[0]->openingHours." to ".$getOpeningHours[0]->closingHours;
                else
                    $openingHours = "";
                
                $getTags = $this->Api_v1_model->getServiceTags($val->businessId);
                if(!empty($getOpeningHours)){
                    $serviceTags = implode(",",$getTags);   
                }else
                    $serviceTags = "";
                
                $tempArr = array("businessId"=>$val->businessId,"businessName"=>$val->businessName,"businessAddress"=>$val->businessAddress1,"latitude"=>$val->latitude,"longitude"=>$val->longitude,"businessesNoOfViews"=>$val->businessesNoOfViews,"noOfBookmarks"=>$val->noOfBookmarks,"noOfReviews"=>$val->noOfReviews,"avgRating"=>$val->avgRating,"cityName"=>$val->cityName,"distance"=>  round($val->distance,2)." km","serviceImages"=>$imgArr,"openingHours"=>$openingHours,"serviceTags"=>$serviceTags);
                array_push($seemoreArr,$tempArr);
            }
            $finalArr = array("Success" => array("message" => $seemoreArr), "StatusCode" => 200);
        }else{
            $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
         echo json_encode($finalArr);
    }
    
    
    function reportError(){
        //userId,accessCode,phoneNumber,address,outletClosed,serviceIncorrect,message
        extract($_REQUEST);
        $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
        if ($checkAccessCode > 0) {
            $getResult = $this->Api_v1_model->reportError($userId,$phoneNumber,$address,$outletClosed,$serviceIncorrect,$message);
            if($getResult > 0){
                $finalArr = array("Success" => array("message" => "Error Repored Successfuly"), "StatusCode" => 200);
                $email_temp = $this->Api_v1_model->getEmailTemplate(6);
                /*$this->load->library('email');
                $this->email->initialize($this->email_config);
                $this->email->set_mailtype("html");
                $this->email->set_newline("\r\n");
                $this->email->from('admin@solulab.com');
                $this->email->to('khushbu@solulab.com'); // change it to yours
                $this->email->subject($email_temp[0]['subject']);
                $message = $email_temp[0]['description'];
                $message = str_replace('%phoneVal%', $phoneNumber, $message);
                $message = str_replace('%addrVal%', $address, $message);
                $message = str_replace('%outletVal%', $outletClosed, $message);
                $message = str_replace('%serviceVal%', $serviceIncorrect, $message);
                $message = str_replace('%errMsg%', $message, $message);
                $this->email->message($message);
                $this->email->send();*/
            }else{
                $finalArr = array("Success" => array("message" => "Fail to report error"), "StatusCode" => 400);
            }
        }else {
            $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
        } 
        echo json_encode($finalArr);
    }
    
    
    function viewBusinessById(){
        //businessId,latitude,longitude
        extract($_REQUEST);
        $getResult = $this->Api_v1_model->viewBusinessById($businessId, $latitude, $longitude);
        //echo "<pre>";print_r($getResult);exit;
        if(!empty($getResult)){
            $getWorkingHours = $this->Api_v1_model->getWorkingHoursById($businessId);
            //echo "<pre>";print_r($getWorkingHours);exit;
            $openingHours = array();
            if(!empty($openingHours)){
               foreach($openingHours as $val){
                   $tempHour = array("day"=>$val->day,"openingHours"=>$val->openingHours,"closingHours"=>$val->closingHours);
                   array_push($openingHours,$tempHour);
               }
            }
            
            $getServiceImages = $this->Api_v1_model->getServiceImages($businessId);
            $imgArr = array();
            if(!empty($getServiceImages)){
               foreach($getServiceImages as $img){
                    if (isset($img->images) &&  $img->images != "") {
                        $img2 = base_url() . "images/banner/" . $img->images;
                    } else {
                        $img2 = "";
                    }
                    $tempImg = array($img->businessesImageId=>$img2);
                    array_push($imgArr,$tempImg);
                } 
            }

            $getTags = $this->Api_v1_model->getServiceTags($businessId);
            if(!empty($getOpeningHours)){
                $serviceTags = implode(",",$getTags);   
            }else
                $serviceTags = "";

            $finalArr = array("businessId"=>$getResult[0]->businessId,"businessName"=>$getResult[0]->businessName,"businessAddress"=>$getResult[0]->businessAddress1,"latitude"=>$getResult[0]->latitude,"longitude"=>$getResult[0]->longitude,"businessesNoOfViews"=>$getResult[0]->businessesNoOfViews,"noOfBookmarks"=>$getResult[0]->noOfBookmarks,"noOfReviews"=>$getResult[0]->noOfReviews,"avgRating"=>$getResult[0]->avgRating,"cityName"=>$getResult[0]->cityName,"distance"=>  round($getResult[0]->distance,2)." km","serviceImages"=>$imgArr,"openingHours"=>$openingHours,"serviceTags"=>$serviceTags);
        }else{
            $finalArr = array("Failure" => array("message" => "No data found"), "StatusCode" => 400);
        } 
        echo json_encode($finalArr);
    }
    
    function getReviewByBusinessId(){//review listing by business
        //businessId,limit,perPage
        extract($_REQUEST);
        
        if (isset($_REQUEST['limit']))
            $limit = $_REQUEST['limit'];
        else
            $limit = 0;

        if (isset($_REQUEST['perPage']))
            $perPage = $_REQUEST['perPage'];
        else
            $perPage = 10;
        
        
        $getResult = $this->Api_v1_model->getReviewByBusinessId($businessId,$limit,$perPage);
        //echo "<pre>";print_r($getResult);exit;
        
        $reviewArr = array();
        $finalArr = array();
        if(!empty($getResult)){
            foreach($getResult as $val){
                
                if($val->ago == 0)$ago = 'Today';else $ago = $val->ago." days ago";
                $tempArr = array("reviewId"=>$val->reviewId,"rating"=>$val->rating,"rateComments"=>$val->rateComments,"totalReviews"=>$val->total,"reviewDate"=>$ago);
                array_push($reviewArr,$tempArr);
            }
            $finalArr = array("Success" => array("message" => $reviewArr), "StatusCode" => 200);
        }else{
             $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
        }
        echo json_encode($finalArr);
    }
    
    function addBookmark(){
        //businessId,countryName,stateName,cityName,address,latitude,longitude,userId,accessCode
        extract($_REQUEST);
        if (!isset($businessId)) {
            $finalArr = array("Failure" => array("message" => "businessId is required"), "StatusCode" => 400);
        } else if (!isset($countryName)) {
            $finalArr = array("Failure" => array("message" => "countryName is required"), "StatusCode" => 400);
        } else if (!isset($stateName)) {
            $finalArr = array("Failure" => array("message" => "stateName is required"), "StatusCode" => 400);
        } else if (!isset($cityName)) {
            $finalArr = array("Failure" => array("message" => "cityName is required"), "StatusCode" => 400);
        } else if (!isset($address)) {
            $finalArr = array("Failure" => array("message" => "address is required"), "StatusCode" => 400);
        } else if (!isset($latitude)) {
            $finalArr = array("Failure" => array("message" => "latitude is required"), "StatusCode" => 400);
        } else if (!isset($longitude)) {
            $finalArr = array("Failure" => array("message" => "longitude is required"), "StatusCode" => 400);
        } else if (!isset($userId)) {
            $finalArr = array("Failure" => array("message" => "userId is required"), "StatusCode" => 400);
        } else if (!isset($accessCode)) {
            $finalArr = array("Failure" => array("message" => "accessCode is required"), "StatusCode" => 400);
        } else {
            $getIdsFromName = $this->Api_v1_model->getIdsFromName($cityName, $stateName, $countryName);
            //echo "<pre>";print_r($getIdsFromName);

            if (!empty($getIdsFromName)) {
                $cityId = $getIdsFromName[0]->cityId;
                $stateId = $getIdsFromName[0]->stateId;
                $countryId = $getIdsFromName[0]->countryId;

                if(isset($userId)){
                    $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
                    if ($checkAccessCode > 0) {

                        $checkBookmark = $this->Api_v1_model->checkBookmark($userId, $businessId);
                        if($checkBookmark > 0){
                            $getResult = $this->Api_v1_model->bookmarkBusiness($businessId,$cityId,$stateId,$countryId,$address,$latitude,$longitude,$checkBookmark,$userId); 
                            if($getResult > 0){
                                $finalArr = array("Success" => array("message" => "Bookmark removed successfully"), "StatusCode" => 200); 
                            }else{
                                $finalArr = array("Failure" => array("message" => "Fail to removed bookmark"), "StatusCode" => 400);
                            }    
                        }else {
                            $getResult = $this->Api_v1_model->bookmarkBusiness($businessId,$cityId,$stateId,$countryId,$address,$latitude,$longitude,$checkBookmark,$userId);    
                            if($getResult > 0){
                                $finalArr = array("Success" => array("message" => "Bookmark added successfully"), "StatusCode" => 200); 
                            }else{
                                $finalArr = array("Failure" => array("message" => "Fail to add Bookmark"), "StatusCode" => 400);     
                            }    
                        }                    
                    }else{
                        $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
                    }
                }else{
                    $finalArr = array("Failure" => array("message" => "User Id requiered"), "StatusCode" => 400); 
                } 
            }else{
               $finalArr = array("Failure" => array("message" => "Unable to match city state country"), "StatusCode" => 400); 
            }    
        }
        log_message("debug", "RESPONSE" . serialize($finalArr));
        echo json_encode($finalArr);   
    }
    
    function bookmarkListing(){
        //userId,accessCode,limit,perPage
        extract($_REQUEST);
        if (isset($_REQUEST['limit']))
            $limit = $_REQUEST['limit'];
        else
            $limit = 0;

        if (isset($_REQUEST['perPage']))
            $perPage = $_REQUEST['perPage'];
        else
            $perPage = 10;
        
        
        if (!isset($userId)) {
            $finalArr = array("Failure" => array("message" => "userId is required"), "StatusCode" => 400);
        } else if (!isset($accessCode)) {
            $finalArr = array("Failure" => array("message" => "accessCode is required"), "StatusCode" => 400);
        } else {
            $bookmarkArr = array();
            $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
            if ($checkAccessCode > 0) {
                $getResult = $this->Api_v1_model->getBookmarkListing($userId,$limit,$perPage); 
                //echo "<pre>";print_r($getResult);exit;
                if(!empty($getResult)){
                    foreach($getResult as $val){
                        if (isset($val->images) &&  $val->images != "") {
                            $img = base_url() . "images/banner/" . $val->images;
                        } else {
                            $img = "";
                        }
                        
                        $tempArr = array("bookmarkId"=>$val->bookmarkId,"businessName"=>$val->businessName,"businessAddress"=>$val->businessAddress1,"avgRating"=>$val->avgRating,"businessImage"=>$img);
                        array_push($bookmarkArr, $tempArr);
                    }
                    $finalArr = array("Success" => array("message" => $bookmarkArr), "StatusCode" => 200);
                }else{
                    $finalArr = array("Success" => array("message" => []), "StatusCode" => 200);
                }    
            }else{
                $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
            }    
        }
        log_message("debug", "RESPONSE" . serialize($finalArr));
        echo json_encode($finalArr); 
    }
    
    function getReviewById(){
        //userId,accessCode,reviewId
        extract($_REQUEST);
        if (!isset($userId)) {
            $finalArr = array("Failure" => array("message" => "userId is required"), "StatusCode" => 400);
        } else if (!isset($accessCode)) {
            $finalArr = array("Failure" => array("message" => "accessCode is required"), "StatusCode" => 400);
        } else {
            $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
            if ($checkAccessCode > 0) {
                $getReviewById = $this->Api_v1_model->getReviewById($reviewId);
                if($getReviewById > 0){
                    //echo "<pre>";print_r($getReviewById);exit;
                    $finalArr = array("Success" => array("message" => array("businessId"=>$getReviewById[0]->businessId,"rating"=>$getReviewById[0]->rating,"rateComments"=>$getReviewById[0]->rateComments)), "StatusCode" => 200);   
                }else {
                    $finalArr = array("Failure" => array("message" => "Fail to delete review"), "StatusCode" => 400);
                }
            }else{
                $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
            }     
        }
        log_message("debug", "RESPONSE" . serialize($finalArr));
        echo json_encode($finalArr); 
    }
    
    function review(){
        //businessId,rating,rateCommnets,countryName,stateName,cityName,address,latitude,longitude,userId,accessCode,reviewId(optional)
        extract($_REQUEST);
        if (!isset($userId)) {
            $finalArr = array("Failure" => array("message" => "userId is required"), "StatusCode" => 400);
        } else if (!isset($accessCode)) {
            $finalArr = array("Failure" => array("message" => "accessCode is required"), "StatusCode" => 400);
        } else {
            $getIdsFromName = $this->Api_v1_model->getIdsFromName($cityName, $stateName, $countryName);
            //echo "<pre>";print_r($getIdsFromName);

            if (!empty($getIdsFromName)) {
                $cityId = $getIdsFromName[0]->cityId;
                $stateId = $getIdsFromName[0]->stateId;
                $countryId = $getIdsFromName[0]->countryId;

                $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
                if ($checkAccessCode > 0) {
                    $checkReview = $this->Api_v1_model->checkReview($userId, $businessId);
                    if($checkReview > 0){
                        if(isset($reviewId)){
                            $getResult = $this->Api_v1_model->reviewBusiness($businessId,$rating,$rateCommnets,$cityId,$stateId,$countryId,$address,$latitude,$longitude,$checkReview,$userId,$reviewId); 
                            if($getResult > 0){
                                $finalArr = array("Success" => array("message" => "Review edited successfully"), "StatusCode" => 200); 
                            }else{
                                $finalArr = array("Failure" => array("message" => "Fail to edit review"), "StatusCode" => 400);
                            }    
                        }else{
                            $finalArr = array("Failure" => array("message" => "Review Id requiered"), "StatusCode" => 400);
                        }    
                    }else {
                        $getResult = $this->Api_v1_model->reviewBusiness($businessId,$rating,$rateCommnets,$cityId,$stateId,$countryId,$address,$latitude,$longitude,$checkReview,$userId);    
                        if($getResult > 0){
                            $finalArr = array("Success" => array("message" => array("reviewId"=>$getResult)), "StatusCode" => 200); 
                        }else{
                            $finalArr = array("Failure" => array("message" => "Fail to add Review"), "StatusCode" => 400);     
                        }    
                    }
                }else{
                    $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
                }  
            }else{
               $finalArr = array("Failure" => array("message" => "Unable to match city state country"), "StatusCode" => 400); 
            }  
        }
        log_message("debug", "RESPONSE" . serialize($finalArr));
        echo json_encode($finalArr); 
    }
    
    function deleteReview(){
        //userId,accessCode,reviewId
        extract($_REQUEST);
        if (!isset($userId)) {
            $finalArr = array("Failure" => array("message" => "userId is required"), "StatusCode" => 400);
        } else if (!isset($accessCode)) {
            $finalArr = array("Failure" => array("message" => "accessCode is required"), "StatusCode" => 400);
        } else {
            $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
            if ($checkAccessCode > 0) {
                $deleteReview = $this->Api_v1_model->deleteReview($reviewId);
                if($deleteReview > 0){
                    $finalArr = array("Success" => array("message" => "Review deleted successfully"), "StatusCode" => 200);   
                }else {
                    $finalArr = array("Failure" => array("message" => "Fail to delete review"), "StatusCode" => 400);
                }
            }else{
                $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
            }     
        }
        log_message("debug", "RESPONSE" . serialize($finalArr));
        echo json_encode($finalArr); 
    }
    
    function feedBack(){//feedback to app
        //userId,accessCode,feedbackComments
        extract($_REQUEST);
        if (!isset($userId)) {
            $finalArr = array("Failure" => array("message" => "userId is required"), "StatusCode" => 400);
        } else if (!isset($accessCode)) {
            $finalArr = array("Failure" => array("message" => "accessCode is required"), "StatusCode" => 400);
        } else {
            $checkAccessCode = $this->Api_v1_model->checkAccessCode($userId, $accessCode);
            if ($checkAccessCode > 0) {
                $checkFeedback = $this->Api_v1_model->checkFeedback($userId);
                if($checkFeedback > 0){
                    $finalArr = array("Success" => array("message" => "Your feedback is already with us"), "StatusCode" => 200);   
                }else {
                    $addFeedback = $this->Api_v1_model->addFeedback($userId,$feedbackComments);
                    if($addFeedback > 0){
                        $finalArr = array("Failure" => array("message" => "Feedback added successfully"), "StatusCode" => 400);
                    }else{
                        $finalArr = array("Failure" => array("message" => "Fail to add feddback"), "StatusCode" => 400);
                    }  
                }
            }else{
                $finalArr = array("Failure" => array("message" => "Access Code does not match"), "StatusCode" => 400);
            }     
        }
        log_message("debug", "RESPONSE" . serialize($finalArr));
        echo json_encode($finalArr); 
    }
}

<div class="header1">
	<div class="container">
		<div class="header-inner">
			<div class="header-logo">                    
				<a class="brand logo-v1" href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>assests/img/logo.jpg" id="logoimg" alt="" style="margin-top:0px; height:70px;">
				</a> 
			</div>
			<div class="header-links">
				<ul class="p-0 m-0">
					<li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#homePageLoginForm">Register Here</button></li>
						    <?php 	if(isset($_SESSION['LoggedIn']) && $_SESSION['LoggedIn'] == 1){	?>
								  <li class="m-l-10"><a href="<?php echo base_url();?>index.php/Home/dashboard" class="btn btn-default" >Dashboard</a></li>
						    <?php } else { ?>
								  <li class="m-l-10"><button type="button" class="btn btn-default" data-toggle="modal" data-target="#homePageLoginForm" >Login</button></li>
						    <?php } ?>
				      </ul>                                              
			</div> 
		</div>
	</div>
</div>      
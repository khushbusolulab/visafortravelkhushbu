<body>
<?php  include("menu.php"); ?>
<!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">My Subscription Management</h3>
				<label class="pull-left">Currently Subscribed Packages</label>
			</div>
		</div> 
	<div class="row">	
		<?php foreach($selectedPackages['allData'] as $value) { ?>
		<div class="col-sm-6">
			<div class="well well-lg">
				<h3><?php echo $value['packageName'];?> Package</h3>
				<p><?php echo $value['packageDetails'];?></p>
				<div class="clearfix"></div>
				<div class="clearfix"></div>
				<?php if($value['packageId'] == "4") { ?>
					<label class="pull-left"><strong>Never Expires</strong></label>
				<?php }else{?>
					<label class="pull-left"><strong>Expires On <?php echo $value['packageExpiryDate'];?></strong></label>
				<span class="pull-right">
					<a href="javascript:void(0);" name="buttonRenew" id="buttonRenew"  data-PackageId = "<?php echo $value['packageId'];?>" data-amount="<?php echo $value['packageAmount'];?>" class="btn btn-info renewNowButton">Renew Now</a>
				</span>
				<?php }?>
				 <div class="clearfix"></div>
			</div>
		</div>
		<?php } ?>		
	</div>
		<div class="clearfix"></div>
		<?php 
		if(!empty($notSelectedPackages)) {
		?>
		<div class="row margin-top-20">
			<div class="col-sm-12">
				<h3 class="page-title">More ways to engage new customers</h3>
				<form name="paymentForm" id="paymentForm" method="post" action="<?php echo base_url()."index.php/Home/insertOrderMaster";?>">
				    <div class="">
					    <div class="pull-right mar-bot10">
						    Amount Payable : <span id="amountDiv">0</span> USD 
						    <!--<a href="<?php echo base_url()."index.php/Home/paymentOption";?>" class="btn btn-primary" id="showPayNowButton" style="display:none;">Pay Now</a> -->
						    <input type="hidden" name="hidAmmountValue" id="hidAmmountValue" value="">
						    <input type="hidden" name="hidPackagePurchased" id="hidPackagePurchased" value="">
						    <input type="hidden" name="hidbusinessId" id="hidbusinessId" value="<?php echo $_SESSION['businessId'];?>">
						    <input type="hidden" name="hidBuyType" id="hidBuyType" value="">
						    <input type="submit" value="Pay Now" class="btn btn-primary" id="showPayNowButton" style="display:none;" >
					    </div>
				    </div>
			    </form>
			    <?php //} ?>
			</div>
		</div> 
        <!-- BEGIN PRICING OPTION -->
        <div class="row margin-bottom-40">
		<?php 
			$class = array(1 => 'yellow',2 => 'green',3 => 'blue',4 => 'red');
			foreach($notSelectedPackages as $value_not_selected){ 			
		?>
            <div class="col-sm-3">
	    <div class=" pricing-active <?php echo $class[$value_not_selected['packageId']];?>">
                <div class="pricing-head">
                    <h3><?php echo $value_not_selected['packageName'];?> Package</h3>
                    <h4><i>$</i><i><?php echo $value_not_selected['packageAmount'];?></i> <span>Per Month</span></h4>
                </div>                
                <div class="pricing-footer">
                    <p><?php echo $value_not_selected['packageDetails'];?></p>
                    <a href="javascript:void(0);" class="btn theme-btn selectThisOption" data-PackageId = "<?php echo $value_not_selected['packageId'];?>" data-amount="<?php echo $value_not_selected['packageAmount'];?>" >Buy Now</a>  
                </div>
		</div>
            </div>
		<?php } } ?>
		</div>
		
	</div>
<body>
<?php  include("menu.php"); ?>
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
		<div class="row">
			<div class="col-sm-12">
				    <h3 class="page-title">Payment History</h3>
			</div>
		</div>
		<div class="row">
			 
<div class="bootstrap-iso">
 <div class="">
 
   <div class="col-md-7 col-sm-7 col-xs-12">
    <form action="<?php echo base_url();?>index.php/Home/paymentHistory"  method="post">
     <div class="form-group">      
      </label>
      <div class="">
       <div class="input-group">
        <!--<div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>-->
		<span>From Date</span>
        <input class="form-control" id="fromDate" name="fromDate"   type="text" <?php if(isset($fromDate) && $fromDate != "" ) {echo "value='".$fromDate."'";} ?>/>
		<span>To Date</span>
        <input class="form-control" id="toDate" name="toDate" type="text" <?php if(isset($toDate) && $toDate != "" ) {echo "value='".$toDate."'";} ?> />
	       <button class="btn btn-info " name="submit" type="submit">
        Submit
       </button>
       </div>
      </div>
  
     
	  </div>
    </form>
   </div>
  
 </div>
</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">					
				<div class="">
					<div class="col-sm-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<?php if(!empty($data)){ ?>
						<div class="portlet box ">							 
							<div class="portlet-body">								 
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Sr.No</th>										
											<th>Transaction ID</th>										
											<th>Payment Date</th>
											<th>Payment Mode</th>											 
											<th>Payment Amount</th>											 
											<th>Payment Status</th>											 
										</tr>
									</thead>
									<tbody>
									<?php  
									$i = 1;
									foreach($data as $value) { 
									?>
										<tr>
											<td><?php echo $i;?></td>											
											<td><?php echo $value['transaction_id'];?></td>																						
											<td><?php echo $value['paymentDate'];?></td>																						
											<td><?php echo $value['payment_method'];?></td>																						
											<td>
												<?php 
													print number_format($value['totalamount'],"2",".","");
													if($value['country'] == "India"){
														echo " INR";
													}else{
														echo " $";
													}
												?>
											</td>																						
											<td>
												<?php 													
													if($value['paymentStatus'] == 1){
														echo "Payment Successfull";
													}else if ($value['paymentStatus'] == 0 ){
														echo "Payment Declined";
													}
												?>
											</td>
										</tr>
									<?php 
										$i++;
									} ?>	 							 
									</tbody>
								</table>
							</div>
						</div>
						<?php }else{ ?>
						<div class="portlet box ">							 
							<div class="portlet-body">								 
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th >No Data Found</th>																					
										</tr>
									</thead>
									<tbody></tbody>
									</table>
							</div>
						</div>
						<?php } ?>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div> 
		</div>
    </div>
	<!-- Define content Clicks-->	
	<div id="clicksContent" class="popover container">    
		<div id="loaderDivClicks">
			<img src="<?php echo base_url();?>assests\img\loader.gif">
		</div>			
		<ul class="nav nav-pills" role="tablist">
			<li role="presentation" class="active"><a href="#countryClicks" aria-controls="countryClicks" role="tab" data-toggle="tab">Country</a></li>
			<li role="presentation"><a href="#cityClicks" aria-controls="cityClicks" role="tab" data-toggle="tab">City</a></li>    
		</ul>		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="countryClicks">				
			</div>
			<div role="tabpanel" class="tab-pane" id="cityClicks">				
			</div>       
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<!-- Define content Bookmarks-->	
	<div id="bookmarksContent" class="popover container"> 
		<div id="loaderDivBookmarks">
			<img src="<?php echo base_url();?>assests\img\loader.gif">
		</div>
		<ul class="nav nav-pills" role="tablist">
			<li role="presentation" class="active"><a href="#countrybookmark" aria-controls="countrybookmark" role="tab" data-toggle="tab">Country</a></li>
			<li role="presentation"><a href="#citybookmark" aria-controls="citybookmark" role="tab" data-toggle="tab">City</a></li>    
		</ul>		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="countrybookmark">				
			</div>
			<div role="tabpanel" class="tab-pane" id="citybookmark">				
			</div>       
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<!-- Define content Reviews-->	
	<div id="reviewsContent" class="popover container"> 
		<div id="loaderDivReviews">
			<img src="<?php echo base_url();?>assests\img\loader.gif">
		</div>
		<ul class="nav nav-pills" role="tablist">
			<li role="presentation" class="active"><a href="#countryreviwews" aria-controls="countryreviwews" role="tab" data-toggle="tab">Country</a></li>
			<li role="presentation"><a href="#cityreviews" aria-controls="cityreviews" role="tab" data-toggle="tab">City</a></li>    
		</ul>		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="countryreviwews">				
			</div>
			<div role="tabpanel" class="tab-pane" id="cityreviews">				
			</div>       
		</div>
		<div class="clearfix"></div>
	</div>
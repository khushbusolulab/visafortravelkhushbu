<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>	

<script src="<?php echo base_url();?>assests/js/admin/jquery.dataTables.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/DT_bootstrap.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/table-editable.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/app.js"></script>      
<script>
	jQuery(document).ready(function() {    
	   App.init();
	    TableEditable.init();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
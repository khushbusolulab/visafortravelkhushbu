<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>MyVisaForTravel | Admin Panel - App Users</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />	
	<link href="<?php echo base_url();?>assests/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/admin/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/admin/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--<link href="<?php echo base_url();?>assests/css/admin/style-metro.css" rel="stylesheet" type="text/css"/>-->
	<link href="<?php echo base_url();?>assests/css/admin/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/admin/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/admin/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo base_url();?>assests/css/admin/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/admin/DT_bootstrap.css" rel="stylesheet" type="text/css"/>	
	<link href="<?php echo base_url();?>assests/css/admin/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css"/>	
	<link href="<?php echo base_url();?>assests/css/admin/admin.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assests/css/custom.css" rel="stylesheet" type="text/css"/>	
</head>
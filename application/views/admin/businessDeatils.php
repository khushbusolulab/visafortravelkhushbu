<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title">
							<strong>Business Deatils</strong>
							<a href="<?php echo base_url();?>index.php/Admin/busniessUsers" class="btn btn-primary pull-right">Back</a>
						</h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">
					<div class="col-sm-2">							
							<div class="user-pic">
								<?php if(!empty($officePictures['images'])) {	$filename = "./uploads/office_pictures/".$officePictures['images'];	if(file_exists($filename)){	?>
									<img src="<?php echo base_url()."uploads/office_pictures/".$officePictures['images'];?>">			
								<?php }else{ ?>
									<img src="<?php echo base_url()."assests/img/User.png";?>">						
								<?php }	}else{	?> 
									<img src="<?php echo base_url()."assests/img/User.png";?>">						
								<?php }	?>
							</div>							
					</div>
					<div class="col-sm-5 col-xs-6">
						<div class="headerInfo bizdetail">							
							<span class="user-name"><strong><?php echo $businessesData['businessName'];?></strong></span>
							<span class="user-info-span" ><?php echo $businessesData['businessEmail'];?></span>
							<span class="user-info-span" ><?php echo $businessesData['businessPhone'];?></span>
							<span class="user-info-span" ><?php if(isset($businessesData['cityName'])){ echo $businessesData['cityName'].", ";}?><?php if(isset($businessesData['stateName'])) {echo $businessesData['stateName'].", ";} ?><?php if(isset($businessesData['countryName'])) { echo $businessesData['countryName'].","; } ?></span>
						</div>
					</div>	
					<div class="col-sm-5 col-xs-6">
						<div class="headerInfo bizdetail">
								<span class="user-name"><strong>Address:</strong></span>
								<span class="user-info-span" ><?php echo $businessesData['businessAddress1'];?></span>
								<span class="user-info-span" ><?php echo $businessesData['businessAddress2'];?></span>
								<span class="user-info-span" ><?php echo $businessesData['businessArea'];?></span>						
						</div>
					</div>						
				</div>
			
			<!-- Busniess Office PIcture -->
			<div class="row margin-bottom-20">					 
				
					<div class="col-sm-12">
						<h3>Office Picture</h3>
						<?php if(!empty($officePictures['images'])) {	$filename = "./uploads/office_pictures/".$officePictures['images'];	if(file_exists($filename)){	?>
						<div class="well well-lg">																			 
							<div class="jFiler-items jFiler-row" id="previewContainer">
								<ul class="jFiler-items-list jFiler-items-grid">
									<li class="jFiler-item" data-jfiler-index="1" style="">
										<div class="jFiler-item-container">
											<div class="jFiler-item-inner">
												<div class="jFiler-item-thumb">
													<div class="jFiler-item-status"></div>                        
													<div class="jFiler-item-thumb-image">
														<img id="previewCroppedImage" src="<?php echo base_url()."uploads/office_pictures/".$officePictures['images'];?>" draggable="false" style="width:100% !important;">
													</div>
												</div>
											</div>
										</div>
									</li>   
								</ul>
							</div>
						</div>
				<?php 	}
						}else{ ?>
							<div class="well well-lg">
								Office Picture Not Fond.
							</div>
						<?php }
						?>
					</div>
				
			</div><!-- Busniess Office PIcture Ends-->			
			
			<?php 	if(isset($serviceTags)){
					if($serviceTags != ""){
				$serviceTags = explode(',',$serviceTags);				
			?>
			<div class="row margin-bottom-20">							
					<div class="col-sm-12">
						<h3>Services</h3>												 																		 						 
						<?php foreach($serviceTags as $value){?>
							<span class="tag tag-default"><?php echo $value;?></span>
						<?php } ?>						 			 
					</div>
			</div>
			<?php }
			}else{ ?>
				<div class="row margin-bottom-20">				
					<div class="col-sm-12">
						<h3>Services</h3>												 																		 						 						 
						<span class="big-fonts">No Service Tags Found</span>						 					 			 
					</div>
			</div>
			<?php } ?>
			
				<!-- Busniess Service PIcture -->
			<div class="row margin-bottom-20">					 
					<div class="col-sm-12">						
						<h3>Service  Picture</h3>
						<?php if(!empty($ServicePictures)) { ?>
							<div class="">	
								<div class="jFiler-items jFiler-row" id="previewContainer">
								<?php	
									foreach($ServicePictures as $value){
									$filename = "./uploads/service_pictures/".$value['images'];	
									if(file_exists($filename)){	
								?>
								<ul class="jFiler-items-list jFiler-items-grid">
									<li class="jFiler-item" data-jfiler-index="1" style="">
										<div class="jFiler-item-container">
											<div class="jFiler-item-inner">
												<div class="jFiler-item-thumb">
													<div class="jFiler-item-status"></div>                        
													<div class="jFiler-item-thumb-image">
														<img id="previewCroppedImage" src="<?php echo base_url()."uploads/service_pictures/".$value['images'];?>" draggable="false" style="width:100% !important;">
													</div>
												</div>
											</div>
										</div>
									</li>   
								</ul>
									<?php 	} } ?>
							</div>						
				</div>
						<?php }else{ ?>
							<div class="well well-lg">
								Service Pictures Not Fond.
							</div>
						<?php }
						?>
					</div>
				
			</div><!-- Busniess Service PIcture Ends-->	


			
			<?php 	if(!empty($finalopeninghours)){		?>
			<div class="row margin-bottom-20">					 
										
					<div class="col-sm-6">
						<h3>Operating Hours</h3>							
							<?php foreach($finalopeninghours as $value){?>
								<div class="">
									<span class="col-xs-3"><?php echo $value['day'];?></span>
									<span class="col-xs-3" ><?php echo $value['openingHours'];?></span>
									<span class="col-xs-3" >TO</span>
									<span class="col-xs-3" ><?php echo $value['closingHours'];?></span>
									<div class="clearfix"></div>
								</div>
								
							<?php } ?>													
					</div>
				
			</div>
			<?php } ?>
			
			<div class="row margin-bottom-20">
					<div class="col-sm-12" style="margin-left:0px !important;">
						<h3>Quick Stats</h3>
						<div class="">
							<div class="col-sm-3">
								<div class="well text-center">
									<h4 class="text-center">Ads Clicked Today</h4>
									<label class="text-center" ><?php echo $businessesData['businessesNoOfViews'];?></label>					 
								</div>
							</div>
							<div class="col-sm-3">
								<div class="well text-center">
									<h4 class="">Number of Reviews</h4>
									<label class="text-center" ><?php echo $businessesData['noOfReviews'];?></label>					 
								</div>
							</div>
							<div class="col-sm-3">
								<div class="well text-center">
									<h4 class="text-center">Number of bookmarks</h4>
									<label class="text-center" ><?php echo $businessesData['noOfBookmarks'];?></label>					 
								</div>
							</div>
							<div class="col-sm-3">
								<div class="well text-center">
									<h4 class="text-center">Average Ratings</h4>
									<label class="text-center" ><?php echo $businessesData['avgRating'];?></label>					 
								</div>
							</div>
						</div>
					</div>
			</div>
			
			
			<div class="row margin-bottom-20">
					<div class="col-sm-12">
						<h3>Reviews Received</h3>
						<div class="well">
							<?php 	if(!empty($RecentReviews)){
										foreach($RecentReviews as $value){ ?>
										<blockquote>
											<span class="pull-left">
											<?php 
												if(!empty($value['profilePicture'])) {	
													$filename = "./uploads/userImages/".$value['profilePicture'];	
														if(file_exists($filename)){	
											?>
												<img src="<?php echo base_url();?>/uploads/userImages/<?php echo $value["profilePicture"];?>" class="width-100">
											<?php 		}
												} else{ 
											?>
												<img src="<?php echo base_url();?>assests/img/User.png" class="width-100">
											<?php } ?>
											</span>
											<span class="pull-left">
												<label class="user-name-label"><?php echo $value['fullName'];?></label>
												<span class="rating-static rating-<?php echo $value['rating'];?>0 class-new-rating"></span>
											</span>
											<span class="pull-right">
												<?php echo $value['createdDate'];?>
											</span>
											<div class="clearfix"></div>
											<p><?php echo $value['rateComments'];?></p>
										</blockquote>		
							<div class="clearfix"></div>
							<?php 
							}
							}else{
							?>	
							<blockquote>
								<p>No Reviews found</p>
							</blockquote>
							<?php 	}
							?>		
						</div>
					</div>
			</div>
			
		</div>
	</div>		
</div>
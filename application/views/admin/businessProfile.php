<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
<div class="page-container" >  
	<div class="page-content">			
		<div class="container">				
			<div class="row">
				<div class="col-sm-12">	
					<?php if($arrAddEditInfo['businessId'] == 0) {?>
						<h3 class="page-title">
							<strong>Add Business User</strong>
							<a href="<?php echo base_url();?>index.php/Admin/busniessUsers" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } else{ ?>
						<h3 class="page-title">
							<strong>Edit Business User</strong>
							<a href="<?php echo base_url();?>index.php/Admin/busniessUsers" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } ?>
				</div>
			</div>	
		<form method="post" name="businessProfileForm" id="businessProfileForm" action="<?php echo base_url();?>index.php/Admin/performBusinessProfile" enctype="multipart/form-data" >			
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			<div class="row margin-bottom-20">					 
							
					
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Business Merchant Name</label>
								<div class="controls">
									<input type="text" placeholder="Business Merchant Name" class="form-control m-wrap medium" name="businessMerchantName" id="businessMerchantName" value="<?php echo $arrAddEditInfo['businessMerchantName'];?>" />									
								</div>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Business Display Name</label>
								<div class="controls">
									<input type="text" placeholder="Business Display Name" class="form-control m-wrap medium" name="businessName" id="businessName" value="<?php echo $arrAddEditInfo['businessName'];?>"  />									
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Contact Number</label>
								<div class="controls">
									<input type="number" placeholder="Contact Number" class="form-control m-wrap medium" name="businessPhone" id="businessPhone" value="<?php echo $arrAddEditInfo['businessPhone'];?>" onkeypress="return onlyNumbers (event)" />									
								</div>
							</div>
						</div>																	 					
					
				
			</div>
					
			<div class="row margin-bottom-20">					 
				
					
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Business E-Mail Address</label>
								<div class="controls">
									<input type="text" placeholder="Business E-Mail Address" class="form-control m-wrap medium" name="businessEmail" id="businessEmail" value="<?php echo $arrAddEditInfo['businessEmail'];?>" />									
								</div>
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Address Line 1</label>
								<div class="controls">
									<input type="text" placeholder="Address Line 1" class="form-control m-wrap medium" name="businessAddress1" id="businessAddress1" value="<?php echo $arrAddEditInfo['businessAddress1'];?>" />									
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Address Line 2</label>
								<div class="controls">
									<input type="text" placeholder="Address Line 2" class="form-control m-wrap medium" name="businessAddress2" id="businessAddress2" value="<?php echo $arrAddEditInfo['businessAddress2'];?>" />									
								</div>
							</div>
						</div>																		 					
					
				
			</div>


			<div class="row margin-bottom-20">					 
				
					
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Area / Locality</label>
								<div class="controls">
									<input type="text" placeholder="Area / Locality" class="form-control m-wrap medium" name="businessArea" id="businessArea" value="<?php echo $arrAddEditInfo['businessArea'];?>" />									
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Country</label>
								<div class="controls">
									<select class="form-control medium m-wrap" name="countryId" id="countryId">
										<option value="">Select Country</option>
										<?php foreach($countryValues as $country_value){ ?>
											<option value="<?php echo $country_value['countryId'];?>" <?php if( $country_value['countryId'] == $arrAddEditInfo['countryId'] ) {echo "selected";}?>  ><?php echo $country_value['countryName'];?></option>							
										<?php  } ?>										
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">State</label>
								<div class="controls">
									<select class="medium m-wrap form-control" id="state" name="state" >
										<?php 										
											if(($arrAddEditInfo['StateList']) && !empty($arrAddEditInfo['StateList'])){
												foreach($arrAddEditInfo['StateList'] as $state_value){ ?>
												<option value="<?php echo $state_value['stateId'];?>" <?php if($arrAddEditInfo['stateId'] == $state_value['stateId']){ echo "Selected"; }?>><?php echo $state_value['stateName'];?></option>							
										<?php	}
											}else{
												?>
												<option value="">Select State</option>
											<?php }
										?>		
									</select>
								</div>
							</div>
						</div>																	 					
					
				
			</div>	
	
	
			<div class="row margin-bottom-20">					 
				
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">City</label>
								<div class="controls">
									<select class="medium m-wrap form-control" id="city" name="city" >										
										<?php 										
											if(($arrAddEditInfo['cityList']) && !empty($arrAddEditInfo['cityList'])){
												foreach($arrAddEditInfo['cityList'] as $city_value){ ?>
												<option value="<?php echo $city_value['cityId'];?>" <?php if($arrAddEditInfo['cityId'] == $city_value['cityId']){ echo "Selected"; }?>><?php echo $city_value['cityName'];?></option>							
										<?php	}
											}else{
												?>
												<option value="">Select City</option>
											<?php }
										?>											
									</select>
								</div>
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Select Business Category *<sub>Maximum 3</sub></label>
								<div class="controls">
									<select id="businessCategory" name="businessCategory[]" multiple="multiple" class="form-control medium" >
										<?php foreach($categoryValues as $category_value) { ?>
										<option value="<?php echo $category_value['categoryId']?>" <?php if(in_array($category_value['categoryId'],$arrAddEditInfo['selectedCategories'])) { echo "Selected";} ?> ><?php echo $category_value['categoryName']?></option>
										<?php } ?>								
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Upload Office Picture</label>
								<div class="controls">
									<input type="file" name="OficeFileUpload" id="OficeFileUpload" accept='image/*' class="form-control" >
								</div>
							</div>									
						</div>	
						<?php 
							if($arrAddEditInfo['officePictures'] != ""){
								$filename = "./uploads/office_pictures/".$arrAddEditInfo['officePictures'];								
								if(file_exists($filename)){
									 
							 
						?>
					<script type="text/javascript">
						document.getElementById("OficeFileUpload").disabled = true;				
					</script>
					<div class="col-sm-4" id="previewContainer1" >
						<div class="form-group">
							<div class="jFiler-items jFiler-row" >
								<ul class="jFiler-items-list jFiler-items-grid">
									<li class="jFiler-item" data-jfiler-index="1" style="">
										<div class="jFiler-item-container">
											<div class="jFiler-item-inner">
												<div class="jFiler-item-thumb">
													<div class="jFiler-item-status"></div>                        
													<div class="jFiler-item-thumb-image">
														<img id="previewCroppedImage" src="<?php echo base_url()."uploads/office_pictures/".$arrAddEditInfo['officePictures'];?>" draggable="false" style="width:100% !important;">
													</div>
												</div>
												<div class="jFiler-item-assets jFiler-row">
													<ul class="list-inline pull-left">
														<li></li>
													</ul>
													<ul class="list-inline pull-right">
														<li><a id="deleteThisPreviewedImage" class="icon-jfi-trash jFiler-item-trash-action" data-imageid="<?php echo $arrAddEditInfo['businessId']?>"></a></li>
													</ul>
												</div>
											</div>
										</div>
									</li>   
								</ul>
							</div>
						</div>
					</div>
		<?php 
				}else{ ?>
				<script type="text/javascript">
					document.getElementById("OficeFileUpload").disabled = false;				
				</script>
				<span>File Not Found.Please Upload a new office picture</span>					
		<?php 
				}
		} ?>
						<div class="col-sm-4">
								<div class="form-group">
									<div class="jFiler-items jFiler-row" id="previewContainer" style="display:none;">
										<ul class="jFiler-items-list jFiler-items-grid">
											<li class="jFiler-item" data-jfiler-index="1" style="">
												<div class="jFiler-item-container">
													<div class="jFiler-item-inner">
														<div class="jFiler-item-thumb">
															<div class="jFiler-item-status"></div>                        
															<div class="jFiler-item-thumb-image">
																<img id="previewCroppedImage" src="" draggable="false" style="width:100% !important;">
															</div>
														</div>
														<div class="jFiler-item-assets jFiler-row">
															<ul class="list-inline pull-left">
																<li></li>
															</ul>
															<ul class="list-inline pull-right">
																<li><a id="deleteThisPreviewedImage" class="icon-jfi-trash jFiler-item-trash-action"></a></li>
															</ul>
														</div>
													</div>
												</div>
											</li>   
										</ul>
									</div>
								</div>
							</div>
					
			</div>


				<div class="row margin-bottom-20">					 
					<div class="col-sm-12">
						<label class="control-label">Services <sub>(Mention your best services to find your business in search boxes)</sub></label>
						<input type="text" id="tokenfield" name="businessServices"  id="businessServices" value="<?php echo $arrAddEditInfo['serviceTags'];?>"  class="form-control token-example-field m-wrap large" />
					</div>	
				</div>
				<div class="row margin-bottom-20">					 
					<div class="col-sm-12">
						<div class="form-group" >
							<label class="control-label">Upload Service Pictures</label>
							<input type="hidden" name="serviceImageCount" id="serviceImageCount" value="<?php if(!empty($arrAddEditInfo['ServicePictures'])){echo count($arrAddEditInfo['ServicePictures']);}else{echo "0";}?>" class="classserviceImageCount">
							<div class="controls">
							<input type="file" name="ServicesFileUpload" id="ServicesFileUpload" accept='image/*' class="form-control">
							</div>
						</div> 
						 
							 
						<?php  if(($arrAddEditInfo['businessId']) && is_numeric($arrAddEditInfo['businessId']) && $arrAddEditInfo['businessId'] > 0){  ?>
						<div class="addbussiness">
							<div class="jFiler-items jFiler-row" id="previewContainerForServiceImages">
							 <ul class="jFiler-items-list jFiler-items-grid" id="ulForSserviceImages">
							 <?php 
							 if(!empty($arrAddEditInfo['ServicePictures'])) {
							  foreach($arrAddEditInfo['ServicePictures'] as $value){ 
							   $filename = "./uploads/service_pictures/".$value['images'];
							   if(file_exists($filename)){ ?>
							   <li class="jFiler-item" data-jfiler-index="1" style="">
							   <li class='jFiler-item' data-jfiler-index='1' id='previewBox_<?php echo $value['businessesImageId'];?>'>
							   <div class="jFiler-item-container">
							    <div class="jFiler-item-inner">
							     <div class="jFiler-item-thumb">
							      <div class="jFiler-item-status"></div>                        
							      <div class="jFiler-item-thumb-image">
							       <img id="previewCroppedImage" src="<?php echo base_url()."uploads/service_pictures/".$value['images'];?>" draggable="false" style="width:100% !important;">
							      </div>
							     </div>
							     <div class="jFiler-item-assets jFiler-row">
							      <ul class="list-inline pull-left">
							       <li></li>
							      </ul>
							      <ul class="list-inline pull-right">              
							       <li><a class='icon-jfi-trash jFiler-item-trash-action deleteThisPreviewedImageOfServices1' data-ServiceId = "<?php echo $value['businessesImageId'];?>"></a></li>
							      </ul>
							     </div>
							    </div>
							   </div>
							  </li>   
							 <?php 
							   }
							  }
							 }  ?>
							</ul>
						       </div> 
					       </div> 
							<?php }else{  ?>
							  <div class="addbussiness"> 
							 <div class="jFiler-items jFiler-row" id="previewContainerForServiceImages" style="display:none;">
							  <ul class="jFiler-items-list jFiler-items-grid" id="ulForSserviceImages"></ul>
							 </div>
							</div>
							<?php  } ?>       
						       
					</div>
				</div>

				<div class="clearfix margin-bottom-20"></div>
				<div class="row">					 
					
						<div class="col-sm-6">
							
								<div class="col-xs-4">
									<div class="form-group">								
										<label class="control-label">Operating Hours</label>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">								
										<label class="control-label">Start Time</label>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">								
										<label class="control-label">End Time</label>
									</div>
								</div>
						</div>
				</div>
								<?php 	if(($arrAddEditInfo['businessId']) && is_numeric($arrAddEditInfo['businessId']) && $arrAddEditInfo['businessId'] > 0){ 	?>
									
									<?php
		
		function openingHoursHtml($index, $day, $data) {
			$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
			//echo "<pre>";print_r($data);echo "</pre>";
			$html = '<div class="row">';
			$html .= '<div class="col-sm-6">';
			$html .= '<div class="col-xs-4">';
			$html .='<div class="form-group">';
			$html .='<div class="controls">';
			$html .='<input type="checkbox" name="weekdays[' . $index . ']" id="weekdays" class="daysCheckBox form-control" ' . (is_array($data) ? 'checked' : '') . ' /> ' . $days[$index];
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='<div class="col-xs-4">';
			$html .='<div class="form-group">';
			$html .='<div class="controls" >';
			$html .='<input type="text" name="fromTime[' . $index . ']" class="m-wrap small pull-left form-control timepickerFromTime fromTimeTextInput" value="'.(is_array($data) ? $data["openingHours"] : '').'" />';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='<div class="col-xs-4">';
			$html .='<div class="form-group">';
			$html .='<div class="controls">';
			$html .='<input type="text" name="toTime[' . $index . ']" class="m-wrap small pull-left timepickerFromTime toTimeTextInput form-control"  value="'. (is_array($data) ? $data['closingHours'] : '').'" />';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='<div class="clearfix margin-bottom-10"></div>';
			return $html;
		}
		
		//for($intI = 1; $intI <= 7; $intI++) {
		$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
		foreach($days as $key => $data) {
			if (isset($arrAddEditInfo['finalopeninghours'][$data])) {
				echo openingHoursHtml($key, $data, $arrAddEditInfo['finalopeninghours'][$data]);
			} else {
				echo openingHoursHtml($key, $data, '');
			}
		} ?>
											
								<?php 	}else{	?>
										<?php
								$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
								for($intI = 1; $intI <= 7; $intI++) { 				
								
							?>
							
							
							<div class="row">
								<div class="col-sm-6">
								<div class="col-xs-4">
									<div class="form-group">								
										<div class="controls">
											<input type="checkbox" name="weekdays[<?php echo $intI;?>]" id="weekdays[<?php echo $intI;?>]" class="daysCheckBox form-control" /><?php echo $days[$intI]; ?>
										</div>
									</div>
								</div>	
								<div class="col-xs-4">
									<div class="form-group">					
										<div class="controls" >						
											<input type="text" name="fromTime[<?php echo $intI;?>]" id="timepickerFromTime_<?php echo $intI;?>"  placeholder="From" class="form-control m-wrap small pull-left form-control timepickerFromTime fromTimeTextInput" readonly="readonly" />						 
										</div>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">					
										<div class="controls">						
											<input type="text" name="toTime[<?php echo $intI;?>]" id="timepickerToTime_<?php echo $intI;?>" placeholder="To" class="form-control m-wrap small pull-left timepickerFromTime toTimeTextInput"  readonly="readonly" />
										</div>
									</div>
								</div>
								</div>
							</div>
							<?php 	} ?>	
									<?php }?>
								
								
				<?php 
					if(!empty($arrAddEditInfo['purchasedPackage'])){
				?>
				<div id="renewPackageDiv">
			<div class="row">	
				<div class="col-sm-4">
					<h3>Renew Package</h3>
				</div>
			</div>
				<div class="row">	
				<div class="col-sm-4">	
		<?php 
			
			foreach($arrAddEditInfo['purchasedPackage'] as $value) { 
				if($value['packageId'] != "4"){
		?>
		<div class="col-sm-6">
			<div class="well well-lg">
				<h3><?php echo $value['packageName'];?> Package</h3>
				<p><?php echo $value['packageDetails'];?></p>
				<div class="clearfix"></div>
				<div class="clearfix"></div>
				<?php if($value['packageId'] == "4") { ?>
					<label class="pull-left"><strong>Never Expires</strong></label>
				<?php }else{?>
					<label class="pull-left"><strong>Expires On <?php echo $value['packageExpiryDate'];?></strong></label>
				<span class="pull-right">
					<a href="javascript:void(0);" data-PackageId = "<?php echo $value['packageId'];?>" data-amount="<?php echo $value['packageAmount'];?>" class="btn theme-btn removePackageButton">Remove Package</a>
					<a href="javascript:void(0);" name="buttonRenew" id="buttonRenew"  data-PackageId = "<?php echo $value['packageId'];?>" data-amount="<?php echo $value['packageAmount'];?>" class="btn theme-btn renewNowButton">Renew Now</a>
				</span>
				<?php }?>
				 <div class="clearfix"></div>
			</div>
		</div>
		<?php 
			}
		}	
		?>		
	</div>
	</div>			
	</div>	
	<?php } ?>	
		<div id="purchasePackageDiv">		
		<div class="row">	
			<div class="col-sm-12">
				<h3>Purchase Package</h3>
			</div>
		</div>
		<?php 	if(!empty($notSelectedPackages)) {		?>
		<div class="row margin-bottom-20">					 
					<?php 
			$class = array(1 => 'yellow',2 => 'green',3 => 'blue',4 => 'red');
			foreach($notSelectedPackages as $value_not_selected){ 			
		?>
            <div class="col-sm-4">
		<div class="pricing-active <?php echo $class[$value_not_selected['packageId']];?>">
                <div class="pricing-head">
                    <h3><?php echo $value_not_selected['packageName'];?> Package</h3>
                    <h4><i>$</i><i><?php echo $value_not_selected['packageAmount'];?></i> <span>Per Month</span></h4>
                </div>                
                <div class="pricing-footer">
                    <p><?php echo $value_not_selected['packageDetails'];?></p>
                    <a href="javascript:void(0);" class="btn theme-btn selectThisOption" data-PackageId = "<?php echo $value_not_selected['packageId'];?>" data-amount="<?php echo $value_not_selected['packageAmount'];?>" >Buy Now</a>  
                </div>
		</div>
            </div>			
		<?php } ?>
			</div>			
 <?php } ?>
 </div>
 <div id="dropdownOfMonths" style="display:none;">
			<div class="row margin-bottom-20">					 
									
						<div class="col-sm-4">
							<div class="form-group">										 
								<label class="control-label">For Months</label>
								<div class="controls">
									<select class="medium m-wrap form-control" id="monthsDropdown" name="monthsDropdown" >										
										<option value="1">1</option>
										<option value="3">3</option>
										<option value="6">6</option>
										<option value="12">12</option>								
										
									</select>
								</div>						 
							</div>
								<label class="control-label">Amount Payable :<span id="amountDiv">0</span> USD </label>
						</div>	
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Cheque Number</label>
								<div class="controls">
									<input type="text" placeholder="Cheque Number" class="form-control m-wrap medium" name="chequeNumber" id="chequeNumber" />									
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Payment Notes</label>
								<div class="controls">
									<textarea class="large m-wrap form-control" rows="3" name="paymentNotes" id="paymentNotes"></textarea>
								</div>
							</div>
						</div>
						
			</div>	
</div>

		<div class="row margin-bottom-20">					 
					<div class="col-sm-4">
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary btn-lg btn-block" id="submitstaffProfileForm">Submit Details</button>
					</div>
					<div class="col-sm-4">
					</div>
			</div>	
			<input type="hidden" name="hidOfficeImage" id="hidOfficeImage">
			<div id="hiddenElements"></div>			
			<input type="hidden" name="businessId" id="businessId" value="<?php echo $arrAddEditInfo['businessId'];?>" />			
			<input type="hidden" name="hidPayableAmount" id="hidPayableAmount" value="" />
			<input type="hidden" name="hidPackageID" id="hidPackageID" value="" />
			<input type="hidden" name="hidCountryName" id="hidCountryName" value="" />
			<input type="hidden" name="hidStateName" id="hidStateName" value="" />
			<input type="hidden" name="hidCityName" id="hidCityName" value="" />
			<input type="hidden" name="hidPurchaseType" id="hidPurchaseType" value="" />
			</form>				
				</div>				
			</div>			
		</div>	
	</div>
</div>


<!-- Modal -->		
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Crop Office Picture</h3>
			</div>
			<div class="modal-body">
				<div>
					<img id="image" src="#">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="saveOfiiceCroppedImage" type="button"> Save changes</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->  
<div id="myModalServices" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Crop Services Picture</h3>
			</div>
			<div class="modal-body">
				<div>
					<img id="imageServices" src="#">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="saveOfiiceServicesCroppedImage" type="button"> Save changes</button>
			</div>
		</div>
	</div>
</div>

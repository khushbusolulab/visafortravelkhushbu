<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title">
							<strong>Staff Members Management</strong>
							<a href="<?php echo base_url();?>index.php/Admin/staffProfile" class="btn btn-primary pull-right" >Add</a>
						</h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">					 
					<div class="col-sm-12">					
						<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/staffMember" class="" >
								<div class="form-group staffmem">
									<label class="label-from-to">Country</label>								 
									<select class="medium m-wrap form-control" name="countryID" id="countryID">
										<option value="">Please Select Country</option>										
										<?php foreach($countryValues as $value) { ?>
										<option value="<?php echo $value['countryId'];?>" <?php if($countryID == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
										<?php } ?>
									</select>
									<button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Go</button>
								</div>
						</form>	
						<div class="portlet box">
							<div class="portlet-title">										
							</div>
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Name</th>
											<th>Email-Address</th>
											<th>Country</th>											
											<th>State</th>											
											<th>City</th>											
											<th>Joining Date</th>											
											<th>Actions</th>
											<th>Block/Unblock</th>											
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($staffMemberData as $value){?>
										<tr>
											<td><?php print htmlentities($value['staffMembersName'],ENT_QUOTES,"UTF-8");?></td>											
											<td><?php print htmlentities($value['staffMembersEmail'],ENT_QUOTES,"UTF-8");?></td>											
											<td><?php echo $value['countryName'];?></td>											
											<td><?php echo $value['stateName'];?></td>											
											<td><?php echo $value['cityName'];?></td>											
											<td><?php echo $value['staffMembersJoiningDate'];?></td>											
											<td><a href="<?php echo base_url();?>index.php/Admin/staffProfile/<?php echo $value['staffMembersId'];?>">Edit</a></td>
											<?php 
												if($value['isBlocked'] == 0){
											?>	
												<td><a href="<?php echo base_url();?>index.php/Admin/blockStaff/<?php echo $value['staffMembersId'];?>">Block</a></td>											
											<?php	} else{ ?>
												<td><a href="<?php echo base_url();?>index.php/Admin/unblockStaff/<?php echo $value['staffMembersId'];?>">Un Block</a></td>											
										<?php 	}
											?>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
			</div>			
		</div>	
	</div>
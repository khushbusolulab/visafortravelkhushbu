<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title">
							<strong>Business User Management</strong>
							<a href="<?php echo base_url();?>index.php/Admin/businessProfile" class="btn btn-primary pull-right" >Add</a>
						</h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">
					<div class="col-sm-12">					
						<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/busniessUsers" class="biz-user-mng" >							 
							<div class="form-group">
								<label for="countryID" class="control-label">Country </label>
								<select class="medium m-wrap form-control" name="countryID" id="countryID">
									<option value="">Please Select Country</option>										
									<?php foreach($countryValues as $value) { ?>
									<option value="<?php echo $value['countryId'];?>" <?php if($countryID == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
									<?php } ?>
								</select>
							</div>
							<div class="form-group">					
								<label class="control-label" for="categoryId" >Category </label>								 
								<select class="medium m-wrap form-control" name="categoryId" id="categoryId">
									<option value="">Please Select Category</option>										
									<?php foreach($categoryValues as $value) { ?>
									<option value="<?php echo $value['categoryId'];?>"  <?php if($categoryId == $value['categoryId']){ echo "Selected"; }?>><?php echo $value['categoryName'];?></option>										
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-md"><i class="icon-ok"></i> Go</button>
							</div>
						
						</form>
					</div>
					<div class="col-sm-12">
						<div class="portlet box">
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Business Name</th>
											<th>Email-Address</th>
											<th>Contact Number</th>											
											<th>Type</th>											
											<th>Country</th>																						
											<th>Actions</th>											
											<th>Edit</th>											
											<th>Block/Unblock</th>											
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($busniessData as $value){?>
										<tr>
											<td><?php print htmlentities($value['businessName'],ENT_QUOTES,"UTF-8");?></td>											
											<td><?php print htmlentities($value['businessEmail'],ENT_QUOTES,"UTF-8");?></td>											
											<td>
												<?php
													if($value['businessPhone'] != ""){
														echo $value['businessPhone'];
													}else{
														echo "";
													}
														
												?>
											</td>
											<td>
												<?php 
													if(($value['strCategories']) &&  $value['strCategories'] != ""){
														echo $value['strCategories'];
													}else{
														echo "";
													}
												?>
											</td>
											<td><?php echo $value['countryName'];?></td>
											<td><a href="<?php echo base_url();?>index.php/Admin/businessDeatils/<?php echo $value['businessId'];?>">View Details</a></td>											
											<td><a href="<?php echo base_url();?>index.php/Admin/businessProfile/<?php echo $value['businessId'];?>">Edit</a></td>
											<?php 	if($value['isBlocked'] == 0){	?>
												<td><a href="<?php echo base_url();?>index.php/Admin/blockBusiness/<?php echo $value['businessId'];?>">Block</a></td>											
											<?php }elseif($value['isBlocked'] == 1 ){ ?>
												<td><a href="<?php echo base_url();?>index.php/Admin/unblockBusiness/<?php echo $value['businessId'];?>">Un Block</a></td>												
											<?php 	}	?>
											
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
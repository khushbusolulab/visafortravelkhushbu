<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>	

<script src="<?php echo base_url();?>assests/js/admin/jquery.dataTables.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/DT_bootstrap.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/table-editable.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.validate.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/additional-methods.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/bootstrap-fileupload.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/app.js"></script>      
<script src="<?php echo base_url();?>assests/js/admin/ui-jqueryui.js"></script>     
<script src="<?php echo base_url();?>assests/js/admin/form-validation.js"></script>      
<script type="text/javascript"> 
	jQuery.validator.addMethod("notEqualTo",
function(value, element, param) {
    var notEqual = true;
    value = $.trim(value);
    for (i = 0; i < param.length; i++) {
        if (value == $.trim($(param[i]).val())) { notEqual = false; }
    }
    return this.optional(element) || notEqual;
},
"Please enter a diferent value."
);
</script>
<script src="<?php echo base_url();?>assests/js/admin/customJS.js"></script>      
<script>
	var base_url = "<?php echo base_url();?>";
	jQuery(document).ready(function() {    	
		App.init();			
		FormValidation.init();
		
		$('#foreignCountry').on('change',function(){		  
			var foreignCountry = $(this).val();					
			if(foreignCountry){				
				$.ajax({
					type:'POST',
					url:base_url+'index.php/Admin/getCityListForThisCountry',
					data:'foreignCountry='+foreignCountry,
					success:function(html){											 						
						$('#foreignCity').html(html);                    
					}
				}); 
			}else{
				$('#state').html('<option value="">Select country first</option>');            
			}
		});		
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
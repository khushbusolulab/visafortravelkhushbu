<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title"><strong>User Management</strong></h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">					 
					<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/appUsers" class="user-mng" >
					<div class="col-sm-12">
						<div class="form-group">
							<label class="label-from-to">Country</label>								 
							<select class="medium form-control" name="countryID" id="countryID">
								<option value="">Please Select Country</option>										
								<?php foreach($countryValues as $value) { ?>
								<option value="<?php echo $value['countryId'];?>" <?php if($countryID == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
								<?php } ?>
							</select>
							<button type="submit" class="btn btn-primary btn-md"><i class="icon-ok"></i> Go</button>
						</div>
					</div>
					</form>
					<div class="col-sm-12">
						<div class="portlet box">
							<div class="portlet-title">										
							</div>
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Name</th>
											<th>Email-Address</th>
											<th>Country</th>											
											<th>State</th>											
											<th>City</th>											
											<th>Last Active</th>											
											<th>Actions</th>											
											<th>Block/Unblock</th>											
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($UsersData as $value){?>
										<tr>
											<td><?php print htmlentities($value['fullName'],ENT_QUOTES,"UTF-8");?></td>											
											<td><?php print htmlentities($value['emailId'],ENT_QUOTES,"UTF-8");?></td>											
											<td><?php echo $value['countryName'];?></td>											
											<td><?php echo $value['stateName'];?></td>											
											<td><?php echo $value['cityName'];?></td>											
											<td><?php echo $value['createdDate'];?></td>											
											<td><a href="<?php echo base_url();?>index.php/Admin/appUserDeatils/<?php echo $value['userId'];?>">View Details</a></td>											
											<?php 
												if($value['isBlocked'] == 0){
											?>	
												<td><a href="<?php echo base_url();?>index.php/Admin/blockUser/<?php echo $value['userId'];?>">Block</a></td>											
											<?php	} else{ ?>
												<td><a href="<?php echo base_url();?>index.php/Admin/unblockUser/<?php echo $value['userId'];?>">Un Block</a></td>											
										<?php 	}
											?>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
<?php 
if(($msg) && $msg != "" && $msg == "INVLD"){
?>	
<script type="text/javascript">
	alert("Invalid Login Credentials.Please try again.");
</script>
<?php  }elseif(($msg) && $msg != "" && $msg == "EMAILSNT") { ?>
<script type="text/javascript">
	alert("Email has been sent to your registered Email Address.");
</script>
<?php  }elseif(($msg) && $msg != "" && $msg == "EMAILNF") { ?>
<script type="text/javascript">
	alert("Please Check your entered Email Address.");
</script>
<?php  }elseif(($msg) && $msg != "" && $msg == "BLOCKED") { ?>
<script type="text/javascript">
	alert("Your account has been blocked,Please contact administrator.");
</script>
<?php } ?>
<body class="login">	
	<div class="logo">
		<img src="<?php echo base_url();?>assests\img\logo.jpg" alt="" width="100px"/> 
	</div>	
	<div class="content">		
		<form class="login-form" action="<?php echo base_url()?>index.php/Admin/doLogin" method="post">
			<h3 class="form-title">Login to your account</h3>
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				<span>Please Enter Email-id and password.</span>
			</div>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email-id</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<input class="placeholder-no-fix form-control" type="text" autocomplete="off" placeholder="Email-id" name="emailAddress"/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<input class="placeholder-no-fix form-control" type="password" autocomplete="off" placeholder="Password" name="password"/>
					</div>
				</div>
			</div>
			<div class="form-group text-center">				 
				<button type="submit" class="btn btn-primary btn-md">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
			<!--<div class="forget-password">
				<h4>Forgot your password ?</h4>
				<p>
					no worries, click <a href="javascript:;"  id="forget-password">here</a>
					to reset your password.
				</p>
			</div>-->			 
		</form>
		<!-- END LOGIN FORM -->        
		<!-- BEGIN FORGOT PASSWORD FORM -->
		<form class="form-vertical forget-form" action="<?php echo base_url()?>index.php/Admin/forgotPassword" method="post">
			<h3 >Forget Password ?</h3>
			<p>Enter your e-mail address below to reset your password.</p>
			<div class="control-group">
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-envelope"></i>
						<input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
					</div>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> Back
				</button>
				<button type="submit" class="btn green pull-right">
				Submit <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
		</form>				
	</div>	
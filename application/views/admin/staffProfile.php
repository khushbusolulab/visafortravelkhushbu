<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
<div class="page-container" >  
	<div class="page-content">			
		<div class="container">				
			<div class="row">
				<div class="col-sm-12">	
					<?php if($arrAddEditInfo['staffMembersId'] == 0) {?>
						<h3 class="page-title">
							<strong>Add Staff Member</strong>
							<a href="<?php echo base_url();?>index.php/Admin/staffMember" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } else{ ?>
						<h3 class="page-title">
							<strong>Edit Staff Member</strong>
							<a href="<?php echo base_url();?>index.php/Admin/staffMember" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } ?>
				</div>
			</div>	
		<form method="post" name="staffProfileForm" id="staffProfileForm" action="<?php echo base_url();?>index.php/Admin/performStaffProfile" enctype="multipart/form-data" >			
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			<div class="row margin-bottom-20">						
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Name</label>
						<div class="controls">
							<input type="text" placeholder="Name" class="m-wrap medium form-control form-control" name="staffMembersName" id="staffMembersName" value="<?php echo $arrAddEditInfo['staffMembersName'];?>"  />									
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Contact Number</label>
						<div class="controls">
							<input type="number" placeholder="Contact Number" class="m-wrap medium form-control" name="staffMembersPhone" id="staffMembersPhone" value="<?php echo $arrAddEditInfo['staffMembersPhone'];?>" onkeypress="return onlyNumbers (event)" />									
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">E-Mail Address</label>
						<div class="controls">
							<input type="text" placeholder="E-Mail Address" class="m-wrap medium form-control" name="staffMembersEmail" id="staffMembersEmail" value="<?php echo $arrAddEditInfo['staffMembersEmail'];?>" />									
						</div>
					</div>
				</div>
			</div>
					
			<div class="row margin-bottom-20">						
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Address Line 1</label>
						<div class="controls">
							<input type="text" placeholder="Address Line 1" class="m-wrap medium form-control" name="staffMembersAddress1" id="staffMembersAddress1" value="<?php echo $arrAddEditInfo['staffMembersAddress1'];?>" />									
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Address Line 2</label>
						<div class="controls">
							<input type="text" placeholder="Address Line 2" class="m-wrap medium form-control" name="staffMembersAddress2" id="staffMembersAddress2" value="<?php echo $arrAddEditInfo['staffMembersAddress2'];?>" />									
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Area / Locality</label>
						<div class="controls">
							<input type="text" placeholder="Area / Locality" class="m-wrap medium form-control" name="staffMembersArea" id="staffMembersArea" value="<?php echo $arrAddEditInfo['staffMembersArea'];?>" />									
						</div>
					</div>
				</div>
			</div>


			<div class="row margin-bottom-20">
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Country</label>
						<div class="controls">
							<select class="medium form-control m-wrap" name="countryId" id="countryId">
								<option value="">Select Country</option>
								<?php foreach($countryValues as $country_value){ ?>
									<option value="<?php echo $country_value['countryId'];?>" <?php if( $country_value['countryId'] == $arrAddEditInfo['countryId'] ) {echo "selected";}?>  ><?php echo $country_value['countryName'];?></option>							
								<?php  } ?>										
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">State</label>
						<div class="controls">
							<select class="medium form-control m-wrap" id="state" name="state" >
								<?php 										
									if(($arrAddEditInfo['StateList']) && !empty($arrAddEditInfo['StateList'])){
										foreach($arrAddEditInfo['StateList'] as $state_value){ ?>
										<option value="<?php echo $state_value['stateId'];?>" <?php if($arrAddEditInfo['stateId'] == $state_value['stateId']){ echo "Selected"; }?>><?php echo $state_value['stateName'];?></option>							
								<?php	}
									}else{
										?>
										<option value="">Select State</option>
									<?php }
								?>		
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">City</label>
						<div class="controls">
							<select class="medium form-control m-wrap" id="city" name="city" >										
								<?php 										
									if(($arrAddEditInfo['CityList']) && !empty($arrAddEditInfo['CityList'])){
										foreach($arrAddEditInfo['CityList'] as $city_value){ ?>
										<option value="<?php echo $city_value['cityId'];?>" <?php if($arrAddEditInfo['cityId'] == $city_value['cityId']){ echo "Selected"; }?>><?php echo $city_value['cityName'];?></option>							
								<?php	}
									}else{
										?>
										<option value="">Select City</option>
									<?php }
								?>											
							</select>
						</div>
					</div>
				</div>
			</div>	


			<div class="row margin-bottom-20">
				<div class="col-sm-12">
					<h3>Joining Details</h3>
				</div>
			</div>

			<div class="row margin-bottom-20">						
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Joining Date</label>
						<div class="controls">
							<input class="m-wrap form-control" size="16" type="text" value="<?php echo $arrAddEditInfo['staffMembersJoiningDate'];?>" id="ui_date_picker" name="staffMembersJoiningDate"/>									
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Reffered By</label>
						<div class="controls">
							<textarea class="large m-wrap form-control" rows="3" name="refferedBy" id="refferedBy" ><?php echo $arrAddEditInfo['refferedBy'];?></textarea>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label">Identity Proof Document</label>
						<div class="controls">
						<?php if($arrAddEditInfo['staffMembersId'] == 0 ){ ?>
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="input-append">
									<div class="uneditable-input">
										<i class="icon-file fileupload-exists"></i> 
										<span class="fileupload-preview"></span>
									</div>
									<div>
									<input type="file" name="proofDocument" class="default form-control" />
									<span class="fileupload-new">Select file</span>
									<span class="fileupload-exists">Change</span>
									</span>
									<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								</div>
							</div>	
						<?php }	elseif($arrAddEditInfo['staffMembersId'] != 0){	
							if($arrAddEditInfo['documentName'] != ""){ 
								$filename = "./uploads/documents/".$arrAddEditInfo['documentName'];
								if(file_exists($filename)){
							?>
								
									<div class="span"><a href="<?php echo base_url();?>/uploads/documents/<?php echo $arrAddEditInfo['documentName'];?>" target="_blank">Preview Document</a></div>	
									<!--<div class="span" style="margin-left: 0px;"><a href="<?php echo base_url();?>index.php/Admin/deleteDocument/<?php echo $arrAddEditInfo['staffMembersId'];?>" class="btn mini red"><i class="icon-trash"></i> Delete Document</a></div>-->
									<div class="span" style="margin-left: 0px;">
										<a href="javascript:void(0);" id="buttonDelteDocument" class="btn mini red">
											<i class="icon-trash"></i> Delete Document
										</a>
									</div>
									<?php	}else{ ?>
									<p>File Not Found on server Please Upload a new file.</p>	
									<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="input-append">
									<div class="uneditable-input">
										<i class="icon-file fileupload-exists"></i> 
										<span class="fileupload-preview"></span>
									</div>
									<div>
									<input type="file" name="proofDocument" class="default form-control" />
									<span class="fileupload-new">Select file</span>
									<span class="fileupload-exists">Change</span>
									</div>
									<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								</div>
							</div>	
							<?php }
								
							}else{
						?>
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="input-append">
									<div class="uneditable-input">
										<i class="icon-file fileupload-exists"></i> 
										<span class="fileupload-preview"></span>
									</div>
									<div>
									<input type="file" name="proofDocument" class="default form-control" />
									<span class="fileupload-new">Select file</span>
									<span class="fileupload-exists">Change</span>
									</div>
									<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								</div>
							</div>		
						<?php 	}	?>
							
						<?php }	?>									
						</div>
					</div>
				</div>	
			</div>
			<div class="clearfix"></div>
			<div class="row margin-bottom-20">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-4">
					<button type="submit" class="btn btn-primary btn-lg btn-block" id="submitstaffProfileForm">Submit Details</button>
				</div>
				<div class="col-sm-4">
				</div>
			</div>	
			<input type="hidden" name="staffMembersId" id="staffMembersId" value="<?php echo $arrAddEditInfo['staffMembersId'];?>" />
			<input type="hidden" name="hidDocumentName" id="hidDocumentName" value="<?php echo $arrAddEditInfo['documentName'];?>" />
			</form>				
				</div>				
			</div>			
		</div>	
	</div>
</div>
<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title">
							<strong>Manage Country Infromation</strong>
							<a href="<?php echo base_url();?>index.php/Admin/countryInfoDetails" class="btn btn-primary pull-right" >Add</a>
						</h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">
					<div class="col-sm-12">					
						<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/countryInfo" class="countryinfo" >
								<div class="form-group">								 							
									<label class="label-from-to">Country</label>								
									<select class="medium m-wrap form-control" name="countryID" id="countryID">
										<option value="">Please Select Country</option>										
										<?php foreach($countryValues as $value) { ?>
										<option value="<?php echo $value['countryId'];?>" <?php if($countryID == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
										<?php } ?>
									</select>
									<button type="submit" class="btn btn-primary btn-md"><i class="icon-ok"></i> Go</button>
								</div>
						</form>	
						<div class="portlet box">
							<div class="portlet-title">										
							</div>
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Country</th>
											<th>Country Information</th>											
											<th>Action</th>																						 											
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($countryData as $value){?>
										<tr>
											<td><?php echo $value['countryName'];?></td>											
											<td><?php print htmlentities($value['countryDetails'],ENT_QUOTES,"UTF-8");?></td>																						
											<td><a href="<?php echo base_url();?>index.php/Admin/countryInfoDetails/<?php echo $value['countryInformationID'];?>">Edit</a></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
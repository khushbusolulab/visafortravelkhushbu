<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.validate.min.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?php echo base_url();?>assests/js/admin/select2.min.js"></script>     	
<script src="<?php echo base_url();?>assests/js/admin/app.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/login.js" type="text/javascript"></script> 	
<script>
	jQuery(document).ready(function() {     
	  App.init();
	  Login.init();		 	
	});
</script>	
</body>
</html>
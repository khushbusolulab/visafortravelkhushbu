<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html>
<head>
	<meta charset="utf-8">
	<title>My Visa4 Travel</title>	
	<link href="<?php echo base_url()."assests/css/bootstrap.min.css";?>" rel="stylesheet">	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>	
	<script src="<?php echo base_url()."assests/js/jquery.js";?>" type="text/javascript"></script>
	<script src="<?php echo base_url()."assests/js/jquery.validate.js";?>" type="text/javascript"></script>		
	<script src="<?php echo base_url()."assests/js/bootstrap.min.js";?>"></script>	
</head>
<body class="gray_bg">    
	<div class="container">
		<div class="row" style="margin-top:20px">
			<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form name='resetPasswordForm' id='resetPasswordForm' method='post' action ='<?php echo site_url()."User/doResetPassword";?>'>
				<input type='hidden' name='ngoid' id='ngoid' value='<?php echo $id;?>'>
					<div class="login-wrap">
						<h2>Reset Password</h2>                
						<div class="form">																
							<input type="password" placeholder="New Password" name='password' id='password' class="validate[required,minSize[6]]" />							
							<input type="password" placeholder="Confirm Password" name='cnfpassword' id='cnfpassword' class="validate[required,minSize[6],equals[password]]" />							
							<input type="button" name='resetPassword' id="resetPassword" value='Reset Password' class="btn button">							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>    
</body> 
<script type="text/javascript">
	$( document ).ready(function() {
		$("#resetPassword").on("click",function(){
			var password = $("#password").val(); 			
			var cnfpassword = $("#cnfpassword").val(); 
			if(password == ""){
				alert("Please Enter New password");
			}else if(cnfpassword == ""){
				alert("Please Enter New password");
			}
			
		});
	});
</script>
</html>
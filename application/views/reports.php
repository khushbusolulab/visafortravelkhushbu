<body>
<?php  include("menu.php"); ?>
<!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
	
    <div class="container min-hight">
	    <div class="row">
			<div class="col-sm-12">
				    <h3 class="page-title">Reports</h3>
			</div>
	    </div>
		
	
	
		<div class="row">
			 
<div class="bootstrap-iso">
 <div class=""> 
   <div class="col-sm-12">
    <form action="<?php echo base_url();?>index.php/Home/reports" class="" method="post">
	<?php  if(isset($packageSelection) && $packageSelection != ""){	?>
		<input type="hidden" name="hidpackageSelection" id="hidpackageSelection" value="<?php echo $packageSelection; ?>">
		<?php } ?>	
     <div class="form-group pull-left">      
      </label>
      <div class="">
       <div class="input-group">
        <!--<div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>-->
		<label>From Date</label>
        <input class="form-control" id="fromDate" name="fromDate"   type="text" <?php if(isset($fromDate) && $fromDate != "" ) {echo "value='".$fromDate."'";} ?>/>
		<label>To Date</label>
        <input class="form-control" id="toDate" name="toDate" type="text" <?php if(isset($toDate) && $toDate != "" ) {echo "value='".$toDate."'";} ?> />
	       <button class="btn btn-info" name="submit" type="submit">
			Submit
		       </button>
       </div>
      </div>    
	  </div>
    </form>
    <form name="reportSelectorForm" id="reportSelectorForm" method="post" action="<?php echo base_url();?>index.php/Home/reports" >
			<div class="pull-right mar-bot10">
				<?php if(!empty($data)) { ?>
				<label>Select package to view report:</label>
				<select name="packageSelection" id="packageSelection">
					<option value="">Please Select Package</option>
					<?php foreach($data as $value){?>
						<option value="<?php echo $value['packageName'];?>" <?php if(isset($packageSelection) && $packageSelection != ""){ if($value['packageName'] == $packageSelection ) {echo " selected ";}} ?>  ><?php echo $value['packageName'];?> Package</option>
					<?php } ?>
				</select>
				<?php } else{ ?>
					<label><a href="<?php echo base_url();?>index.php/Home/subscription">Purchase package</a> to view report.</label>
				<?php } ?>
			</div>
			<?php if(!empty($packageWiseData)) { ?>
			</form>
	    </div>
   </div>
	    
</div>
		</div>
		<div class="clearfix"></div>
	
		<div class="row">					
				<div class="">
					<div class="col-sm-12">					
					 
						<div class="portlet box ">							 
							<div class="portlet-body">								 
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Sr.No</th>										
											<th>Date</th>										
											<th>Clicks</th>
											<th>Bookmark</th>											 
											<th>Reviewed</th>											 
										</tr>
									</thead>
									<tbody>
									<?php  
									$i = 1;
									foreach($packageWiseData as $value) { 
									?>
										<tr>
											<td><?php echo $i;?></td>											
											<td><?php echo $value['currentLoopDate'];?></td>											
											<td><a href="javascript:void(0);" class="clicks" data-eleDate = "<?php echo $value['dateYMDFormat'];?>" ><?php echo $value['clicks'];?></a></td>
											<td><a href="javascript:void(0);" class="bookmarks" data-eleDate = "<?php echo $value['dateYMDFormat'];?>" ><?php echo $value['bookMarks'];?></a></td>											
											<td><a href="javascript:void(0);" class="reviews" data-eleDate = "<?php echo $value['dateYMDFormat'];?>" ><?php echo $value['reviews'];?></a></td>											
										</tr>
									<?php 
										$i++;
									} ?>	 							 
									</tbody>
								</table>
							</div>
						</div>
				
					</div>
				</div> 
		</div>
					<?php }?>						
    </div>

<!-- Define content Clicks-->	
	<div id="clicksContent" class="popover container">    
		<div id="loaderDivClicks">
			<img src="<?php echo base_url();?>assests\img\loader.gif">
		</div>			
		<ul class="nav nav-pills mar-bot10" role="tablist">
			<li role="presentation" class="active"><a href="#countryClicks" aria-controls="countryClicks" role="tab" data-toggle="tab" class="btn-sm">Country</a></li>
			<li role="presentation"><a href="#cityClicks" aria-controls="cityClicks" role="tab" data-toggle="tab" class="btn-sm">City</a></li>    
		</ul>		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="countryClicks">				
			</div>
			<div role="tabpanel" class="tab-pane" id="cityClicks">				
			</div>       
		</div>
		<div class="clearfix"></div>
	</div>
	
	<!-- Define content Bookmarks-->	
	<div id="bookmarksContent" class="popover container"> 
		<div id="loaderDivBookmarks">
			<img src="<?php echo base_url();?>assests\img\loader.gif">
		</div>
		<ul class="nav nav-pills mar-bot10" role="tablist">
			<li role="presentation" class="active"><a href="#countrybookmark" aria-controls="countrybookmark" role="tab" data-toggle="tab" class="btn-sm">Country</a></li>
			<li role="presentation"><a href="#citybookmark" aria-controls="citybookmark" role="tab" data-toggle="tab" class="btn-sm">City</a></li>    
		</ul>		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="countrybookmark">				
			</div>
			<div role="tabpanel" class="tab-pane" id="citybookmark">				
			</div>       
		</div>
		<div class="clearfix"></div>
	</div>
	
	
	<!-- Define content Reviews-->	
	<div id="reviewsContent" class="popover container"> 
		<div id="loaderDivReviews">
			<img src="<?php echo base_url();?>assests\img\loader.gif">
		</div>
		<ul class="nav nav-pills mar-bot10" role="tablist">
			<li role="presentation" class="active"><a href="#countryreviwews" aria-controls="countryreviwews" role="tab" data-toggle="tab" class="btn-sm">Country</a></li>
			<li role="presentation"><a href="#cityreviews" aria-controls="cityreviews" role="tab" data-toggle="tab" class="btn-sm">City</a></li>    
		</ul>		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="countryreviwews">				
			</div>
			<div role="tabpanel" class="tab-pane" id="cityreviews">				
			</div>       
		</div>
		<div class="clearfix"></div>
	</div>  
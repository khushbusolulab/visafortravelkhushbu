 <!-- END CONTAINER --> 
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/bootstrap-datepicker.min.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/jquery.dataTables.js" type="text/javascript"></script>    
    <!--<script src="<?php echo base_url();?>assests/js/select2.min.js" type="text/javascript"></script>    -->
    <script src="<?php echo base_url();?>assests/js/DT_bootstrap.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/table-editable.js" type="text/javascript"></script>    
    <!--<script src="<?php echo base_url();?>assests/js/bootstrap-modal-popover.js" type="text/javascript"></script>    -->
    
    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>         
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script>      
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
			TableEditable.init();

			
			$('#topProfileButton').on('click', function(){	
				$.ajax({
					url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
					success: function(response){	
						if(response == 1){
							window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
						}else if(response == 0){
							window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
						}						
					}
				});	
			});
			
			
		var date_input=$('input[id="fromDate"]'); 
		var date_input1=$('input[id="toDate"]'); 
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({		
			format: 'yyyy/mm/dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
			endDate: '+0d'
		})
		
		date_input1.datepicker({			
			format: 'yyyy/mm/dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
			endDate: '+0d'
		})			
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
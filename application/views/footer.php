<footer id="footer">
    <div class="empty-space-70"></div>
    <div class="container">
      <div class="go-to-top">
        <a href="#page-top" class='smooth'><img src="<?php echo base_url();?>assests/img/go-to-top.png"></a>
      </div>
      <div class="row">
        <div class="col-md-2 col-sm-2">
          <div class="footer-logo">
            <a href="#"><img class="img-responsive" src='<?php echo base_url();?>assests/img/footer-logo.png'></a>
          </div>
        </div>
        <div class="col-md-10 col-sm-10">
          <div class="social-media">
            <ul class="p-0 m-0 ">
              <li><a target="_blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a target="_blank" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a target="_blank" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              <li><a target="_blank" href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
              <li><a target="_blank" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-middle-path">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <div class="footer-nav">
              <div class="top-title">
                <h2 class="font-24 font-w-bold color-white text-upper m-b-15">Quick Links</h2>
                <div class="top-title-line left"></div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <ul class="p-0 m-0">
                    <li><a href='<?php echo base_url();?>index.php/Home/about' class=''>About Us</a></li>
                    <li><a href='<?php echo base_url();?>index.php/Home/advertise' class=''>Advertise With Us</a></li>
                    <li><a href='<?php echo base_url();?>index.php/Home#mobileapp' class=''>Mobile App</a></li>
                  </ul>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <ul class="p-0 m-0">
                    <li><a href='<?php echo base_url();?>index.php/Home/career' class=''>Career</a></li>
                    <li><a href='<?php echo base_url();?>index.php/Home/faq' class=''>FAQ</a></li>
                    <li><a href='<?php echo base_url();?>index.php/Home/privacy' class=''>Terms & Privacy</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="reach-at">
              <div class="top-title">
                <h2 class="font-24 font-w-bold color-white text-upper m-b-15">Reach Us at</h2>
                <div class="top-title-line left"></div>
              </div>
              <div class="reachus">
		<ul>
		    <li><i class="fa fa-map-marker"></i> 75 N. Bohemia Drive Yorktown, VA 23693</li>
		    <li><i class="fa fa-envelope"></i> info@myvisa4travel.com</li>
		    <li><i class="fa fa-mobile"></i> +1 321 654 3210</li>
		</ul>
	      </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <p class="text-center font-14 m-0">Copyright &copy; 2017 My Visa4 Travel. All Rights Reserved</p>
      </div>
    </div>
  </footer>
  <!--  /footer  -->
	<script type="text/javascript">
		var base_url = "<?php echo base_url();?>";
	</script>		
  <script src="<?php echo base_url();?>assests/js/jquery.js"></script>
  <script src="<?php echo base_url();?>assests/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assests/js/bootstrap-dialog.min.js"></script>
  <script src="<?php echo base_url();?>assests/js/jquery.validate.js"></script>
  <script src="<?php echo base_url();?>assests/js/customJs.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?php echo base_url();?>assests/js/owl.carousel.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.website.min.js"></script>
  


  <script type="text/javascript">

    $(function() {
      $('a.smooth[href*="#"]:not([href="#"])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
        }
      });
    });
  
       $(document).ready(function() {

        $(".testimonials-1").owlCarousel({
          autoPlay: 3000, 
          navigation : true , // Show next and prev buttons
          slideSpeed : 400,
          paginationSpeed : 400,
          singleItem:true
        });
        
        $('.carousel').carousel({
              interval: 2000
            })
      });
  </script>
  <?php	if($msg == "SUCC"){		?>
	<script type="text/javascript">	
		showDialog("You have been succesfully registered with us.");		    
	</script>        
<?php }elseif($msg == "ERR") { ?>
	<script type="text/javascript">	
		showDialog("Some error occured please try again.");		    		    
	</script>
<?php }elseif($msg == "EMAILEX") { ?>		
	<script type="text/javascript">	
		showDialog("Email Address Already Exists.PLease try again.");		    		    
	</script>
<?php }elseif($msg == "EMAILSNT") { ?>		
<script type="text/javascript">	
		showDialog("Reset Password link has been send to your email address.");		    		    
	</script>
	<?php }elseif($msg == "EMAILNF") { ?>		
<script type="text/javascript">	
		showDialog("Email is not registered with our system.");		    		    
	</script>	
	<?php }elseif($msg == "NTLGD") { ?>		
<script type="text/javascript">	
		showDialog("PLease Login First To view this page");		    		    
	</script>
	<?php }elseif($msg == "INVLD") { ?>		
<script type="text/javascript">	
		showDialog("Invalid Login Credentials");		    		    
	</script>	
	<?php }elseif($msg == "BLOCKED") { ?>		
<script type="text/javascript">	
		showDialog("Your account has been blocked.Please contact administrator.");		    		    
	</script>
	<?php } ?>
</body>
</html>
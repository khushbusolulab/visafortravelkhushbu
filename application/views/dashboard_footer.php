<script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
<script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>  
<script src="<?php echo base_url();?>assests/js/bootstrap-dialog.min.js"></script>       
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
<script src="<?php echo base_url();?>assests/js/app.js"></script>      
<script type="text/javascript">
	jQuery(document).ready(function() {
		App.init();
		
		$('#topProfileButton').on('click', function(){	
			$.ajax({
				url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
				success: function(response){	
					if(response == 1){
						window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
					}else if(response == 0){
						window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
					}						
				}
			});	
		});
	});

</script>
	<script type="text/javascript">
		function showDialog(strMessage){
			BootstrapDialog.show({
				title: 'My Visa For Travel',
				message: strMessage
			});	
		}
	</script>
    <!-- END JAVASCRIPTS -->
	
	<?php if($msg == "Succ"){		?>
	<script type="text/javascript">	
		showDialog("Package Has been purchased succesfully");		    
	</script>        
<?php } ?>
</body>
<!-- END BODY -->
</html>
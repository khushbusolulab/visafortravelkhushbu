<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Metronic Frotnend | Prices</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="<?php echo base_url();?>assests/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/reset.css" rel="stylesheet" type="text/css"/>
    <!--<link href="<?php echo base_url();?>assests/css/style-metro.css" rel="stylesheet" type="text/css"/>-->
    <link href="<?php echo base_url();?>assests/css/style_dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assests/css/jquery.fancybox.css">               
    <link href="<?php echo base_url();?>assests/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url();?>assests/css/prices.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/DT_bootstrap.css" rel="stylesheet" type="text/css"/>
    
    <!-- END PAGE LEVEL STYLES -->    
    <link href="<?php echo base_url();?>assests/css/blue.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo base_url();?>assests/css/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico" />
</head>
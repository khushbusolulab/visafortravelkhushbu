<!DOCTYPE html>
<html lang="en">

<head>
	<!-- All the files that are required -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

<style type="text/css">
.btn-primary {
    color: #fff;
    background-color: #558ED5;
    border-color: #558ED5;
}
.btn-primary:hover {
    color: #fff;
    background-color: #497DBF;
    border-color: #497DBF;
}
</style>

</head>

<body>
<hr>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          <h3><img src="<?php echo base_url();?>images/logo.png"></h3>
                          <h2 class="text-center">Change your Password</h2>
                          <p>Password must be at least 6 characters.</p>
                            <div class="panel-body">
                              
                              <form class="form" action="<?php echo base_url() ?>index.php/Api/setNewPassword" method="post">
                                <fieldset>
                                  <div class="form-group">
                                    <div class="input-group" style="margin-bottom:10px;">
                                      <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                      
										<input class="form-control" placeholder="New Password" type="password" id="password" name="password" >
										<span id="pwdErr"></span>
                                    </div>

                                      <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                      <input class="form-control" placeholder="Confirm New Pasword" type="password" id="cnfpassword" name="cnfpassword" >
										<span id="cnfpwdErr">

                                    </div>
                                  </div>
                                  <div class="form-group">
									<input type="hidden" name="id" value="<?php echo $userId; ?>">
									<input type="hidden" name="token" value="<?php echo $token; ?>">
                                    <input class="btn btn-lg btn-primary btn-block" onclick="return checkPassword();" value="Submit" type="submit">
                                  </div>
                                </fieldset>
                              </form>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script>

    function checkPassword()
    {
        document.getElementById("cnfpwdErr").textContent = "";
        document.getElementById("pwdErr").textContent = "";
        var pwd = document.getElementById("password").value;
        var cnfpwd = document.getElementById("cnfpassword").value;
        var len = document.getElementById("password").value.length;
        var lencnf = document.getElementById("cnfpassword").value.length;
        if (pwd != cnfpwd) {
            document.getElementById("cnfpwdErr").textContent = "Password does not match";
            return false;
        } else if (pwd == "") {
            document.getElementById("pwdErr").textContent = "Please Enter Password";
            return false;
        } else if (cnfpwd == "") {
            document.getElementById("cnfpwdErr").textContent = "Please Confirm Password";
            return false;
        } else if (len <= 6) {
            document.getElementById("pwdErr").textContent = "Length must be more then 6 character";
            return false;
        } else if (lencnf <= 6) {
            document.getElementById("cnfpwdErr").textContent = "Length must be more then 6 character";
            return false;
        } else {
            document.getElementById("pwdErr").textContent = "";
            document.getElementById("cnfpwdErr").textContent = "";
            return true;
        }
    }
</script>

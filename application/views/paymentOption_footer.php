	
	
	
    <!-- END CONTAINER --> 
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>
	<script src="<?php echo base_url();?>assests/js/jquery.validate.js"></script>	
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script> 
	
    <script type="text/javascript">
        jQuery(document).ready(function() {

		var $form = $('#payment-form');
		$form.submit(function(event) {
			// Disable the submit button to prevent repeated clicks:
			$form.find('.submit').prop('disabled', true);

			// Request a token from Stripe:
			Stripe.card.createToken($form, stripeResponseHandler);

			// Prevent the form from being submitted:
			return false;
		});
		
			
			$('#topProfileButton').on('click', function(){	
				$.ajax({
					url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
					success: function(response){	
						if(response == 1){
							window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
						}else if(response == 0){
							window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
						}						
					}
				});	
			});
			
			$("#currencyDropdown").on("change",function(){
				var valueDropDownCurrency = $(this).val();				
				if(valueDropDownCurrency == "NOTIndia"){
					 // $('#payNowButton').prop('disabled', true);
					// $("#bypassPayment").submit();
					$("#currencyToShow").html("$");
				}else if (valueDropDownCurrency == "India"){
					$("#currencyToShow").html("INR");
				}
			});
			
			
	$.validator.addMethod('CCExp', function(value, element, params) {
		var minMonth = new Date().getMonth() + 1;
		var minYear = new Date().getFullYear();
		var month = parseInt($(params.month).val(), 10);
		var year = parseInt($(params.year).val(), 10);
		return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');
			
          App.init();
			$(document.body).delegate('.dropdownForMonthsSelection', 'change', function (e) {
				var monthsSelected = $(this).val();
				var monthlyAmount = $(this).attr("data-monthlyAmount");
				var subAmountToShow 	= parseFloat(parseFloat(monthsSelected) * parseFloat(monthlyAmount)).toFixed(2);				
				//$(this).parent().find("#spanTotAmount").html(subAmountToShow);
				$("#spanTotAmount").html(subAmountToShow);
				var objAllPackageSelect = $('.dropdownForMonthsSelection');
				
				var totalBillAmountUpdated = 0.00;
				$.each(objAllPackageSelect, function(index, selectEle){
					var monthlyAmount = $(selectEle).attr('data-monthlyAmount');
					var monthSelected = $(selectEle).val();
					console.log(monthSelected);
					
					totalBillAmountUpdated = parseFloat(parseFloat(totalBillAmountUpdated) + (parseFloat(monthlyAmount) * parseFloat(monthSelected))).toFixed(2);
					
				});				
				$('#totalBillAmount').html(totalBillAmountUpdated);
				$('#totalBillAmount1').html(totalBillAmountUpdated);
				$("#totAmountPayable").val(totalBillAmountUpdated);				
				
			});        			
			
			
			
			// $("#payNowButton").on("click",function(){				
				// $("#paymentForm").show();				
			// });
			
			
		$("#payment-form").validate({	 
		rules: {			
				cardNumber: {
					required: true,
					creditcard: true
				},
				 exp_month: {
					 required: true,
                  CCExp: {
                        month: '#exp_month',
                        year: '#exp_year'
                  }
            },
			exp_year: {
					 required: true,
                  CCExp: {
                        month: '#exp_month',
                        year: '#exp_year'
                  }
            },
			cvc: {
					required: true,
					maxlength: 3,
					number: true
				},			
		},
		messages: {				
			cardNumber: {
				required: "Please provide a valid card number",
				creditcard: "PLease enter a valid credit card number"
			},
			cardNumber: {
				required: "Please provide CVC number",
				maxlength: "Maximum Lenght 3",
				number: "PLease enter number only"
			}				
		}
	});
        });
		
	function stripeResponseHandler(status, response) {
  // Grab the form:
  var $form = $('#payment-form');

  if (response.error) { // Problem!

    // Show the errors on the form:
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false); // Re-enable submission

  } else { // Token was created!

    // Get the token ID:
    var token = response.id;

    // Insert the token ID into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripeToken">').val(token));

    // Submit the form:
    $form.get(0).submit();
  }
}
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
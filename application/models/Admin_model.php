<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model
{
	public function checkUserSession(){
		//echo "<pre>";print_r($_SESSION);exit;
		if(isset($_SESSION['adminId'])){
			$adminId = $_SESSION['adminId'];		
		}else{
			$adminId = 0;			
		}
		if(isset($_SESSION['AdminLoggedIn'])){
			$AdminLoggedIn = $_SESSION['AdminLoggedIn'];		
		}else{
			$AdminLoggedIn = 0;			
		}	
		if(isset($adminId) && is_numeric($adminId) && $adminId> 0 && isset($AdminLoggedIn) && $AdminLoggedIn == 1){
			return "logeedIn";
		}
		return "NotLogged";	
	}
	
	
	public function checkStaffAccess() {
		if(isset($_SESSION['LoggedInType']) && $_SESSION['LoggedInType'] == "Staff"){
			redirect(base_url()."index.php/Admin/busniessUsers/NOACCESS");
		}
	}
	
	public function doLogin($emailAddress,$password){		
		if($emailAddress != "" && $password != ""){
			$this->db->select('*');
			$this->db->from('admin');
			$this->db->where('email', $emailAddress);		
			$this->db->where('password', $password);				
			$query = $this->db->get();
			$resLogin = $query->result_array();				
			if(!empty($resLogin)){			
				$resLogin = $resLogin[0];
				$this->session->set_userdata(array(
					'adminId'       				=> $resLogin['id'],				
					'adminEmail'       				=> $resLogin['email'],								
					'AdminLoggedIn'       			=> TRUE,
					'LoggedInType'       			=> "Admin"
				));				
				return "1";
			}else{						
				$this->db->select('*');
				$this->db->from('staffMembers');
				$this->db->where('staffMembersEmail', $emailAddress);		
				$this->db->where('staffMembersPassword', $password);				
				$query_staffMembers = $this->db->get();
				$resLogin_staffMembers = $query_staffMembers->result_array();	
				if(!empty($resLogin_staffMembers)){
					if($resLogin_staffMembers[0]['isBlocked'] == 1){
						return "2";
					}else{
						$resLogin_staffMembers = $resLogin_staffMembers[0];
						$this->session->set_userdata(array(
							'adminId'       				=> $resLogin_staffMembers['staffMembersId'],				
							'adminEmail'       				=> $resLogin_staffMembers['staffMembersEmail'],								
							'AdminLoggedIn'       			=> TRUE,
							'LoggedInType'       			=> "Staff"
						));				
						return "1";
					}
				}else{
					return "0";
				}
				//Code to check from Sales Person Table Will come here
				return "0";
			}
		}
	}
	
	public function getAdminDetailsFromEmail($email){
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('email', $email);				
		$query = $this->db->get();
		$getAdminDetailsFromEmail = $query->result_array();
		if(!empty($getAdminDetailsFromEmail)){
			$getAdminDetailsFromEmail = $getAdminDetailsFromEmail[0];
			return $getAdminDetailsFromEmail;
		}else{
			return false;
		}
	}
	
	public function getDashboardData(){
		$arrReturn = array();
		
		
		//Code for total Users
		$this->db->select('count(*) as totUsers');
		$this->db->from('users');				
		$query_users = $this->db->get();
		$res_users = $query_users->result_array();
		if(!empty($res_users)){
			$arrReturn['totUsers'] = $res_users[0]['totUsers'];			
		}
		
		
		//Code for total Business
		$this->db->select('count(*) as totBusinesses');
		$this->db->from('businesses');				
		$query_businesses = $this->db->get();
		$res_businesses = $query_businesses->result_array();
		if(!empty($res_businesses)){
			$arrReturn['totBusinesses'] = $res_businesses[0]['totBusinesses'];			
		}
		
		//Code for total Clicks
		$this->db->select('count(*) as totClicks');
		$this->db->from('clicks');				
		$query_clicks = $this->db->get();
		$res_clicks = $query_clicks->result_array();
		if(!empty($res_clicks)){
			$arrReturn['totClicks'] = $res_clicks[0]['totClicks'];			
		}
		
		
		//Query for Users Across Globe
		$this->db->select('countryId,countryName');
		$this->db->from('country');				
		$query_country = $this->db->get();
		$res_country = $query_country->result_array();
		if(!empty($res_country)){
			$arrReturn['countries'] = $res_country;					
		}
		
		
		$tempUserArray = array();
		$this->db->select('countryId,count(users.userId) as totUsers');
		$this->db->from('users');									
		$this->db->group_by("users.countryId");
		$query_groupby_user = $this->db->get();		
		$res_groupby_user = $query_groupby_user->result_array();					
		if(!empty($res_groupby_user)){
			foreach($res_groupby_user as $value){
				$tempUserArray[$value['countryId']] = $value['totUsers'];
			}
		}
		$arrReturn['usersCount'] = $tempUserArray;
		
		$tempBusinessArray = array();		
		$this->db->select('businesses.countryId,count( distinct businesses.businessId) as totBusiness');
		$this->db->from('businesses');
		$this->db->join('packagesOfBusiness', 'businesses.businessId = packagesOfBusiness.businessId',"left");
		$this->db->where('packagesOfBusiness.packageId <> ', 4);			
		$this->db->group_by("businesses.countryId");
		$query_groupby_business = $this->db->get();
		$res_groupby_business = $query_groupby_business->result_array();		
		if(!empty($res_groupby_business)){
			foreach($res_groupby_business as $value){
				if($value['countryId'] != 0){
					$tempBusinessArray[$value['countryId']] = $value['totBusiness'];	
				}				
			}
		}
		$arrReturn['businessCount'] = $tempBusinessArray;		
		return $arrReturn;
	}
	

	public function getappUsersData($countryID = ""){
		$arrReturn = array();
 
		$this->db->select('
							u.userId,
							u.fullName,							
							u.emailId,
							c.countryName,
							s.stateName,
							ci.cityName,
							u.isBlocked
						');
		$this->db->from('users u');
		$this->db->join('country c', 'u.countryId = c.countryId',"left");
		$this->db->join('states s', 'u.stateId = s.stateId',"left");		
		$this->db->join('city ci', 'u.cityId = ci.cityId',"left");		
		if(isset($countryID) && $countryID != "" && $countryID > 0){
		$this->db->where('u.countryId', $countryID);				
		}
		$query_users = $this->db->get();
		$res_users = $query_users->result_array();			
	 
		if(!empty($res_users)){
			foreach($res_users as $value){
				$this->db->select('DATE_FORMAT(createdDate,"%D %M %Y At %l:%i %p") as createdDate');
				$this->db->from('tokenaccess');	
				$this->db->where('userId', $value['userId']);					
				$this->db->order_by("tokenId", "desc"); 	
				$this->db->limit(1);	
				$query_tokenaccess = $this->db->get();		
				$res_tokenaccess = $query_tokenaccess->result_array();
				if(!empty($res_tokenaccess)){
					$value['createdDate'] = $res_tokenaccess[0]['createdDate'];
				}else{
					$value['createdDate'] = "";
				}
				array_push($arrReturn,$value);
			}				
		}		
		return $arrReturn;
	}	
	
	public function getcountryValues(){
		$arrReturn = array();
		$this->db->select('
							countryId,
							countryName
						');
		$this->db->from('country');		
		$query_country = $this->db->get();
		$res__country = $query_country->result_array();
		if(!empty($res__country)){
			$arrReturn = $res__country;			
		}
		return $arrReturn;
	}
	
	
	public function getUserDataFromID($userId){
		$arrReturn = array();
		$this->db->select('
							u.*,
							c.countryName,
							s.stateName,
							ci.cityName,
							DATE_FORMAT(t.createdDate,"%D %M %Y At %l:%i %p") as createdDate
						');
		$this->db->from('users u');
		$this->db->join('country c', 'u.countryId = c.countryId',"left");
		$this->db->join('states s', 'u.stateId = s.stateId',"left");		
		$this->db->join('city ci', 'u.cityId = ci.cityId',"left");
		$this->db->join('tokenaccess t', 'u.userId = t.userId',"left");		
		$this->db->where('u.userId', $userId);
		$query_users = $this->db->get();
		$res_users = $query_users->result_array();			
		if(!empty($res_users)){
			$arrReturn['userData'] = $res_users[0];
		}else{
			$arrReturn['userData'] = array();
		}
		
		
		$arrReturn['reviewsData'] = array();
		$this->db->select('r.*,DATE_FORMAT(r.reviewDateTime,"%D %M %Y At %l:%i %p") as reviewDateTime,b.businessName');
		$this->db->from('review r');
		$this->db->join('businesses	b', 'r.businessId = b.businessId',"left");				
		$this->db->where('r.userId', $userId);		
		$query_review = $this->db->get();
		$res_review = $query_review->result_array();
		if(!empty($res_review)){
			foreach($res_review as $value){
				$this->db->select('images');
				$this->db->from('businessesImage');				
				$this->db->where('businessId', $value['businessId']);		
				$this->db->where('imageType', "Office");		
				$query_review_img = $this->db->get();
				$res_review_img = $query_review_img->result_array();
				if(!empty($res_review_img)){
					$value['images'] = $res_review_img[0]['images'];
				}else{
					$value['images'] = "";
				}
				array_push($arrReturn['reviewsData'],$value);
			}			 
		}else{
			$arrReturn['reviewsData'] = array();
		}
		
		
		
		$arrReturn['bookMarkData'] = array();
		$this->db->select('bk.*,DATE_FORMAT(bk.bookmarkedDate,"%D %M %Y At %l:%i %p") as bookmarkedDate,b.businessName,b.avgRating as rating,b.businessAddress1,b.businessAddress2,b.businessArea');
		$this->db->from('bookmarks bk');
		$this->db->join('businesses	b', 'bk.businessId = b.businessId',"left");				
		$this->db->where('bk.userId', $userId);		
		$query_bookmarks = $this->db->get();
		$res_bookmarks = $query_bookmarks->result_array();
		if(!empty($res_bookmarks)){
			foreach($res_bookmarks as $value){
				$this->db->select('images');
				$this->db->from('businessesImage');				
				$this->db->where('businessId', $value['businessId']);		
				$this->db->where('imageType', "Office");		
				$query_review_img = $this->db->get();
				$res_review_img = $query_review_img->result_array();
				if(!empty($res_review_img)){
					$value['images'] = $res_review_img[0]['images'];
				}else{
					$value['images'] = "";
				}
				array_push($arrReturn['bookMarkData'],$value);
			}			 
		}else{
			$arrReturn['bookMarkData'] = array();
		}	 
		return $arrReturn;
	}
	
	public function blockUser($userId){		
		$data = array(	'isBlocked' 	=> 1	);
		$this->db->where('userId', $userId);
		if($this->db->update('users', $data)){
			return "BLOCKED";
		}else{
			return "NTBLOCKED";
		} 		
	}
	
	
	
	public function blockStaff($staffMembersId){		
		$data = array(	'isBlocked' 	=> 1	);
		$this->db->where('staffMembersId', $staffMembersId);
		if($this->db->update('staffMembers', $data)){
			return "BLOCKED";
		}else{
			return "NTBLOCKED";
		} 		
	}
	
	
	
	public function unblockUser($userId){		
		$data = array(	'isBlocked' 	=> 0	);
		$this->db->where('userId', $userId);
		if($this->db->update('users', $data)){
			return "UNBLOCKED";
		}else{
			return "NTUNBLOCKED";
		} 		
	}
	
	
	
	public function unblockStaff($staffMembersId){		
		$data = array(	'isBlocked' 	=> 0	);
		$this->db->where('staffMembersId', $staffMembersId);
		if($this->db->update('staffMembers', $data)){
			return "UNBLOCKED";
		}else{
			return "NTUNBLOCKED";
		} 		
	}
	
	
		public function getbusniessUsersCount(){
			$totBusiness = 0;
			$this->db->select('count(*) as totBusiness');
			$this->db->from('businesses');				
			$query_businesses = $this->db->get();
			$res_businesses = $query_businesses->result_array();
			//echo "<pre>";print_r($res_businesses);exit;
			if(!empty($res_businesses)){
				$totBusiness = $res_businesses[0]['totBusiness'];	
			}
			return $totBusiness;
		}
		public function getbusniessUsersData($countryID = "", $cityId = ""){
		$arrReturn = array();
		
		$this->db->select('
							b.*,
							c.countryName,
							DATE_FORMAT(pb.packagePurchaseDate,"%D %M %Y") as packagePurchaseDate,							
							DATE_FORMAT(pb.packageExpiryDate,"%D %M %Y") as packageExpiryDate,														
							p.packageName,
							ct.cityName
							');
		$this->db->from('businesses b');
		$this->db->join('country c', 'b.countryId = c.countryId',"left");		
		$this->db->join('city ct', 'b.cityId = ct.cityId',"left");		
		$this->db->join('packagesOfBusiness pb', 'b.businessId = pb.businessId',"left");		
		$this->db->join('packages p', 'pb.packageId = p.packageId',"left");		
		if(isset($cityId) && $cityId != "" && $cityId > 0){
			$this->db->where('b.cityId', $cityId);				
		}
		if(isset($countryID) && $countryID != "" && $countryID > 0){
			$this->db->where('b.countryId', $countryID);				
		}
		$this->db->where('pb.packageId <> ', "4");				
		$query_businesses = $this->db->get();
		$res_businesses = $query_businesses->result_array();		
		//echo "<pre>";print_r($res_businesses);exit;
		if(!empty($res_businesses)){
			foreach($res_businesses as $value){
				
				$strCategories = "";	
				$this->db->select('c.categoryName');
				$this->db->from('businessCategory bc');
				$this->db->join('category c', 'bc.categoryId = c.categoryId',"left");						
				$this->db->where('bc.businessId', $value['businessId']);						
				$query_category = $this->db->get();
				$res_category = $query_category->result_array();
				if(!empty($res_category)){
					foreach($res_category as $value_category){
						$strCategories .= $value_category['categoryName'].",";
						$strCategories = rtrim($strCategories, ", ");
					}
				}
				$value['strCategories'] = $strCategories;
				
				
				if($value['createdBy'] == "Admin"){
					$value['addedBy'] = "Admin";
				}elseif($value['createdBy'] == "Staff"){					
					$this->db->select('staffMembersName');
					$this->db->from('staffMembers');
					$this->db->where('staffMembersId',$value['createdById']);		
					$query_staffMembers = $this->db->get();
					$res_staffMembers = $query_staffMembers->result_array();
					if(!empty($res_staffMembers)){
						$value['addedBy'] = $res_staffMembers[0]['staffMembersName'];
					}
				}elseif($value['createdBy'] == "Self"){
					$value['addedBy'] = "Self";
				}
				array_push($arrReturn,$value);
			}
			//$arrReturn = $res_businesses;
		}		
		return $arrReturn;
	}		
	
	
	public function getcategoryValues(){
		$arrReturn = array();
		$this->db->select('categoryId,categoryName');
		$this->db->from('category');				
		$query_category = $this->db->get();
		$res_category = $query_category->result_array();
		if(!empty($res_category)){
			$arrReturn = $res_category;
		}
		return $arrReturn;
		 
	}
	
	
	public function blockBusiness($businessId){		
		$data = array(	'isBlocked' 	=> 1	);
		$this->db->where('businessId', $businessId);
		if($this->db->update('businesses', $data)){
			return "BLOCKED";
		}else{
			return "NTBLOCKED";
		} 		
	}
	
	
	public function unblockBusiness($businessId){		
		$data = array(	'isBlocked' 	=> 0	);
		$this->db->where('businessId', $businessId);
		if($this->db->update('businesses', $data)){
			return "UNBLOCKED";
		}else{
			return "NTUNBLOCKED";
		} 		
	}
	
	
		public function getBusinessDataFromID($businessId){
		$arrReturn = array();
		$this->db->select('
							b.*,
							c.countryName,
							s.stateName,
							ci.cityName,
						');
		$this->db->from('businesses b');
		$this->db->join('country c', 'b.countryId = c.countryId',"left");
		$this->db->join('states s', 'b.stateId = s.stateId',"left");		
		$this->db->join('city ci', 'b.cityId = ci.cityId',"left");	
		$this->db->where('businessId', $businessId);	
		$query_businesses = $this->db->get();		
		$res_businesses = $query_businesses->result_array();		
		if(!empty($res_businesses)){
			$arrReturn['businessesData'] = $res_businesses[0];			
		}
		
		$categoryValues = $this->getcategoryValues();
		if(!empty($categoryValues)){
			$arrReturn['categoryValues'] = $categoryValues;			
		}
		
		
		$arrReturn['selectedCategories'] = array();
		$selectedCategories = $this->getSelectedCategoriesOfUser($arrReturn['businessesData']['businessId']);
		if(!empty($selectedCategories)){
			foreach($selectedCategories as $key=>$value){
				array_push($arrReturn['selectedCategories'],$value['categoryId']);
			}		
		}
		
		
		$officePictures = $this->getOfficePictures($arrReturn['businessesData']['businessId']);
		if(!empty($officePictures)){
			$arrReturn['officePictures'] = $officePictures;			
		}
		
		
		$serviceTags = $this->getserviceTags($arrReturn['businessesData']['businessId']);		
		if(!empty($serviceTags)){
			$serviceTags = implode(",",$serviceTags);		
			$arrReturn['serviceTags'] = $serviceTags;			
		}
		
		
		$ServicePictures = $this->getServicePictures($arrReturn['businessesData']['businessId']);
		if(!empty($ServicePictures)){
			$arrReturn['ServicePictures'] = $ServicePictures;			
		}
		
		$openinghours = $this->getOpeningHoursData($arrReturn['businessesData']['businessId']);
		if(!empty($openinghours)){
			$finalOpeningHours = array();
			foreach($openinghours as $data) {
				$finalOpeningHours[$data['day']] = $data;
			}
			$arrReturn['openinghours'] = $openinghours;
			$arrReturn['finalopeninghours'] = $finalOpeningHours;
		}
		
		$RecentReviews = $this->getRecentReviews($arrReturn['businessesData']['businessId']);
		if(!empty($RecentReviews)){
			$arrReturn['RecentReviews'] = $RecentReviews;			
		}
		
		return $arrReturn;
	}
	
		public function getSelectedCategoriesOfUser($businessId){
		$arrReturn = array();
		$this->db->select('categoryId');
		$this->db->from('businessCategory');		
		$this->db->where('businessId', $businessId);
		$query_businesscategory = $this->db->get();		
		$res_businesscategory = $query_businesscategory->result_array();		
		if(!empty($res_businesscategory)){
			$arrReturn = $res_businesscategory;			
		}
		return $arrReturn;
	}
	
	
	public function getOfficePictures($businessId){
		$arrReturn = array();
		$this->db->select('images');
		$this->db->from('businessesImage');		
		$this->db->where('businessId', $businessId);
		$this->db->where('imageType', "Office");
		$query_businessesimage = $this->db->get();		
		$res_businessesimage = $query_businessesimage->result_array();		
		if(!empty($res_businessesimage)){
			$arrReturn = $res_businessesimage[0];			
		}
		return $arrReturn;
	}
	
	public function getserviceTags($businessId){
		$arrReturn = array();
		$this->db->select('serviceTag');
		$this->db->from('services');		
		$this->db->where('businessId', $businessId);		
		$query_services = $this->db->get();		
		$res_services = $query_services->result_array();		
		if(!empty($res_services)){
			foreach($res_services as $value){
				array_push($arrReturn,$value['serviceTag']);	
			}	
			
		}
		return $arrReturn;
	}
	
	
	public function getServicePictures($businessId){
		$arrReturn = array();
		$this->db->select('
							images,
							businessesImageId
						');
		$this->db->from('businessesImage');		
		$this->db->where('businessId', $businessId);
		$this->db->where('imageType', "Service");
		$query_businessesimage = $this->db->get();		
		$res_businessesimage = $query_businessesimage->result_array();		
		if(!empty($res_businessesimage)){			
			$arrReturn = $res_businessesimage;			
		}
		return $arrReturn;
	}
	
	public function getOpeningHoursData($businessId){
		$arrReturn = array();
		$this->db->select('
							*,
							DATE_FORMAT(openingHours,"%I:%i %p") as openingHours,
							DATE_FORMAT(closingHours,"%I:%i %p") as closingHours
						');
		$this->db->from('openingHours');		
		$this->db->where('businessId', $businessId);		
		$query_openinghours = $this->db->get();		
		$res_openinghours = $query_openinghours->result_array();		
		if(!empty($res_openinghours)){
			$arrReturn = $res_openinghours;			
		}
		return $arrReturn;
	}
	
		public function getRecentReviews($businessId){
		$arrReturn = array();
		$this->db->select('
							r.*,
							u.fullName,
							u.profilePicture,
							DATE_FORMAT(r.createdDate,"%D %M %Y At %l:%i %p") as createdDate
							
							');
		$this->db->from('review r');	
		$this->db->join('users u', 'r.userId = u.userId',"left");	
		$this->db->where('r.businessId', $businessId);		
		$query_review = $this->db->get();		
		$res_review = $query_review->result_array();		
		if(!empty($res_review)){
				$arrReturn = $res_review;
		}
		return $arrReturn;
	}
	
	public function getstaffMemberData($countryID = ""){
		$arrReturn = array();
 
		$this->db->select('
							s.*,
							c.countryName,
							st.stateName,
							ci.cityName,							
							DATE_FORMAT(s.staffMembersJoiningDate,"%D %M %Y") as staffMembersJoiningDate
						');
		$this->db->from('staffMembers s');
		$this->db->join('country c', 's.countryId = c.countryId',"left");
		$this->db->join('states st', 's.stateId = st.stateId',"left");		
		$this->db->join('city ci', 's.cityId = ci.cityId',"left");		
		if(isset($countryID) && $countryID != "" && $countryID > 0){
		$this->db->where('s.countryId', $countryID);				
		}			
		$query_staffMembers = $this->db->get();
		$res_staffMembers = $query_staffMembers->result_array();						
		if(!empty($res_staffMembers)){
		 	$arrReturn = $res_staffMembers;		
		}		
		return $arrReturn;
	}		
	
	
	public function getStateList($country_id){
		$arrReturn = array();
		$this->db->select('
							stateId,
							stateName
						');
		$this->db->from('states');
		$this->db->where('countryId', $country_id);			
		$query_state = $this->db->get();
		$res__state = $query_state->result_array();
		if(!empty($res__state)){
			$arrReturn = $res__state;			
		}
		return $arrReturn;
	}
	
	public function getCityList($state_id){
		$arrReturn = array();
		$this->db->select('
							cityId,
							cityName
						');
		$this->db->from('city');
		$this->db->where('stateId', $state_id);			
		$query_city = $this->db->get();
		$res__city = $query_city->result_array();
		if(!empty($res__city)){
			$arrReturn = $res__city;			
		}
		return $arrReturn;		
	}
	
	
	public function getStaffData($staffMembersId){
		$arrReturn = array();
		$this->db->select('
							staffMembersId,
							staffMembersName,
							staffMembersPhone,
							staffMembersEmail,
							staffMembersAddress1,
							staffMembersAddress2,
							staffMembersArea,
							documentName,
							cityId,
							stateId,
							countryId,
							staffMembersJoiningDate,						
							DATE_FORMAT(staffMembersJoiningDate,"%Y-%m-%d") as staffMembersJoiningDate,
							refferedBy						
						');
		$this->db->from('staffMembers');
		$this->db->where('staffMembersId', $staffMembersId);			
		$query_staffMembers = $this->db->get();
		$res_staffMembers = $query_staffMembers->result_array();
		if(!empty($res_staffMembers)){
			$arrReturn = $res_staffMembers[0];			
			
			
			$StateList = $this->getStateList($res_staffMembers[0]['countryId']);
			if(!empty($StateList)){
				$arrReturn['StateList'] = $StateList;			
			}
			
			
			$CityList = $this->getCityList($res_staffMembers[0]['stateId']);
			if(!empty($CityList)){
				$arrReturn['CityList'] = $CityList;			
			}
		}		
		return $arrReturn;		
	}		
	
	public function checkEmailForStaffAndAdmin($email,$staffMembersId){
			$this->db->select('*');
			$this->db->from('admin');
			$this->db->where('email', $email);					
			$query = $this->db->get();
			$res_admin = $query->result_array();		
			if(!empty($res_admin)){
				return "EXISTS";
			}else{
				$this->db->select('*');
				$this->db->from('staffMembers');
				$this->db->where('staffMembersEmail', $email);					
				$this->db->where('staffMembersId <> ', $staffMembersId);					
				$query_staffMembers = $this->db->get();
				$res_staffMembers = $query_staffMembers->result_array();
				//echo $this->db->last_query();exit;
				if(!empty($res_staffMembers)){
					return "EXISTS";	
				}else{
					return "NTEXISTS";	
				}
			}
	}



	public function checkEmailForBusiness($email,$businessId){			
		$this->db->select('*');
		$this->db->from('businesses');
		$this->db->where('businessEmail', $email);					
		$this->db->where('businessId <> ', $businessId);					
		$query_businesses = $this->db->get();
		$res_businesses = $query_businesses->result_array();
		//echo $this->db->last_query();exit;
		if(!empty($res_businesses)){
			return "EXISTS";	
		}else{
			return "NTEXISTS";	
		}			
	}
	
	
	public function addStaffMember($data){		
			$arrReturn =array();
		//Code to upload document
		if(isset($_FILES['proofDocument']) && $_FILES['proofDocument']['name'] != "" && $_FILES['proofDocument']['error'] != "4"){			
			$docPath = date("YmdHis")."_".$_FILES['proofDocument']['name'];			
			$target_dir2 = './uploads/documents/'.$docPath;
			$file_name = $docPath;			
			if(move_uploaded_file($_FILES['proofDocument']["tmp_name"], $target_dir2)){
				$documentName = $file_name;
			}	
		}				
		
		$password = $this->generateRandomToken(6, 'all');
		$staffMembersPassword = md5($password);		
		$data_StaffMember = array(
			'staffMembersName' 			=> $data['staffMembersName'],			
			'staffMembersPhone' 		=> $data['staffMembersPhone'],			
			'staffMembersEmail' 		=> $data['staffMembersEmail'],			
			'staffMembersPassword' 		=> $staffMembersPassword,			
			'staffMembersAddress1' 		=> $data['staffMembersAddress1'],			
			'staffMembersAddress2' 		=> $data['staffMembersAddress2'],			
			'staffMembersArea' 			=> $data['staffMembersArea'],			
			'documentName' 				=> $documentName,			
			'cityId' 					=> $data['city'],			
			'stateId' 					=> $data['state'],			
			'countryId' 				=> $data['countryId'],			
			'staffMembersJoiningDate' 	=> $data['staffMembersJoiningDate'],			
			'refferedBy' 				=> $data['refferedBy'],			
		);
		if($this->db->insert('staffMembers', $data_StaffMember)){
			$arrReturn['staffMembersName'] = $data['staffMembersName'];
			$arrReturn['staffMembersEmail'] = $data['staffMembersEmail'];
			$arrReturn['password'] = $password;
		}
		return $arrReturn;		
	}



	public function editStaffMember($data){			 		
		//Code to upload document
		if(isset($_FILES['proofDocument']) && $_FILES['proofDocument']['name'] != "" && $_FILES['proofDocument']['error'] != "4"){			
			$docPath = date("YmdHis")."_".$_FILES['proofDocument']['name'];			
			$target_dir2 = './uploads/documents/'.$docPath;
			$file_name = $docPath;			
			if(move_uploaded_file($_FILES['proofDocument']["tmp_name"], $target_dir2)){
				$documentName = $file_name;
			}	
		}else{
			$documentName = $data['hidDocumentName'];
		}						
		$data_StaffMember = array(
			'staffMembersName' 			=> $data['staffMembersName'],			
			'staffMembersPhone' 		=> $data['staffMembersPhone'],			
			'staffMembersEmail' 		=> $data['staffMembersEmail'],						
			'staffMembersAddress1' 		=> $data['staffMembersAddress1'],			
			'staffMembersAddress2' 		=> $data['staffMembersAddress2'],			
			'staffMembersArea' 			=> $data['staffMembersArea'],			
			'documentName' 				=> $documentName,			
			'cityId' 					=> $data['city'],			
			'stateId' 					=> $data['state'],			
			'countryId' 				=> $data['countryId'],			
			'staffMembersJoiningDate' 	=> $data['staffMembersJoiningDate'],			
			'refferedBy' 				=> $data['refferedBy'],			
		);
		
		$this->db->where('staffMembersId', $data['staffMembersId']);		
		if($this->db->update('staffMembers', $data_StaffMember)){
			return true;
		}
		return false;		
	}
	
	
	public function generateRandomToken($length = 10, $string_type = 'all') {
        $random_string = "";
        $all_chars = "";

        if ($string_type == 'small_alpha') {
            $all_chars = "abcdefghijklmnopqrstuvwxyz";
        } else if ($string_type == 'caps_alpha') {
            $all_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if ($string_type == 'small_alpha_numeric') {
            $all_chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        } else if ($string_type == 'caps_alpha_numeric') {
            $all_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if ($string_type == 'alpha_numeric') {
            $all_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if ($string_type == 'numeric') {
            $all_chars = "0123456789";
        }
        if ($string_type != 'all') {
            for ($i = 0; $i < $length; $i++) {
                $random_string .= $all_chars[rand(0, strlen($all_chars) - 1)];
            }
        }
        if ($string_type == 'all') {
            $all_part = substr(str_shuffle("*^%#@!0123456789abcdefghijklmno*^%#@!qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length - 3);
            $caps_part = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1);
            $number_part = substr(str_shuffle("0123456789"), 0, 1);
            $special_part = substr(str_shuffle("*^%#@!"), 0, 1);

            $random_string = $number_part . $all_part . $caps_part . $special_part;
        }

        return $random_string;
    }
	
	public function deleteDocument($staffMembersId){			
		$file_name = "";
		$this->db->select('documentName');
		$this->db->from('staffMembers');
		$this->db->where('staffMembersId', $staffMembersId);					
		$query_name = $this->db->get();
		$res_name = $query_name->result_array();
		if(!empty($res_name)){
			$file_name = $res_name[0]['documentName'];
		}
		$delete_file = "./uploads/documents/".$file_name;		
		if((unlink($delete_file))){
			$data = array(	'documentName' 	=> ""	);
			$this->db->where('staffMembersId', $staffMembersId);
			$this->db->update('staffMembers', $data); 			
			return "DELETED";
		}else{
			return "NOTDELETED";
		}
	}
	
	
	public function getpaymentTransactions( $start_time = "" , $end_time = "" ){
		$arrReturn = array();
		$this->db->select('
							payment.*,							 
							DATE_FORMAT(payment.paymentDate,"%D %M %Y At %l:%i %p") as paymentDate,							 
							businesses.businessId as BusinessRecid,
							businesses.businessName,
							orderpackagemaster.purchaseType,
							orderpackagemaster.packageType
						');
		$this->db->from('payment');		
		$this->db->join('businesses', 'payment.businessId = businesses.businessId',"left");
		$this->db->join('orderpackagemaster', 'payment.ordermasterID = orderpackagemaster.ordermasterID',"left");
		if($start_time != "" && $end_time != ""){	
			$start_time .=" 00:00:00";
			$end_time .=" 23:59:59";		
			$this->db->where('payment.paymentDate >= ', $start_time);
			$this->db->where('payment.paymentDate <= ', $end_time);
		}
		$query_payment = $this->db->get();
		$res_payment = $query_payment->result_array();						
		if(!empty($res_payment)){
			$arrReturn = $res_payment;
		}
		return $arrReturn;
	}
	
	public function getembassiesData($countryID){
		$arrReturn = array();
		$this->db->select('
							e.*,
							c1.countryName as residentCountryName,
							c2.countryName as foreignCountryName,
							ct.cityName
						');
		$this->db->from('embessay e');
		$this->db->join('country c1', 'c1.countryId = e.residentCountry',"left");
		$this->db->join('country c2', 'c2.countryId = e.foreignCountry',"left");
		$this->db->join('city ct', 'e.foreignCity = ct.cityId',"left");
		if(isset($countryID) && is_numeric($countryID) && $countryID > 0){
			$this->db->where('e.foreignCountry', $countryID);
		}
		$query_embessay = $this->db->get();
		$res_embessay = $query_embessay->result_array();	
		if(!empty($res_embessay)){
			$arrReturn = $res_embessay;
		}
		return $arrReturn;
	}
	
	public function getCityListForThisCountry($country_id){
		$arrReturn = array();
		$this->db->select('country.*,city.*');
		$this->db->from('city');
		$this->db->join('states', 'city.stateId = states.stateId');
		$this->db->join('country', 'states.countryId = country.countryId');				 
		$this->db->where('country.countryId', $country_id);
		$query_cityList = $this->db->get();
		$res_cityList = $query_cityList->result_array();			
		//echo $this->db->last_query();exit;
		if(!empty($res_cityList)){
			$arrReturn = $res_cityList;
		}
		return $arrReturn;
	}	
	
	public function addEmbassyDetails($data){		
	
		$data_insert = array(
			'residentCountry' 		=> $data['residentCountry'],			
			'foreignCountry' 		=> $data['foreignCountry'],			
			'foreignCity' 			=> $data['foreignCity'],			
			'embassyAddress' 		=> $data['embassyAddress'],			
			'embassyContact' 		=> $data['embassyContact']			
		);		
		if($this->db->insert('embessay', $data_insert)){
			return true;
		}else{
			return false;
		}
	}	
	
	public function getEmbassyDetails($embessayId){		
		$arrReturn = array();
		
		$this->db->select('*');
		$this->db->from('embessay');		
		$this->db->where('embessayId', $embessayId);
		$query_embessay = $this->db->get();
		$res_embessay = $query_embessay->result_array();			
		if(!empty($res_embessay)){
			$arrReturn = $res_embessay[0];


			$CityList = $this->getCityListForThisCountry($res_embessay[0]['foreignCountry']);
			if(!empty($CityList)){
				$arrReturn['CityList'] = $CityList;			
			}
		}
		return $arrReturn;
	}
	
	public function editEmbassyDetails($data){		
		$data_update = array(
			'residentCountry' 		=> $data['residentCountry'],			
			'foreignCountry' 		=> $data['foreignCountry'],			
			'foreignCity' 			=> $data['foreignCity'],			
			'embassyAddress' 		=> $data['embassyAddress'],
			'embassyContact' 		=> $data['embassyContact']
		);		
		$this->db->where('embessayId', $data['embessayId']);		
		if($this->db->update('embessay', $data_update)){
			return true;
		}else{
			return false;
		}		
	}
	
	
	public function getpassportData($countryID = ""){
		$arrReturn = array();
		$this->db->select('
							passportValue.*,
							country.countryName 														
						');
		$this->db->from('passportValue');
		$this->db->join('country', 'country.countryId = passportValue.countryId',"left");				
		if(isset($countryID) && is_numeric($countryID) && $countryID > 0){
			$this->db->where('passportValue.countryId', $countryID);
		}
		$query_passportValue = $this->db->get();
		$res_passportValue = $query_passportValue->result_array();	
		if(!empty($res_passportValue)){
			$arrReturn = $res_passportValue;
		}		
		return $arrReturn;	
	}
	
	
	public function addPassportDetails($data){		
		$data_insert = array(
			'countryId' 			=> $data['countryId'],			
			'countryDetails' 		=> $data['countryDetails']			
		);		
		if($this->db->insert('passportValue', $data_insert)){
			return true;
		}else{
			return false;
		}	
	}
	
	public function getpassportDetails($passportValueId){
		$arrReturn = array();
		
		$this->db->select('*');
		$this->db->from('passportValue');		
		$this->db->where('passportValueId', $passportValueId);
		$query_passportValue = $this->db->get();
		$res_passportValue = $query_passportValue->result_array();			
		if(!empty($res_passportValue)){
			$arrReturn = $res_passportValue[0];	
		}
		return $arrReturn;	
	}
	
	public function editPassportDetails($data){
		$data_update = array(
			'countryId' 			=> $data['countryId'],			
			'countryDetails' 		=> $data['countryDetails']	
		);
		$this->db->where('passportValueId', $data['passportValueId']);		
		if($this->db->update('passportValue', $data_update)){
			return true;
		}else{
			return false;
		}			
	}
	
	
	public function getcountryData($countryID = ""){
				$arrReturn = array();
		$this->db->select('
							 countryInformation.*,
							country.countryName 														
						');
		$this->db->from('countryInformation');
		$this->db->join('country', 'country.countryId = countryInformation.countryId',"left");				
		if(isset($countryID) && is_numeric($countryID) && $countryID > 0){
			$this->db->where('countryInformation.countryId', $countryID);
		}
		$query_countryInformation = $this->db->get();
		$res_countryInformation = $query_countryInformation->result_array();	
		if(!empty($res_countryInformation)){
			$arrReturn = $res_countryInformation;
		}		
		return $arrReturn;		
	}
	
	public function addCountryInfoDetails($data){
		$data_insert = array(
			'countryId' 			=> $data['countryId'],			
			'countryDetails' 		=> $data['countryDetails']			
		);		
		if($this->db->insert('countryInformation', $data_insert)){
			return true;
		}else{
			return false;
		}		
	}
	
	public function getCountryInfoDetails($countryInformationID){
				$arrReturn = array();
		
		$this->db->select('*');
		$this->db->from('countryInformation');		
		$this->db->where('countryInformationID', $countryInformationID);
		$query_countryInformation = $this->db->get();
		$res_countryInformation = $query_countryInformation->result_array();			
		if(!empty($res_countryInformation)){
			$arrReturn = $res_countryInformation[0];	
		}
		return $arrReturn;		
	}
	
	public function editCountryInfoDetails($data){
		$data_update = array(
			'countryId' 			=> $data['countryId'],			
			'countryDetails' 		=> $data['countryDetails']	
		);
		$this->db->where('countryInformationID', $data['countryInformationID']);		
		if($this->db->update('countryInformation', $data_update)){
			return true;
		}else{
			return false;
		}	
	}
	
	public function getUsersReportData( $countryID = "" , $cityId = ""){
		$arrReturn = array();
 
		$this->db->select('
							u.userId,
							u.fullName,							
							u.emailId,
							c.countryName,
							s.stateName,
							ci.cityName,
							u.isBlocked
						');
		$this->db->from('users u');
		$this->db->join('country c', 'u.countryId = c.countryId',"left");
		$this->db->join('states s', 'u.stateId = s.stateId',"left");		
		$this->db->join('city ci', 'u.cityId = ci.cityId',"left");				
		if(isset($countryID) && $countryID != "" && $countryID > 0){
		$this->db->where('u.countryId', $countryID);				
		}
		if(isset($cityId) && $cityId != "" && $cityId > 0){
		$this->db->where('u.cityId', $cityId);				
		}
		$query_users = $this->db->get();
		$res_users = $query_users->result_array();			
	 
		if(!empty($res_users)){
			foreach($res_users as $value){
				$this->db->select('DATE_FORMAT(createdDate,"%D %M %Y At %l:%i %p") as createdDate');
				$this->db->from('tokenaccess');	
				$this->db->where('userId', $value['userId']);					
				$this->db->order_by("tokenId", "desc"); 	
				$this->db->limit(1);	
				$query_tokenaccess = $this->db->get();		
				$res_tokenaccess = $query_tokenaccess->result_array();
				if(!empty($res_tokenaccess)){
					$value['createdDate'] = $res_tokenaccess[0]['createdDate'];
				}else{
					$value['createdDate'] = "";
				}
				
				
				$this->db->select('deviceType');
				$this->db->from('userdetails');	
				$this->db->where('userId', $value['userId']);					
				$this->db->where('isDeleted', 0);					
				$this->db->order_by("tokenId", "desc"); 	
				$this->db->group_by("deviceType");					
				$query_userdetails = $this->db->get();		
				$res_userdetails = $query_userdetails->result_array();
				
				$strDeviceType = "";
				
				if(!empty($res_userdetails)){
					foreach($res_userdetails AS $key => $deviceType) {
						$strDeviceType .= $deviceType['deviceType'] . ", ";
					}
					$strDeviceType = rtrim($strDeviceType, ", ");
				}
				$value['deviceType'] = $strDeviceType;
				
				$this->db->select('count(*) as totBookmarks');
				$this->db->from('bookmarks');
				$this->db->where('userId', $value['userId']);					
				$query_bookmarks = $this->db->get();		
				$res_bookmarks = $query_bookmarks->result_array();			
				if(!empty($res_bookmarks)){
					$value['totBookmarks'] = $res_bookmarks[0]['totBookmarks'];
				}else{
					$value['totBookmarks'] = 0;
				}
				
				
				$this->db->select('count(*) as totReviews');
				$this->db->from('review');
				$this->db->where('userId', $value['userId']);					
				$query_review = $this->db->get();		
				$res_review = $query_review->result_array();			
				if(!empty($res_review)){
					$value['totReviews'] = $res_review[0]['totReviews'];
				}else{
					$value['totReviews'] = 0;
				}
				
				
				
				
				
				array_push($arrReturn,$value);
			}				
		}		
		return $arrReturn;	
	}
	
	
	public function getcityValues(){
		$arrReturn = array();
		$this->db->select('
							countryId,
							countryName
						');
		$this->db->from('country');		
		$query_country = $this->db->get();
		$res__country = $query_country->result_array();
		if(!empty($res__country)){
			$arrReturn = $res__country;			
		}
		return $arrReturn;	
	}


	public function getNotSelectedPackages($idsToIgnore){		
		$arrReturn = array();
		$this->db->select('
							packageId,
							packageName,
							packageDetails,
							packageType,
							packageAmount

						');
		$this->db->from('packages');		
		if(!empty($idsToIgnore)){
			$this->db->where_not_in('packageId', $idsToIgnore);			
		}
		$query_packages = $this->db->get();		
		$res_packages = $query_packages->result_array();		
		if(!empty($res_packages)){
			$arrReturn = $res_packages;			
		}
		return $arrReturn;
		
	}
	
	
	public function performBusinessProfile($data){
		//echo "<pre>";print_r($data);exit;
		//Code to get latitude and longitude 	 
		$arrReturn = array();			
		$checkEmail = $this->checkEmailForBusiness($data['businessEmail'],0);		
		if($checkEmail  == "NTEXISTS"){
			$businessId = 0;	
			$dlocation = $data['businessAddress1'].",".$data['businessAddress2'].",".$data['businessArea'].",".$data['hidCityName'].",".$data['hidStateName'].",".$data['hidCountryName'];				
			$address = urlencode($dlocation);         
			$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
			$output= json_decode($geocode);
			if(!empty($output->results)){
				$latitude = $output->results[0]->geometry->location->lat;
				$longitude = $output->results[0]->geometry->location->lng;	
			}else{
				$dlocation = $data['hidCityName'];				
				$address = urlencode($dlocation);         		
				$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
				$output= json_decode($geocode);		
				$latitude = $output->results[0]->geometry->location->lat;
				$longitude = $output->results[0]->geometry->location->lng;	
			} 	
			if(isset($_SESSION['LoggedInType']) && isset($_SESSION['adminId'])){
				$createdBy  = $_SESSION['LoggedInType'];
				$createdById  = $_SESSION['adminId'];	
			}else{
				$createdBy  = "";
				$createdById  = "";	
			}	
			$password = $this->generateRandomToken(6, 'all');
			$newPassword = md5($password);
			$data_businesses = array(
			   'businessMerchantName' 	=> $data['businessMerchantName'],
			   'businessName' 			=> $data['businessName'],
			   'businessPhone' 			=> $data['businessPhone'],
			   'businessEmail' 			=> $data['businessEmail'],
			   'businessAddress1' 		=> $data['businessAddress1'],
			   'businessAddress2' 		=> $data['businessAddress2'],
			   'businessArea' 			=> $data['businessArea'],
			   'countryId' 				=> $data['countryId'],
			   'stateId' 				=> $data['state'],
			   'cityId' 				=> $data['city'],
			   'latitude' 				=> $latitude,
			   'longitude' 				=> $longitude,
			   'password' 				=> $newPassword,
			   'isPublished' 			=> 1,
			   'createdBy' 				=> $createdBy,
			   'createdById' 			=> $createdById,		   
			   'isBlocked' 				=> 0,		   
			   'isDeleted' 				=> 0		   
			);				 
			if($this->db->insert('businesses', $data_businesses)){
				 $businessId = $this->db->insert_id();
			}
			if(($businessId) && is_numeric($businessId) && $businessId > 0){
				// Code to enter data into "services" table
				$arrServices = explode(",",$data['businessServices']);
				foreach($arrServices as $key=>$value){
					$data_services = array(
						'businessId' 		=> $businessId,
						'serviceTag' 		=> $value 
					);				
					$this->db->insert('services', $data_services);
				}
				//Code to insert Data into "openinghours"
				$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
				if(!empty($data['weekdays'])){
					foreach($data['weekdays'] as $key_days => $value_days){					
						$data_openinghours = array(
							'businessId' 		=> $businessId,
							'day' 				=> $days[$key_days], 
							'openingHours' 		=> date("H:i:s",strtotime($data['fromTime'][$key_days])), 
							'closingHours' 		=> date("H:i:s",strtotime($data['toTime'][$key_days])), 
						);					
						$this->db->insert('openingHours', $data_openinghours);
					}				
				}
				
				//Inserting data into "businesscategory"
				if(!empty($data['businessCategory'])){
					foreach($data['businessCategory'] as $value_category){
						$data_businesscategory = array(						
							'categoryId' 		=> $value_category ,
							'businessId' 		=> $businessId							
						);					
						$this->db->insert('businessCategory', $data_businesscategory);
					}
				}
				if(!empty($data['hidOfficeImage']) && $data['hidOfficeImage'] != ""){
					//Code to upload office image 		
					$dataOfoficeImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['hidOfficeImage']));
					$microtime = microtime();
					$imagePath = date("YmdHis");
					$imagePath = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $imagePath) . '.jpg';
					$target_dir2 = './uploads/office_pictures/'.$businessId."_". $imagePath;
					$file_name = $businessId."_". $imagePath;				
					if(file_put_contents($target_dir2, $dataOfoficeImage)){
						//Code to create thumbnail
						$config['image_library'] = 'gd2';
						$config['source_image'] = $target_dir2;
						$config['new_image'] = './uploads/office_pictures/thumb_'.$businessId."_".$imagePath;
						$config['create_thumb'] = False;
						$config['maintain_ratio'] = TRUE;
						$config['width']         = 100;
						$config['height']       = 100;			
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						
						$data_to_insert = array(
							'businessId'				=>	$businessId,
							'images'					=>	$file_name,
							'imageType'					=>	"Office"
						);		
						$this->db->insert('businessesImage', $data_to_insert);
						
					}
				}
					//Code to upload Service Images
				if(!empty($data['hidServiceImages'])){
					foreach($data['hidServiceImages'] as $key => $value){						
						$dataOfServiceImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $value));	
						$microtime_service = microtime();
						$imagePath_service = date("YmdHis")."_".$key;
						$imagePath_service = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $imagePath_service) . '.jpg';
						$target_dir_service = './uploads/service_pictures/'.$businessId."_". $imagePath_service;
						$file_name_service = $businessId."_". $imagePath_service;
						if(file_put_contents($target_dir_service, $dataOfServiceImage)){									
							$config_service['image_library'] = 'gd2';
							$config_service['source_image'] = $target_dir_service;
							$config_service['new_image'] = './uploads/service_pictures/thumb_'.$businessId."_".$imagePath_service;
							$config_service['create_thumb'] = False;
							$config_service['maintain_ratio'] = TRUE;
							$config_service['width']         = 100;
							$config_service['height']       = 100;
							$this->image_lib->initialize($config_service);
							$returnThumb = $this->image_lib->resize();							
							//code to insert name into DB		
							$data_to_insert_services = array(
								'businessId'				=>	$businessId,
								'images'					=>	$file_name_service,
								'imageType'					=>	"Service"
							);		
							$this->db->insert('businessesImage', $data_to_insert_services);					
						}							
					}
					 
				}

				//code to insert package details
				$PackageID = $data['hidPackageID'];
				$packageData = $this->getPackageDetails($PackageID);
				$ordermasterID = 0;
				// echo "<pre>";
				// print_r($packageData);
				// exit;
					//insert into orderMaster
					$data_orderMaster = array(
						'businessId'				=>	$businessId,
						'createdDate'				=>	TODAY,
						'flagActive'				=>	"0",
						'flagCompleted'				=>	"1",
						'totalAmountCharged'		=>	number_format($data['hidPayableAmount'],"2",",","")
					);		
					$this->db->insert('orderMaster', $data_orderMaster);	
					
					//insert into orderMaster
					$data_orderMaster = array(
						'businessId'				=>	$businessId,
						'createdDate'				=>	TODAY,
						'flagActive'				=>	"0",
						'flagCompleted'				=>	"1",
						'totalAmountCharged'		=>	number_format($data['hidPayableAmount'],"2",",","")
					);		
					if($this->db->insert('orderMaster', $data_orderMaster)){
						$ordermasterID = $this->db->insert_id();
					}
					//insert into orderpackagemaster
					if($data['hidPurchaseType'] == "RENEWPACKAGE"){
						$purchaseType = "RENEW";
					}elseif($data['hidPurchaseType'] == "BUYPACKAGE"){
						$purchaseType = "NEW";
					}
					$data_orderpackagemaster = array(
						'ordermasterID'				=>	$ordermasterID,
						'packageid'					=>	$packageData['packageId'],
						'packageName'				=>	$packageData['packageName'],
						'packageDetails'			=>	$packageData['packageDetails'],
						'packageType'				=>	$packageData['packageType'],
						'packageAmount'				=>	$packageData['packageAmount'],
						'monthsSubscribed'			=>	$data['monthsDropdown'],
						'totalPackageAmount'		=>	number_format($data['hidPayableAmount'],"2",",",""),
						'purchaseType'				=>	$purchaseType,
						'createdDate'				=>	TODAY
					);	
					$this->db->insert('orderpackagemaster', $data_orderpackagemaster);
					
					//insert into  payment
					$data_payment = array(
						'ordermasterID'				=>	$ordermasterID,
						'transaction_id'			=>	$data['chequeNumber'],
						'totalamount'				=>	number_format($data['hidPayableAmount'],"2",",",""),
						'paymentDate'				=>	TODAY,
						'businessId'				=>	$businessId,
						'country'					=>	"India",
						'paymentStatus'				=>	"1",
						'transaction_currency'		=>	"INR",
						'payment_method'			=>	"Cheque",
						'transaction_state'			=>	"completed",
						'paymentNotes'				=>	$data['paymentNotes']
					);	
					$this->db->insert('payment', $data_payment);
					$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $data['monthsDropdown'] . ' months', strtotime(TODAY)));
					//insert into packagesOfBusiness
					$data_packagesOfBusiness = array(
						'packageId'					=>	$packageData['packageId'],
						'businessId'				=>	$businessId,
						'packagePurchaseDate'		=>	TODAY,
						'packageExpiryDate'			=>	$packageExpiryDate,
						'isActive'					=>	"1",
						'isExpired'					=>	"0"
					);	
					$this->db->insert('packagesOfBusiness', $data_packagesOfBusiness);
					
					//For free package
					$packageExpiryDate_free = date('Y-m-d H:i:s',strtotime('+12 years', strtotime(TODAY)));
					$data_packagesOfBusiness_free = array(
						'packageId'					=>	"4",
						'businessId'				=>	$businessId,
						'packagePurchaseDate'		=>	TODAY,
						'packageExpiryDate'			=>	$packageExpiryDate_free,
						'isActive'					=>	"1",
						'isExpired'					=>	"0"
					);	
					$this->db->insert('packagesOfBusiness', $data_packagesOfBusiness_free);
					
				
			}				
			$arrReturn['businessMerchantName'] 	= $data['businessMerchantName'];
			$arrReturn['businessEmail'] 		= $data['businessEmail'];
			$arrReturn['password'] 				= $password;
			$arrReturn['inserted'] 				= "inserted";
			return $arrReturn;
		}else{
			return false;
		}		
	}	
	
	
	
	public function performEditBusinessProfile($data){
		$businessId = $data['businessId'];
		
			// echo "<pre>";
			// print_r($data);
			// exit;
		$arrReturn = array();			
		$checkEmail = $this->checkEmailForBusiness($data['businessEmail'],$businessId);
		if($checkEmail  == "NTEXISTS"){			 	
			$dlocation = $data['businessAddress1'].",".$data['businessAddress2'].",".$data['businessArea'].",".$data['hidCityName'].",".$data['hidStateName'].",".$data['hidCountryName'];				
			
			$address = urlencode($dlocation);         
			$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
			$output= json_decode($geocode);
			if(!empty($output->results)){
				$latitude = $output->results[0]->geometry->location->lat;
				$longitude = $output->results[0]->geometry->location->lng;	
			}else{
				$dlocation = $data['hidCityName'];				
				$address = urlencode($dlocation);         		
				$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
				$output= json_decode($geocode);		
				$latitude = $output->results[0]->geometry->location->lat;
				$longitude = $output->results[0]->geometry->location->lng;	
			} 						
			$data_businesses = array(
			   'businessMerchantName' 	=> $data['businessMerchantName'],
			   'businessName' 			=> $data['businessName'],
			   'businessPhone' 			=> $data['businessPhone'],
			   'businessEmail' 			=> $data['businessEmail'],
			   'businessAddress1' 		=> $data['businessAddress1'],
			   'businessAddress2' 		=> $data['businessAddress2'],
			   'businessArea' 			=> $data['businessArea'],
			   'countryId' 				=> $data['countryId'],
			   'stateId' 				=> $data['state'],
			   'cityId' 				=> $data['city'],
			   'latitude' 				=> $latitude,
			   'longitude' 				=> $longitude,			   			   	   
			   'updatedOn' 				=> TODAY			   			   	   
			);				
			$this->db->where('businessId',$businessId);
			if($this->db->update('businesses', $data_businesses)){
				// Code to enter data into "services" table
					if(isset($data['businessServices']) && $data['businessServices'] != ""){
						//Code to delte old data
						$this->db->where('businessId', $businessId);
						$this->db->delete('services'); 
						
						//Code to insert new data
						$arrServices = explode(",",$data['businessServices']);
						foreach($arrServices as $key=>$value){
							$data_services = array(
								'businessId' 		=> $businessId,
								'serviceTag' 		=> $value 
							);				
							$this->db->insert('services', $data_services);
						}
					}
					//Code to insert Data into "openinghours"
					if(!empty($data['weekdays'])){
						//Code to delte old data
						$this->db->where('businessId', $businessId);
						$this->db->delete('openingHours'); 
						$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');				 
						foreach($data['weekdays'] as $key_days => $value_days){					
							$data_openinghours = array(
								'businessId' 		=> $businessId,
								'day' 				=> $days[$key_days], 
								'openingHours' 		=> date("H:i:s",strtotime($data['fromTime'][$key_days])), 
								'closingHours' 		=> date("H:i:s",strtotime($data['toTime'][$key_days])), 
							);					
							$this->db->insert('openingHours', $data_openinghours);
						}								 	
					}
					
					//Inserting data into "businesscategory"
					if(!empty($data['businessCategory'])){
						//Code to delte old data
						$this->db->where('businessId', $businessId);
						$this->db->delete('businessCategory'); 	
						foreach($data['businessCategory'] as $value_category){
							$data_businesscategory = array(						
								'categoryId' 		=> $value_category ,
								'businessId' 		=> $businessId							
							);					
							$this->db->insert('businessCategory', $data_businesscategory);
						}
					}
					
					//Code to upload office image 		
					if(!empty($data['hidOfficeImage']) && $data['hidOfficeImage'] != ""){
						$dataOfoficeImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['hidOfficeImage']));
						$microtime = microtime();
						$imagePath = date("YmdHis");
						$imagePath = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $imagePath) . '.jpg';
						$target_dir2 = './uploads/office_pictures/'.$businessId."_". $imagePath;
						$file_name = $businessId."_". $imagePath;				
						if(file_put_contents($target_dir2, $dataOfoficeImage)){
							//Code to create thumbnail
							$config['image_library'] = 'gd2';
							$config['source_image'] = $target_dir2;
							$config['new_image'] = './uploads/office_pictures/thumb_'.$businessId."_".$imagePath;
							$config['create_thumb'] = False;
							$config['maintain_ratio'] = TRUE;
							$config['width']         = 100;
							$config['height']       = 100;			
							$this->load->library('image_lib', $config);
							$this->image_lib->resize();
							//Code to delte old data
							$this->db->where('businessId', $businessId);
							$this->db->where('imageType', "Office");
							$this->db->delete('businessesImage');
							
							//Code to insert new data
							$data_to_insert = array(
								'businessId'				=>	$businessId,
								'images'					=>	$file_name,
								'imageType'					=>	"Office"
							);		
							$this->db->insert('businessesImage', $data_to_insert);
							
						}
					}
					
					//Code to upload Service Images
					if(!empty($data['hidServiceImages'])){
							foreach($data['hidServiceImages'] as $key => $value){						
								$dataOfServiceImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $value));	
								$microtime_service = microtime();
								$imagePath_service = date("YmdHis")."_".$key;
								$imagePath_service = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $imagePath_service) . '.jpg';
								$target_dir_service = './uploads/service_pictures/'.$businessId."_". $imagePath_service;
								$file_name_service = $businessId."_". $imagePath_service;
								if(file_put_contents($target_dir_service, $dataOfServiceImage)){									
									$config_service['image_library'] = 'gd2';
									$config_service['source_image'] = $target_dir_service;
									$config_service['new_image'] = './uploads/service_pictures/thumb_'.$businessId."_".$imagePath_service;
									$config_service['create_thumb'] = False;
									$config_service['maintain_ratio'] = TRUE;
									$config_service['width']         = 100;
									$config_service['height']       = 100;
									$this->image_lib->initialize($config_service);
									$returnThumb = $this->image_lib->resize();							
									//code to insert name into DB		
									$data_to_insert_services = array(
										'businessId'				=>	$businessId,
										'images'					=>	$file_name_service,
										'imageType'					=>	"Service"
									);		
									$this->db->insert('businessesImage', $data_to_insert_services);					
							}							
						}					 
					}
					
						//code to insert package details
						if(($data['hidPackageID']) && is_numeric($data['hidPackageID']) && $data['hidPackageID']> 0){
							$PackageID = $data['hidPackageID'];
				$packageData = $this->getPackageDetails($PackageID);
				$ordermasterID = 0;
				// echo "<pre>";
				// print_r($packageData);
				// exit;
					//insert into orderMaster
					$data_orderMaster = array(
						'businessId'				=>	$businessId,
						'createdDate'				=>	TODAY,
						'flagActive'				=>	"0",
						'flagCompleted'				=>	"1",
						'totalAmountCharged'		=>	number_format($data['hidPayableAmount'],"2",",","")
					);		
					$this->db->insert('orderMaster', $data_orderMaster);	
					
					//insert into orderMaster
					$data_orderMaster = array(
						'businessId'				=>	$businessId,
						'createdDate'				=>	TODAY,
						'flagActive'				=>	"0",
						'flagCompleted'				=>	"1",
						'totalAmountCharged'		=>	number_format($data['hidPayableAmount'],"2",",","")
					);		
					if($this->db->insert('orderMaster', $data_orderMaster)){
						$ordermasterID = $this->db->insert_id();
					}
					//insert into orderpackagemaster
					if($data['hidPurchaseType'] == "RENEWPACKAGE"){
						$purchaseType = "RENEW";
					}elseif($data['hidPurchaseType'] == "BUYPACKAGE"){
						$purchaseType = "NEW";
					}
					$data_orderpackagemaster = array(
						'ordermasterID'				=>	$ordermasterID,
						'packageid'					=>	$packageData['packageId'],
						'packageName'				=>	$packageData['packageName'],
						'packageDetails'			=>	$packageData['packageDetails'],
						'packageType'				=>	$packageData['packageType'],
						'packageAmount'				=>	$packageData['packageAmount'],
						'monthsSubscribed'			=>	$data['monthsDropdown'],
						'totalPackageAmount'		=>	number_format($data['hidPayableAmount'],"2",",",""),
						'purchaseType'				=>	$purchaseType,
						'createdDate'				=>	TODAY
					);	
					$this->db->insert('orderpackagemaster', $data_orderpackagemaster);
					
					//insert into  payment
					$data_payment = array(
						'ordermasterID'				=>	$ordermasterID,
						'transaction_id'			=>	$data['chequeNumber'],
						'totalamount'				=>	number_format($data['hidPayableAmount'],"2",",",""),
						'paymentDate'				=>	TODAY,
						'businessId'				=>	$businessId,
						'country'					=>	"India",
						'paymentStatus'				=>	"1",
						'transaction_currency'		=>	"INR",
						'payment_method'			=>	"Cheque",
						'transaction_state'			=>	"completed",
						'paymentNotes'				=>	$data['paymentNotes']
					);	
					$this->db->insert('payment', $data_payment);					
					//insert into packagesOfBusiness
					if($data['hidPurchaseType'] == "RENEWPACKAGE"){
							$this->db->select('DATE_FORMAT(packageExpiryDate,"%Y-%m-%d") as packageExpiryDate ');
							$this->db->from('packagesOfBusiness');
							$this->db->where('packageId', $packageData['packageId']);				
							$this->db->where('businessId', $businessId);				
							$query_packagesOfBusiness = $this->db->get();		
							$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();			
							if(!empty($res_packagesOfBusiness)){				
								$packageExpiryDate = $res_packagesOfBusiness[0]['packageExpiryDate'];
							}
							$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $data['monthsDropdown'] . ' months', strtotime($packageExpiryDate)));
							$data_packagesOfBusiness = array(
								'packageExpiryDate' 			=> $packageExpiryDate							
							);		 
							$this->db->where('packageId', $packageData['packageId']);				
							$this->db->where('businessId', $businessId);				
							$this->db->update('packagesOfBusiness', $data_packagesOfBusiness); 
						
					}elseif($data['hidPurchaseType'] == "BUYPACKAGE"){
						$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $data['monthsDropdown'] . ' months', strtotime(TODAY)));
						$data_packagesOfBusiness = array(
							'packageId'					=>	$packageData['packageId'],
							'businessId'				=>	$businessId,
							'packagePurchaseDate'		=>	TODAY,
							'packageExpiryDate'			=>	$packageExpiryDate,
							'isActive'					=>	"1",
							'isExpired'					=>	"0"
						);	
						$this->db->insert('packagesOfBusiness', $data_packagesOfBusiness);
						}				
					}		 
					
					
				}
				$arrReturn['updated'] 				= "updated";
				return $arrReturn;
			}else{
				return false;
			}
		}	 
	
	
	
	
	public function getPackageDetails($PackageID){
		$this->db->select('*');
		$this->db->from('packages');
		$this->db->where('packageId', $PackageID);									
		$query = $this->db->get();
		$res = $query->result_array();	
		if(!empty($res)){
			return $res[0];
		}
		
	}	
	
		public function deleteOfficePicture($dataid){
		//echo $dataid;exit;
		$file_name = "";
		$this->db->select('images');
		$this->db->from('businessesImage');
		$this->db->where('businessId', $dataid);			
		$this->db->where('imageType', "Office");			
		$query_name = $this->db->get();
		$res_name = $query_name->result_array();
		if(!empty($res_name)){
			$file_name = $res_name[0]['images'];
		}
		$delete_file_cropped = "./uploads/office_pictures/".$file_name;
		$delete_file_thumb = "./uploads/office_pictures/thumb_".$file_name;
		if((unlink($delete_file_cropped)) && (unlink($delete_file_thumb))){
			$this->db->where('businessId', $dataid);
			$this->db->where('imageType', "Office");
			$this->db->delete('businessesImage'); 			
			return "DELETED";
		}else{
			return "NOTDELETED";
		}
	}
	
		public function deleteOfficeServicePicture($dataid){
		$file_name = "";
		$this->db->select('images');
		$this->db->from('businessesImage');
		$this->db->where('businessesImageId', $dataid);			
		$this->db->where('imageType', "Service");			
		$query_name = $this->db->get();
		$res_name = $query_name->result_array();
		if(!empty($res_name)){
			$file_name = $res_name[0]['images'];
		}
		$delete_file_cropped = "./uploads/service_pictures/".$file_name;
		$delete_file_thumb = "./uploads/service_pictures/thumb_".$file_name;
		if((unlink($delete_file_cropped)) && (unlink($delete_file_thumb))){
			$this->db->where('businessesImageId', $dataid);
			$this->db->delete('businessesImage'); 			
			return "DELETED";
		}else{
			return "NOTDELETED";
		}
	}
	
	public function getSelectedPackagesForThisBusiness($businessId){
		$arrReturn['allData'] = array();
		$arrReturn['ignoreIds'] = array();
		$this->db->select('
							pb.*,
							DATE_FORMAT(pb.packageExpiryDate,"%D %M %Y") as packageExpiryDate,	
							p.packageName,
							p.packageAmount,
							p.packageDetails
						');
		$this->db->from('packagesOfBusiness pb');
		$this->db->join('packages p', 'pb.packageId = p.packageId',"left");
		$this->db->where('pb.businessId', $businessId);					
		$this->db->where('pb.isActive', 1);			
		$this->db->where('pb.isExpired', 0);		
		$query_packagesOfBusiness = $this->db->get();
		$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();
		if(!empty($res_packagesOfBusiness)){
			$arrReturn['allData'] = $res_packagesOfBusiness;
			foreach($res_packagesOfBusiness as $value){
				array_push($arrReturn['ignoreIds'],$value['packageId']);
			}
		}
		return $arrReturn;
	}
	
	
	public function performChangePassword($data){		
		$arrReturn = array();
		$password = $data['password'];
		$confirmPassword = $data['confirmPassword'];
		if($password == $confirmPassword){			
			$password = md5($password);
			$LoggedInType = $_SESSION['LoggedInType'];			
			$AdminLoggedIn = $_SESSION['adminId'];
			if($LoggedInType == "Admin"){
				$data = array(	'password' 	=> $password	);
				$this->db->where('id', $AdminLoggedIn);
				$this->db->update('admin', $data); 				
			}elseif($LoggedInType == "Staff"){
				$data = array(	'staffMembersPassword' 	=> $password	);
				$this->db->where('staffMembersId', $AdminLoggedIn);
				$this->db->update('staffMembers', $data); 			
			}			 		
			return "Updated";
		}else{
			return false;
		}
		
	}
	
	public function removePackageOfUser($packageId,$businessId){
			
		$this->db->where('packageId', $packageId);
		$this->db->where('businessId', $businessId);
		if($this->db->delete('packagesOfBusiness')){
			return "DELETED";
		}else{
			return "NTDELETED";
		} 
	}
	
}
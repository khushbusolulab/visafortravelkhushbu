<?php

class Api_v1_model extends CI_Model {

    function __construct() {
// Call the Model constructor
        parent::__construct();
    }
    
    function checkAccessCode($userId, $accessCode) {
        $this->db->where("userId", $userId);
        $this->db->where("accessCode", $accessCode);
        $this->db->where("isDeleted !=", 1);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    function normalRegiToDeviceId($deviceId){
            $result = $this->db->query("SELECT GROUP_CONCAT(emailId) as emails
                            FROM users u
                            JOIN userdetails ud on ud.userId=u.userId
                            WHERE ud.deviceId='$deviceId'
                    ");
            return $result->result();

    }

    function getIdsFromName($cityName, $stateName, $countryName) {
        $query = $this->db->select('c.*,s.*,cn.*')
                ->from('city c')
                ->join('states s', 's.stateId = c.stateId', 'left')
                ->join('country cn', 'cn.countryId = s.countryId', 'left')
                ->where('c.cityName', $cityName)
                ->where('s.stateName', $stateName)
                ->where('cn.countryName', $countryName)
                ->get();

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }

    function getGoldSpotPosition($cityId) {
        $query = $this->db->select('fp.*,pb.*')
                ->from('featuredPlacement fp')
                ->join('packagesOfBusiness pb', 'pb.businessId = fp.businessId', 'left')
                ->where("cityId", $cityId)
                ->where('pb.packageId', 1)
                ->get();
        
        /*$this->db->where("cityId", $cityId);
        $this->db->order_by('placementOrder', 'asc');
        $query = $this->db->get("featuredPlacement");*/
        //echo $this->db->last_query();exit;		
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }

    function getBanner($businessId) {
        $query = $this->db->select('b.*,bi.*')
                ->from('businesses b')
                ->join('businessesImage bi', 'b.businessId = bi.businessId', 'left')
                ->where("b.businessId", $businessId)
                ->where("bi.imageType", "Cover")
                ->get();    
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function getBannerImage($businessId){ //not in use 23-11-16
        $this->db->where("businessId", $businessId);
        $this->db->where("imageType","Cover");
        $query = $this->db->get("businessesImage");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;    
    }

    function getRestNearByGoldBanner($remaining, $latitude, $longitude) {
        $query = $this->db->query("SELECT b.*,pb.*,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
                FROM businesses AS b
                INNER JOIN packagesOfBusiness as pb ON pb.businessId=b.businessId
                INNER JOIN packages as p ON p.packageId=pb.packageId 
                where p.packageName = 'Gold' and b.businessId NOT IN (SELECT businessId FROM featuredPlacement)
                ORDER BY distance limit 0,$remaining");
        //echo $this->db->last_query();//exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
        
    }
    
    
    function getCategory(){
        $query = $this->db->get("category");
        return $query->result();
    }
    
    function getCategoryById($categoryId){
        $this->db->where("categoryId",$categoryId);
        //$this->db->limit($dataCount, 0);
        $query = $this->db->get("category");
        return $query->result();
    }
    
    function getSliverSonsers($catId, $latitude, $longitude){
        $query = $this->db->query("SELECT b.*,pb.*,c.cityName,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
                FROM businesses AS b
                INNER JOIN packagesOfBusiness as pb ON pb.businessId=b.businessId
                INNER JOIN packages as p ON p.packageId=pb.packageId 
                INNER JOIN businessCategory as bc ON bc.businessId=b.businessId 
                INNER JOIN city as c ON c.cityId=b.cityId 
                where p.packageName = 'Silver' and bc.categoryId = $catId
                ORDER BY distance limit 0,10");
        //echo $this->db->last_query();//exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function getBronze($catId, $latitude, $longitude,$dataCount){
        $query = $this->db->query("SELECT b.*,pb.*,c.cityName,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
                FROM businesses AS b
                INNER JOIN packagesOfBusiness as pb ON pb.businessId=b.businessId
                INNER JOIN packages as p ON p.packageId=pb.packageId 
                INNER JOIN businessCategory as bc ON bc.businessId=b.businessId 
                INNER JOIN city as c ON c.cityId=b.cityId 
                where p.packageName = 'Bronze' and bc.categoryId = $catId
                ORDER BY distance limit 0,$dataCount");
        //echo $this->db->last_query();//exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    
    function getLatLongbaseBusiness($categoryId, $latitude, $longitude, $limit, $perPage){
        $query = $this->db->query("SELECT b.*,c.cityName,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
                FROM businesses AS b
                INNER JOIN businessCategory as bc ON bc.businessId=b.businessId 
                INNER JOIN city as c ON c.cityId=b.cityId 
                where bc.categoryId = $categoryId
                ORDER BY distance limit $limit,$perPage");
        //echo $this->db->last_query();//exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function getServiceImages($businessId){
        $this->db->where("businessId", $businessId);
        $this->db->where("imageType","Service");
        $query = $this->db->get("businessesImage");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;    
    }
    
    function getOpeningHours($businessId,$day){
        $this->db->where("businessId", $businessId);
        $this->db->where("day",$day);
        $query = $this->db->get("openingHours");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;    
    }
    
    function getServiceTags($businessId){
        $this->db->where("businessId", $businessId);
        $query = $this->db->get("services");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0; 
    }
    
    function getSearchResult($searchKeyword,$latitude, $longitude, $limit, $perPage){
        $query = $this->db->query("SELECT b.*,c.cityName,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
        FROM businesses AS b
        INNER JOIN businessCategory as bc ON bc.businessId=b.businessId 
        INNER JOIN city as c ON c.cityId=b.cityId 
        where $searchKeyword
        ORDER BY distance limit $limit,$perPage");
        //echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function getFilters($where,$cond,$catVal,$latitude, $longitude, $limit, $perPage){
        $query = $this->db->query("SELECT b.*,c.cityName,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
        FROM businesses AS b
        INNER JOIN businessCategory as bc ON bc.businessId=b.businessId 
        INNER JOIN city as c ON c.cityId=b.cityId
        INNER JOIN openingHours as oh ON oh.businessId = b.businessId
        $cond
        where $catVal $where
        ORDER BY distance limit $limit,$perPage");
        //echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function reportError($userId,$phoneNumber,$address,$outletClosed,$serviceIncorrect,$message){
        
        
        
        $this->db->where("userId", $userId);
        $query = $this->db->get("reportError");

        if ($query->num_rows() > 0) {
            $data = array(
                'phoneNumber' => $phoneNumber,
                'address' => $address,
                'outletClosed' => $outletClosed,
                'serviceIncorrect' => $serviceIncorrect,
                'message' => $message
            );
            $this->db->where("userId", $userId);
            $this->db->update('reportError', $data);
            return $this->db->affected_rows();
        } else {
            $data = array(
                'userId' => $userId,
                'phoneNumber' => $phoneNumber,
                'address' => $address,
                'outletClosed' => $outletClosed,
                'serviceIncorrect' => $serviceIncorrect,
                'message' => $message
            );

            $this->db->insert('reportError', $data);
            $insert_id = $this->db->insert_id();

            return $insert_id;
        }
        
    }
    
    
    function viewBusinessById($businessId, $latitude, $longitude){
        $query = $this->db->query("SELECT b.*,c.cityName,( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( b.latitude ) ) * cos( radians( b.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( b.latitude ) ) ) ) AS distance
                FROM businesses AS b
                INNER JOIN businessCategory as bc ON bc.businessId=b.businessId 
                INNER JOIN city as c ON c.cityId=b.cityId 
                where b.businessId = $businessId
                ORDER BY distance");
        //echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function getWorkingHoursById($businessId){
        $this->db->where("businessId", $businessId);
        $query = $this->db->get("openingHours");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;    
    }
    
    function getReviewByBusinessId($businessId,$limit,$perPage){
        $query = $this->db->query("SELECT *,count(reviewId) as total,DATEDIFF(DATE(NOW()), reviewDateTime)as ago
        FROM review
        where businessId =$businessId and isDeleted = 0
        limit $limit,$perPage");
        //echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function checkBookmark($userId, $businessId){
        $this->db->where("userId", $userId);
        $this->db->where("businessId", $businessId);
        $query = $this->db->get("bookmarks");
        return $query->num_rows();
    }
    
    function bookmarkBusiness($businessId,$cityId,$stateId,$countryId,$address,$latitude,$longitude,$checkBookmark,$userId=0){
        if ($checkBookmark > 0) {
            $this->db->where("userId", $userId);
            $this->db->where("businessId", $businessId);
            $this->db->delete('bookmarks');
            return $this->db->affected_rows();
        } else {
            $data = array(
                'userId' => $userId,
                'businessId' => $businessId,
                'cityId' => $cityId,
                'stateId' => $stateId,
                'countryId' => $countryId,                
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'bookmarkedDate'=>date('Y-m-d H:i:s')
            );

            $this->db->insert('bookmarks', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }
    
    
    function checkReview($userId, $businessId){
        $this->db->where("userId", $userId);
        $this->db->where("businessId", $businessId);
        $query = $this->db->get("review");
        return $query->num_rows();
    }
    
    function reviewBusiness($businessId,$rating,$rateCommnets,$cityId,$stateId,$countryId,$address,$latitude,$longitude,$checkBookmark,$userId,$reviewId=0){
        if ($checkBookmark > 0) {
            
            $data = array(
                'userId' => $userId,
                'businessId' => $businessId,
                'rating'=>$rating,
                'rateComments'=>$rateCommnets,
                'cityId' => $cityId,
                'stateId' => $stateId,
                'countryId' => $countryId,                
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'isEditedDate'=>date('Y-m-d H:i:s'),
                'isEdited'=>1
            );
            
            $this->db->where("reviewId", $reviewId);
            $this->db->update('review',$data);
            return $this->db->affected_rows();
        } else {
            $data = array(
                'userId' => $userId,
                'businessId' => $businessId,
                'rating'=>$rating,
                'rateComments'=>$rateCommnets,
                'cityId' => $cityId,
                'stateId' => $stateId,
                'countryId' => $countryId,                
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'createdDate'=>date('Y-m-d H:i:s')
            );

            $this->db->insert('review', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }
    function getReviewById($reviewId){
        $query = $this->db->select('r.*')
                ->from('review r')
                ->where("r.reviewId", $reviewId)
                ->get();
        //echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function deleteReview($reviewId){
        $data = array(
            'isDeleted'=>1
        );
            
        $this->db->where("reviewId", $reviewId);
        $this->db->update('review',$data);
        return $this->db->affected_rows();
    }
    
    function getBookmarkListing($userId,$limit,$perPage){
        
        $query = $this->db->select('bs.*,b.*,bi.*')
                ->from('bookmarks b')
                ->join('businesses bs', 'b.businessId = bs.businessId', 'left')
                ->join('businessesImage bi', 'bs.businessId = bi.businessId', 'left')
                ->where("bi.imageType", "Cover")
                ->where("userId",$userId)
                ->limit($perPage,$limit)
                ->get();
        //echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }
    
    function checkFeedback($userId){
        $this->db->where("userId", $userId);
        $query = $this->db->get("feedback");
        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    function addFeedback($userId,$feedbackComments){
        $data = array(
            'userId' => $userId,
            'feedbackComments' => $feedbackComments,
        );

        $this->db->insert('feedback', $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }
    
    function getEmailLinkedToDeviceId($deviceId,$emailId){
        $result = $this->db->query("SELECT emailId
                        FROM socialLogin usl
                        JOIN userdetails ud on ud.userId=usl.userId
                        WHERE ud.deviceId='$deviceId' and ud.deviceId = 0 and usl.emailId = '$emailId'
                ");
        
        if ($result->num_rows() > 0)
            return $result->result();
        else
            return 0;

        //exit;
        //return $result->result();
			
	}
        
     function updateLastLogin($loginCount, $userId, $verificationCode) {
        $data = array(
            'loginCounts' => $loginCount,
            'verificationCode' => $verificationCode,
        );
        $this->db->where("userId", $userId);
        $this->db->update('users', $data);
        return $this->db->affected_rows();
    }
    
     function updateToken($device_id, $registration_id, $user_id, $deviceType) {
        $data = array(
            'deviceToken' => $registration_id,
            'deviceId' => $device_id,
            'deviceType' => $deviceType,
            'userId' => $user_id
        );

        $this->db->where("deviceId", $device_id);
	$this->db->where("userId", $user_id);
        $query = $this->db->get("userdetails");
		
        if ($query->num_rows() > 0) {
            $this->db->where("deviceId", $device_id);
            $this->db->where("userId", $user_id);
            $this->db->update('userdetails', $data);
			//echo $this->db->last_query();exit;
            return $this->db->affected_rows();
        } else {
            $this->db->insert('userdetails', $data);
			//echo $this->db->last_query();exit;
            return $this->db->affected_rows();
        }
		
		
    }
    
    function deleteDevice($deviceToken){
        $data = array(
            'isDeleted' => 1,
        );
        $this->db->where("deviceToken", $deviceToken);
        $this->db->update('userdetails', $data);
        return $this->db->affected_rows();
    }
    
    
    function addSocialLogin($data, $key,$type,$emailId) {
        //print_r($data);echo $key;echo $type;
        $data2 = array(
            'userId' => $data['userId'],
            $key=>$type,
            'emailId' => $emailId
        );
        $this->db->insert('socialLogin', $data2);
		//echo $this->db->last_query();exit;
        return $this->db->affected_rows();
    }
    
    function checkFacebookId($emailId,$facebookId){
        $this->db->where("emailId", $emailId);
        $this->db->where("facebookId", $facebookId);
        $query = $this->db->get("socialLogin");
        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    function Registration($email, $mobile, $password, $profilePicture, $verificationCode, $longitude, $latitude, $userName) {
        $data = array(
            'emailId' => $email,
            //'created' => date('Y-m-d h:i:s'),
            //'DOB' => $DOB,
            'mobile' => $mobile,
            'password' => md5($password),
            'profilePicture' => $profilePicture,
            'verificationCode' => $verificationCode,
            'isVarified' => 0, //to be removed
            'isDeleted' => 0, //to be removed
            'longitude' => $longitude,
            'latitude' => $latitude,
            'fullName' => $userName
        );

        $this->db->insert('users', $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function checkVerificationCode($verificationCode, $userId) {
        $this->db->where("userId", $userId);
        $this->db->where("verificationCode", $verificationCode);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    function deleteVerificationCode($verificationCode, $userId) {

        $data = array(
            'isVarified' => 1,
            'verificationCode' => ""
        );
        $this->db->where("userId", $userId);
        $this->db->where("verificationCode", $verificationCode);
        $this->db->update('users', $data);
        return $this->db->affected_rows();
    }

    function Signup($signUp, $deviceToken, $deviceId, $deviceType) {
        $data = array(
            'userId' => $signUp,
            //'created' => date('Y-m-d h:i:s'),
            'deviceToken' => $deviceToken,
            'deviceId' => $deviceId,
            'deviceType' => $deviceType
        );

        $this->db->insert('userdetails', $data);
        return $this->db->insert_id();
    }

    function CheckEmailExsist($email) {

        $where = "usl.emailId='$email' OR u.emailId='$email'";

        $query = $this->db->select('u.*,usl.*')
                ->from('users u')
                ->join('socialLogin usl', 'usl.userId = u.userId', 'left')
                ->where($where)
                ->get();

        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    function CheckEmailIdExist($email) {
        /* $this->db->where("email", $email);
          $query = $this->db->get("users"); */

        $where = "usl.emailId='$email' OR u.emailId='$email'";

        $query = $this->db->select('u.*,usl.*')
                ->from('socialLogin usl')
                ->join('users u', 'usl.userId = u.userId', 'left')
                ->where($where)
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            $this->db->where("emailId", $email);
            $query = $this->db->get("users");
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return 0;
            }
        }
    }

    function login($email, $password) {
        $where = "usl.emailId='$email' OR u.emailId='$email'";



        $query = $this->db->select('u.*,usl.emailId')
                ->from('users u')
                ->join('socialLogin usl', 'usl.userId = u.userId', 'left')
                ->where($where)
                ->where("password", MD5($password))
                ->where("isBlocked", 0)
                ->get();

        //echo $this->db->last_query();exit;		
        //$query = $this->db->query("SELECT * FROM users WHERE email='" . $email . "' AND password ='" . MD5($password) . "' AND isBlocked = 0");
        if ($query->num_rows() > 0) {
            $result = $query->result();
            if ($result[0]->isVarified == 1) {
                $this->db->query("UPDATE users SET loginCount= loginCount + 1 WHERE emailId='" . $email . "' AND password ='" . MD5($password) . "'");
                if ($query->num_rows() > 0) {
                    return $query->result();
                } else
                    return '0';
            } else {
                return 'verify';
            }
        } else {
            return '0';
        }
    }

    function getUserId($userId) {
        $this->db->where("emailId", $userId);
        $query = $this->db->get("users");
        return $query->result();
    }

    function setNewPassword($id, $password) {
        $data = array(
            'password' => md5($password)
        );

        $this->db->where('userId', $id);
        $this->db->update('users', $data);
        return $this->db->affected_rows();
    }

    function ifExistUserId($userId) {
        $this->db->where("userId", $userId);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    function checkIdExist($facebookId) {
        $this->db->where("userSocialId", $facebookId);
        $this->db->where("socialLoginName", 'facebook');
        $query = $this->db->get("usersociallogin");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }

    function getUserById($userId) {
        $this->db->where("userId", $userId);
        $query = $this->db->get("users");
        return $query->result();
    }

    function generateRandomString($length = 12) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
//return $randomString;
        return $this->checkAccessCodeExist($randomString);
    }
    
    function checkAccessCodeExist($accessCode) {
        $this->db->where("accessCode", $accessCode);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0) {
            $this->generateRandomString();
        } else {
            return $accessCode;
        }
    }


    function updateUser($userId) {
        $this->db->where('userId', $userId);
        $this->db->set('loginCount', 'loginCount+1', FALSE);
        $this->db->update('users');
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else
            return 0;
    }

    function registersocialLogin($data) {
        $accessCode = $this->generateRandomString();
        $data2 = array(
            'emailId' => $data['emailId'],
            'created' => date('Y-m-d h:i:s'),
            'accessCode' => $accessCode,
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'isDeleted' => 0,
            'isVarified' => 1
        );

        $this->db->insert('users', $data2);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

    function updateAccessCode($accessCode, $userId) {
        $query = $this->db->query("UPDATE users SET accessCode= '" . $accessCode . "' WHERE userId= '" . $userId . "'");
        return $this->db->affected_rows();
    }

    function getUserData($userId) {
        $sql = $this->db->select('u.*,ud.*')
                ->from('users u')
                ->join('userdetails ud', 'ud.userId = u.userId', 'left')
                ->where('u.userId', $userId)
                ->get();
        return $sql->result();
    }

    function getCurrentUserLatLong($userId) {
        $this->db->where("userId", $userId);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }

    function logOut($accessCode, $userId, $deviceId) {
        $this->db->where("userId", $userId);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0) {
            $res = $query->result();
            $loginCount = $res[0]->loginCounts;
            if ($loginCount == 1) {
                $data = array(
                    'loginCounts' => $loginCount - 1,
                        //  'accessCode' => "",
                );
                $this->db->where("userId", $userId);
                $this->db->update('users', $data);
                $affected_rows = $this->db->affected_rows();

                $data2 =array(
                    'isDeleted' => 1
                );
                $this->db->where('userId', $userId);
                $this->db->where('deviceId', $deviceId);
                //$this->db->delete('userdetails');
                $this->db->update('userdetails', $data2);

                return $affected_rows;
            } else {
                $data = array(
                    'loginCounts' => $loginCount - 1
                );
                $this->db->where("userId", $userId);
                $this->db->update('users', $data);
                $affected_rows = $this->db->affected_rows();

                $data2 =array(
                    'isDeleted' => 1
                );
                $this->db->where('userId', $userId);
                $this->db->where('deviceId', $deviceId);
                //$this->db->delete('userdetails');
                $this->db->update('userdetails', $data2);

                return $affected_rows;
            }
        } else
            return 0;
    }

    function updateLatLong($userId, $latitude, $longitude) {
        $data = array(
            'latitude' => $latitude,
            'longitude' => $longitude,
        );
        $this->db->where("userId", $userId);
        $this->db->update('users', $data);
        return $this->db->affected_rows();
    }

    function getidByEmail($email) {
        $where = "usl.emailId='$email' OR u.emailId='$email'";
        $query = $this->db->select('u.*,usl.emailId')
                ->from('users u')
                ->join('socialLogin usl', 'usl.userId = u.userId', 'left')
                ->where($where)
                //->where('usl.Email', $email)
                ->get();


        //  $this->db->where("email", $email);
        //  $query = $this->db->get("users");
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return 0;
    }

    function updateProfile($data, $imagePath) {
        $data2 = array(
            'profilePicture' => $imagePath,
            //'profilePictureThumb' => $imagePathThumb,
            'fullName' => $data['userName'],
                /* 'personalWebsite'=>$data['personalWebsite'],
                  'youtubeChannel'=>$data['youtubeChannel'],
                  'twitterPage'=>$data['twitterPage'] */
        );
        $this->db->where("userId", $data['userId']);
        $this->db->update('users', $data2);
        $this->db->trans_complete();
        if ($this->db->affected_rows() == '1') {
            return 1;
        } else {
// any trans error?
            if ($this->db->trans_status() === FALSE) {
                return 0;
            }
            return 1;
        }
    }

    function getProfile($userId) {
        $this->db->where("userId", $userId);
        $query = $this->db->get("users");
        return $query->result();
    }

    function getRepRating($userId) {
        $sql = $this->db->select('u.*,uf.*')
                ->from('users u')
                ->join('userfeed uf', 'uf.userId = u.userId', 'left')
                ->where('u.userId', $userId)
                ->get();
        $activityFeed = $sql->num_rows();

        $sql1 = $this->db->select('u.*,i.*')
                ->from('users u')
                ->join('inspiration i', 'i.userId = u.userId', 'left')
                ->where('u.userId', $userId)
                ->get();
        $inspiration = $sql1->num_rows();

        $repRating = $activityFeed + $inspiration;
        return $repRating;
    }

   

    function deleteAccount($userId) {
        $this->db->delete('users', array('userId' => $userId));
        $this->db->delete('userdetails', array('userId' => $userId));
        $this->db->delete('userfeed', array('userId' => $userId));
        $this->db->delete('userfilters', array('userId' => $userId));
        $this->db->delete('socialLogin', array('userId' => $userId));
        $this->db->delete('feedduration', array('userId' => $userId));
        $this->db->delete('feedevents', array('userId' => $userId));
        $this->db->delete('feedrating', array('userId' => $userId));
        $this->db->delete('followers', array('userId' => $userId));
        $this->db->delete('followers', array('followerId' => $userId));
        $this->db->delete('followerstats', array('userId' => $userId));
        $this->db->delete('inspiration', array('userId' => $userId));
        $this->db->delete('inspirationComment', array('userId' => $userId));
        $this->db->delete('inspirationComment', array('commenterId' => $userId));
        $this->db->delete('votes', array('userId' => $userId));
        $this->db->delete('userprivacy', array('userId' => $userId));
        $this->db->delete('blockuser', array('userId' => $userId));
        $this->db->delete('blockuser', array('blockedUser' => $userId));
        $this->db->delete('userrating', array('userId' => $userId));
        $this->db->delete('userrating', array('ratedUserId' => $userId));
        $this->db->delete('usersociallogin', array('userId' => $userId));
        return 1;
    }

    function saveUserToken($userId, $token) {
        $data = array(
            'userId' => $userId,
            'token' => $token,
            'createdDate' => date('Y-m-d H:i:s'),
            'updatedDate' => date('Y-m-d H:i:s')
        );
        $this->db->insert('tokenaccess', $data);
        return $this->db->insert_id();
    }

    function updateUserToken($userId, $token) {
        $data = array(
            'status' => 1,
            'updatedDate' => date('Y-m-d H:i:s')
        );
        $this->db->where('userId', $userId);
        $this->db->where('token', $token);
        $this->db->update('tokenaccess', $data);
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else
            return 0;
    }

    function getTokenDetails($token) {
        $this->db->where('token', $token);
        $query = $this->db->get('tokenaccess');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getStaticPages($page) {
        $this->db->where('page_key', $page);
        $query = $this->db->get('staticpages');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getEmailTemplate($templateId) {
        $this->db->where('template_id', $templateId);
        $query = $this->db->get('emailtemplates');
        return $query->result_array();
    }

    function getUserDeviceTokens($userId) {
        $query = $this->db->query("SELECT 
        (SELECT GROUP_CONCAT(DISTINCT deviceToken) FROM userdetails WHERE deviceType='android' AND userId=$userId) AS android,
        (SELECT GROUP_CONCAT(DISTINCT deviceToken) FROM userdetails WHERE deviceType='IOS' AND userId=$userId) AS ios");
        return $query->result_array();
    }

    function checkDeviceIdExist($deviceId) {
        $sql = $this->db->select('u.*,ud.*')
                ->from('userdetails ud')
                ->join('users u', 'ud.userId = u.userId', 'right') /* left */
                ->where("ud.deviceId", $deviceId)
                //->where("u.email", $emailId)
                ->get();
        //echo $this->db->last_query();exit;		
        if ($sql->num_rows() > 0)
            return $sql->result();
        else
            return 0;
    }

    function updateRegistrationData($password, $userId) {
        $data = array(
            'password' => md5($password)
        );
        $this->db->where('userId', $userId);
        $result = $this->db->update('users', $data);
        //echo $this->db->last_query();exit;
        $this->db->trans_complete();
        if ($this->db->affected_rows() == '1') {
            return 1;
        } else {
// any trans error?
            if ($this->db->trans_status() === FALSE) {
                return 0;
            }
            return 1;
        }
    }

    function checkEmailInSocialLogin($email) {
        $this->db->where("emailId", $email);
        $query = $this->db->get("socialLogin");
        return $query->result();
    }

    function getAvgRating($userId) {
        $query = $this->db->query("SELECT AVG(rating) AS avgRating
                FROM userrating
                WHERE userId=$userId");

        //  echo $this->db->last_query();
        return $query->result();
    }

    function getNotificationSettings($userId) {
        $this->db->select('*');
        $this->db->where('userId', $userId);
        $query = $this->db->get('users');
        return $query->result();
    }

}

?>

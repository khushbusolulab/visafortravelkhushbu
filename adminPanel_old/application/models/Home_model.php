<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Home_model extends CI_Model
{
	public function checkUserSession(){
		//echo "<pre>";print_r($_SESSION);exit;
		if(isset($_SESSION['businessId'])){
			$businessId = $_SESSION['businessId'];		
		}else{
			$businessId = 0;			
		}
		if(isset($_SESSION['LoggedIn'])){
			$LoggedIn = $_SESSION['LoggedIn'];		
		}else{
			$LoggedIn = 0;			
		}	
		if(isset($businessId) && is_numeric($businessId) && $businessId> 0 && isset($LoggedIn) && $LoggedIn == 1){
			return "logeedIn";
		}
		return "NotLogged";	
	}
	
	
	public function registerBusiness($data){		 
		$password = md5($data['password']);
		$data_to_insert = array(
			'businessMerchantName'		=>	$data['businessMerchantName'],
			'businessName'				=>	$data['businessName'],
			'businessEmail'				=>	$data['businessEmail'],
			'businessPhone'				=>	$data['businessPhone'],
			'password'					=>	$password,			
			'isPublished'				=>	"0",			
			'createdOn'					=>	TODAY
		);		
		if($this->db->insert('businesses', $data_to_insert)){
			return $this->db->insert_id();					
		}else{
			return false;
		} 
	}
	
	
	public function checkEmailExists($email){
		$this->db->select('businessEmail');
		$this->db->from('businesses');
		$this->db->where('businessEmail', $email);		
		$this->db->where('isDeleted', 0);		
		$query = $this->db->get();
		$rescheckEmailExists = $query->result_array();
		if(empty($rescheckEmailExists)){
			return "1";
		}else{
			return "0";
		}
	}
	
	public function businessMerchantLogin($data){
		$password = md5($data['passwordLogin']);
		$this->db->select('*');
		$this->db->from('businesses');
		$this->db->where('businessEmail', $data['businessEmailLogin']);		
		$this->db->where('password', $password);		
		$this->db->where('isDeleted', 0);		
		$query = $this->db->get();
		$resbusinessMerchantLogin = $query->result_array();		
		
		if(!empty($resbusinessMerchantLogin)){	
			if($resbusinessMerchantLogin[0]['isBlocked'] == 1){
				return "2";
			}elseif($resbusinessMerchantLogin[0]['isBlocked'] == 0){
				$resbusinessMerchantLogin = $resbusinessMerchantLogin[0];
				$this->session->set_userdata(array(
				'businessId'       				=> $resbusinessMerchantLogin['businessId'],				
				'businessMerchantName'       	=> $resbusinessMerchantLogin['businessMerchantName'],				
				'businessName'       			=> $resbusinessMerchantLogin['businessName'],				
				'businessEmail'       			=> $resbusinessMerchantLogin['businessEmail'],
				'businessPhone'       			=> $resbusinessMerchantLogin['businessPhone'],
				'LoggedIn'       				=> TRUE
			));				
			return "1";	
			}		
		}else{
			return "0";
		}
	}
	
	public function getBusinessDetailsFromEmail($email){
		$this->db->select('*');
		$this->db->from('businesses');
		$this->db->where('businessEmail', $email);		
		$this->db->where('isDeleted', 0);		
		$query = $this->db->get();
		$getBusinessDetailsFromEmail = $query->result_array();
		if(!empty($getBusinessDetailsFromEmail)){
			$getBusinessDetailsFromEmail = $getBusinessDetailsFromEmail[0];
			return $getBusinessDetailsFromEmail;
		}else{
			return false;
		}
	}
	
	
	public function getcategoryValues(){
		$arrReturn = array();
		$this->db->select('
							categoryId,
							categoryName
						');
		$this->db->from('category');		
		$query_category = $this->db->get();
		$res__category = $query_category->result_array();
		if(!empty($res__category)){
			$arrReturn = $res__category;			
		}
		return $arrReturn;
	}
	
	
	
	public function getcountryValues(){
		$arrReturn = array();
		$this->db->select('
							countryId,
							countryName
						');
		$this->db->from('country');		
		$query_country = $this->db->get();
		$res__country = $query_country->result_array();
		if(!empty($res__country)){
			$arrReturn = $res__country;			
		}
		return $arrReturn;
	}
	
	
	public function getStateList($country_id){
		$arrReturn = array();
		$this->db->select('
							stateId,
							stateName
						');
		$this->db->from('states');
		$this->db->where('countryId', $country_id);			
		$query_state = $this->db->get();
		$res__state = $query_state->result_array();
		if(!empty($res__state)){
			$arrReturn = $res__state;			
		}
		return $arrReturn;
	}
	
	
	
	public function getCityList($state_id){
		$arrReturn = array();
		$this->db->select('
							cityId,
							cityName
						');
		$this->db->from('city');
		$this->db->where('stateId', $state_id);			
		$query_city = $this->db->get();
		$res__city = $query_city->result_array();
		if(!empty($res__city)){
			$arrReturn = $res__city;			
		}
		return $arrReturn;
	}
	
	
	
	public function AddProfileData($data){
		
		//Code to get lat long from address
		$dlocation = $data['businessAddress1'].",".$data['businessAddress2'].",".$data['businessArea'].",".$data['hidCityName'].",".$data['hidStateName'].",".$data['hidCountryName'];		
		$address = urlencode($dlocation);         
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
        $output= json_decode($geocode);
       if(!empty($output->results)){
			$latitude = $output->results[0]->geometry->location->lat;
			$longitude = $output->results[0]->geometry->location->lng;	
		}else{
			$dlocation = $data['hidCityName'];				
			$address = urlencode($dlocation);         		
			$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
			$output= json_decode($geocode);		
			$latitude = $output->results[0]->geometry->location->lat;
			$longitude = $output->results[0]->geometry->location->lng;	
		}  
		
		// Code to enter data into "services" table
			$arrServices = explode(",",$data['businessServices']);
			foreach($arrServices as $key=>$value){
				$data_services = array(
					'businessId' 		=> $_SESSION['businessId'] ,
					'serviceTag' 		=> $value 
				);				
				$this->db->insert('services', $data_services);
			}			
			//Code to insert Data into "openinghours"
			$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
			if(!empty($data['weekdays'])){
				foreach($data['weekdays'] as $key_days => $value_days){					
					$data_openinghours = array(
						'businessId' 		=> $_SESSION['businessId'],
						'day' 				=> $days[$key_days], 
						'openingHours' 		=> date("H:i:s",strtotime($data['fromTime'][$key_days])), 
						'closingHours' 		=> date("H:i:s",strtotime($data['toTime'][$key_days])), 
					);					
					$this->db->insert('openingHours', $data_openinghours);
				}				
			}
				//Inserting data into "businesscategory"
				if(!empty($data['businessCategory'])){
					foreach($data['businessCategory'] as $value_category){
						$data_businesscategory = array(						
							'categoryId' 		=> $value_category ,
							'businessId' 		=> $_SESSION['businessId']							
						);					
						$this->db->insert('businessCategory', $data_businesscategory);
					}
				}
		$data_businesses = array(
		   'businessName' 			=> $data['businessName'],
		   'businessPhone' 			=> $data['businessPhone'],
		   'businessEmail' 			=> $data['businessEmail'],
		   'businessAddress1' 		=> $data['businessAddress1'],
		   'businessAddress2' 		=> $data['businessAddress2'],
		   'businessArea' 			=> $data['businessArea'],
		   'countryId' 				=> $data['country'],
		   'stateId' 				=> $data['state'],
		   'cityId' 				=> $data['city'],
		   'latitude' 				=> $latitude,
		   'longitude' 				=> $longitude,
		   'isPublished' 			=> 1,
		   'updatedOn' 				=> TODAY,
		);		
		$this->db->where('businessId', $_SESSION['businessId']);
		$this->db->update('businesses', $data_businesses); 
		return "Inserted";
	}




	public function EditProfileData($data){
		//echo "<pre>";print_r($data);exit;
		$recid = 0;
		if(isset($data['recid'])){
			$recid = $data['recid'];
		}else{
			$recid = $_SESSION['businessId'];
		}
		//Code to get lat long from address
		$dlocation = $data['businessAddress1'].",".$data['businessAddress2'].",".$data['businessArea'].",".$data['hidCityName'].",".$data['hidStateName'].",".$data['hidCountryName'];				
		$address = urlencode($dlocation);         		
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
        $output= json_decode($geocode);		
		if(!empty($output->results)){
			$latitude = $output->results[0]->geometry->location->lat;
			$longitude = $output->results[0]->geometry->location->lng;	
		}else{
			$dlocation = $data['hidCityName'];				
			$address = urlencode($dlocation);         		
			$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
			$output= json_decode($geocode);		
			$latitude = $output->results[0]->geometry->location->lat;
			$longitude = $output->results[0]->geometry->location->lng;	
		}        		
		
		// Code to enter data into "services" table
			//Code to delete Old service tags
			$this->db->where('businessId', $recid);
			$this->db->delete('services'); 
			//Code to insert new data
			$arrServices = explode(",",$data['businessServices']);			
			foreach($arrServices as $key=>$value){
				$data_services = array(
					'businessId' 		=> $recid ,
					'serviceTag' 		=> $value 
				);				
				$this->db->insert('services', $data_services);
			}			
			//Code to insert Data into "openinghours"
			
			//Code to delete old data from "openinghours" table
			$this->db->where('businessId', $recid);
			$this->db->delete('openingHours'); 
			//Code to insert new data
			$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
			if(!empty($data['weekdays'])){
				foreach($data['weekdays'] as $key_days => $value_days){					
					$data_openinghours = array(
						'businessId' 		=> $recid,
						'day' 				=> $days[$key_days], 
						'openingHours' 		=> date("H:i:s",strtotime($data['fromTime'][$key_days])), 
						'closingHours' 		=> date("H:i:s",strtotime($data['toTime'][$key_days])), 
					);
					$this->db->insert('openingHours', $data_openinghours);
				}
			}				
				//Inserting data into "businesscategory"
				//Code to delete old data from "businesscategory" table
					$this->db->where('businessId', $recid);
					$this->db->delete('businessCategory'); 
					//Code to enter new data
				if(!empty($data['businessCategory'])){
					foreach($data['businessCategory'] as $value_category){
						$data_businesscategory = array(						
							'categoryId' 		=> $value_category ,
							'businessId' 		=> $recid							
						);					
						$this->db->insert('businessCategory', $data_businesscategory);
					}
				}
		$data_businesses = array(
		   'businessName' 			=> $data['businessName'],
		   'businessPhone' 			=> $data['businessPhone'],
		   'businessEmail' 			=> $data['businessEmail'],
		   'businessAddress1' 		=> $data['businessAddress1'],
		   'businessAddress2' 		=> $data['businessAddress2'],
		   'businessArea' 			=> $data['businessArea'],
		   'countryId' 				=> $data['country'],
		   'stateId' 				=> $data['state'],
		   'cityId' 				=> $data['city'],
		   'latitude' 				=> $latitude,
		   'longitude' 				=> $longitude,		   
		   'updatedOn' 				=> TODAY,
		);		
		$this->db->where('businessId', $recid);
		$this->db->update('businesses', $data_businesses); 
		return "Updated";
	}
	
	
	
	public function addOfficePicture(){		
		$arrReturn = array();
		//Code to check any image is already uploaded or not 
		$check = $this->getOfficePictures($_SESSION['businessId']);
		if(!empty($check)){			
			$image_name = $check['images'];
			$this->db->where('images', $image_name);
			$this->db->where('businessId', $_SESSION['businessId']);
			$this->db->where('imageType', "Office");
			$this->db->delete('businessesImage'); 
			$delete_file = "./uploads/office_pictures/".$image_name;
			@unlink($delete_file);
		}
		if(isset($_FILES['croppedImage']) && $_FILES['croppedImage']['name'] != "" && $_FILES['croppedImage']['error'] != "4"){
			$microtime = microtime();
			$imagePath = date("YmdHis");
			$imagePath = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $imagePath) . '.jpg';
			$target_dir2 = './uploads/office_pictures/'.$_SESSION['businessId']."_". $imagePath;
			$file_name = $_SESSION['businessId']."_". $imagePath;
			$arrReturn['croppedImage'] = base_url()."uploads/office_pictures/".$_SESSION['businessId']."_". $imagePath;
			if(move_uploaded_file($_FILES['croppedImage']["tmp_name"], $target_dir2)){
				//Code to create thumbnail
				$config['image_library'] = 'gd2';
				$config['source_image'] = $target_dir2;
				$config['new_image'] = './uploads/office_pictures/thumb_'.$_SESSION['businessId']."_".$imagePath;
				$config['create_thumb'] = False;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 100;
				$config['height']       = 100;			
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$arrReturn['thumbImage'] = base_url()."uploads/office_pictures/thumb_".$_SESSION['businessId']."_".$imagePath;
				
				//code to insert name into DB		
				// $data_businesses = array(
					// 'businessImage' 			=> $file_name
				// );		
				// $this->db->where('businessId', $_SESSION['businessId']);
				// $this->db->update('businesses', $data_businesses);
				$data_to_insert = array(
						'businessId'				=>	$_SESSION['businessId'],
						'images'					=>	$file_name,
						'imageType'					=>	"Office"
					);		
					$this->db->insert('businessesImage', $data_to_insert);
					$arrReturn['officeImageID'] = $this->db->insert_id();
			}				
		}		
		return $arrReturn;
		 
	}




	public function addOfficeServicePicture(){		
		$arrReturn = array();

		if(isset($_FILES['croppedImage']) && $_FILES['croppedImage']['name'] != "" && $_FILES['croppedImage']['error'] != "4"){
			$microtime = microtime();
			$imagePath = date("YmdHis");
			$imagePath = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $imagePath) . '.jpg';
			$target_dir2 = './uploads/service_pictures/'.$_SESSION['businessId']."_". $imagePath;
			$file_name = $_SESSION['businessId']."_". $imagePath;
			$arrReturn['croppedImage'] = base_url()."uploads/service_pictures/".$_SESSION['businessId']."_". $imagePath;
			if(move_uploaded_file($_FILES['croppedImage']["tmp_name"], $target_dir2)){
				//Code to create thumbnail
				$config['image_library'] = 'gd2';
				$config['source_image'] = $target_dir2;
				$config['new_image'] = './uploads/service_pictures/thumb_'.$_SESSION['businessId']."_".$imagePath;
				$config['create_thumb'] = False;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 100;
				$config['height']       = 100;			
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$arrReturn['thumbImage'] = base_url()."uploads/service_pictures/thumb_".$_SESSION['businessId']."_".$imagePath;
				
				//code to insert name into DB		
					$data_to_insert = array(
						'businessId'				=>	$_SESSION['businessId'],
						'images'					=>	$file_name,
						'imageType'					=>	"Service"
					);		
					$this->db->insert('businessesImage', $data_to_insert);
					$arrReturn['serviceImageID'] = $this->db->insert_id();
					
			}				
		}		
		return $arrReturn;
	}
	
	
	public function deleteOfficePicture($dataid){
		//echo $dataid;exit;
		$file_name = "";
		$this->db->select('images');
		$this->db->from('businessesImage');
		$this->db->where('businessId', $dataid);			
		$this->db->where('imageType', "Office");			
		$query_name = $this->db->get();
		$res_name = $query_name->result_array();
		if(!empty($res_name)){
			$file_name = $res_name[0]['images'];
		}
		$delete_file_cropped = "./uploads/office_pictures/".$file_name;
		$delete_file_thumb = "./uploads/office_pictures/thumb_".$file_name;
		if((unlink($delete_file_cropped)) && (unlink($delete_file_thumb))){
			$this->db->where('businessId', $dataid);
			$this->db->where('imageType', "Office");
			$this->db->delete('businessesImage'); 			
			return "DELETED";
		}else{
			return "NOTDELETED";
		}
	}



	public function deleteOfficeServicePicture($dataid){
		$file_name = "";
		$this->db->select('images');
		$this->db->from('businessesImage');
		$this->db->where('businessesImageId', $dataid);			
		$this->db->where('imageType', "Service");			
		$query_name = $this->db->get();
		$res_name = $query_name->result_array();
		if(!empty($res_name)){
			$file_name = $res_name[0]['images'];
		}
		$delete_file_cropped = "./uploads/service_pictures/".$file_name;
		$delete_file_thumb = "./uploads/service_pictures/thumb_".$file_name;
		if((unlink($delete_file_cropped)) && (unlink($delete_file_thumb))){
			$this->db->where('businessesImageId', $dataid);
			$this->db->delete('businessesImage'); 			
			return "DELETED";
		}else{
			return "NOTDELETED";
		}
	}
	
	
	public function checkPurchasedPackage($businessId){		
		$arrReturn = array();
		$this->db->select('pb.*,p.packageName,p.packageAmount,b.isPublished');
		$this->db->from('packagesOfBusiness pb');
		$this->db->join('packages p', 'pb.packageId = p.packageId',"left");
		$this->db->join('businesses b', 'pb.businessId = b.businessId',"left");
		$this->db->where('pb.businessId', $businessId);			
		$this->db->where('pb.isActive', 1);			
		$this->db->where('pb.isExpired', 0);			
		$query_package = $this->db->get();
		$res_package = $query_package->result_array();		
		if(empty($res_package)){
			return "Package";
		}elseif(!empty($res_package)){
			if($res_package[0]['isPublished'] == 0){
				return "AddProfile";
			}elseif($res_package[0]['isPublished'] == 1){
				return "DashBoard";
			}
		}		
	}


	public function getpackageData(){
		$arrReturn = array();
		$this->db->select('
							packageId,
							packageName,
							packageDetails,
							packageType,
							packageAmount

						');
		$this->db->from('packages');		
		$query_packages = $this->db->get();
		$res_packages = $query_packages->result_array();
		if(!empty($res_packages)){
			$arrReturn = $res_packages;			
		}
		return $arrReturn;	
	}


	public function paymentOption($data){
	$packages = explode(",",$data['hidPackagePurchased']);		
		foreach($packages as $key=>$value){
			$expiry_date = date("Y-m-d H:i:s", strtotime("+1 years", strtotime(TODAY)));	
			$data_to_insert = array(
				'packageId'					=>	$value,
				'businessId'				=>	$data['hidbusinessId'],
				'packagePurchaseDate'		=>	TODAY,
				'packageExpiryDate'			=>	$expiry_date,
				'isActive'					=>	1,			
				'isExpired'					=>	0
			);					
			$this->db->insert('packagesOfBusiness', $data_to_insert);		
		}		
		return "Done";
	}
	
	public function getSelectedPackagesForThisBusiness($businessId){
		$arrReturn['allData'] = array();
		$arrReturn['ignoreIds'] = array();
		$this->db->select('
							pb.*,
							DATE_FORMAT(pb.packageExpiryDate,"%D %M %Y") as packageExpiryDate,	
							p.packageName,
							p.packageAmount,
							p.packageDetails
						');
		$this->db->from('packagesOfBusiness pb');
		$this->db->join('packages p', 'pb.packageId = p.packageId',"left");
		$this->db->where('pb.businessId', $businessId);					
		$this->db->where('pb.isActive', 1);			
		$this->db->where('pb.isExpired', 0);		
		$query_packagesOfBusiness = $this->db->get();
		$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();
		if(!empty($res_packagesOfBusiness)){
			$arrReturn['allData'] = $res_packagesOfBusiness;
			foreach($res_packagesOfBusiness as $value){
				array_push($arrReturn['ignoreIds'],$value['packageId']);
			}
		}
		return $arrReturn;
	}
	
	public function getNotSelectedPackages($idsToIgnore){		
		$arrReturn = array();
		$this->db->select('
							packageId,
							packageName,
							packageDetails,
							packageType,
							packageAmount

						');
		$this->db->from('packages');		
		if(!empty($idsToIgnore)){
			$this->db->where_not_in('packageId', $idsToIgnore);			
		}
		$query_packages = $this->db->get();		
		$res_packages = $query_packages->result_array();		
		if(!empty($res_packages)){
			$arrReturn = $res_packages;			
		}
		return $arrReturn;
		
	}
	
	public function getSelectedCategoriesOfUser($businessId){
		$arrReturn = array();
		$this->db->select('categoryId');
		$this->db->from('businessCategory');		
		$this->db->where('businessId', $businessId);
		$query_businesscategory = $this->db->get();		
		$res_businesscategory = $query_businesscategory->result_array();		
		if(!empty($res_businesscategory)){
			$arrReturn = $res_businesscategory;			
		}
		return $arrReturn;
	}


	public function getOfficePictures($businessId){
		$arrReturn = array();
		$this->db->select('images');
		$this->db->from('businessesImage');		
		$this->db->where('businessId', $businessId);
		$this->db->where('imageType', "Office");
		$query_businessesimage = $this->db->get();		
		$res_businessesimage = $query_businessesimage->result_array();		
		if(!empty($res_businessesimage)){
			$arrReturn = $res_businessesimage[0];			
		}
		return $arrReturn;
	}

	public function getServicePictures($businessId){
		$arrReturn = array();
		$this->db->select('
							images,
							businessesImageId
						');
		$this->db->from('businessesImage');		
		$this->db->where('businessId', $businessId);
		$this->db->where('imageType', "Service");
		$query_businessesimage = $this->db->get();		
		$res_businessesimage = $query_businessesimage->result_array();		
		if(!empty($res_businessesimage)){			
			$arrReturn = $res_businessesimage;			
		}
		return $arrReturn;
	}
	
	
	public function getserviceTags($businessId){
		$arrReturn = array();
		$this->db->select('serviceTag');
		$this->db->from('services');		
		$this->db->where('businessId', $businessId);		
		$query_services = $this->db->get();		
		$res_services = $query_services->result_array();		
		if(!empty($res_services)){
			foreach($res_services as $value){
				array_push($arrReturn,$value['serviceTag']);	
			}	
			
		}
		return $arrReturn;
	}
	
	
	
	public function getOpeningHoursData($businessId){
		$arrReturn = array();
		$this->db->select('
							*,
							DATE_FORMAT(openingHours,"%I:%i %p") as openingHours,
							DATE_FORMAT(closingHours,"%I:%i %p") as closingHours
						');
		$this->db->from('openingHours');		
		$this->db->where('businessId', $businessId);		
		$query_openinghours = $this->db->get();		
		$res_openinghours = $query_openinghours->result_array();		
		if(!empty($res_openinghours)){
			$arrReturn = $res_openinghours;			
		}
		return $arrReturn;
	}
	
	
	
	public function getEditProfileData($businessId){	
		$arrReturn = array();
		$this->db->select('*');
		$this->db->from('businesses');
		$this->db->where('businessId', $businessId);	
		$query_businesses = $this->db->get();		
		$res_businesses = $query_businesses->result_array();		
		if(!empty($res_businesses)){
			$arrReturn['businessesData'] = $res_businesses[0];			
		}

		
		$getcountryValues = $this->getcountryValues();
		if(!empty($getcountryValues)){
			$arrReturn['countryValues'] = $getcountryValues;			
		}
		
		
		$StateList = $this->getStateList($arrReturn['businessesData']['countryId']);
		if(!empty($StateList)){
			$arrReturn['StateList'] = $StateList;			
		}
		
		
		$CityList = $this->getCityList($arrReturn['businessesData']['stateId']);
		if(!empty($CityList)){
			$arrReturn['CityList'] = $CityList;			
		}
				
		
		$categoryValues = $this->getcategoryValues();
		if(!empty($categoryValues)){
			$arrReturn['categoryValues'] = $categoryValues;			
		}
		
		
		$arrReturn['selectedCategories'] = array();
		$selectedCategories = $this->getSelectedCategoriesOfUser($arrReturn['businessesData']['businessId']);
		if(!empty($selectedCategories)){
			foreach($selectedCategories as $key=>$value){
				array_push($arrReturn['selectedCategories'],$value['categoryId']);
			}		
		}
		
		
		$officePictures = $this->getOfficePictures($arrReturn['businessesData']['businessId']);
		if(!empty($officePictures)){
			$arrReturn['officePictures'] = $officePictures;			
		}
		
		
		$serviceTags = $this->getserviceTags($arrReturn['businessesData']['businessId']);		
		if(!empty($serviceTags)){
			$serviceTags = implode(",",$serviceTags);		
			$arrReturn['serviceTags'] = $serviceTags;			
		}
		
		
		$ServicePictures = $this->getServicePictures($arrReturn['businessesData']['businessId']);
		if(!empty($ServicePictures)){
			$arrReturn['ServicePictures'] = $ServicePictures;			
		}
		
		$openinghours = $this->getOpeningHoursData($arrReturn['businessesData']['businessId']);
		if(!empty($openinghours)){
			$finalOpeningHours = array();
			foreach($openinghours as $data) {
				$finalOpeningHours[$data['day']] = $data;
			}
			$arrReturn['openinghours'] = $openinghours;
			$arrReturn['finalopeninghours'] = $finalOpeningHours;
		}
		
		return $arrReturn;
	}
	
	public function getRecentReviews($businessId){
		$arrReturn = array();
		$this->db->select('
							r.*,
							u.fullName,
							u.profilePicture,
							DATE_FORMAT(r.createdDate,"%D %M %Y At %l:%i %p") as createdDate
							
							');
		$this->db->from('review r');	
		$this->db->join('users u', 'r.userId = u.userId',"left");	
		$this->db->where('r.businessId', $businessId);		
		$query_review = $this->db->get();		
		$res_review = $query_review->result_array();		
		if(!empty($res_review)){
				$arrReturn = $res_review;
		}
		return $arrReturn;
	}
	
	public function getDashboardData(){
		$arrReturn = array();
		$recid = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$recid = $_SESSION['businessId'];
		}
		
		$this->db->select('*');
		$this->db->from('businesses');
		$this->db->where('businessId', $recid);	
		$query_businesses = $this->db->get();		
		$res_businesses = $query_businesses->result_array();		
		if(!empty($res_businesses)){
			$arrReturn['businessesData'] = $res_businesses[0];			
		}
		$RecentReviews = $this->getRecentReviews($recid);		
		if(!empty($RecentReviews)){
			$arrReturn['RecentReviews'] = $RecentReviews;			
		}
		return $arrReturn;
		//echo "<pre>";print_r($recid);exit;
	}
	
	
	public function getSelectedPackageDetails($orderMasterID){
		$arrReturn = array();
		
		$this->db->select('*');
		$this->db->from('orderpackagemaster');
		$this->db->where('orderMasterID', $orderMasterID);	
		$query_packages = $this->db->get();		
		$res_packages = $query_packages->result_array();				
		if(!empty($res_packages)){
			$arrReturn = $res_packages;			
		}
		
		return $arrReturn;		
	}
	
	public function insertorderMaster($data){		
	// echo "<pre>";
	// print_r($data);
	// exit;
		$packages = "";
		$businessId  = 0;
		$orderMasterRecID = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] >0){
			$businessId  = $_SESSION['businessId'];
		}
		if(isset($data['hidPackagePurchased']) && $data['hidPackagePurchased'] !=""){
			$packages = $data['hidPackagePurchased'];
		}
		$packages = explode(",",$packages);		
		//code to insert data in "orderMaster" table	
		$data_to_insert = array(
			'businessId'				=>	$businessId,
			'createdDate'				=>	TODAY			
		);		
		if($this->db->insert('orderMaster', $data_to_insert)){	
			$orderMasterRecID = $this->db->insert_id();	
			$this->db->select('*');
			$this->db->from('packages');
			$this->db->where_in('packageId', $packages);	
			$query_packages = $this->db->get();		
			$res_packages = $query_packages->result_array();				
			if(!empty($res_packages)){			
				foreach($res_packages as $value){
					$purchaseType = "NEW";
					if(isset($data['hidBuyType'])){							
						if($data['hidBuyType'] != "" && $data['hidBuyType'] == "renew"){
							$purchaseType = "RENEW";		
						}						
					}
					$data_orderpackagemaster = array(
						'orderMasterID' 		=>	$orderMasterRecID,
						'packageid' 			=>	$value['packageId'],
						'packageName' 			=>	$value['packageName'],
						'packageDetails' 		=>	$value['packageDetails'],
						'packageType' 			=>	$value['packageType'],
						'packageAmount' 		=>	$value['packageAmount'],
						'monthsSubscribed' 		=>	1,
						'totalPackageAmount' 	=>	$value['packageAmount'],
						'purchaseType' 			=>	$purchaseType,
						'createdDate' 			=>	TODAY			
					);			
					$this->db->insert('orderpackagemaster', $data_orderpackagemaster);					
				}
			}
			
		}
		return	$orderMasterRecID;	
	}
	
	public function deletePackageFromorderMaster($orderpackagemasterID,$orderMasterID){
		$this->db->where('recid', $orderpackagemasterID);
		$this->db->where('orderMasterID', $orderMasterID);
		if($this->db->delete('orderpackagemaster')){
			return true;
		}else{
			return false;
		}			
	}
	
	public function redirectUserProfile($businessId){
		$this->db->select('isPublished');
		$this->db->from('businesses');
		$this->db->where('businessId', $businessId);	
		$query_businesses = $this->db->get();		
		$res_businesses = $query_businesses->result_array();
		if(!empty($res_businesses)){
			$isPublished = $res_businesses[0]['isPublished'];
		}
		return $isPublished;
	}
	
	
	public function getAnalyticsData($fromDate = "" , $toDate = ""){
		$arrReturn = array();
		$tempArray = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}
		$start_date = date('Y-m-d',strtotime('-10 days', strtotime(TODAY)));		
		$end_date = date('Y-m-d', strtotime(TODAY));
		if(null!=$fromDate && null != $toDate){
			$start_date = $fromDate;
			$end_date = $toDate;
		}
		
		$begin = new DateTime( $start_date );		
		$end = new DateTime( $end_date );

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);		

		foreach ( $period as $dt ){
			$currentLoopDate = $dt->format("jS F Y");			
			$dateYMDFormat = $dt->format("Y-m-d");			
			
			//Code for Clicks table
			$this->db->select('count(*) as totClicks');
			$this->db->from('clicks');
			$this->db->where('businessId', $businessId);	
			$this->db->where('clickDate > ', $dt->format('Y-m-d 00:00:00'));	
			$this->db->where('clickDate < ', $dt->format('Y-m-d 23:59:59'));				
			$query_clicks = $this->db->get();		
			$res_clicks = $query_clicks->result_array();			
			if(!empty($res_clicks)){
				$tempArray['clicks'] = $res_clicks[0]['totClicks'];
			}
			
			
			//Code for bookmarks table
			$this->db->select('count(*) as totBookmarks');
			$this->db->from('bookmarks');
			$this->db->where('businessId', $businessId);	
			$this->db->where('bookmarkedDate > ', $dt->format('Y-m-d 00:00:00'));	
			$this->db->where('bookmarkedDate < ', $dt->format('Y-m-d 23:59:59'));	
			$query_bookmarks = $this->db->get();		
			$res_bookmarks = $query_bookmarks->result_array();			
			if(!empty($res_bookmarks)){
				$tempArray['bookMarks'] = $res_bookmarks[0]['totBookmarks'];
			}
			
			
			
			//Code for review table
			$this->db->select('count(*) as totReviews');
			$this->db->from('review');
			$this->db->where('businessId', $businessId);	
			$this->db->where('createdDate > ', $dt->format('Y-m-d 00:00:00'));	
			$this->db->where('createdDate < ', $dt->format('Y-m-d 23:59:59'));	
			$query_review = $this->db->get();		
			$res_review = $query_review->result_array();			
			if(!empty($res_review)){
				$tempArray['reviews'] = $res_review[0]['totReviews'];
			}
			$tempArray['currentLoopDate'] = $currentLoopDate;
			$tempArray['dateYMDFormat'] = $dateYMDFormat;
			array_push($arrReturn,$tempArray);			
		}
		$arrReturn = array_reverse($arrReturn);		
		return $arrReturn;
	}
	
	public function getClicksBifurcation($currentElementDate){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}		
		$this->db->select('country.countryName,count(clicks.clickId) as totClicks');
		$this->db->from('clicks');
		$this->db->join('country', 'clicks.countryId = country.countryId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('clickDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('clickDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->group_by("clicks.countryId");
		$query_country = $this->db->get();		
		$res_country = $query_country->result_array();					
		if(!empty($res_country)){
			$arrReturn['country'] = $res_country;
		}
		##################################################################################################
		$this->db->select('city.cityName,count(clicks.clickId) as totClicks');
		$this->db->from('clicks');
		$this->db->join('city', 'clicks.cityId = city.cityId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('clickDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('clickDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->group_by("clicks.cityId");
		$query_city = $this->db->get();		
		$res_city = $query_city->result_array();						
		if(!empty($res_city)){
			$arrReturn['city'] = $res_city;
		}
		return $arrReturn;		
	}




	public function getClicksBifurcationWithPackage($currentElementDate,$hidpackageSelection){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}		
		$this->db->select('country.countryName,count(clicks.clickId) as totClicks');
		$this->db->from('clicks');
		$this->db->join('country', 'clicks.countryId = country.countryId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('clickDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('clickDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->where('clickType', $hidpackageSelection);				
		$this->db->group_by("clicks.countryId");
		$query_country = $this->db->get();		
		$res_country = $query_country->result_array();					
		if(!empty($res_country)){
			$arrReturn['country'] = $res_country;
		}
		##################################################################################################
		$this->db->select('city.cityName,count(clicks.clickId) as totClicks');
		$this->db->from('clicks');
		$this->db->join('city', 'clicks.cityId = city.cityId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('clickDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('clickDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));
		$this->db->where('clickType', $hidpackageSelection);					
		$this->db->group_by("clicks.cityId");
		$query_city = $this->db->get();		
		$res_city = $query_city->result_array();						
		if(!empty($res_city)){
			$arrReturn['city'] = $res_city;
		}
		return $arrReturn;		
	}
	
	
	
	public function getBookmarksBifurcation($currentElementDate){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}		
		$this->db->select('country.countryName,count(bookmarks.bookmarkId) as totClicks');
		$this->db->from('bookmarks');
		$this->db->join('country', 'bookmarks.countryId = country.countryId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('bookmarkedDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('bookmarkedDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->group_by("bookmarks.countryId");
		$query_country = $this->db->get();		
		$res_country = $query_country->result_array();					
		if(!empty($res_country)){
			$arrReturn['country'] = $res_country;
		}
		##################################################################################################
		$this->db->select('city.cityName,count(bookmarks.bookmarkId) as totClicks');
		$this->db->from('bookmarks');
		$this->db->join('city', 'bookmarks.cityId = city.cityId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('bookmarkedDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('bookmarkedDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->group_by("bookmarks.cityId");
		$query_city = $this->db->get();		
		$res_city = $query_city->result_array();						
		if(!empty($res_city)){
			$arrReturn['city'] = $res_city;
		}
		return $arrReturn;		
	}
	
	
	public function getReviewsBifurcation($currentElementDate){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}		
		$this->db->select('country.countryName,count(review.reviewId) as totClicks');
		$this->db->from(' review');
		$this->db->join('country', 'review.countryId = country.countryId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('createdDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('createdDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->group_by("review.countryId");
		$query_country = $this->db->get();		
		$res_country = $query_country->result_array();					
		if(!empty($res_country)){
			$arrReturn['country'] = $res_country;
		}
		##################################################################################################
		$this->db->select('city.cityName,count(review.reviewId) as totClicks');
		$this->db->from(' review');
		$this->db->join('city', ' review.cityId = city.cityId',"left");
		$this->db->where('businessId', $businessId);	
		$this->db->where('createdDate > ',  date("Y-m-d 00:00:00",strtotime($currentElementDate)));	
		$this->db->where('createdDate < ', date("Y-m-d 23:59:59",strtotime($currentElementDate)));				
		$this->db->group_by("review.cityId");
		$query_city = $this->db->get();		
		$res_city = $query_city->result_array();						
		if(!empty($res_city)){
			$arrReturn['city'] = $res_city;
		}
		return $arrReturn;		
	}
	
	
	public function bypassPaymentAndInsertData($data){
 
		$recidKeys = array();
		$i = 0;
		$monthsCount = array();
		foreach($data['months'] as $key => $value){
			$recidKeys[$i] = $key;
			$monthsCount[$i] = $value;
			$i++;
		}		
		$monthlyAmount = array();
		$ordermasterID = $data['hidOrderMasterId'];
		$allData = $this->Home_model->getSelectedPackageDetails($ordermasterID);
		
		foreach($allData as $value){
			if(in_array($value['recid'],$recidKeys)){
				array_push($monthlyAmount,$value['packageAmount']);
				
			}
		}		
		
		$total_pay = array_map(function($monthsCount, $monthlyAmount) {
			return $monthsCount * $monthlyAmount;
		}, $monthsCount, $monthlyAmount);
		
		
		
		$finalTot = array_sum($total_pay);		
		$finalTot = number_format($finalTot,"2",".","");
		
		//ordermaster table
		$data_ordermaster = array(
		   'flagActive' 			=> 0,
		   'flagCompleted' 			=> 1,
		   'totalAmountCharged' 	=> $finalTot
		);		
		$this->db->where('recid', $ordermasterID);
		$this->db->update('orderMaster', $data_ordermaster);	 		 
		
		foreach($allData as $value){
			if(in_array($value['recid'],$recidKeys)){
				$orderPakageId = $value['recid'];				
				if(isset($data['months'][$orderPakageId])) {
					$monthCount = $data['months'][$orderPakageId];
					$multiplication = $monthCount * $value['packageAmount'];
					$data_orderpackagemaster = array(
						'monthsSubscribed' 			=> $monthCount,
						'totalPackageAmount' 		=> $multiplication 
					);		
					$this->db->where('recid', $orderPakageId);
					$this->db->update('orderpackagemaster', $data_orderpackagemaster); 
					
					
					$CheckPaymentType = $this->getPaymentType($orderPakageId);
						if($CheckPaymentType == "RENEW"){
							$this->db->select('DATE_FORMAT(packageExpiryDate,"%Y-%m-%d") as packageExpiryDate ');
							$this->db->from('packagesOfBusiness');
							$this->db->where('packageId', $value['packageid']);				
							$this->db->where('businessId', $_SESSION['businessId']);				
							$query_packagesOfBusiness = $this->db->get();		
							$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();			
							if(!empty($res_packagesOfBusiness)){				
								$packageExpiryDate = $res_packagesOfBusiness[0]['packageExpiryDate'];
							}
							$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $monthCount . ' months', strtotime($packageExpiryDate)));
							$data_packagesOfBusiness = array(
								'packageExpiryDate' 			=> $packageExpiryDate							
							);		 
							$this->db->where('packageId', $value['packageid']);				
							$this->db->where('businessId', $_SESSION['businessId']);				
							$this->db->update('packagesOfBusiness', $data_packagesOfBusiness); 
						}elseif($CheckPaymentType == "NEW"){
							$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $monthCount . ' months', strtotime(TODAY)));
							$data_packagesofbusiness = array(
								'packageId' 			=> $value['packageid'],						
								'businessId' 			=> $_SESSION['businessId'],						
								'packagePurchaseDate' 	=> TODAY,
								'packageExpiryDate' 	=> $packageExpiryDate,
								'isActive' 				=> 1,
								'isExpired' 			=> 0
							);				
							$this->db->insert('packagesOfBusiness', $data_packagesofbusiness);						
						}
				}
			}
		}
		$check = $this->CheckFreePlan($_SESSION['businessId']);
		if($check){
			$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+12 years', strtotime(TODAY)));
			$data_packagesofbusiness = array(
			'packageId' 			=> 4,
			'businessId' 			=> $_SESSION['businessId'],						
			'packagePurchaseDate' 	=> TODAY,
			'packageExpiryDate' 	=> $packageExpiryDate,
			'isActive' 				=> 1,
			'isExpired' 			=> 0
		);				
		$this->db->insert('packagesOfBusiness', $data_packagesofbusiness);	
		}		
		
		$data_payment = array(
			'ordermasterID' 			=> $ordermasterID,						
			'totalamount' 				=> $finalTot,						
			'paymentDate' 				=> TODAY,
			'country' 					=> "India",
			'businessId' 				=> $_SESSION['businessId'],
			'paymentStatus' 			=> 1,
			'transaction_currency' 		=> "INR",
			'payment_method' 			=> "INR Currency Payment",
			'transaction_state' 		=> "completed",
			'transaction_id' 			=> ""
		);						
		$this->db->insert('payment', $data_payment);
					
					
		return true;
		
	}
	
	public function CheckFreePlan($businessId){
		$this->db->select('*');
		$this->db->from('packagesOfBusiness');		
		$this->db->where('businessId', $businessId);	
		$this->db->where('packageId', 4);					
		$query_packagesOfBusiness = $this->db->get();		
		$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();						
		if(!empty($res_packagesOfBusiness)){
			return false;
		}else{
			return true;
		}	
		
	}
	
	public function getpaymentHistory($fromDate = "" , $toDate = ""){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}			
		$this->db->select('
							*,
							DATE_FORMAT(paymentDate,"%D %M %Y At %l:%i %p") as paymentDate
						');
		$this->db->from('payment');		
		$this->db->where('businessId', $businessId);			
		if($fromDate != "" && $toDate != ""){			
			$this->db->where('paymentDate > ', date("Y-m-d 00:00:00",strtotime($fromDate)));			
			$this->db->where('paymentDate < ', date("Y-m-d 23:59:59",strtotime($toDate)));						
		}
		$query_payment = $this->db->get();		
		$res_payment = $query_payment->result_array();		
		if(!empty($res_payment)){
			$arrReturn = $res_payment;			
		}
		return $arrReturn;
	}
	
	
	public function getReportsData(){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}
		$this->db->select('
							pb.packageId,
							p.packageName
						');
		$this->db->from('packagesOfBusiness pb');		
		$this->db->join('packages p', 'pb.packageId = p.packageId',"left");
		$this->db->where('pb.businessId', $businessId);					 
		$this->db->where('pb.isActive', 1);					 
		$this->db->where('pb.isExpired', 0);					 
		$query_packagesOfBusiness = $this->db->get();		
		$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();		
		if(!empty($res_packagesOfBusiness)){
			foreach($res_packagesOfBusiness as $value){
				if($value['packageId'] != 4){
					array_push($arrReturn,$value);
				}
			}			 
		}		
		return $arrReturn;				
	}
	
	public function getPackageWiseAnalytics( $packageSelection , $fromDate = '' , $toDate = ''){			
		$arrReturn = array();
		$tempArray = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}
		$start_date = date('Y-m-d',strtotime('-10 days', strtotime(TODAY)));		
		$end_date = date('Y-m-d', strtotime(TODAY));
		if(null!=$fromDate && null != $toDate){
			$start_date = $fromDate;
			$end_date = $toDate;
		}
		
		$begin = new DateTime( $start_date );		
		$end = new DateTime( $end_date );

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);		

		foreach ( $period as $dt ){
			$currentLoopDate = $dt->format("jS F Y");			
			$dateYMDFormat = $dt->format("Y-m-d");			
			
			//Code for Clicks table
			$this->db->select('count(*) as totClicks');
			$this->db->from('clicks');
			$this->db->where('businessId', $businessId);	
			$this->db->where('clickDate > ', $dt->format('Y-m-d 00:00:00'));	
			$this->db->where('clickDate < ', $dt->format('Y-m-d 23:59:59'));				
			$this->db->where('clickType', $packageSelection);				
			$query_clicks = $this->db->get();		
			$res_clicks = $query_clicks->result_array();			
			if(!empty($res_clicks)){
				$tempArray['clicks'] = $res_clicks[0]['totClicks'];
			}
			
			
			//Code for bookmarks table
			$this->db->select('count(*) as totBookmarks');
			$this->db->from('bookmarks');
			$this->db->where('businessId', $businessId);	
			$this->db->where('bookmarkedDate > ', $dt->format('Y-m-d 00:00:00'));	
			$this->db->where('bookmarkedDate < ', $dt->format('Y-m-d 23:59:59'));	
			$query_bookmarks = $this->db->get();		
			$res_bookmarks = $query_bookmarks->result_array();			
			if(!empty($res_bookmarks)){
				$tempArray['bookMarks'] = $res_bookmarks[0]['totBookmarks'];
			}
			
			
			
			//Code for review table
			$this->db->select('count(*) as totReviews');
			$this->db->from('review');
			$this->db->where('businessId', $businessId);	
			$this->db->where('createdDate > ', $dt->format('Y-m-d 00:00:00'));	
			$this->db->where('createdDate < ', $dt->format('Y-m-d 23:59:59'));	
			$query_review = $this->db->get();		
			$res_review = $query_review->result_array();			
			if(!empty($res_review)){
				$tempArray['reviews'] = $res_review[0]['totReviews'];
			}
			$tempArray['currentLoopDate'] = $currentLoopDate;
			$tempArray['dateYMDFormat'] = $dateYMDFormat;
			array_push($arrReturn,$tempArray);			
		}
		$arrReturn = array_reverse($arrReturn);		
		// echo "<pre>";
		// print_r($arrReturn);
		// exit;
		return $arrReturn;
	
	}	
	
	
	public function getPackageDetails(){
		$businessId = 0;
		$purchaseType = "";
		$arrReturn = array();
		$arrReturn['packageInfo'] = array();
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}
		
		$this->db->select('pb.*,p.packageName,p.packageAmount,b.isPublished');
		$this->db->from('packagesOfBusiness pb');
		$this->db->join('packages p', 'pb.packageId = p.packageId',"left");
		$this->db->join('businesses b', 'pb.businessId = b.businessId',"left");
		$this->db->where('pb.businessId', $businessId);			
		$this->db->where('pb.isActive', 1);			
		$this->db->where('pb.isExpired', 0);			
		
		
		// $this->db->select('*');
		// $this->db->from('packagesOfBusiness');
		// $this->db->where('businessId', $businessId);				
		$query_packagesOfBusiness = $this->db->get();		
		$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();			
		if(!empty($res_packagesOfBusiness)){				
			foreach($res_packagesOfBusiness as $value){
				if($value['packageId'] != 4){
					array_push($arrReturn['packageInfo'],$value);
				}
			}
		}if(!empty($arrReturn)){
			$arrReturn['packagePurchased'] = "YES";
			//$packagePurchased = "YES";
		}elseif(empty($arrReturn)){
			$arrReturn['packagePurchased'] = "NO";
			//$packagePurchased = "NO";
		}
		// echo "<pre>";
		// print_r($arrReturn);
		// exit;
		return $arrReturn;	
	}
	
	public function getPaymentType($orderPakageId){
			$purchaseType = "";
			$this->db->select('purchaseType');
			$this->db->from('orderpackagemaster');
			$this->db->where('recid', $orderPakageId);				
			$query_orderpackagemaster = $this->db->get();		
			$res_orderpackagemaster = $query_orderpackagemaster->result_array();			
			if(!empty($res_orderpackagemaster)){				
				$purchaseType = $res_orderpackagemaster[0]['purchaseType'];
			}
			return $purchaseType;
	}
	
	public function checkPackages(){
		$arrReturn = array();
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}
		$this->db->select('*');
		$this->db->from('packagesOfBusiness');
		$this->db->where('businessId', $businessId);				
		$query_packagesOfBusiness = $this->db->get();		
		$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();					
		if(!empty($res_packagesOfBusiness)){				
			foreach($res_packagesOfBusiness as $value){
				if($value['packageId'] != 4){
					array_push($arrReturn,$value);
				}
			} 			
		}		
		return $arrReturn;	
	}
	
	
	public function insertFreePackage(){
		$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+12 years', strtotime(TODAY)));
			$data_packagesofbusiness = array(
			'packageId' 			=> 4,
			'businessId' 			=> $_SESSION['businessId'],						
			'packagePurchaseDate' 	=> TODAY,
			'packageExpiryDate' 	=> $packageExpiryDate,
			'isActive' 				=> 1,
			'isExpired' 			=> 0
		);				
		$this->db->insert('packagesOfBusiness', $data_packagesofbusiness);		
		return true;
	}
	
	public function doResetPassword($data){
	 
		if($data['password'] == $data['cnfpassword']){
			$password = md5($data['password']);
			$data = array(	'password' => $password	);
			$this->db->where('businessId', $data['businessId']);
			if($this->db->update('businesses', $data)){
				return "CHANGED";
			}else{
				return "NTCHANGED";
			} 
			
		}else{
			return "NTCHANGED";
		}
	}
	
	public function performChangePassword($data){			 
		$arrReturn = array();
		$password = $data['password'];
		$confirmPassword = $data['confirmPassword'];
		if($password == $confirmPassword){			
			$password = md5($password);
			$data = array(	'password' 	=> $password	);
			$this->db->where('businessId', $_SESSION['businessId']);
			$this->db->update('businesses', $data); 			
			return "Updated";
		}else{
			return false;
		}
		
	}
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {	
	
	public function __construct(){
        parent::__construct();		
		$this->load->model('Admin_model');						
    } 
	
	public function index(){		
		//echo "<pre>";print_r($_SESSION);exit;		
		$res['msg'] =  "";			
		if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
			$res['msg'] = $this->uri->segment(3);
		}		
		$this->load->view('admin/login_header');		
		$this->load->view('admin/login',$res);		
		$this->load->view('admin/login_footer');
	}	
	
	public function doLogin(){		
	$emailAddress = "";	
	$password = "";	
		if(($this->input->post('emailAddress')) && $this->input->post('emailAddress') != ""){
			$emailAddress = $this->input->post('emailAddress');		
		}
		if(($this->input->post('password')) && $this->input->post('password') != ""){
			$password = md5($this->input->post('password'));		
		}
		$doLogin = $this->Admin_model->doLogin($emailAddress,$password);
		if(($doLogin) && is_numeric($doLogin) && $doLogin == 1){
			if(isset($_SESSION['LoggedInType']) && $_SESSION['LoggedInType'] != "" && $_SESSION['LoggedInType'] == "Admin") {
			redirect(base_url()."index.php/Admin/dashboard");
			}elseif(isset($_SESSION['LoggedInType']) && $_SESSION['LoggedInType'] != "" && $_SESSION['LoggedInType'] == "Staff"){
				redirect(base_url()."index.php/Admin/busniessUsers");
			}
		}elseif(($doLogin) && is_numeric($doLogin) && $doLogin == 2){
			redirect(base_url()."index.php/Admin/index/BLOCKED");	
		}else{
			redirect(base_url()."index.php/Admin/index/INVLD");
		}		
		
	}	
	
	public function forgotPassword(){
		$email = "";		
		if(($this->input->post('email')) && $this->input->post('email') != ""){
			$email = $this->input->post('email');
		}	
		$res = $this->Admin_model->getAdminDetailsFromEmail($email);		
		if(($res)){			
			$link = "Your Link Comes Here"	;
			$msg  = "Hello ".$res['email'].",<br/>					
					You have received this message because you have requested to reset your password on the My Visa For travel website.<br/>
					Please, follow this link to create a new password: <br/>
					".$link."<br/>
					Thank you for your trust in our solutions,<br/> 
					Team MyVisaForTravel";					 
						
				$this->load->library('email');
//				$this->email->initialize($this->email_config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('admin@solulab.com'); // change it to yours
				$this->email->to($email); // change it to yours
				$this->email->subject('Reset Password');
				$message = $msg;
				$this->email->message($message);				
				if ($this->email->send()) {
					redirect(base_url()."index.php/Admin/index/EMAILSNT");		
				} else {
					echo $this->email->print_debugger();
				}
		}else{
			redirect(base_url()."index.php/Admin/index/EMAILNF");	
		}				
	}
	
	public function dashboard(){		
	$loggedIN = $this->Admin_model->checkUserSession();
	if($loggedIN == "logeedIn"){		
		$this->Admin_model->checkStaffAccess();	
		$msg= "";
		if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
			$msg = $this->uri->segment(3);
		}
		
		$res = $this->Admin_model->getDashboardData();
		$res['msg'] =  $msg;	
		
		$this->load->view('admin/dashboard_header');		
		$this->load->view('admin/dashboard',$res);		
		$this->load->view('admin/dashboard_footer');
	}else{
		redirect(base_url()."index.php/Admin/index/NTLGD");
	}	
	}
	
	
	
	public function logout(){
		session_unset();
		redirect(base_url()."index.php/Admin");	
	}
	
	public function appUsers(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$countryID = 0;
			$msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['UsersData'] = $this->Admin_model->getappUsersData($countryID);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['countryID'] = $countryID;
			$res['msg'] = $msg;
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/appUsers_header');		
			$this->load->view('admin/appUsers',$res);		
			$this->load->view('admin/appUsers_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
	}
	
	
	public function appUserDeatils(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$userId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $userId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->getUserDataFromID($userId);
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/userDeatils_header');		
			$this->load->view('admin/userDeatils',$res);		
			$this->load->view('admin/userDeatils_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	public function blockUser(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$userId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $userId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->blockUser($userId);
			if(($res) && $res != "" && $res == "BLOCKED"){
				redirect(base_url()."index.php/Admin/appUsers/BLOCKED");	
			}else{
				redirect(base_url()."index.php/Admin/appUsers/NTBLOCKED");
			}			
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	
	
	public function blockStaff(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$staffMembersId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $staffMembersId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->blockStaff($staffMembersId);
			if(($res) && $res != "" && $res == "BLOCKED"){
				redirect(base_url()."index.php/Admin/staffMember/BLOCKED");	
			}else{
				redirect(base_url()."index.php/Admin/staffMember/NTBLOCKED");
			}			
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	
	public function unblockUser(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$userId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $userId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->unblockUser($userId);
			if(($res) && $res != "" && $res == "UNBLOCKED"){
				redirect(base_url()."index.php/Admin/appUsers/UNBLOCKED");	
			}else{
				redirect(base_url()."index.php/Admin/appUsers/NTUNBLOCKED");
			}			
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}


	public function unblockStaff(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$staffMembersId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $staffMembersId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->unblockStaff($staffMembersId);
			if(($res) && $res != "" && $res == "UNBLOCKED"){
				redirect(base_url()."index.php/Admin/staffMember/UNBLOCKED");	
			}else{
				redirect(base_url()."index.php/Admin/staffMember/NTUNBLOCKED");
			}			
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}	
	
	
	public function busniessUsers(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$countryID = 0;
			$categoryId = 0;
			$msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->input->post('categoryId')) && is_numeric($this->input->post('categoryId')) && $this->input->post('categoryId') > 0){
				 $categoryId = $this->input->post('categoryId');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['busniessData'] = $this->Admin_model->getbusniessUsersData($countryID,$categoryId);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['categoryValues'] = $this->Admin_model->getcategoryValues();
			$res['countryID'] = $countryID;
			$res['categoryId'] = $categoryId;
			$res['msg'] = $msg;
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/busniessUsers_header');		
			$this->load->view('admin/busniessUsers',$res);		
			$this->load->view('admin/busniessUsers_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
	}
	
	
	
	public function blockBusiness(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$businessId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $businessId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->blockBusiness($businessId);
			if(($res) && $res != "" && $res == "BLOCKED"){
				redirect(base_url()."index.php/Admin/busniessUsers/BLOCKED");	
			}else{
				redirect(base_url()."index.php/Admin/busniessUsers/NTBLOCKED");
			}			
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	public function unblockBusiness(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$businessId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $businessId = $this->uri->segment(3);
			}
			$res = $this->Admin_model->unblockBusiness($businessId);
			if(($res) && $res != "" && $res == "UNBLOCKED"){
				redirect(base_url()."index.php/Admin/busniessUsers/UNBLOCKED");	
			}else{
				redirect(base_url()."index.php/Admin/busniessUsers/NTUNBLOCKED");
			}			
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}	
	
	public function businessDeatils(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$businessId = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				 $businessId = $this->uri->segment(3);
			}
			//echo $businessId;exit;
			$res = $this->Admin_model->getBusinessDataFromID($businessId);
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/businessDeatils_header');		
			$this->load->view('admin/businessDeatils',$res);		
			$this->load->view('admin/businessDeatils_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
	}
	
	public function staffMember(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$this->Admin_model->checkStaffAccess();
			$countryID = 0;
			$msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['staffMemberData'] = $this->Admin_model->getstaffMemberData($countryID);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['countryID'] = $countryID;
			$res['msg'] = $msg;
			//echo "<pre>";print_r($res);exit;
			//$res = array();
			$this->load->view('admin/staffMember_header');		
			$this->load->view('admin/staffMember',$res);		
			$this->load->view('admin/staffMember_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
		
	}
	
	
	public function staffProfile(){	
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$this->Admin_model->checkStaffAccess();	
		$arrUserData = array();
		$staffMembersId = 0;		
		$msg = "";			
		if(($this->uri->segment(4)) && $this->uri->segment(4) != ""){
			 $msg = $this->uri->segment(4);
		}			
		if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $staffMembersId = $this->uri->segment(3);
		}
		
		$arrUserData['staffMembersId'] 			= "0";
		$arrUserData['staffMembersName'] 		= "";		 
		$arrUserData['staffMembersPhone'] 		= "";		 
		$arrUserData['staffMembersEmail'] 		= "";		 
		$arrUserData['staffMembersAddress1'] 	= "";		 
		$arrUserData['staffMembersAddress2'] 	= "";		 
		$arrUserData['staffMembersArea'] 		= "";		 
		$arrUserData['documentName'] 			= "";		 
		$arrUserData['cityId'] 					= "0";		 
		$arrUserData['stateId'] 				= "0";		 
		$arrUserData['countryId'] 				= "0";		 
		$arrUserData['staffMembersJoiningDate'] = "";		 
		$arrUserData['refferedBy'] 				= "";		 		
		
		if(($staffMembersId) && is_numeric($staffMembersId) && $staffMembersId > 0 ){
			$arrReturn  = $this->Admin_model->getStaffData($staffMembersId);			
			if(!empty($arrReturn)){
				$arrUserData['staffMembersId']			 = $arrReturn['staffMembersId'];	
				$arrUserData['staffMembersName']		 = $arrReturn['staffMembersName'];				
				$arrUserData['staffMembersPhone'] 		 = $arrReturn['staffMembersPhone'];				
				$arrUserData['staffMembersEmail'] 		 = $arrReturn['staffMembersEmail'];				
				$arrUserData['staffMembersAddress1'] 	 = $arrReturn['staffMembersAddress1'];				
				$arrUserData['staffMembersAddress2'] 	 = $arrReturn['staffMembersAddress2'];				
				$arrUserData['staffMembersArea'] 		 = $arrReturn['staffMembersArea'];				
				$arrUserData['documentName'] 		 	 = $arrReturn['documentName'];				
				$arrUserData['cityId'] 		 	 		 = $arrReturn['cityId'];				
				$arrUserData['stateId'] 		 	 	 = $arrReturn['stateId'];				
				$arrUserData['countryId'] 		 	 	 = $arrReturn['countryId'];				
				$arrUserData['staffMembersJoiningDate']  = $arrReturn['staffMembersJoiningDate'];				
				$arrUserData['refferedBy'] 				 = $arrReturn['refferedBy'];				
				$arrUserData['StateList'] 				 = $arrReturn['StateList'];				
				$arrUserData['CityList'] 				 = $arrReturn['CityList'];				
			}			
		}
		
		$res['arrAddEditInfo'] = $arrUserData;		
		$res['countryValues'] = $this->Admin_model->getcountryValues();		
		$res['msg'] = $msg;		
		//echo "<pre>";print_r($res);exit;
		$this->load->view('admin/staffProfile_header');		
		$this->load->view('admin/staffProfile',$res);		
		$this->load->view('admin/staffProfile_footer');		
	}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	public function getStateList(){
		$country_id = 0;
		$html = "";
		if(($this->input->post('country_id')) && is_numeric($this->input->post('country_id')) && $this->input->post('country_id') > 0){
			$country_id = $this->input->post('country_id');
		}
		$StateList = $this->Admin_model->getStateList($country_id);
		if(($StateList)){
			$html .= "<option value=''>Select State</option>";
			foreach($StateList as $value){
				$html.= "<option value='".$value['stateId']."'>".$value['stateName']."</option>";
			}
		}
		echo $html;
	}
	
	public function getCityList(){
		$state_id = 0;
		$html = "";
		if(($this->input->post('state_id')) && is_numeric($this->input->post('state_id')) && $this->input->post('state_id') > 0){
			$state_id = $this->input->post('state_id');
		}
		$CityList = $this->Admin_model->getCityList($state_id);
		if(($CityList)){
			$html .= "<option value=''>Select City</option>";
			foreach($CityList as $value){
				$html.= "<option value='".$value['cityId']."'>".$value['cityName']."</option>";
			}
		}
		echo $html;
	}
	
	
	public function performStaffProfile(){
		
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$this->Admin_model->checkStaffAccess();	
		$staffMembersId = $this->input->post('staffMembersId');
		$data = $this->input->post();		
		if($staffMembersId == 0){
			$returnArray = $this->Admin_model->addStaffMember($data);
			if(!empty($returnArray)){
				$msg  = "Hello ".$returnArray['staffMembersName'].",<br/>
						You have been successfully Registered with us.<br/>						
						Please find the login details:<br/>
						Email Address : ".$returnArray['staffMembersEmail']."<br/>
						Password : ".$returnArray['password']."<br/>
						Admin Panel Link :".base_url()."adminPanel/index.php/Admin<br/>
						
						We wish to have a long Association with you.<br/><br/>
						Thanks & Regards<br/>
						Team MyVisaForTravel";
						 				
				
				$this->load->library('email');
				//$this->email->initialize($this->email_config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('admin@solulab.com'); // change it to yours
				$this->email->to($returnArray['staffMembersEmail']); // change it to yours
				$this->email->subject('Registration Successfull');
				$message = $msg;
				$this->email->message($message);				
				if ($this->email->send()) {
					redirect(base_url()."index.php/Admin/staffMember/SUCC");	
				} else {
					echo $this->email->print_debugger();
				}		
			}
		}elseif(is_numeric($staffMembersId) && $staffMembersId > 0 ){
			$completed = $this->Admin_model->editStaffMember($data);
			if($completed){
				redirect(base_url()."index.php/Admin/staffMember/EDSUCC");
			}else{
				redirect(base_url()."index.php/Admin/staffMember/EDNTSUCC");
			}
		}
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
		
	}
	
	public function checkEmailForStaffAndAdmin(){
		$email = "";
		$staffMembersId = 0;
		if(($this->input->post('email')) && $this->input->post('email') != ""){
			$email = $this->input->post('email');
		}
		if(($this->input->post('staffMembersId')) && $this->input->post('staffMembersId') != ""){
			$staffMembersId = $this->input->post('staffMembersId');
		}
		$exists = $this->Admin_model->checkEmailForStaffAndAdmin($email,$staffMembersId);
		echo $exists;
	}



	public function checkEmailForBusiness(){
		$email = "";
		$businessId = 0;
		if(($this->input->post('email')) && $this->input->post('email') != ""){
			$email = $this->input->post('email');
		}
		if(($this->input->post('businessId')) && $this->input->post('businessId') != ""){
			$businessId = $this->input->post('businessId');
		}
		$exists = $this->Admin_model->checkEmailForBusiness($email,$businessId);
		echo $exists;
	}	
	
	public function deleteDocument(){
		$staffMembersId = 0;
		if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
			 $staffMembersId = $this->uri->segment(3);
		}			
		$deleted  = $this->Admin_model->deleteDocument($staffMembersId);
		if($deleted == "DELETED"){
			redirect(base_url()."index.php/Admin/staffProfile/".$staffMembersId."/DEL");
		}elseif($deleted == "NOTDELETED"){
			redirect(base_url()."index.php/Admin/staffProfile/".$staffMembersId."/NTDEL");
		}		
	}
	
	public function paymentTransactions(){		
	$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();	
			//echo "<pre>";print_r($this->input->post());exit;
			$start_time = date('Y-m-01');
			$end_time = date("Y-m-d");
			if(($this->input->post('searchFromTime')) && $this->input->post('searchFromTime') != ""){
				$start_time = $this->input->post('searchFromTime');
				
			}
			if(($this->input->post('searchToTime')) && $this->input->post('searchToTime') != ""){
				$end_time = $this->input->post('searchToTime');
				
			}
			$res = array();
			$res['paymentTransactions'] = $this->Admin_model->getpaymentTransactions($start_time,$end_time);
			$res['msg'] = "";
			$res['start_time'] = $start_time;
			$res['end_time'] = $end_time;
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/paymentTransactions_header');		
			$this->load->view('admin/paymentTransactions',$res);		
			$this->load->view('admin/paymentTransactions_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}		
	
	public function embassies(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();	
			 $countryID = 0;
			 $msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['embassiesData'] = $this->Admin_model->getembassiesData($countryID);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			 $res['countryID'] = $countryID;
			 $res['msg'] = $msg;
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/embassies_header');		
			$this->load->view('admin/embassies',$res);		
			$this->load->view('admin/embassies_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}

	}
	
	
	public function embassyDetails(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();	
		$arrUserData = array();
		$embessayId = 0;		
		$msg = "";			
		if(($this->uri->segment(4)) && $this->uri->segment(4) != ""){
			 $msg = $this->uri->segment(4);
		}			
		if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $embessayId = $this->uri->segment(3);
		}
		
		$arrEmbassyData['embessayId'] 				= 0;
		$arrEmbassyData['residentCountry'] 			= 0;		 
		$arrEmbassyData['foreignCountry'] 			= 0;		 
		$arrEmbassyData['foreignCity'] 				= 0;		 
		$arrEmbassyData['embassyAddress'] 			= "";		 
		$arrEmbassyData['embassyContact'] 			= "";		 		
		
		if(($embessayId) && is_numeric($embessayId) && $embessayId > 0 ){
			$arrReturn  = $this->Admin_model->getEmbassyDetails($embessayId);						
			// echo "<pre>";
			// print_r($arrReturn);
			// exit;
			if(!empty($arrReturn)){
				$arrEmbassyData['embessayId']			 = $arrReturn['embessayId'];	
				$arrEmbassyData['residentCountry']		 = $arrReturn['residentCountry'];								 				
				$arrEmbassyData['foreignCountry']		 = $arrReturn['foreignCountry'];								 				
				$arrEmbassyData['foreignCity']		 	 = $arrReturn['foreignCity'];								 				
				$arrEmbassyData['embassyAddress']		 = $arrReturn['embassyAddress'];								 				
				$arrEmbassyData['embassyContact']		 = $arrReturn['embassyContact'];								 				
				$arrEmbassyData['CityList']		 		 = $arrReturn['CityList'];								 				
			}			
		}
		
		$res['arrAddEditInfo'] = $arrEmbassyData;		
		$res['countryValues'] = $this->Admin_model->getcountryValues();		
		$res['msg'] = $msg;		
		//echo "<pre>";print_r($res);exit;
		$this->load->view('admin/embassyDetails_header');		
		$this->load->view('admin/embassyDetails',$res);		
		$this->load->view('admin/embassyDetails_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	public function getCityListForThisCountry(){
		$foreignCountry = 0;
		$html = "";
		if(($this->input->post('foreignCountry')) && is_numeric($this->input->post('foreignCountry')) && $this->input->post('foreignCountry') > 0){
			$foreignCountry = $this->input->post('foreignCountry');
		}		
		$cityList = $this->Admin_model->getCityListForThisCountry($foreignCountry);		 
		if(($cityList)){
			$html .= "<option value=''>Please Select City</option>";
			foreach($cityList as $value){
				$html.= "<option value='".$value['cityId']."'>".$value['cityName']."</option>";
			}
		}
		echo $html;	
	}
	
	
	public function performEmbassyDetails(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();			
			$embessayId = $this->input->post('embessayId');
			$data = $this->input->post();		
			if($embessayId == 0){
				$inserted = $this->Admin_model->addEmbassyDetails($data);			
				if(($inserted)){								
					redirect(base_url()."index.php/Admin/embassies/ADDSUCC");	
				} else {
					redirect(base_url()."index.php/Admin/embassies/NTADD");	
				}
			}elseif(is_numeric($embessayId) && $embessayId > 0 ){
				$completed = $this->Admin_model->editEmbassyDetails($data);
				if($completed){
					redirect(base_url()."index.php/Admin/embassies/EDSUCC");
				}else{
					redirect(base_url()."index.php/Admin/embassies/EDNTSUCC");
				}
			}
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	public function passport(){
	$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();	
			 $countryID = 0;
			 $msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['passportData'] = $this->Admin_model->getpassportData($countryID);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['countryID'] = $countryID;
			$res['msg'] = $msg;
			$this->load->view('admin/passport_header');		
			$this->load->view('admin/passport',$res);		
			$this->load->view('admin/passport_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
	}
	
	
	public function passportDetails(){
	$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();		
			$arrUserData = array();
			$passportValueId = 0;		
			$msg = "";			
			if(($this->uri->segment(4)) && $this->uri->segment(4) != ""){
				 $msg = $this->uri->segment(4);
			}			
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
					 $passportValueId = $this->uri->segment(3);
			}
		
			$arrPassportData['passportValueId'] 		= 0;
			$arrPassportData['countryId'] 				= 0;		 
			$arrPassportData['countryDetails'] 			= "";		 		
			
			if(($passportValueId) && is_numeric($passportValueId) && $passportValueId > 0 ){
				$arrReturn  = $this->Admin_model->getpassportDetails($passportValueId);									 
				if(!empty($arrReturn)){
					$arrPassportData['passportValueId']			 = $arrReturn['passportValueId'];					
					$arrPassportData['countryId']			 	 = $arrReturn['countryId'];					
					$arrPassportData['countryDetails']			 = $arrReturn['countryDetails'];					
				}			
			}
		
			$res['arrAddEditInfo'] = $arrPassportData;	
			$passportData = $this->Admin_model->getpassportData();
			$res['IgnoreIDS'] = array();
			if(!empty($passportData)){
				foreach($passportData as $value){
					array_push($res['IgnoreIDS'],$value['countryId']);	
				}	
			}			
			//echo "<pre>";print_r($res);exit;
			$res['countryValues'] = $this->Admin_model->getcountryValues();		
			$res['msg'] = $msg;		
			$this->load->view('admin/passportDetails_header');		
			$this->load->view('admin/passportDetails',$res);		
			$this->load->view('admin/passportDetails_footer');
		
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	public function performPassportDetails(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$passportValueId = $this->input->post('passportValueId');
			$data = $this->input->post();		
			if($passportValueId == 0){
				$inserted = $this->Admin_model->addPassportDetails($data);			
				if(($inserted)){								
					redirect(base_url()."index.php/Admin/passport/ADDSUCC");	
				} else {
					redirect(base_url()."index.php/Admin/passport/NTADD");	
				}
			}elseif(is_numeric($passportValueId) && $passportValueId > 0 ){
				$completed = $this->Admin_model->editPassportDetails($data);
				if($completed){
					redirect(base_url()."index.php/Admin/passport/EDSUCC");
				}else{
					redirect(base_url()."index.php/Admin/passport/EDNTSUCC");
				}
			}
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	public function countryInfo(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();	
			 $countryID = 0;
			 $msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['countryData'] = $this->Admin_model->getcountryData($countryID);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['countryID'] = $countryID;
			$res['msg'] = $msg;
			// echo "<pre>";
			// print_r($res);
			// exit;
			$this->load->view('admin/countryInfo_header');		
			$this->load->view('admin/countryInfo',$res);		
			$this->load->view('admin/countryInfo_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
	}
	
	
	public function countryInfoDetails(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();	
			$arrUserData = array();
			$countryInformationID = 0;		
			$msg = "";			
			if(($this->uri->segment(4)) && $this->uri->segment(4) != ""){
				 $msg = $this->uri->segment(4);
			}			
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
					 $countryInformationID = $this->uri->segment(3);
			}
			
			$arrPassportData['countryInformationID'] 		= 0;
			$arrPassportData['countryId'] 					= 0;		 
			$arrPassportData['countryDetails'] 				= "";		 		
			
			if(($countryInformationID) && is_numeric($countryInformationID) && $countryInformationID > 0 ){
				$arrReturn  = $this->Admin_model->getCountryInfoDetails($countryInformationID);						
				// echo "<pre>";
				// print_r($arrReturn);
				// exit;
				if(!empty($arrReturn)){
					$arrPassportData['countryInformationID']			 = $arrReturn['countryInformationID'];					
					$arrPassportData['countryId']			 	 		 = $arrReturn['countryId'];					
					$arrPassportData['countryDetails']			 		 = $arrReturn['countryDetails'];					
				}			
			}
			
			$res['arrAddEditInfo'] = $arrPassportData;	
			$countryData = $this->Admin_model->getcountryData();	
			$res["ignoreIDs"] = array();
			foreach($countryData as $value){
				array_push($res["ignoreIDs"],$value['countryId']);
			}
			$res['countryValues'] = $this->Admin_model->getcountryValues();		
			$res['msg'] = $msg;		
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/countryInfoDetails_header');		
			$this->load->view('admin/countryInfoDetails',$res);		
			$this->load->view('admin/countryInfoDetails_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}
	}
	
	
	public function performCountryInfoDetails(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$countryInformationID = $this->input->post('countryInformationID');
			$data = $this->input->post();		
			if($countryInformationID == 0){
				$inserted = $this->Admin_model->addCountryInfoDetails($data);			
				if(($inserted)){								
					redirect(base_url()."index.php/Admin/countryInfo/ADDSUCC");	
				} else {
					redirect(base_url()."index.php/Admin/countryInfo/NTADD");	
				}
			}elseif(is_numeric($countryInformationID) && $countryInformationID > 0 ){
				$completed = $this->Admin_model->editCountryInfoDetails($data);
				if($completed){
					redirect(base_url()."index.php/Admin/countryInfo/EDSUCC");
				}else{
					redirect(base_url()."index.php/Admin/countryInfo/EDNTSUCC");
				}
			}
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}		
	}
	
	public function userReport(){
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			$countryID = 0;
			$cityId = 0;
			$msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->input->post('cityId')) && is_numeric($this->input->post('cityId')) && $this->input->post('cityId') > 0){
				 $cityId = $this->input->post('cityId');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['UsersData'] = $this->Admin_model->getUsersReportData($countryID,$cityId);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['cityValues'] = $this->Admin_model->getcityValues();
			$res['CityList'] = $this->Admin_model->getCityListForThisCountry($countryID);			
			$res['UsersDataFULL'] = count($this->Admin_model->getUsersReportData());
			
			$res['countryID'] = $countryID;
			$res['cityId'] = $cityId;
			$res['msg'] = $msg;
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/userReport_header');		
			$this->load->view('admin/userReport',$res);		
			$this->load->view('admin/userReport_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}		
	}
	
	
	public function businessReport(){		
		$loggedIN = $this->Admin_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$this->Admin_model->checkStaffAccess();
			// echo "<pre>";
			// print_r($this->input->post());
			// echo "</pre>";
			$countryID = 0;
			$cityId = 0;
			$msg = "";
			if(($this->input->post('countryID')) && is_numeric($this->input->post('countryID')) && $this->input->post('countryID') > 0){
				 $countryID = $this->input->post('countryID');
			}
			if(($this->input->post('cityId')) && is_numeric($this->input->post('cityId')) && $this->input->post('cityId') > 0){
				 $cityId = $this->input->post('cityId');
			}
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				 $msg = $this->uri->segment(3);
			}			
			$res['busniessData'] = $this->Admin_model->getbusniessUsersData($countryID,$cityId);
			$res['countryValues'] = $this->Admin_model->getcountryValues();
			$res['categoryValues'] = $this->Admin_model->getcategoryValues();
			$res['countryID'] = $countryID;
			$res['cityId'] = $cityId;
			$res['msg'] = $msg;
			$res['CityList'] = $this->Admin_model->getCityListForThisCountry($countryID);
			$res['businessDataFULL'] = $this->Admin_model->getbusniessUsersCount();
			//echo "<pre>";print_r($res);exit;
			$this->load->view('admin/businessReport_header');		
			$this->load->view('admin/businessReport',$res);		
			$this->load->view('admin/businessReport_footer');
		}else{
			redirect(base_url()."index.php/Admin/index/NTLGD");
		}	
	}

	public function businessProfile(){
		$msg = "";
		$businessId = 0;
		$idsToIgnore = array("0" => "4");
		$delMsg = "";
		if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
			if(is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				$businessId = $this->uri->segment(3);
			}else{
				$msg = $this->uri->segment(3);
			}			
		}
		if(($this->uri->segment(4)) && $this->uri->segment(4) != ""){
			$delMsg = $this->uri->segment(4);						
		}
		 // echo "<pre>";
		 // print_r($businessId);
		 // exit;
		$arrUserData['businessId'] 				= "0";
		$arrUserData['businessMerchantName'] 	= "";		 
		$arrUserData['businessName'] 			= "";		 
		$arrUserData['businessPhone'] 			= "";		 
		$arrUserData['businessEmail'] 			= "";		 
		$arrUserData['businessAddress1'] 		= "";		 
		$arrUserData['businessAddress2'] 		= "";		 
		$arrUserData['businessArea'] 			= "";		 		
		$arrUserData['cityId'] 					= "0";		 
		$arrUserData['stateId'] 				= "0";		 
		$arrUserData['countryId'] 				= "0";	
		$arrUserData['officePictures'] 			= "";
		$arrUserData['cityList'] 				= array();		 			
		$arrUserData['StateList'] 				= array();
		$arrUserData['selectedCategories'] 		= array();		
		$arrUserData['serviceTags'] = "Visa,Travel Consultancy";
		
		if(isset($businessId) && is_numeric($businessId) && $businessId >0 ){
			$businessData = $this->Admin_model->getBusinessDataFromID($businessId);
			$StateList = $this->Admin_model->getStateList($businessData['businessesData']['countryId']);	
			$cityList = $this->Admin_model->getCityList($businessData['businessesData']['stateId']);
			$purchasedPackage = $this->Admin_model->getSelectedPackagesForThisBusiness($businessId);
			if(!empty($purchasedPackage) && !empty($purchasedPackage['allData'])){
				$arrUserData['purchasedPackage'] 				= $purchasedPackage['allData'];
				$idsToIgnore = $purchasedPackage['ignoreIds'];
			}else{
				$idsToIgnore = array("0" => "4");	
			}	
			// echo "<pre>";
			// print_r($purchasedPackage);
			// exit;
			//echo "<pre>";print_r($businessData);exit;
			$arrUserData['businessId'] 				= $businessId;
			$arrUserData['businessMerchantName'] 	= $businessData['businessesData']['businessMerchantName'];		 
			$arrUserData['businessName'] 			= $businessData['businessesData']['businessName'];		 
			$arrUserData['businessPhone'] 			= $businessData['businessesData']['businessPhone'];		 
			$arrUserData['businessEmail'] 			= $businessData['businessesData']['businessEmail'];		 
			$arrUserData['businessAddress1'] 		= $businessData['businessesData']['businessAddress1'];		 
			$arrUserData['businessAddress2'] 		= $businessData['businessesData']['businessAddress2'];		 
			$arrUserData['businessArea'] 			= $businessData['businessesData']['businessArea'];		 		
			$arrUserData['cityId'] 					= $businessData['businessesData']['cityId'];		 
			$arrUserData['stateId'] 				= $businessData['businessesData']['stateId'];		 
			$arrUserData['countryId'] 				= $businessData['businessesData']['countryId'];		 			
			$arrUserData['cityList'] 				= $cityList;		 			
			$arrUserData['StateList'] 				= $StateList;		 			
			$arrUserData['selectedCategories'] 		= $businessData['selectedCategories'];		
			if(isset($businessData['officePictures']['images']) && !empty($businessData['officePictures']['images'])){
				$arrUserData['officePictures'] 			= $businessData['officePictures']['images'];		 				
			}else{
				$arrUserData['officePictures'] 			= "";
			}
			if(isset($businessData['serviceTags'])){
				$arrUserData['serviceTags'] 			= $businessData['serviceTags'];		 				
			}else{
				$arrUserData['serviceTags'] 			= "Visa,Travel Consultancy";
			}
			if(!empty($businessData['finalopeninghours'])){
				$arrUserData['finalopeninghours'] 			= $businessData['finalopeninghours'];		 				
			}else{
				$arrUserData['finalopeninghours'] 			= array();
			}
			if(!empty($businessData['ServicePictures'])){
				$arrUserData['ServicePictures'] 			= $businessData['ServicePictures'];		 				
			}else{
				$arrUserData['ServicePictures'] 			= array();
			}				
		}
		$res['arrAddEditInfo'] = $arrUserData;		
		$res['msg'] = $msg;
		$res['delMsg'] = $delMsg;
		$res['countryValues'] = $this->Admin_model->getcountryValues();			
		$res['categoryValues'] = $this->Admin_model->getcategoryValues();				
		$res['notSelectedPackages'] = $this->Admin_model->getNotSelectedPackages($idsToIgnore);
		//echo "<pre>";print_r($res);exit;
		
		
		$this->load->view('admin/businessProfile_header');		
		$this->load->view('admin/businessProfile',$res);		
		$this->load->view('admin/businessProfile_footer');	
	}
	
	
	public function performBusinessProfile(){
		// echo "<pre>";print_r($_REQUEST);echo "</pre>";
		// exit;
		$data = array();	
		if(($this->input->post())){
			$data = $this->input->post();
		}
		$businessId = 0;
		if(($this->input->post('businessId')) && is_numeric($this->input->post('businessId'))){
			$businessId = $this->input->post('businessId');
		}
		if($businessId > 0){
			$update_data = $this->Admin_model->performEditBusinessProfile($data);		
			if ($update_data['updated'] == "updated") {	
				redirect(base_url()."index.php/Admin/busniessUsers/EDSUCC");			
			}else{
				redirect(base_url()."index.php/Admin/busniessUsers/EDNTSUCC");
			}			
		}elseif($businessId == "0"){
			$insert_data = $this->Admin_model->performBusinessProfile($data);		
			if(!empty($insert_data) && $insert_data['inserted'] == "inserted"){
			$msg  = "Hello ".$insert_data['businessMerchantName'].",<br/>
						You have been successfully Registered with us.<br/>						
						Please find the login details:<br/>
						Email Address : ".$insert_data['businessEmail']."<br/>
						Password : ".$insert_data['password']."<br/>
						Login Link :".base_url()."adminPanel/index.php/Home<br/>
						
						We wish to have a long Association with you.<br/><br/>
						Thanks & Regards<br/>
						Team MyVisaForTravel";
						 				
				
				$this->load->library('email');				
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('admin@solulab.com'); // change it to yours
				$this->email->to($insert_data['businessEmail']); // change it to yours
				$this->email->subject('Registration Successfull');
				$message = $msg;
				$this->email->message($message);				
				if ($this->email->send()) {	
					redirect(base_url()."index.php/Admin/busniessUsers/SUCC");			
				}else{
					redirect(base_url()."index.php/Admin/busniessUsers/MLNTSNT");
				}
			}else{
				redirect(base_url()."index.php/Admin/businessProfile/EMEXITS");			
			}	
		}		
	}
	
	public function deleteOfficePicture(){
		$dataid = 0;
		if(($this->input->post('dataid')) && is_numeric($this->input->post('dataid')) && $this->input->post('dataid') > 0){
			$dataid = $this->input->post('dataid');
		}
		$deleted = $this->Admin_model->deleteOfficePicture($dataid);
		echo $deleted;		
	}
	
	public function deleteOfficeServicePicture(){
		$dataid = 0;
		if(($this->input->post('dataid')) && is_numeric($this->input->post('dataid')) && $this->input->post('dataid') > 0){
			$dataid = $this->input->post('dataid');
		}
		$deleted = $this->Admin_model->deleteOfficeServicePicture($dataid);
		echo $deleted;		
	}
	
	public function changePassword(){
		$this->load->view('admin/changePassword_header');		
		$this->load->view('admin/changePassword');		
		$this->load->view('admin/changePassword_footer');
	}
	
	public function performChangePassword(){		
		$data = $this->input->post();
		$updated = $this->Admin_model->performChangePassword($data);
		if(($updated) && $updated == "Updated"){
			if($_SESSION['LoggedInType'] == "Admin"){
				redirect(base_url()."index.php/Admin/dashboard/PWCHNG");		
			}elseif($_SESSION['LoggedInType'] == "Staff"){
				redirect(base_url()."index.php/Admin/busniessUsers/PWCHNG");
			}			
		}else{
			if($_SESSION['LoggedInType'] == "Admin"){
				redirect(base_url()."index.php/Admin/dashboard/PWNTCHNG");		
			}elseif($_SESSION['LoggedInType'] == "Staff"){
				redirect(base_url()."index.php/Admin/busniessUsers/PWNTCHNG");
			}					
		}
		
	}
	
	public function removePackageOfUser(){		
		$packageId = 0;
		$businessId = 0;
		if(($this->input->post('packageId')) && is_numeric($this->input->post('packageId')) && $this->input->post('packageId') > 0){
			 $packageId = $this->input->post('packageId');
		}
		if(($this->input->post('businessId')) && is_numeric($this->input->post('businessId')) && $this->input->post('businessId') > 0){
			 $businessId = $this->input->post('businessId');
		}
		$removed = $this->Admin_model->removePackageOfUser($packageId,$businessId);
		echo $removed;
	}
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	
	public function __construct(){
        parent::__construct();		
		$this->load->model('Home_model');		
		$this->load->model('Mail_model');		
    } 
	
	public function index(){					
		
		
		$this->load->view('payment/stripetest');
		
	}
	
	
	public function chargeCustomer() {			
	 
		include('assests/plugins/vendor/autoload.php');//include PayPal SDK				
		include('assests/plugins/vendor/functions.inc.php');//our PayPal functions	
		if(isset($_GET["token"]) && isset($_GET["PayerID"]) && isset($_SESSION["payment_id"])){
	try{
		$result = execute_payment($_SESSION["payment_id"], $_GET["PayerID"]);  //call execute payment function.	 
		
		if($result->state == "approved"){ //if state = approved continue..
			//SUCESS
			
			//unset($_SESSION["payment_id"]); //unset payment_id, it is no longer needed 
			
			//get transaction details
			$transaction_id 		= $result->transactions[0]->related_resources[0]->sale->id;
			$transaction_time 		= $result->transactions[0]->related_resources[0]->sale->create_time;
			$transaction_currency 	= $result->transactions[0]->related_resources[0]->sale->amount->currency;
			$transaction_amount 	= $result->transactions[0]->related_resources[0]->sale->amount->total;
			$transaction_method 	= $result->payer->payment_method;
			$transaction_state 		= $result->transactions[0]->related_resources[0]->sale->state;
			
			//get payer details
			$payer_first_name 		= $result->payer->payer_info->first_name;
			$payer_last_name 		= $result->payer->payer_info->last_name;
			$payer_email 			= $result->payer->payer_info->email;
			$payer_id				= $result->payer->payer_info->payer_id;
			
			//get shipping details 
			$shipping_recipient		= $result->transactions[0]->item_list->shipping_address->recipient_name;
			$shipping_line1			= $result->transactions[0]->item_list->shipping_address->line1;
			$shipping_line2			= $result->transactions[0]->item_list->shipping_address->line2;
			$shipping_city			= $result->transactions[0]->item_list->shipping_address->city;
			$shipping_state			= $result->transactions[0]->item_list->shipping_address->state;
			$shipping_postal_code	= $result->transactions[0]->item_list->shipping_address->postal_code;
			$shipping_country_code	= $result->transactions[0]->item_list->shipping_address->country_code;
			$data = array();
			if(($_SESSION['paymentFormData'])){
				$data = $_SESSION['paymentFormData'];			
			}		
			$recidKeys = array();
			$i = 0;
			$monthsCount = array();
			foreach($data['months'] as $key => $value){
				$recidKeys[$i] = $key;
				$monthsCount[$i] = $value;
				$i++;
			}		
			$monthlyAmount = array();
			$ordermasterID = $data['hidOrderMasterId'];
			$allData = $this->Home_model->getSelectedPackageDetails($ordermasterID);
		
			foreach($allData as $value){
				if(in_array($value['recid'],$recidKeys)){
					array_push($monthlyAmount,$value['packageAmount']);
					
				}
			}
		
		
			$total_pay = array_map(function($monthsCount, $monthlyAmount) {
				return $monthsCount * $monthlyAmount;
			}, $monthsCount, $monthlyAmount);
			$finalTot = array_sum($total_pay);		
			$finalTot = number_format($finalTot,"2",".","");
			
		//ordermaster table
		$data_ordermaster = array(
			'flagActive' 			=> 0,
			'flagCompleted' 			=> 1,
			'totalAmountCharged' 	=> $finalTot
		);		

		$this->db->where('recid', $ordermasterID);
		$this->db->update('orderMaster', $data_ordermaster); 
		
		
		
		//orderpackagemaster table
		foreach($allData as $value){
			if(in_array($value['recid'],$recidKeys)){
				$orderPakageId = $value['recid'];				
				if(isset($data['months'][$orderPakageId])) {
					$monthCount = $data['months'][$orderPakageId];
					$multiplication = $monthCount * $value['packageAmount'];
					$data_orderpackagemaster = array(
						'monthsSubscribed' 			=> $monthCount,
						'totalPackageAmount' 		=> $multiplication 
					);		 
					$this->db->where('recid', $orderPakageId);
					$this->db->update('orderpackagemaster', $data_orderpackagemaster); 
					$CheckPaymentType = $this->Home_model->getPaymentType($orderPakageId);
					if($CheckPaymentType == "RENEW"){
						$this->db->select('DATE_FORMAT(packageExpiryDate,"%Y-%m-%d") as packageExpiryDate ');
						$this->db->from('packagesOfBusiness');
						$this->db->where('packageId', $value['packageid']);				
						$this->db->where('businessId', $_SESSION['businessId']);				
						$query_packagesOfBusiness = $this->db->get();		
						$res_packagesOfBusiness = $query_packagesOfBusiness->result_array();			
						if(!empty($res_packagesOfBusiness)){				
							$packageExpiryDate = $res_packagesOfBusiness[0]['packageExpiryDate'];
						}
						$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $monthCount . ' months', strtotime($packageExpiryDate)));
						$data_packagesOfBusiness = array(
							'packageExpiryDate' 			=> $packageExpiryDate							
						);		 
						$this->db->where('packageId', $value['packageid']);				
						$this->db->where('businessId', $_SESSION['businessId']);				
						$this->db->update('packagesOfBusiness', $data_packagesOfBusiness); 
						
						
					}elseif($CheckPaymentType == "NEW"){
						$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+' . $monthCount . ' months', strtotime(TODAY)));
						$data_packagesofbusiness = array(
							'packageId' 			=> $value['packageid'],						
							'businessId' 			=> $_SESSION['businessId'],						
							'packagePurchaseDate' 	=> TODAY,
							'packageExpiryDate' 	=> $packageExpiryDate,
							'isActive' 				=> 1,
							'isExpired' 			=> 0
						);									 
						$this->db->insert('packagesOfBusiness', $data_packagesofbusiness);						
					}
				}
			}
		}
		$check = $this->Home_model->CheckFreePlan($_SESSION['businessId']);
		if($check){
			$packageExpiryDate = date('Y-m-d H:i:s',strtotime('+12 years', strtotime(TODAY)));
			$data_packagesofbusiness = array(
			'packageId' 			=> 4,
			'businessId' 			=> $_SESSION['businessId'],						
			'packagePurchaseDate' 	=> TODAY,
			'packageExpiryDate' 	=> $packageExpiryDate,
			'isActive' 				=> 1,
			'isExpired' 			=> 0
		);				
		$this->db->insert('packagesOfBusiness', $data_packagesofbusiness);	
		}	
		
		$data_payment = array(
			'ordermasterID' 			=> $ordermasterID,						
			'totalamount' 				=> $finalTot,						
			'paymentDate' 				=> $transaction_time,
			'country' 					=> "NOTIndia",
			'businessId' 				=> $_SESSION['businessId'],
			'paymentStatus' 			=> 1,
			'transaction_currency' 		=> $transaction_currency,
			'payment_method' 			=> $transaction_method,
			'transaction_state' 		=> $transaction_state,
			'transaction_id' 			=> $transaction_id
		);						
		$this->db->insert('payment', $data_payment);		
		$last_payment_id  = $this->db->insert_id();
		
		
		$data_paypal_response = array(
			'paymentID' 				=> $last_payment_id,						
			'first_name' 				=> $payer_first_name,						
			'last_name' 				=> $payer_last_name,						
			'email' 					=> $payer_email,						
			'payer_id' 					=> $payer_id,
			'shipping_recipient' 		=> $shipping_recipient,
			'shipping_line1' 			=> $shipping_line1,
			'shipping_line2' 			=> $shipping_line2,
			'shipping_city' 			=> $shipping_city,
			'shipping_state' 			=> $shipping_state,
			'shipping_postal_code' 		=> $shipping_postal_code,
			'shipping_country_code' 	=> $shipping_country_code,
			'createdDate' 				=> TODAY
			
		);						
		$this->db->insert('paypal_response', $data_paypal_response);
		
		 // $this->session->unset_userdata('paymentFormData');
			redirect(base_url().'index.php/Home/dashboard/Succ');
				
			
			
		}
		
	}catch(PPConnectionException $ex) {
		$ex->getData();
	} catch (Exception $ex) {
		echo $ex->getMessage();
	}

	}		
	}

		
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public $email_config = array();
	
	public function __construct(){
        parent::__construct();		
		$this->load->model('Home_model');		
		$this->load->model('Mail_model');				
    } 
	
	public function index(){					
		$res['msg'] = "";		
		if($this->uri->segment(3) != ""){
			$res['msg'] = $this->uri->segment(3);
		}
		$this->load->view('header');
		$this->load->view('home',$res);
		$this->load->view('footer');
	}

	public function registerBusiness(){
		$data = array();
		if(($this->input->post())){
			$data = $this->input->post();
		}		
		$email = $data['businessEmail'];
		$checkEmailExists = $this->Home_model->checkEmailExists($email);		
		if($checkEmailExists && is_numeric($checkEmailExists) && $checkEmailExists == 1){
			$inserted = $this->Home_model->registerBusiness($data);
			if(($inserted) && is_numeric($inserted) && $inserted > 0) {
				
				$msg  = "Hello ".$data['businessMerchantName'].",<br/>
						You have been successfully Registered with us.<br/>						
						We wish to have a long Association with you.<br/><br/>
						Thanks & Regards<br/>
						Team MyVisaForTravel";
						$mail_data = array(													
							'to_mail' => $email,
							'subject' => "Registration Successfull",
							'message' => $msg						
						);				
				
				$this->load->library('email');
				$this->email->initialize($this->email_config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('admin@solulab.com'); // change it to yours
				$this->email->to($email); // change it to yours
				$this->email->subject('Registration Successfull');
				$message = $msg;
				$this->email->message($message);				
				if ($this->email->send()) {
					redirect(base_url()."index.php/Home/index/SUCC");	
				} else {
					echo $this->email->print_debugger();
				}		
				
			}else{
				redirect(base_url()."index.php/Home/index/ERR");	
			}	
		}else{
			redirect(base_url()."index.php/Home/index/EMAILEX");	
		}				
	}		
	
	
	public function businessMerchantLogin(){
		$data = array();
		if(($this->input->post())){
			$data = $this->input->post();
		}		
		$loggedIn = $this->Home_model->businessMerchantLogin($data);				
		
		if(($loggedIn) && is_numeric($loggedIn) && $loggedIn == 1){
			if(isset($_SESSION['businessId'])){
				if(is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
					$purchasedPackage = $this->Home_model->checkPurchasedPackage($_SESSION['businessId']);	
					if($purchasedPackage == "Package"){
						redirect(base_url()."index.php/Home/packages");	
					}elseif($purchasedPackage == "AddProfile"){
						redirect(base_url()."index.php/Home/addProfile/LGD");	
					}elseif($purchasedPackage == "DashBoard"){
						redirect(base_url()."index.php/Home/dashboard");	
					}						
				}
			}
		}elseif(($loggedIn) && is_numeric($loggedIn) && $loggedIn == 0){
			redirect(base_url()."index.php/Home/index/INVLD");
		}elseif(($loggedIn) && is_numeric($loggedIn) && $loggedIn == 2){
			redirect(base_url()."index.php/Home/index/BLOCKED");
		}		
	}
	
	public function businessForgotPassword(){
		$forgotEmailID = "";
		if(($this->input->post())){
			$forgotEmailID = $this->input->post('forgotEmailID');
		}
		$res = $this->Home_model->getBusinessDetailsFromEmail($forgotEmailID);		
		if(($res)){
			$id_md5 = md5($res['businessMerchantName']);
			$link = base_url()."index.php/Home/forgotPasswordPage/".$res['businessId'];
			$msg  = "Hello ".$res['businessMerchantName'].",<br/>					
					You have received this message because you have requested to reset your password on the My Visa For travel website.<br/>
					Please, follow this link to create a new password: <br/>
					".$link."<br/>
					Thank you for your trust in our solutions,<br/> 
					Team MyVisaForTravel";					 
						
				$this->load->library('email');
				$this->email->initialize($this->email_config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('admin@solulab.com'); // change it to yours
				$this->email->to($forgotEmailID); // change it to yours
				$this->email->subject('Reset Password');
				$message = $msg;
				$this->email->message($message);				
				if ($this->email->send()) {
					redirect(base_url()."index.php/Home/index/EMAILSNT");		
				} else {
					echo $this->email->print_debugger();
				}
		}else{
			redirect(base_url()."index.php/Home/index/EMAILNF");	
		}		
	}
	
	
	public function forgotPassword(){
		$this->load->view('forgotPassword');
	}
	
	
	public function setNewPassword(){
		
	}
	
	public function packages(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$checkPackages = $this->Home_model->checkPackages();
			if(!empty($checkPackages)){
				if(count($checkPackages) >= 1){
					$redirect = true;
				}else{
					$redirect = false;
				}
			}else{
				$redirect = false;
			}
			//var_dump($redirect);exit;
			if(($redirect) && $redirect == true){
				redirect(base_url()."index.php/Home/dashboard");
			}elseif(!($redirect)){				
				$res['packageData'] = $this->Home_model->getpackageData();
				$this->load->view('packages_header');
				$this->load->view('packages',$res);
				$this->load->view('packages_footer');	
			}			
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}
	}

	public function insertOrderMaster(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){	
			// echo "<pre>";
			// print_r($this->input->post());
			// exit;
			$data = array();	
			if(($this->input->post())){
				$data = $this->input->post();	
			}
			$inserted = $this->Home_model->insertOrderMaster($data);
			if(($inserted) && is_numeric($inserted) && $inserted > 0 ){
				redirect(base_url()."index.php/Home/paymentOption/".$inserted);	
			}
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}
	}
	public function dashBoard(){		
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){	
			$msg = "";			
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				$msg = $this->uri->segment(3);
			}
			$res['businessesData'] = $this->Home_model->getDashboardData();		
			$res['packagePurchased'] = $this->Home_model->getPackageDetails();		
			$res['msg'] = $msg;						
			//echo "<pre>";print_r($res);exit;
			$this->load->view('dashboard_header');		
			$this->load->view('dashboard',$res);
			$this->load->view('dashboard_footer');
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}
	}	
	public function addProfile(){		
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$msg = "";
			if(($this->uri->segment(3)) && $this->uri->segment(3) != ""){
				$msg = $this->uri->segment(3);
			}
			$res['countryValues'] = $this->Home_model->getcountryValues();
			$res['categoryValues'] = $this->Home_model->getcategoryValues();
			$res['msg'] = $msg;
			//echo "<pre>";print_r($res);exit;
			$this->load->view('addProfile_header');	
			$this->load->view('addProfile',$res);	
			$this->load->view('addProfile_footer');	
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}
	}
	public function editProfile(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){
			$businessId = 0;
			if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
				$businessId = $_SESSION['businessId'];
			}
			$res = $this->Home_model->getEditProfileData($businessId);		
			$this->load->view('editProfile_header');
			$this->load->view('editProfile',$res);
			$this->load->view('editProfile_footer');
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}
	}
	public function performAddProfile(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){	
			$data = array();	
			if(($this->input->post())){
				$data = $this->input->post();
			}
			$insert_data = $this->Home_model->AddProfileData($data);
			if($insert_data == "Inserted"){
				redirect(base_url()."index.php/Home/dashBoard");			
			}
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}	
	}
	public function getStateList(){
		$country_id = 0;
		$html = "";
		if(($this->input->post('country_id')) && is_numeric($this->input->post('country_id')) && $this->input->post('country_id') > 0){
			$country_id = $this->input->post('country_id');
		}
		$StateList = $this->Home_model->getStateList($country_id);
		if(($StateList)){
			$html .= "<option value=''>Select State</option>";
			foreach($StateList as $value){
				$html.= "<option value='".$value['stateId']."'>".$value['stateName']."</option>";
			}
		}
		echo $html;
	}
	
	
	public function getCityList(){
		
		$state_id = 0;
		$html = "";
		if(($this->input->post('state_id')) && is_numeric($this->input->post('state_id')) && $this->input->post('state_id') > 0){
			$state_id = $this->input->post('state_id');
		}
		$CityList = $this->Home_model->getCityList($state_id);
		if(($CityList)){
			$html .= "<option value=''>Select City</option>";
			foreach($CityList as $value){
				$html.= "<option value='".$value['cityId']."'>".$value['cityName']."</option>";
			}
		}
		echo $html;
	}
	
	public function uploadOfficePictures(){
		if(!move_uploaded_file($_FILES['files']['tmp_name'][0], 'uploads/'.$_FILES['files']['name'][0])){
			echo "error";
		}
		echo "success";
	}
	
	
	public function addOfficePicture(){		
		$uploadedImage = $this->Home_model->addOfficePicture();
		if(($uploadedImage)){
			$arrReturn = json_encode($uploadedImage);
		}
		echo $arrReturn;
	}
	
	public function deleteOfficePicture(){
		$dataid = 0;
		if(($this->input->post('dataid')) && is_numeric($this->input->post('dataid')) && $this->input->post('dataid') > 0){
			$dataid = $this->input->post('dataid');
		}
		$deleted = $this->Home_model->deleteOfficePicture($dataid);
		echo $deleted;		
	}
	
	
	public function deleteOfficeServicePicture(){
		$dataid = 0;
		if(($this->input->post('dataid')) && is_numeric($this->input->post('dataid')) && $this->input->post('dataid') > 0){
			$dataid = $this->input->post('dataid');
		}
		$deleted = $this->Home_model->deleteOfficeServicePicture($dataid);
		echo $deleted;		
	}
	
	
	public function addOfficeServicePicture(){		
		$uploadedImage = $this->Home_model->addOfficeServicePicture();
		if(($uploadedImage)){
			$arrReturn = json_encode($uploadedImage);
		}
		echo $arrReturn;
	}

	public function paymentOption(){		
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			$ordermasterID = 0;
			if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
				$ordermasterID = $this->uri->segment(3);
			}
			$res['packageDetails'] = $this->Home_model->getSelectedPackageDetails($ordermasterID);
			$res['ordermasterID'] = $ordermasterID;			
			$res['countryData'] = $this->Home_model->getcountryValues();			
			$this->load->view('paymentOption_header');
			$this->load->view('paymentOption',$res);
			$this->load->view('paymentOption_footer');
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");	
		}
	}
		
	public function subscription(){
		$businessId = 0;
		$idsToIgnore = array();
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];	
		}
		$res['selectedPackages'] = $this->Home_model->getSelectedPackagesForThisBusiness($businessId);
		if(($res['selectedPackages'])){
			$idsToIgnore = $res['selectedPackages']['ignoreIds'];
		}
		$res['notSelectedPackages'] = $this->Home_model->getNotSelectedPackages($idsToIgnore);
		$this->load->view('subscription_header');
		$this->load->view('subscription',$res);
		$this->load->view('subscription_footer');
	}
	
	public function performEditProfile(){		
		$data = array();	
		if(($this->input->post())){
			$data = $this->input->post();
		}
		$updated_data = $this->Home_model->EditProfileData($data);
		if($updated_data == "Updated"){
			redirect(base_url()."index.php/Home/dashBoard");			
		}	
	}
	
	public function deletePackageFromOrderMaster(){		
		$ordermasterID = 0;
		$orderpackagemasterID = 0;
		if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
			$ordermasterID = $this->uri->segment(3);
		}
		if(($this->uri->segment(4)) && is_numeric($this->uri->segment(4)) && $this->uri->segment(4) > 0){
			$orderpackagemasterID = $this->uri->segment(4);
		}
		$deleted = $this->Home_model->deletePackageFromOrderMaster($orderpackagemasterID,$ordermasterID);
		if($deleted){
			redirect(base_url()."index.php/Home/paymentOption/".$ordermasterID);		
		}else{
			redirect(base_url());		
		}
	}
	
	public function logout(){
		session_unset();
		redirect(base_url()."index.php/Home");		
	}
	
	public function redirectUserProfile(){
		$businessId = 0;
		if(isset($_SESSION['businessId']) && is_numeric($_SESSION['businessId']) && $_SESSION['businessId'] > 0){
			$businessId = $_SESSION['businessId'];
		}
		$check = $this->Home_model->redirectUserProfile($businessId);
		echo $check;	
	}
	
	public function analytics(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){						
			$fromDate = "";
			$toDate = "";
			if((($this->input->post('fromDate'))) && $this->input->post('fromDate') != ""){
				$fromDate = $this->input->post('fromDate');
			}
			if((($this->input->post('toDate'))) && $this->input->post('toDate') != ""){
				$toDate = $this->input->post('toDate');
			}
			//echo "<pre>";print_r($this->input->post());exit;
			
			$res['data'] = $this->Home_model->getAnalyticsData($fromDate,$toDate);
			$res['fromDate'] = $fromDate;
			$res['toDate'] = $toDate;
			
			$this->load->view('analytics_header');				
			$this->load->view('analytics',$res);				
			$this->load->view('analytics_footer');				
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");		
		}

	}
	
	public function getClicksBifurcation(){		
		$currentElementDate = "";
		if($this->input->post('currentElementDate') != "" ){
			 $currentElementDate = $this->input->post('currentElementDate');
		}
		$data = $this->Home_model->getClicksBifurcation($currentElementDate);		
		if(($data)){
			$data['status'] = "sucess";
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);
		}else{
			$data = array("status" => "error");
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);			
		}
	}


	public function getClicksBifurcationWithPackage(){		
		$currentElementDate = "";
		$hidpackageSelection = "";
		if($this->input->post('currentElementDate') != "" ){
			 $currentElementDate = $this->input->post('currentElementDate');
		}
		if($this->input->post('hidpackageSelection') != "" ){
			 $hidpackageSelection = $this->input->post('hidpackageSelection');
		}
		$data = $this->Home_model->getClicksBifurcationWithPackage($currentElementDate,$hidpackageSelection);		
		if(($data)){
			$data['status'] = "sucess";
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);
		}else{
			$data = array("status" => "error");
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);			
		}
	}
	
	
	public function getBookmarksBifurcation(){		
		$currentElementDate = "";
		if($this->input->post('currentElementDate') != "" ){
			 $currentElementDate = $this->input->post('currentElementDate');
		}
		$data = $this->Home_model->getBookmarksBifurcation($currentElementDate);				
		if(($data)){
			$data['status'] = "sucess";
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);
		}else{
			$data = array("status" => "error");
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);			
		}
	}
	
	
	public function getReviewsBifurcation(){		
		$currentElementDate = "";
		if($this->input->post('currentElementDate') != "" ){
			 $currentElementDate = $this->input->post('currentElementDate');
		}
		$data = $this->Home_model->getReviewsBifurcation($currentElementDate);				
		if(($data)){
			$data['status'] = "sucess";
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);
		}else{
			$data = array("status" => "error");
			$strJson = json_encode($data);
			$this->output->set_content_type('application/json')->set_output($strJson);			
		}
	}
	
	
	public function reports(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){			
			// echo "<pre>";
			// print_r($this->input->post());
			// echo "</pre>";
			if(($this->input->post('packageSelection')) && $this->input->post('packageSelection') != '' ){
				 $packageSelection = $this->input->post('packageSelection');
			}elseif(($this->input->post('hidpackageSelection')) && $this->input->post('hidpackageSelection') != '' ){
				$packageSelection = $this->input->post('hidpackageSelection');				
			}
			if((($this->input->post('fromDate'))) && $this->input->post('fromDate') != ""){
				$fromDate = $this->input->post('fromDate');
			}
			if((($this->input->post('toDate'))) && $this->input->post('toDate') != ""){
				$toDate = $this->input->post('toDate');
			}
			if(isset($packageSelection)){
				if(isset($fromDate) && isset($toDate)){
					$res['packageWiseData'] = $this->Home_model->getPackageWiseAnalytics($packageSelection,$fromDate,$toDate);
					$res['fromDate'] = $fromDate;
					$res['toDate'] = $toDate;
				}else{
					$res['packageWiseData'] = $this->Home_model->getPackageWiseAnalytics($packageSelection,"","");	
				}				
				$res['packageSelection'] = $packageSelection;
			}
			
			$res['data'] = $this->Home_model->getReportsData();			
			$this->load->view('reports_header');				
			$this->load->view('reports',$res);
			$this->load->view('reports_footer');
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");		
		}

	}
	
	
	public function bypassPaymentAndInsertData(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){	
			$data = array();
			if(($this->input->post())){
				$data = $this->input->post();			
			}				
			
			//$this->session->set_userdata('paymentFormData', $data);//Save post data into session	
			if($data['currencyDropdown'] == "NOTIndia"){			
				$_SESSION['paymentFormData'] = $data;
				$ordermasterID = $data['hidOrderMasterId'];
				$allData = $this->Home_model->getSelectedPackageDetails($ordermasterID);								
				$allData[0]['packageAmount'] = number_format($allData[0]['packageAmount'],2,".","");
				include('assests/plugins/vendor/autoload.php');//include PayPal SDK				
				include('assests/plugins/vendor/functions.inc.php');//our PayPal functions				
				$payment_method = "paypal";
				$items = array();
				foreach($data['months'] as $value){
					$noOfMonths = $value;
				}				
				$total_amount = ($allData[0]['packageAmount'] * $noOfMonths);
				
				$total_amount = number_format($total_amount,2,".","");
				$temp_array = array(
					"name"  			=>$allData[0]['packageName']." Package",
					"quantity"  		=> 1,
					"price"  			=> $total_amount,
					"sku"  				=> "ABC123",
					"currency"  		=> "USD",
				);
				array_push($items,$temp_array);				 
				try{ 
					######## if payment method is PayPal ##############	
					if($payment_method == "paypal"){
						//if payment method was PayPal, we need to redirect user to PayPal approval URL
						$result = create_paypal_payment($total_amount, PP_CURRENCY, '', $items, RETURN_URL, CANCEL_URL);			
						if($result->state == "created" && $result->payer->payment_method == "paypal"){
							$_SESSION["payment_id"] = $result->id; //set payment id for later use, we need this to execute payment
							//unset($_SESSION["items"]); //unset item session, not required anymore.
							//unset($_SESSION["items_total"]); //unset items_total session, not required anymore.
							header("location: ". $result->links[1]->href); //after success redirect user to approval URL 
							exit();
						}
					}
				}catch (PayPal\Exception\PayPalConnectionException $ex) {
					// echo "<pre>";
					// echo $ex->getCode(); // Prints the Error Code
					// echo $ex->getData(); // Prints the detailed error message 
					// die($ex);
				} catch (Exception $ex) {
					// die($ex);
				}
				
			}elseif($data['currencyDropdown'] == "India"){
				$inserted = $this->Home_model->bypassPaymentAndInsertData($data);	
				if($inserted){
					redirect(base_url().'index.php/Home/dashboard/Succ');
				}else{
					redirect(base_url().'index.php/Home/dashboard/NTSucc');
				}
			}						
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");
		}
	}
	
	public function paymentHistory(){
		$loggedIN = $this->Home_model->checkUserSession();
		if($loggedIN == "logeedIn"){						
			$fromDate = "";
			$toDate = "";
			if((($this->input->post('fromDate'))) && $this->input->post('fromDate') != ""){
				$fromDate = $this->input->post('fromDate');
			}
			if((($this->input->post('toDate'))) && $this->input->post('toDate') != ""){
				$toDate = $this->input->post('toDate');
			}
	
			
			$res['data'] = $this->Home_model->getpaymentHistory($fromDate,$toDate);
			$res['fromDate'] = $fromDate;
			$res['toDate'] = $toDate;			
			$this->load->view('paymentHistory_header');				
			$this->load->view('paymentHistory',$res);				
			$this->load->view('paymentHistory_footer');
		}else{
			redirect(base_url()."index.php/Home/index/NTLGD");		
		}

	}
		
	public function aboutUs(){
		$this->load->view('aboutUs_header');				
		$this->load->view('aboutUs');				
		$this->load->view('aboutUs_footer');
	}
	
	
	public function services(){
		$this->load->view('services_header');				
		$this->load->view('services');				
		$this->load->view('services_footer');
	}	
	
	
	public function order_process(){		

	}
	
	public function payment_cancel(){
		echo "cancel";
	}
	
	public function addFreePackage(){
		// echo "<pre>";
		// print_r($_SESSION);
		// exit;
		$res = $this->Home_model->insertFreePackage();
		if($res){
			redirect(base_url()."index.php/Home/addProfile/LGD");	
		}else{
			redirect(base_url()."index.php/Home/addProfile/LGD");	
		}	
	}
	
	public function forgotPasswordPage(){
		// echo "<pre>";
		// print_r($this->uri->segment_array());
		// exit;
		$businessId = 0;
		if(($this->uri->segment(3)) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0){
			 $businessId = $this->uri->segment(3);
		}
		$this->load->view('resetPassword',$businessId);	
		
	}
	
	
	public function websearch(){
		$res['msg'] = "";
		$this->load->view('header');
		$this->load->view('websearch', $res);
		$this->load->view('footer');												
	}
	
	
	public function about(){
		$res['msg'] = "";
		$this->load->view('header');
		$this->load->view('about',$res);
		$this->load->view('footer');												
	}
	
	
	public function advertise(){
		$res['msg'] = "";
		$this->load->view('header');
		$this->load->view('advertise',$res);
		$this->load->view('footer');												
	}
	
	public function career(){
		$res['msg'] = "";
		$this->load->view('header');
		$this->load->view('career',$res);
		$this->load->view('footer');												
	}
	
	
	public function faq(){
		$res['msg'] = "";
		$this->load->view('header');
		$this->load->view('faq',$res);
		$this->load->view('footer');												
	}
	
	public function privacy(){
		$res['msg'] = "";
		$this->load->view('header');
		$this->load->view('privacy',$res);
		$this->load->view('footer');												
	}
	
	



	
}
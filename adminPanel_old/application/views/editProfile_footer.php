 <!-- END CONTAINER --> 
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-multiselect.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/dropzone.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-tokenfield.js"></script>         	
	<script type="text/javascript" src="<?php echo base_url();?>assests/js/cropper.js"></script>         
	<script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-timepicker.min.js"></script>         
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script>      
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init(); 
			
			$('#topProfileButton').on('click', function(){	
				$.ajax({
					url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
					success: function(response){	
						if(response == 1){
							window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
						}else if(response == 0){
							window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
						}						
					}
				});	
			});
			
			
			
			$('#example-dropRight').multiselect({
				buttonWidth: '400px',
				minHeight: 400
			});	
			
		$(".dropzone2").dropzone({
			url: 'upload.php',			
			allowedFileTypes: 'image.*, pdf',
			params:{
				'action': 'save'
			},
			uploadOnDrop: true,
			uploadOnPreview: false,
			success: function(res, index){
				console.log(res, index);
			}
    });
		
		$('#tokenfield').tokenfield({  
			showAutocompleteOnFocus: true
		});
		
		//Code for Country City state
		
		var  CountryName= $('#country option:selected').text();							
			$("#hidCountryName").val(CountryName);
		
		
		var  StateName= $('#state option:selected').text();			
			$("#hidStateName").val(StateName);
		
		
		var  CityName= $('#city option:selected').text();		
		$("#hidCityName").val(CityName);
		
	   $('#country').on('change',function(){		  
        var countryID = $(this).val();		
        if(countryID){
			var  CountryName= $('#country option:selected').text();			
			$("#hidCountryName").val(CountryName);	
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/Home/getStateList',
                data:'country_id='+countryID,
                success:function(html){					
                    $('#state').html(html);                   
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');            
        }
	   });
	   
	$('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
			var  StateName= $('#state option:selected').text();			
			$("#hidStateName").val(StateName);
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/Home/getCityList',
                data:'state_id='+stateID,
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
		
	$('#city').on('change',function(){
		var  CityName= $('#city option:selected').text();		
		$("#hidCityName").val(CityName);
	});	
		
		$('#businessCategory').multiselect({
				buttonWidth: '380px',
				noneSelectedText: 'Select Something (required)',
				minHeight:400,	
				onChange: function(option, checked) {                
					var selectedOptions = $('#businessCategory option:selected');
 
					if (selectedOptions.length >= 3) {						
						var nonSelectedOptions = $('#businessCategory option').filter(function() {
                        return !$(this).is(':selected');
					});
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('#businessCategory option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
				
			});	
			$("#OficeFileUpload").change(function(){	
				readURL(this);	
			});
			
	$('#saveOfiiceCroppedImage').on('click', function(){	
	var canvasData = $('#image').cropper('getCroppedCanvas');	
	//var dataUrl = canvasData.toDataURL('image/jpeg');				
	canvasData.toBlob(function(blob){
		 var formData = new FormData();
		formData.append('croppedImage', blob);		
		$.ajax({
			url:  '<?php echo base_url();?>index.php/Home/addOfficePicture',
			data: formData,
			method: "POST",
			processData: false,
			contentType: false,
			success: function(response){	
				if(response != ""){
					var data = jQuery.parseJSON(response);	
					$("#previewContainer1").show();					
					$("#previewCroppedImage1").attr('src',data['croppedImage']);					
					$('#myModal').modal('hide');
				}
			}
		});
	});
});
		

$('#deleteThisPreviewedImage').on('click', function(){	
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataid = $(this).attr("data-imageid");
	$.ajax({
			url:  '<?php echo base_url();?>index.php/Home/deleteOfficePicture',
			data: "dataid="+dataid,
			method: "POST",			
			success: function(response){	
				if(response == "DELETED"){
					$("#previewContainer").hide();
					$("#OficeFileUpload").prop('disabled', false);
					alert("Image Deleted");	
				}else if(response == "NOTDELETED"){
					$("#previewContainer").hide();
					alert("Image not deleted. Please try again");
				}				
			}
		});	
    } else {
     
    }
});
		
		$("#ServicesFileUpload").change(function(){	
			readServicesURL(this);	
		});
		
		$('#saveOfiiceServicesCroppedImage').on('click', function(){	
	var canvasData = $('#imageServices').cropper('getCroppedCanvas');	
	//var dataUrl = canvasData.toDataURL('image/jpeg');				
	canvasData.toBlob(function(blob){
		 var formData = new FormData();
		formData.append('croppedImage', blob);		
		$.ajax({
			url:  '<?php echo base_url();?>index.php/Home/addOfficeServicePicture',
			data: formData,
			method: "POST",
			processData: false,
			contentType: false,
			success: function(response){	
				if(response != ""){
					var data = jQuery.parseJSON(response);						
					$("#previewContainerForServiceImages").show();										
					 
					var html ="<li class='jFiler-item' data-jfiler-index='1' id='previewBox_"+data['serviceImageID']+"'>" +
						"<div class='jFiler-item-container'>" +
							"<div class='jFiler-item-inner'>" +
								"<div class='jFiler-item-thumb'>" +
									"<div class='jFiler-item-status'></div>                        " +
									"<div class='jFiler-item-thumb-image'>" +
										"<img id='previewCroppedServcieImage' src="+data['croppedImage']+" draggable='false' style='width:100% !important;'>" +
									"</div>" +
								"</div>" +
								"<div class='jFiler-item-assets jFiler-row'>" +
									"<ul class='list-inline pull-left'>" +
										"<li></li>" +
									"</ul>" +
									"<ul class='list-inline pull-right'>" +
										"<li><a class='icon-jfi-trash jFiler-item-trash-action deleteThisPreviewedImageOfServices' data-ServiceId = "+data['serviceImageID']+"></a></li>" +
									"</ul>" +
								"</div>" +
							"</div>" +
						"</div>" +
					"</li>";
					$("#ulForSserviceImages").append(html);										
					$('#myModalServices').modal('hide');
					$('#imageServices').cropper('destroy');
					$("#ServicesFileUpload").val("");
					
					
				}
			}
		});
	});
})
		
		
$(document.body).delegate('.deleteThisPreviewedImageOfServices', 'click', function (e) {
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataServiceId = $(this).attr("data-ServiceId");	
	$.ajax({
			url:  '<?php echo base_url();?>index.php/Home/deleteOfficeServicePicture',
			data: "dataid="+dataServiceId,
			method: "POST",			
			success: function(response){					
				if(response == "DELETED"){
					$("#previewBox_"+dataServiceId).hide();
					alert("Image Deleted");	
				}else if(response == "NOTDELETED"){
					alert("Image not deleted. Please try again");
				}				
			}
		});	
    } else {
     
    }
});
		//$(".timepickerFromTime").timepicker({defaultTime: 'value'});
		//$.each($('.timepickerFromTime'), function(index, item){			
		setTimeout(function(){
			$('.timepickerFromTime').each(function(index, item){			
				if($(item).val() == '' || $(item).val() == 'From Time' || $(item).val() == 'To Time') {
					$(item).timepicker({defaultTime: false});
				} else {								
					$(item).timepicker({defaultTime: $(item).val()});
				}
			});
		},2000);
		
        });
		
		
		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#image').attr('src', e.target.result);
					
					$('#myModal').modal('show'); 
						$('#image').cropper({
						aspectRatio: 1 / 1,
						crop: function(e) {		
							console.log(e.x);
							console.log(e.y);
							console.log(e.width);
							console.log(e.height);
							console.log(e.rotate);
							console.log(e.scaleX);
							console.log(e.scaleY);
						},
						minContainerWidth: 500,
						minContainerHeight: 300,
						minCropBoxHeight:100,
						minCropBoxWidth:100,
						viewMode: 1,
						ready: function () {
							// Strict mode: set crop box data first
							cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
						}
					});	
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		
		
			function readServicesURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#imageServices').attr('src', e.target.result);
					
					$('#myModalServices').modal('show'); 
						$('#imageServices').cropper({
						aspectRatio: 1 / 1,
						crop: function(e) {		
							console.log(e.x);
							console.log(e.y);
							console.log(e.width);
							console.log(e.height);
							console.log(e.rotate);
							console.log(e.scaleX);
							console.log(e.scaleY);
						},
						minContainerWidth: 500,
						minContainerHeight: 300,
						minCropBoxHeight:100,
						minCropBoxWidth:100,
						viewMode: 1,
						ready: function () {
							// Strict mode: set crop box data first
							cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
						}
					});	
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
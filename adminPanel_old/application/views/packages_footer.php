 <!-- END CONTAINER --> 
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>             
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script>      
    <script type="text/javascript">
        jQuery(document).ready(function() {
			App.init();
			
			
			$('#topProfileButton').on('click', function(){	
				$.ajax({
					url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
					success: function(response){	
						if(response == 1){
							window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
						}else if(response == 0){
							window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
						}						
					}
				});	
			});
			
			
			
			//Code for select packages page
			var totAmount = 0;
			var allPackages = ['4'];
			$(document.body).delegate('.selectThisOption', 'click', function (e) {
				var amount = $(this).attr("data-amount");
				var packageId = $(this).attr("data-PackageId");				
				var allLength = allPackages.length;
				amount = parseFloat(amount);
				if($(this).hasClass('theme-btn')) {					
				
					var allBtn = $('.selectThisOption');
					
					if(allBtn.length > 0) {
						$.each(allBtn, function(index, eachEle){
							var thisEle = $(eachEle);
							
							thisEle.removeClass("btn-success").addClass("theme-btn");	
							thisEle.html('Select');
							totAmount = 0;
							allPackages.splice($.inArray(packageId, allPackages), 1);
							
						});
					}
					
					$(this).removeClass("theme-btn").addClass("btn-success");	
					$(this).html('Selected');
					totAmount = totAmount + amount;					
					allPackages.push(packageId);										
				}
				else {
					$(this).removeClass("btn-success").addClass("theme-btn");	
					$(this).html('Select');
					totAmount =totAmount - amount;
					allPackages.splice($.inArray(packageId, allPackages), 1);
				}
				
				$("#amountDiv").html(totAmount);					
				if(totAmount > 0){					
					var allLength = allPackages.length;
					if(allLength > 2){					
						allPackages.pop();
					}					
					$("#hidAmmountValue").val(totAmount);
					$("#showPayNowButton").show();
				}else{
					$("#showPayNowButton").hide();
				}	
				$("#hidPackagePurchased").val(allPackages);			
			});        
			// $("#freePackageId").on("click",function(){
				// $("#hidAmmountValue").val(totAmount);
				// $("#hidPackagePurchased").val(allPackages);	
				// $("#paymentForm").submit();
			// });
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/bootstrap-datepicker.min.js" type="text/javascript"></script>    
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>	

<script src="<?php echo base_url();?>assests/js/admin/jquery.dataTables.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/DT_bootstrap.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/table-editable.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/ui-jqueryui.js"></script>
<script src="<?php echo base_url();?>assests/js/admin/app.js"></script>      
<script>
	jQuery(document).ready(function() {    
	   App.init();
	    TableEditable.init();
		UIJQueryUI.init();
	});
</script>
<?php	if(($msg) && $msg != "" && $msg == "BLOCKED"){	?>	
	<script type="text/javascript">
		alert("User has been blocked to use this platform.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTBLOCKED"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to block user.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "UNBLOCKED"){ ?>
	<script type="text/javascript">
		alert("User has been unblocked to use this platform.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTUNBLOCKED"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to unblock user.");
	</script>
<?php }	?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
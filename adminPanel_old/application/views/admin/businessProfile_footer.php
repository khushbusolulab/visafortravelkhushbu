<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/admin/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.redirect.js" type="text/javascript" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.dataTables.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/DT_bootstrap.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/table-editable.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.validate.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/additional-methods.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/bootstrap-fileupload.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/app.js"></script>      
<script src="<?php echo base_url();?>assests/js/admin/ui-jqueryui.js"></script>       
<script src="<?php echo base_url();?>assests/js/admin/form-validation.js"></script>      
<script type="text/javascript" src="<?php echo base_url();?>assests/js/cropper.js"></script>	
<script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-tokenfield.js"></script>

<script src="<?php echo base_url();?>assests/js/admin/customJS.js"></script>      
<script>
	var base_url = "<?php echo base_url();?>";
	jQuery(document).ready(function() {

	var dateNow = new Date();
		$('.timepickerFromTime').timepicker({
            defaultDate:dateNow
        });	



	$("#staffMembersEmail").on("blur",function(){
		var email = $(this).val();		
		var staffMembersId = $("#staffMembersId").val();
		$.ajax({
			type:'POST',
			url:base_url+'index.php/Admin/checkEmailForStaffAndAdmin',
			data:'email='+email+'&staffMembersId='+staffMembersId,
			success:function(response){									
				if(response == "EXISTS"){
					$("#staffMembersEmail").val("");
					alert("Email has been taken.Please try another one.");
				}else if(response == "NTEXISTS"){
					//Do nothing
				}
			}
		}); 
	});
	
	$.validator.addMethod("noBlankSpace", function (value, element) {
        if (!(value == '' || value.trim().length != 0)) {
            return false;
        } else {
            //$(element).val($(element).val().trim());
            return true;
        }
    }, 'Only Blanks spaces are not allowed for this field');
	$.validator.addMethod("needsSelection", function (value, element) {
		var count = $(element).find('option:selected').length;
		return count > 0;
	}, "Please select atleast one business category.");
	
	// $.validator.addMethod('require-one', function(value) {
		// return $('.daysCheckBox:checked').size() > 0;
	// }, 'Please select atleast one business day.');
	
	$.validator.addMethod('checkForPackage', function(value, element) {				
		if($('.selectThisOption.btn-success').length == "1" || $('.renewNowButton.btn-success').length == "1") {
			if(value == "") {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return true;
		}
	}, 'Please fill the payment details');
	$.validator.addMethod('requireDays', function(value, element) {				
	
		if($(element).attr('checked') == "checked") {
			
			console.log($(element));
			
			var fromTime = $(element).closest('.row-fluid').find('.fromTimeTextInput').val();
			var toTime = $(element).closest('.row-fluid').find('.toTimeTextInput').val();
			
			if(fromTime < toTime) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return true;
		}	
	}, 'Please select valid time range.');
	
	
	$.validator.addMethod("checkServiceImage", function (value, element) {		
		return value > 0;
	}, "Please select atleast one Service Image.");
	
	   App.init();
	    TableEditable.init();
		UIJQueryUI.init();
		FormValidation.init();
		
		
		
		var  CountryName= $('#countryId option:selected').text();			
				$("#hidCountryName").val(CountryName);
		$('#countryId').on('change',function(){		  
			var countryID = $(this).val();		
			if(countryID){			
				var  CountryName= $('#countryId option:selected').text();			
				$("#hidCountryName").val(CountryName);		
				$.ajax({
					type:'POST',
					url:base_url+'index.php/Admin/getStateList',
					data:'country_id='+countryID,
					success:function(html){					
						$('#state').html(html);                    
					}
				}); 
			}else{
				$('#state').html('<option value="">Select country first</option>');            
			}
		});
		var  StateName= $('#state option:selected').text();			
			$("#hidStateName").val(StateName);	
	  $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
			var  StateName= $('#state option:selected').text();			
			$("#hidStateName").val(StateName);	
            $.ajax({
                type:'POST',
                url:base_url+'index.php/Admin/getCityList',
                data:'state_id='+stateID,
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
	var  CityName= $('#city option:selected').text();		
		$("#hidCityName").val(CityName);
	$('#city').on('change',function(){
		var  CityName= $('#city option:selected').text();		
		$("#hidCityName").val(CityName);
	});	
		
		
		$('#businessCategory').multiselect({
				buttonWidth: '380px',
				noneSelectedText: 'Select Something (required)',
				minHeight:400,	
				onChange: function(option, checked) {                
					var selectedOptions = $('#businessCategory option:selected');
 
					if (selectedOptions.length >= 3) {						
						var nonSelectedOptions = $('#businessCategory option').filter(function() {
                        return !$(this).is(':selected');
					});
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('#businessCategory option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
				
			});		
		 
			//Code for single file upload 
			function readURL(input) {
				var url = $(input).val();
				var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
				if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg") ) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#image').attr('src', e.target.result);
					
					$('#myModal').modal('show'); 
						$('#image').cropper({
						aspectRatio: 1 / 1,
						crop: function(e) {		
							console.log(e.x);
							console.log(e.y);
							console.log(e.width);
							console.log(e.height);
							console.log(e.rotate);
							console.log(e.scaleX);
							console.log(e.scaleY);
						},
						minContainerWidth: 500,
						minContainerHeight: 300,
						minCropBoxHeight:100,
						minCropBoxWidth:100,
						viewMode: 1,
						ready: function () {
							// Strict mode: set crop box data first
							cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
						}
					});	
				}
				reader.readAsDataURL(input.files[0]);
				return true;
			}else{
				alert("Please Upload Image Only.");
				return false;
			}
		}


		function readServicesURL(input) {
			var url = $(input).val();
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg") ) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#imageServices').attr('src', e.target.result);
					
					$('#myModalServices').modal('show'); 
						$('#imageServices').cropper({
						aspectRatio: 1 / 1,
						crop: function(e) {		
							console.log(e.x);
							console.log(e.y);
							console.log(e.width);
							console.log(e.height);
							console.log(e.rotate);
							console.log(e.scaleX);
							console.log(e.scaleY);
						},
						minContainerWidth: 500,
						minContainerHeight: 300,
						minCropBoxHeight:100,
						minCropBoxWidth:100,
						viewMode: 1,
						ready: function () {
							// Strict mode: set crop box data first
							cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
						}
					});	
				}
				reader.readAsDataURL(input.files[0]);
				return true;
			}else{
			alert("Please Upload Image Only.");			
			return false;			
			}	
		}
		
		
		$("#OficeFileUpload").change(function(){					 
			return readURL(this);	
		});

		$("#ServicesFileUpload").change(function(){	
			return readServicesURL(this);
				
		});

		
		$("#deleteThisPreviewedImage").on("click",function(){
			$("#hidOfficeImage").val("");
			$("#previewContainer").hide();
			$("#OficeFileUpload").val("");
		});

			
		//Code for Token field
		$('#tokenfield').tokenfield({			
			showAutocompleteOnFocus: true
		});
var imageCounter = 1;
var counter = 0;
$('#saveOfiiceServicesCroppedImage').on('click', function(){	
	counter++;
	var imageCounterVal = $("#serviceImageCount").val();
	if(imageCounterVal == ""){
		$("#serviceImageCount").val(imageCounter);
	}else{
		imageCounter++;
		$("#serviceImageCount").val(imageCounter);
	}	
	var canvasData = $('#imageServices').cropper('getCroppedCanvas');		
	var dataUrl = canvasData.toDataURL('image/jpeg');				
//console.log(dataUrl);
//return false;
	$("#hidServiceImages").val(dataUrl);
	$("#previewContainerForServiceImages").show();
	var html ="<li class='jFiler-item' data-jfiler-index='1' id='previewBox_"+counter+"'>" +
		"<div class='jFiler-item-container'>" +
			"<div class='jFiler-item-inner'>" +
				"<div class='jFiler-item-thumb'>" +
					"<div class='jFiler-item-status'></div>                        " +
					"<div class='jFiler-item-thumb-image'>" +
						"<img id='previewCroppedServcieImage' src="+dataUrl+" draggable='false' style='width:100% !important;'>" +
					"</div>" +
				"</div>" +
				"<div class='jFiler-item-assets jFiler-row'>" +
					"<ul class='list-inline pull-left'>" +
						"<li></li>" +
					"</ul>" +
					"<ul class='list-inline pull-right'>" +
						"<li><a class='icon-jfi-trash jFiler-item-trash-action deleteThisPreviewedImageOfServices' data-counter = '"+counter+"' ></a></li>" +
					"</ul>" +
				"</div>" +
			"</div>" +
		"</div>" +
	"</li>";
		$("#ulForSserviceImages").append(html);							
		$('#myModalServices').modal('hide');		
		$('#imageServices').cropper('destroy');
		$("#ServicesFileUpload").val("");
	
	// var hiddenInput = $('<input />');
	// hiddenInput.name = "hidServiceImages[]";
	// hiddenInput.id = "hidServiceImages";
	// hiddenInput.value = dataUrl;

	$('<input>').attr({
		type: "hidden",
		name: "hidServiceImages[]",
		id:	"hidServiceImages_"+counter,
		value: dataUrl
	}).appendTo('#hiddenElements'); 
});



$(document.body).delegate('.removePackageButton', 'click', function (e) {
	if (confirm("Are you sure,you want to remove this package?") == true) {
	var packageId = $(this).attr("data-PackageId");
	var businessId = $("#businessId").val();	
	if(packageId){
		$.ajax({
			type:'POST',
			url:base_url+'index.php/Admin/removePackageOfUser',
			data:'packageId='+packageId+'&businessId='+businessId,
			success:function(response){
				if(response == "DELETED"){
					 window.location.href = base_url+"index.php/Admin/businessProfile/"+businessId+"/DELSUCC";
				}else if(response == "NTDELETED"){
					 window.location.href = base_url+"index.php/Admin/businessProfile/"+businessId+"/NTDELSUCC";
				}
			
			}
		});
	}
	}else{
		//do nothing
	}
	
});



$(document.body).delegate('.deleteThisPreviewedImageOfServices', 'click', function (e) {
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataServiceId = $(this).attr("data-counter");
		var serviceImageCountVal = $("#serviceImageCount").val();
		serviceImageCountVal--;
		$("#serviceImageCount").val(serviceImageCountVal);
		$("#previewBox_"+dataServiceId).remove();
		$("#hidServiceImages_"+dataServiceId).remove(); 
    }
});


 
	//$('.timepickerFromTime').timepicker();

	//For Purchase
	var totAmount = 0;
			var allPackages = [];			
			$(document.body).delegate('.selectThisOption', 'click', function (e) {
				var amount = $(this).attr("data-amount");
				var packageId = $(this).attr("data-PackageId");				
				var allLength = allPackages.length;
				amount = parseFloat(amount);
				var type = "BUYPACKAGE";	
				$("#hidPurchaseType").val(type);	
				if($(this).hasClass('theme-btn')) {					
				
					var allBtn = $('.selectThisOption');
					
					if(allBtn.length > 0) {
						$.each(allBtn, function(index, eachEle){
							var thisEle = $(eachEle);							
							thisEle.removeClass("btn-success").addClass("theme-btn");	
							thisEle.html('Buy Now');
							totAmount = 0;
							allPackages.splice($.inArray(packageId, allPackages), 1);							
						});
					}
					
					$(this).removeClass("theme-btn").addClass("btn-success");	
					$(this).html('Selected');
					totAmount = totAmount + amount;					
					allPackages.push(packageId);
					$( "#renewPackageDiv" ).hide();		
					$("#dropdownOfMonths").show();	
				}
				else {
					$(this).removeClass("btn-success").addClass("theme-btn");	
					$(this).html('Buy Now');
					totAmount =totAmount - amount;
					allPackages.splice($.inArray(packageId, allPackages), 1);
					$("#hidAmmountValue").val("0");	
					$("#hidPackagePurchased").val("0");		
					$("#chequeNumber").val("");							
					$("#paymentNotes").val("");							
					$("#monthsDropdown").val("");							
					$("#dropdownOfMonths").hide();
					$( "#renewPackageDiv" ).show();
					$("#amountDiv").html("0");
				}				
				$("#amountDiv").html(totAmount);
				$("#hidPayableAmount").val(totAmount);				
				$("#hidPackageID").val(packageId);				
				if(totAmount > 0){					
					var allLength = allPackages.length;
					if(allLength > 1){					
						allPackages.pop();
					}					
					$("#hidAmmountValue").val(totAmount);
					$("#hidPackagePurchased").val(allPackages);							
					
				}else{
					$("#showPayNowButton").hide();
				}	
			});  
			
			//For renew
	var totAmount = 0;
	var allPackages = [];			
	$(document.body).delegate('.renewNowButton', 'click', function (e) {
		var packageID 		= $(this).attr("data-PackageId");	
		var packageAmount 	= $(this).attr("data-amount");		
		var type = "RENEWPACKAGE";		
		$("#hidAmmountValue").val(packageAmount);
		$("#hidPackageID").val(packageID);
		$("#hidPurchaseType").val(type);		
		var allLength = allPackages.length;
				amount = parseFloat(packageAmount);
				if($(this).hasClass('theme-btn')) {									
					var allBtn = $('.renewNowButton');					
					if(allBtn.length > 0) {
						$.each(allBtn, function(index, eachEle){
							var thisEle = $(eachEle);							
							thisEle.removeClass("btn-success").addClass("theme-btn");	
							thisEle.html('Buy Now');
							totAmount = 0;
							allPackages.splice($.inArray(packageID, allPackages), 1);							
						});
					}
					
					$(this).removeClass("theme-btn").addClass("btn-success");	
					$(this).html('Selected');
					totAmount = totAmount + amount;					
					allPackages.push(packageID);
					$( "#purchasePackageDiv" ).hide();						
					$("#dropdownOfMonths").show();	
				}
				else {
					$(this).removeClass("btn-success").addClass("theme-btn");	
					$(this).html('Buy Now');
					totAmount =totAmount - amount;
					allPackages.splice($.inArray(packageID, allPackages), 1);
					$("#hidAmmountValue").val("0");	
					$("#hidPackagePurchased").val("0");		
					$("#chequeNumber").val("");							
					$("#paymentNotes").val("");							
					$("#monthsDropdown").val("");
					$( "#purchasePackageDiv" ).show();	
					$("#dropdownOfMonths").hide();
					$("#amountDiv").html("0");
				}				
				$("#amountDiv").html(totAmount);
				$("#hidPayableAmount").val(totAmount);				
				$("#hidPackageID").val(packageId);				
				if(totAmount > 0){					
					var allLength = allPackages.length;
					if(allLength > 1){					
						allPackages.pop();
					}					
					$("#hidAmmountValue").val(totAmount);
					$("#hidPackagePurchased").val(allPackages);							
					
				}else{
					$("#showPayNowButton").hide();
				}
		
		
		              
	});
           	

	$("#monthsDropdown").on("change",function(){
		var monthsSelected = $(this).val();
		var PayableAmount = $("#hidPayableAmount").val();
		var totAmount = parseFloat((parseFloat(PayableAmount)) * monthsSelected);
			$("#amountDiv").html(totAmount);
			$("#hidPayableAmount").val(totAmount);	
	});
			
			
		$("#businessEmail").on("blur",function(){
		var email = $(this).val();		
		var businessId = $("#businessId").val();		
		$.ajax({
			type:'POST',
			url:base_url+'index.php/Admin/checkEmailForBusiness',
			data:'email='+email+'&businessId='+businessId,
			success:function(response){													
				if(response == "EXISTS"){
					$("#businessEmail").val("");
					alert("Email has been taken.Please try another one.");
				}else if(response == "NTEXISTS"){
					//Do nothing
				}
			}
		}); 
	});

	
	$('#deleteThisPreviewedImage').on('click', function(){	
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataid = $(this).attr("data-imageid");
	$.ajax({
			url:  '<?php echo base_url();?>index.php/Admin/deleteOfficePicture',
			data: "dataid="+dataid,
			method: "POST",			
			success: function(response){	
				if(response == "DELETED"){
					$("#previewContainer1").hide();
					$("#OficeFileUpload").prop('disabled', false);
					alert("Image Deleted");	
				}else if(response == "NOTDELETED"){
					$("#previewContainer1").hide();
					alert("Image not deleted. Please try again");
				}				
			}
		});	
    } else {
     
    }
});


$(document.body).delegate('.deleteThisPreviewedImageOfServices1', 'click', function (e) {
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataServiceId = $(this).attr("data-ServiceId");	
	$.ajax({
			url:  '<?php echo base_url();?>index.php/Admin/deleteOfficeServicePicture',
			data: "dataid="+dataServiceId,
			method: "POST",			
			success: function(response){					
				if(response == "DELETED"){
					$("#previewBox_"+dataServiceId).hide();
					alert("Image Deleted");	
				}else if(response == "NOTDELETED"){
					alert("Image not deleted. Please try again");
				}				
			}
		});	
    } else {
     
    }
});


	
$("#myModalServices").on("shown.bs.modal", function () {
	$("body").addClass("modal-open");
      }).on("hidden.bs.modal", function () {
	$("body").removeClass("modal-open")
      });
$("#myModal").on("shown.bs.modal", function () {
	$("body").addClass("modal-open");
      }).on("hidden.bs.modal", function () {
	$("body").removeClass("modal-open")
      });
		
	});
	




</script>
<?php	if(($msg) && $msg != "" && $msg == "DEL"){	?>	
	<script type="text/javascript">
		alert("Document has been deleted.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTDEL"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to delete document.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "EMEXITS"){ ?>
	<script type="text/javascript">
		alert("Email Already exists in our system.");
	</script>
<?php 	}elseif(($delMsg) && $delMsg != "" && $delMsg == "DELSUCC"){ ?>
	<script type="text/javascript">
		alert("Package has been removed successfully.");
	</script>
<?php 	}elseif(($delMsg) && $delMsg != "" && $delMsg == "NTDELSUCC"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to remove package.");
	</script>
<?php 	} ?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<div class="admin-header">		
	<div class="container">
		<div class="navbar navbar-default">
			<div class="navbar-header"> 
				<a class="brand logo-v1" href="javascript:void(0);" >
					<img src="<?php echo base_url();?>assests\img\logo.jpg" alt="logo" style="height:70px;" />
				</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			<?php 
			// echo "<pre>";
			// print_r($this->uri->segment_array());	
			// echo "</pre>";
			?>
			</div>
			<div>
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
					<?php if(isset($_SESSION['LoggedInType']) && $_SESSION['LoggedInType'] != "" && $_SESSION['LoggedInType'] == "Admin") { ?>	
						<li <?php if($this->uri->segment(2) == "dashboard"){echo 'class="active"';}?>><a href="<?php echo base_url();?>index.php/Admin/dashboard">Dashboard</a></li>						  						 						  
						<li <?php if($this->uri->segment(2) == "appUsers" || $this->uri->segment(2) == "appUserDeatils"){echo 'class="active"';}?>><a href="<?php echo base_url();?>index.php/Admin/appUsers">App Users</a></li>						  						 						  
						<li <?php if($this->uri->segment(2) == "busniessUsers" || $this->uri->segment(2) == "businessDeatils"){echo 'class="active"';}?>><a href="<?php echo base_url();?>index.php/Admin/busniessUsers">Business Users</a></li>						  						 						  
						<li <?php if($this->uri->segment(2) == "staffMember" || $this->uri->segment(2) == "staffProfile"){echo 'class="active"';}?>><a href="<?php echo base_url();?>index.php/Admin/staffMember">Staff Member</a></li>						  						 						  
						<li <?php if($this->uri->segment(2) == "paymentTransactions"){echo 'class="active"';}?>><a href="<?php echo base_url();?>index.php/Admin/paymentTransactions">Payments</a></li>						  						 						  
						<li <?php if($this->uri->segment(2) == "embassies" || $this->uri->segment(2) == "embassyDetails" || $this->uri->segment(2) == "passport" || $this->uri->segment(2) == "passportDetails" || $this->uri->segment(2) == "countryInfo" || $this->uri->segment(2) == "countryInfoDetails" ){echo 'class="active"';}?> >
							<a data-hover="dropdown" data-close-others="true" href="javascript:void(0);">Information<span class="arrow"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url();?>index.php/Admin/embassies">Manage Embassies</a></li>								
								<li><a href="<?php echo base_url();?>index.php/Admin/passport">Manage Passport values</a></li>								
								<li><a href="<?php echo base_url();?>index.php/Admin/countryInfo">Manage Country Information</a></li>								
							</ul>
							<b class="caret-out"></b>                        
						</li>		
						<li <?php if($this->uri->segment(2) == "userReport" || $this->uri->segment(2) == "businessReport"){echo 'class="active"';}?> >
							<a data-hover="dropdown" data-close-others="true" href="javascript:void(0);">Reports<span class="arrow"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url();?>index.php/Admin/userReport">App Users</a></li>								
								<li><a href="<?php echo base_url();?>index.php/Admin/businessReport">Business Users</a></li>																
							</ul>
							<b class="caret-out"></b>                        
						</li>		
					<?php } elseif(isset($_SESSION['LoggedInType']) && $_SESSION['LoggedInType'] != "" && $_SESSION['LoggedInType'] == "Staff"){ ?>	
						<li <?php if($this->uri->segment(2) == "busniessUsers" || $this->uri->segment(2) == "businessDeatils"){echo 'class="active"';}?>><a href="<?php echo base_url();?>index.php/Admin/busniessUsers">Business Users</a></li>						  						 						  
					<?php } ?>
						<li><a href="<?php echo base_url();?>index.php/Admin/logout">Logout</a></li>						  						 						  
					</ul>
				</div>
			</div>				 
		</div>
	</div>		
</div>
<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
<div class="page-container row-fluid" >  
	<div class="page-content">			
		<div class="container-fluid">				
			<div class="row-fluid">
				<div class="span12">
					<h3 class="page-title"><strong>Change Password</strong></h3>						
				</div>
			</div>	
		<form method="post" name="changePasswordForm" id="changePasswordForm" action="<?php echo base_url();?>index.php/Admin/performChangePassword" enctype="multipart/form-data" >			
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			<div class="row-fluid margin-bottom-20">					 
				<div class="row-fluid">				
					<div class="span12">
						<div class="control-group">
							<label class="control-label">Password</label>
							<div class="controls">
								<input type="password" placeholder="Password" class="m-wrap medium" name="password" id="password" />									
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid margin-bottom-20">					 
				<div class="row-fluid">				
					<div class="span12">
						<div class="control-group">
							<label class="control-label">Confirm Password</label>
							<div class="controls">
								<input type="password" placeholder="Confirm Password" class="m-wrap medium" name="confirmPassword" id="confirmPassword" />									
							</div>
						</div>
					</div>
				</div>
			</div>
			 
			 
			 
			<div class="row-fluid margin-bottom-20">					 
				<div class="row-fluid">	
					<div class="span4">
					</div>
					<div class="span4">
						<button type="submit" class="btn green big btn-block" id="submitChangePasswordForm">Submit Details</button>
					</div>
					<div class="span4">
					</div>
				</div>
			</div>				
			</form>				
				</div>				
			</div>			
		</div>	
	</div>
</div>
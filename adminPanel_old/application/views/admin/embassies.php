<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title">
							<strong>Manage Embassies</strong>
							<a href="<?php echo base_url();?>index.php/Admin/embassyDetails" class="btn btn-primary pull-right" >Add</a>
						</h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">
					<div class="col-sm-12">					
						<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/embassies" class="mangemb" >
								<div class="form-group">								 							
									<label class="label-from-to">Country</label>								
									<select class="medium m-wrap form-control" name="countryID" id="countryID">
										<option value="">Please Select Country</option>										
										<?php foreach($countryValues as $value) { ?>
										<option value="<?php echo $value['countryId'];?>" <?php if($countryID == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
										<?php } ?>
									</select>
									<button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Go</button>
								</div>
						</form>	
					
						<div class="portlet box">
							<div class="portlet-title">										
							</div>
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Embassy Of</th>
											<th>Embassy In </th>
											<th>City</th>											
											<th>Embassy Address</th>											
											<th>Contact</th>																						 											
											<th>Action</th>																						 											
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($embassiesData as $value){?>
										<tr>
											<td><?php echo $value['residentCountryName'];?></td>											
											<td><?php echo $value['foreignCountryName'];?></td>											
											<td><?php echo $value['cityName'];?></td>											
											<td><?php print htmlentities($value['embassyAddress'],ENT_QUOTES,"UTF-8");?></td>											
											<td><?php echo  $value['embassyContact'];?></td>
											<td><a href="<?php echo base_url();?>index.php/Admin/embassyDetails/<?php echo $value['embessayId'];?>">Edit</a></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
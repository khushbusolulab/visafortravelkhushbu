<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title"><strong>Reports Managemnt - App Users</strong></h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">					 
					<div class="col-sm-12">					
						<label class="label-from-to"><strong>Total App Users : <?php echo $UsersDataFULL;?></strong></label>
					</div>
				</div>
				<div class="row margin-bottom-20">
					<div class="col-sm-12">					
						<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/userReport" class="usrrep" >
							 
								
									<div class="form-group">								 							
										<label class="label-from-to">Filter By Location</label>
										<select class="medium m-wrap form-control" name="countryID" id="countryID">
											<option value="">Please Select Country</option>										
											<?php foreach($countryValues as $value) { ?>
											<option value="<?php echo $value['countryId'];?>" <?php if($countryID == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
											<?php } ?>
										</select>
									</div>
									<div class="form-group">								 																	
										<select class="medium m-wrap form-control" name="cityId" id="cityId">
											<?php 										
										if(($CityList) && !empty($CityList)){
											foreach($CityList as $city_value){ ?>
											<option value="<?php echo $city_value['cityId'];?>" <?php if($cityId == $city_value['cityId']){ echo "Selected"; }?>><?php echo $city_value['cityName'];?></option>							
										<?php	}	}else{	?>
											<option value="">Please Select City</option>
										<?php }	?>	
										</select>
									</div>
									<div class=" form-group">								 									
										<button type="submit" class="btn btn-primary btn-md"><i class="icon-ok"></i> Go</button>						 
									</div>
								 
							
						</form>	
						<div class="portlet box">
							<div class="portlet-title">										
							</div>
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Name</th>
											<th>Country</th>											
											<th>City</th>											
											<th>Email-Address</th>
											<th>Platform</th>											
											<th>Active Since</th>											
											<th>Total Bookmarked</th>											
											<th>Total Reviewed</th>											
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($UsersData as $value){?>
										<tr>
											<td><?php echo $value['fullName'];?></td>											
											<td><?php echo $value['countryName'];?></td>											
											<td><?php echo $value['cityName'];?></td>											
											<td><?php echo $value['emailId'];?></td>											
											<td><?php echo $value['deviceType'];?></td>											
											<td><?php echo $value['createdDate'];?></td>																						 
											<td><?php echo $value['totBookmarks'];?></td>																						 
											<td><?php echo $value['totReviews'];?></td>																						 
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
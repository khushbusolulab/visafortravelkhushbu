<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
<div class="page-container" >  
	<div class="page-content">			
		<div class="container">				
			<div class="row">
				<div class="col-sm-12">	
					<?php if($arrAddEditInfo['embessayId'] == 0) {?>
						<h3 class="page-title">
							<strong>Add Embassy</strong>
							<a href="<?php echo base_url();?>index.php/Admin/embassies" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } else{ ?>
						<h3 class="page-title">
							<strong>Edit Embassy</strong>
							<a href="<?php echo base_url();?>index.php/Admin/embassies" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } ?>
				</div>
			</div>	
			<form method="post" name="embassyDetailsForm" id="embassyDetailsForm" action="<?php echo base_url();?>index.php/Admin/performEmbassyDetails" >			
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			
			
			
			<div class="row margin-bottom-20">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="controls">
							<label class="label-from-to">Select Embasssy For</label>							
							<select class="medium m-wrap form-control" name="residentCountry" id="residentCountry">
								<option value="">Please Select Country</option>										
								<?php foreach($countryValues as $value) { ?>
									<option value="<?php echo $value['countryId'];?>" <?php if($arrAddEditInfo['residentCountry'] == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div> 	
			
			
			<div class="row margin-bottom-20">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="controls">
							<label class="label-from-to">Select Embasssy In</label>
							<select class="medium m-wrap form-control" name="foreignCountry" id="foreignCountry">
								<option value="">Please Select Country</option>										
								<?php foreach($countryValues as $value) { ?>
									<option value="<?php echo $value['countryId'];?>" <?php if($arrAddEditInfo['foreignCountry'] == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div> 	
			
			
			<div class="row margin-bottom-20">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="controls">
							<label class="label-from-to">Select City</label>
							<select class="medium m-wrap form-control" name="foreignCity" id="foreignCity">
							<?php 										
								if(($arrAddEditInfo['CityList']) && !empty($arrAddEditInfo['CityList'])){
									foreach($arrAddEditInfo['CityList'] as $city_value){ ?>
									<option value="<?php echo $city_value['cityId'];?>" <?php if($arrAddEditInfo['foreignCity'] == $city_value['cityId']){ echo "Selected"; }?>><?php echo $city_value['cityName'];?></option>							
								<?php	}	}else{	?>
									<option value="">Please Select City</option>
								<?php }	?>		
							</select>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="row margin-bottom-20">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="controls">
								<label class="label-from-to">Embassy Contact</label>
								<input type="number" placeholder="Embassy Contact" class="m-wrap medium form-control"  name="embassyContact" id="embassyContact" value="<?php echo $arrAddEditInfo['embassyContact'];?>" onkeypress="return onlyNumbers (event)" >
						</div>
					</div>
				</div>
			</div>
			
			
			
			<div class="row margin-bottom-20">
				<div class="col-sm-4">
					<div class="form-group">
						<div class="controls">
							<label class="label-from-to">Embassy Address</label>
							<textarea class="large m-wrap form-control" rows="3" name="embassyAddress" id="embassyAddress" ><?php echo $arrAddEditInfo['embassyAddress'];?></textarea>
						</div>
					</div>
				</div>
			</div> 				 
		 	
			<div class="row margin-bottom-20">
					<div class="col-sm-4">
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary btn-lg btn-block" id="submitembassyDetailsForm">Submit Details</button>
					</div>
					<div class="col-sm-4">
					</div>
			</div>	
			<input type="hidden" name="embessayId" id="embessayId" value="<?php echo $arrAddEditInfo['embessayId'];?>" />			
			</form>		
		</div>	
	</div>
</div>
<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/admin/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.redirect.js" type="text/javascript" ></script>	

<script src="<?php echo base_url();?>assests/js/admin/jquery.dataTables.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/DT_bootstrap.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/table-editable.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.validate.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/additional-methods.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/bootstrap-fileupload.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/app.js"></script>      
<script src="<?php echo base_url();?>assests/js/admin/ui-jqueryui.js"></script>      
<script src="<?php echo base_url();?>assests/js/admin/form-validation.js"></script>      
<script src="<?php echo base_url();?>assests/js/admin/customJS.js"></script>      
<script>
	var base_url = "<?php echo base_url();?>";
	jQuery(document).ready(function() {    
	
	$("#staffMembersEmail").on("blur",function(){
		var email = $(this).val();		
		var staffMembersId = $("#staffMembersId").val();
		$.ajax({
			type:'POST',
			url:base_url+'index.php/Admin/checkEmailForStaffAndAdmin',
			data:'email='+email+'&staffMembersId='+staffMembersId,
			success:function(response){									
				if(response == "EXISTS"){
					$("#staffMembersEmail").val("");
					alert("Email has been taken.Please try another one.");
				}else if(response == "NTEXISTS"){
					//Do nothing
				}
			}
		}); 
	});
	
	$.validator.addMethod("noBlankSpace", function (value, element) {
        if (!(value == '' || value.trim().length != 0)) {
            return false;
        } else {
            //$(element).val($(element).val().trim());
            return true;
        }
    }, 'Only Blanks spaces are not allowed for this field');
	
	
	   App.init();
	    TableEditable.init();
		UIJQueryUI.init();
		FormValidation.init();
		
		
		
		
		$('#countryId').on('change',function(){		  
			var countryID = $(this).val();		
			if(countryID){				
				$.ajax({
					type:'POST',
					url:base_url+'index.php/Admin/getStateList',
					data:'country_id='+countryID,
					success:function(html){					
						$('#state').html(html);                    
					}
				}); 
			}else{
				$('#state').html('<option value="">Select country first</option>');            
			}
		});
		
	  $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){			
            $.ajax({
                type:'POST',
                url:base_url+'index.php/Admin/getCityList',
                data:'state_id='+stateID,
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
		
		
		
	 
		
	});
</script>
<?php	if(($msg) && $msg != "" && $msg == "DEL"){	?>	
	<script type="text/javascript">
		alert("Document has been deleted.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTDEL"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to delete document.");
	</script>
<?php 	} ?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
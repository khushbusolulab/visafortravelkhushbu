<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container row-fluid" >  
		<div class="page-content">			
			<div class="container-fluid">				
				<div class="row-fluid">
					<div class="span12">					
						<h3 class="page-title">
							<strong>User Deatils</strong>
							<a href="<?php echo base_url();?>index.php/Admin/appUsers" class="btn btn-info pull-right">Back</a>
						</h3>						
					</div>
				</div>				
				<div class="row-fluid margin-bottom-20">					 
				<div class="row-fluid">
					<div class="span2">							
							<div class="user-pic">
								<?php if(!empty($userData['profilePicture'])) {	$filename = "./uploads/userImages/".$userData['profilePicture'];	if(file_exists($filename)){	?>
									<img src="<?php echo base_url()."uploads/userImages/".$userData['profilePicture'];?>">			
								<?php }else{ ?>
									<img src="<?php echo base_url()."assests/img/User.png";?>">						
								<?php }	}else{	?> 
									<img src="<?php echo base_url()."assests/img/User.png";?>">						
								<?php }	?>
							</div>							
					</div>
					<div class="span10">
						<div class="headerInfo">							
							<div class="row">
								<span class="user-name"><strong><?php echo $userData['fullName'];?></strong></span>
							</div>
							<div class="row">
								<span class="user-info-span" ><?php echo $userData['emailId'];?></span>
							</div>							
							<div class="row">
								<span class="user-info-span" ><?php echo $userData['mobile'];?></span>
							</div>
							<div class="row">
								<span class="user-info-span" ><?php echo $userData['cityName'];?>,<?php echo $userData['stateName'];?>,<?php echo $userData['countryName'];?></span>
							</div>							
						</div>
					</div>						
				</div>
			</div>
			<div class="row-fluid margin-bottom-20">					 
				<div class="row-fluid">
				
					<!-- for User Reviews -->
					<div class="span6">	
						<h3>User Reviews</h3><?php 						
						if(!empty($reviewsData)) { 
							foreach($reviewsData as $value){
								?><div class="well">									
										<div class="span12">
											<div class="span2">	
												<div class="user-pic-small">
													<?php if(!empty($value['images'])) {	$filename = "./uploads/office_pictures/".$value['images'];	if(file_exists($filename)){	?>
														<img src="<?php echo base_url()."uploads/office_pictures/".$value['images'];?>">			
													<?php }else{ ?>
														<img src="<?php echo base_url()."assests/img/User.png";?>">						
													<?php }	}else{	?> 
														<img src="<?php echo base_url()."assests/img/User.png";?>">						
													<?php }	?>
												</div>
											</div>
											<div class="span10">								 
												<span class="user-name"><strong><?php echo $value['businessName'];?></strong></span>
												<?php 
													$value['rating'] = $value['rating']*10;
													$classString = "rating-".$value['rating'];
												?>
												<span class="rating-static <?php echo $classString;?> class-new-rating"></span>
												<br/><span class="pull-right"><?php echo $value['reviewDateTime'];?></span>							
											</div>
										</div>
									
									<div class="span12">
										<div class="rate-comments"><?php echo $value['rateComments'];?></div>
									</div>
									<div class="clearfix"></div>
								</div><?php	
							}
						} else { 
							?><div class="well">
								<h4>No Reviews Given</h4>							
							</div><?php 
						} 
					?></div>
					
					<!-- for User Bookmarks -->
					<div class="span6">	
						<h3>User Bookmarks</h3>
						<?php if(!empty($bookMarkData)) { 
								foreach($bookMarkData as $value){
						?>
							<div class="well">
								<div class="span12">
										<div class="span2">	
											<div class="user-pic-bookmark">
												<?php if(!empty($value['images'])) {	$filename = "./uploads/office_pictures/".$value['images'];	if(file_exists($filename)){	?>
												<img src="<?php echo base_url()."uploads/office_pictures/".$value['images'];?>">			
												<?php }else{ ?>
												<img src="<?php echo base_url()."assests/img/User.png";?>">						
												<?php }	}else{	?> 
												<img src="<?php echo base_url()."assests/img/User.png";?>">						
												<?php }	?>
											</div>
										</div>
										<div class="span10">
										<span class="user-name" ><strong><?php echo $value['businessName'];?></strong></span>
										<?php 
											$value['rating'] = $value['rating']*10;
											$classString = "rating-".$value['rating'];
										?>																				
										<div class="clearfix"></div>
										<?php if($value['businessAddress1'] != ""){ ?>
											<span><?php echo $value['businessAddress1'];?></span>
										<?php } ?>
										<?php if($value['businessAddress2'] != ""){ ?>
											<span><?php echo $value['businessAddress2'];?></span>
										<?php } ?>
										<?php if($value['businessArea'] != ""){ ?>
											<span><?php echo $value['businessArea'];?></span>
										<?php } ?>
										<span class="rating-static <?php echo $classString;?> class-new-rating"></span>
									</div>	
								</div>
							</div>
							 <?php	}	?>
						<?php } else { ?>
						<div class="well">
							<h4>No Bookmarks Done</h4>							
						</div>
						<?php } ?>	
					</div>
				</div>
			</div>			
		</div>	
	</div>
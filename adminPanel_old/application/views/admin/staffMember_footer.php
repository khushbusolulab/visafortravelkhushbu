<script src="<?php echo base_url();?>assests/js/admin/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assests/js/admin/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assests/js/admin/jquery.uniform.min.js" type="text/javascript" ></script>	

<script src="<?php echo base_url();?>assests/js/admin/jquery.dataTables.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/DT_bootstrap.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/table-editable.js" type="text/javascript" ></script>	
<script src="<?php echo base_url();?>assests/js/admin/app.js"></script>      
<script>
	jQuery(document).ready(function() {    
	   App.init();
	    TableEditable.init();
	});
</script>
<?php	if(($msg) && $msg != "" && $msg == "SUCC"){	?>	
	<script type="text/javascript">
		alert("Staff member has been added.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "EDSUCC"){ ?>
	<script type="text/javascript">
		alert("Profile has been edited.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "EDNTSUCC"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to edit staff profile.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTUNBLOCKED"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to unblock user.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "BLOCKED"){ ?>
	<script type="text/javascript">
		alert("Staff member has been blocked to use this platform.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTBLOCKED"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to block staff member.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "UNBLOCKED"){ ?>
	<script type="text/javascript">
		alert("Staff member has been unblocked to use this platform.");
	</script>
<?php 	}elseif(($msg) && $msg != "" && $msg == "NTUNBLOCKED"){ ?>
	<script type="text/javascript">
		alert("Some error occured please try again to unblock staff member.");
	</script>
<?php }	?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
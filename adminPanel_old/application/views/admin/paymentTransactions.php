<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title"><strong>Payment Transactions</strong></h3>						
					</div>
				</div>				
				<div class="row margin-bottom-20">					 
					<div class="col-sm-12">					
						<form name="searchForm" id="searchForm" method="post" action = "<?php echo base_url();?>index.php/Admin/paymentTransactions" class="pay-tra" >
							<div class="form-group">								 							
								<label class="label-from-to">From</label>
								<input class="m-wrap form-control" size="16" type="text"  id="ui_date_picker" name="searchFromTime" value="<?php echo $start_time;?>" />								 
							</div>
							<div class="form-group">								 									
								<label class="label-from-to">To</label>
								<input class="m-wrap form-control" size="16" type="text" id="ui_date_picker1" name="searchToTime" value="<?php echo $end_time;?>" />								 
							</div>
							<div class="form-group">								 									
								<button type="submit" class="btn btn-primary btn-md"><i class="icon-ok"></i> Go</button>						 
							</div>
						</form>
					</div>
					<div class="col-sm-12">
						<div class="portlet box">
							<div class="portlet-title">										
							</div>
							<div class="portlet-body">										
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Transactions ID</th>
											<th>Amount</th>
											<th>Payment Date & Time</th>											
											<th>Business Name</th>											
											<th>Status</th>											
											<th>Type</th>																						
										</tr>
									</thead>
									<tbody>										 
										<?php foreach($paymentTransactions as $value){?>
										<tr>
											<td><?php echo $value['transaction_id'];?></td>											
											<td><?php echo number_format($value['totalamount'],"2",".","");?> <?php echo $value['transaction_currency'];?></td>											
											<td><?php echo $value['paymentDate'];?></td>											
											<td><a href="<?php echo base_url();?>index.php/Admin/businessDeatils/<?php echo $value['BusinessRecid']?>" target="_blank"><?php print htmlentities($value['businessName'],ENT_QUOTES,"UTF-8");?></a></td>											
											<td><?php echo $value['transaction_state'];?></td>											
											<td>
												<?php 
													if($value['purchaseType'] == "NEW"){
														echo "Purchase Payment for ".$value['packageType']." Package";
													}elseif($value['purchaseType'] == "RENEW"){
														echo "Renewal Payment for ".$value['packageType']." Package";
													}
												?>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
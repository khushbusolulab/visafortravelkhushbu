	<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
<div class="page-container" >  
	<div class="page-content">			
		<div class="container">				
			<div class="row">
				<div class="col-sm-12">	
					<?php if($arrAddEditInfo['countryInformationID'] == 0) {?>
						<h3 class="page-title">
						<strong>Add Country Information</strong>
						<a href="<?php echo base_url();?>index.php/Admin/countryInfo" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } else{ ?>
						<h3 class="page-title">
						<strong>Edit Country Information</strong>
						<a href="<?php echo base_url();?>index.php/Admin/countryInfo" class="btn btn-primary pull-right">Back</a>
						</h3>						
					<?php } ?>
				</div>
			</div>	
			<form method="post" name="countryDetailsForm" id="countryDetailsForm" action="<?php echo base_url();?>index.php/Admin/performCountryInfoDetails" >			
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			
			
			
			<div class="row margin-bottom-20">			
				<div class="col-sm-4">
					<div class="form-group">
						<div class="controls">
							<label class="label-from-to">Select Country</label>							
							<select class="medium m-wrap form-control" name="countryId" id="countryId">
								<option value="">Please Select Country</option>										
								<?php foreach($countryValues as $value) { 
									if(!in_array($value['countryId'],$ignoreIDs) || $arrAddEditInfo['countryId'] == $value['countryId']){
								?>
									<option value="<?php echo $value['countryId'];?>" <?php if($arrAddEditInfo['countryId'] == $value['countryId']){ echo "Selected"; }?>><?php echo $value['countryName'];?></option>										
								<?php 
									}
								} ?>
							</select>						
						</div>
					</div>						 
				</div>
			</div> 
			
			
			<div class="row margin-bottom-20">			
					<div class="col-sm-4">
						<div class="form-group">
							<div class="controls">
								<label class="label-from-to">Country Details</label>
								<textarea class="large m-wrap form-control" rows="3" name="countryDetails" id="countryDetails" ><?php echo $arrAddEditInfo['countryDetails'];?></textarea>
							</div>
						</div>						 
					</div>
			</div> 				 
		 	
			<div class="row margin-bottom-20">
					<div class="col-sm-4">
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary btn-lg btn-block" id="submitcountryDetailsForm">Submit Details</button>
					</div>
					<div class="col-sm-4">
					</div>
			</div>	
			<input type="hidden" name="countryInformationID" id="countryInformationID" value="<?php echo $arrAddEditInfo['countryInformationID'];?>" />			
			</form>				
				</div>				
			</div>			
		</div>	
	</div>
</div>
<body class="page-header-fixed page-full-width">	
<?php  include("menu.php"); ?>	
	<div class="page-container" >  
		<div class="page-content">			
			<div class="container">				
				<div class="row">
					<div class="col-sm-12">					
						<h3 class="page-title"><strong>Welcome To My Visa4 Travel Administration</strong></h3>						
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row margin-bottom-20">
					<div class="col-sm-4 responsive"> <!--data-tablet="span4" data-desktop="span4"-->
						<div class="dashboard-stat">
							<div class="visual">
								<i class="icon-user"></i>
							</div>
							<div class="details">
								<div class="number">
									<?php echo $totUsers;?>
								</div>
								<div class="desc">                           
									Total App Users
								</div>
							</div>								                  
						</div>
					</div>	
					<div class="col-sm-4 responsive"> <!--data-tablet="span4" data-desktop="span4"-->
						<div class="dashboard-stat">
							<div class="visual">
								<i class="icon-home"></i>
							</div>
							<div class="details">
								<div class="number">
									<?php echo $totBusinesses;?>
								</div>
								<div class="desc">                           
									Total Business Users
								</div>
							</div>								                  
						</div>
					</div>					 					
					<div class="col-sm-4 responsive"> <!--data-tablet="span4" data-desktop="span4"-->
						<div class="dashboard-stat">
							<div class="visual">
								<i class="icon-location-arrow"></i>
							</div>
							<div class="details">
								<div class="number">
									<?php echo $totClicks;?>
								</div>
								<div class="desc">                           
									Total Clicks
								</div>
							</div>								                  
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">						
						<div class="portlet box">
							<div class="portlet-title">
								<div class="caption">Users Across Globe</div>								
							</div>
							<div class="portlet-body">								 
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Country</th>
											<th>App Users</th>
											<th>Sponsors</th>											
										</tr>
									</thead>
									<tbody>
									<?php foreach($countries as $value) { ?>
										<tr >
											<td><?php echo $value['countryName'];?></td>
											<td>
												<?php 
													if(array_key_exists($value['countryId'],$usersCount)){
														echo $usersCount[$value['countryId']];
													}else{
														echo "0";
													}
												?>
											</td>
											<td>
												<?php 
													if(array_key_exists($value['countryId'],$businessCount)){
														echo $businessCount[$value['countryId']];
													}else{
														echo "0";
													}
												?>
											</td>											 
										</tr>
									<?php } ?>
										 
									</tbody>
								</table>
							</div>
						</div>						
					</div>
				</div>				
			</div>			
		</div>	
	</div>
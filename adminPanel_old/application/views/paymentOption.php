<body>
<?php  include("menu.php"); ?>
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER --> 
<!--<form action="<?php print base_url(); ?>index.php/Payment/chargeCustomer" method="POST" id="payment-form">	-->

    <div class="container">
		<div class="row">
			<div class="input-field col m12">
				<h3><u>Payment Option</u></h3>				
			</div>
		</div> 
	<form name="bypassPayment" id="bypassPayment" method="post" action="<?php echo base_url();?>index.php/Home/bypassPaymentAndInsertData">	
	<input type="hidden" name="hidOrderMasterId" id="hidOrderMasterId" value="<?php echo $ordermasterID;?>" >
	<div class="row">	
		<?php $totalBillAmount = 0;
		
		foreach($packageDetails as $value) { 
		$packageAmount = number_format($value['packageAmount'], 2,".","");
		$totalBillAmount += $packageAmount;
		?><div class="well well-lg">
			<div class="row" style="border-bottom: 3px solid #4c4739;padding: 13px;padding-bottom: 26px;font-size: 20px;margin-left: 0px;text-align: left;">		
				<div class="span3">
					<strong>Package Name</strong>
				</div>
				<div class="span3">
					<strong>Monthly Cost</strong>
				</div>
				<div class="span3">
					<strong>Months Subsription</strong> 
				</div>
				<div class="span3 pull-right">
					<strong>Total Cost</strong>
				</div>
				<div class="clearfix"></div>
				<div class="clearfix"></div>
			</div>
			<div class="row" style="padding: 13px;padding-bottom: 26px;font-size: 15px;margin-left: 0px;text-align: left;">	
				<div class="span3">
					<strong><?php echo $value['packageName'];?> Package</strong>				
				</div>
				<?php if($value['packageid'] != 4 ){ ?>
				<div class="span3">
					<span class="text-center"><strong> <?php echo $packageAmount;?> </strong></span>														
					<div class="clearfix"></div>				
				</div>
				<div class="span3">
					
					<select name="months[<?php echo $value['recid'];?>]" id="months" class="dropdownForMonthsSelection months-dropdown" data-monthlyAmount = "<?php echo $packageAmount;?>" >
						<option value="1">1</option>
						<option value="3">3</option>
						<option value="6">6</option>
						<option value="9">9</option>
						<option value="12">12</option>
					</select>				
					<div class="clearfix"></div>									
				</div>
				<div class="span3">	
					<label class="pull-left"><strong><span id="spanTotAmount"><?php echo $packageAmount;?></span></strong></label>
				</div>	
				<?php }else {?>
				<span class="pull-left"><strong>Never Expires</strong></span>															
				<div class="clearfix"></div>
				<?php } ?>
				 <div class="clearfix"></div>
			</div>		 
			</div>		 
		<?php } ?>		
	</div>
		<div class="clearfix"></div>
		<div class="row">
		<div class="span6">
			<div class="well well-lg">
				<span class="pull-left"><strong>Select Country (Currency)</strong></span>												
				<span class="pull-right">
					<select name="currencyDropdown" id="currencyDropdown">
						<option value="">Please Select Country</option>
						<?php foreach($countryData as $value) { ?>
						<option value="<?php if($value['countryId'] == 101){echo "India";}else{echo "NOTIndia";} ?>" <?php if($value['countryId'] == 101 ){ echo "selected";}?>><?php echo $value['countryName'];?></option>
						<?php } ?>						
					</select>
				</span>			
				<div class="clearfix"></div>
			</div>
		</div>
		</div>
		
		<div class="row">
		<div class="span6">
			<div class="well well-lg">
				<span class="pull-left"><strong>Total Amount To Be paid (<span id="currencyToShow">INR</span>):  </strong></span>				
				<span class="padding-middle" id="totalBillAmount"><?php print number_format($totalBillAmount,"2",".",""); ?></span>
				<!--<span class="pull-right"><a href="javascript:void(0);" id="payNowButton" class="btn btn-info" >Pay Now </a></span>		-->
				<span class="pull-right"><input type="submit" name="payNowButton" id="payNowButton" class="btn btn-info" value="Pay Now">
					<!--<a href="javascript:void(0);" id="payNowButton" class="btn btn-info" >Pay Now </a>-->
				</span>			
				<div class="clearfix"></div>
			</div>
		</div>
		</div>
		</form>
		<div class="row" id="paymentForm" style="display:none;">
		<div class="span6">
			<div class="well well-lg">				
					<span class="payment-errors"></span>
					<div class="form-row">
						<label>
							<span>Card Number</span>
							<input type="text" size="20" data-stripe="number" name="cardNumber" id="cardNumber">
						</label>
					</div>
					<div class="form-row">
						<label>
							<span>Expiration (MM/YY)</span>
							<input type="text" size="2" data-stripe="exp_month" name="exp_month" id="exp_month" >
						</label>
					<span> / </span>
						<input type="text" size="2" data-stripe="exp_year" name="exp_year" id="exp_year" >
					</div>
					<div class="form-row">
						<label>
							<span>CVC</span>
							<input type="text" size="4" data-stripe="cvc" name="cvc" id="cvc" >
						</label>
					</div>
					<input type="submit" class="submit" value="Submit Payment">
					<input type="hidden" name="totAmountPayable" id="totAmountPayable" value="" />
				<!--</form>-->
			</div>
		</div>
	</div>
</div>
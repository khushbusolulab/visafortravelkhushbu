<body>
	<?php  include("menu.php"); ?>
    <!-- BEGIN BREADCRUMBS -->   
<form method="post" name="changePasswordForm" id="changePasswordForm" action="<?php echo base_url();?>index.php/Home/performChangePassword" enctype="multipart/form-data" >			
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;"></div>    
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Change Password </h3>
			</div>
		</div> 
		 
		<div class="row">		
			<div class="col-sm-6">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Password</label>
						<input type="password" placeholder="Password" class="m-wrap medium" name="password" id="password" />									
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Confirm Password</label>
						<input type="password" placeholder="Confirm Password" class="m-wrap medium" name="confirmPassword" id="confirmPassword" />									
					</div>
				</div>
			</div>
			 
		</div>  
		
	 
		 
		 <div class="row text-center">	 	
			<div class="col-sm-12">
				<div class="form-group">					
					<div class="controls">						
						<input type="Submit" name="Save" value="Change Password" class="btn btn-info">
					</div>
				</div>
			</div>			
		</div>	  
		</div>	 		
</form>
 
<?php	
	defined('BASEPATH') OR exit('No direct script access allowed');	
	
	
?>

<body id="page-top" data-spy="scroll">
  <!--  banner  -->
  <section id="home" class="banner-section">
    <div class="empty-space-40"></div>
    <div class="">
      <div class="container">
        <div class="banner-header-search">
          <div class="pull-left"> 
            <a class="btn btn-primary" href="<?php echo base_url();?>index.php/Home/career">We're Hiring!</a>
          </div>
          <div class="pull-right">
            <ul class="p-0 m-0">
              <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#homePageLoginForm">Register Here</button></li>
			  <?php 	if(isset($_SESSION['LoggedIn']) && $_SESSION['LoggedIn'] == 1){	?>
					<li class="m-l-10"><a href="<?php echo base_url();?>index.php/Home/dashboard" class="btn btn-default" >Dashboard</a></li>
			  <?php } else { ?>
					<li class="m-l-10"><button type="button" class="btn btn-default" data-toggle="modal" data-target="#homePageLoginForm" >Login</button></li>
			  <?php } ?>
            </ul>
          </div>
        </div>
	<form name="searchForm" id="searchForm" method="post" action="<?php echo base_url();?>index.php/home/websearch">
        <div class="banner-contain">
          <div class="banner-top">
              <div class="text-center color-white">
                <div class="banner-contain-box">
                  <h1 class="font-36 line-h-24 font-w-bold m-b-20"><a class="logo" href="#"><img class="img-responsive" src="<?php echo base_url();?>assests/img/logo.jpg"></a></h1>
                  <p class="font-24 line-h-24">Find your visa & Travel related businesses within your city</p>
                </div>
                <section id="search">
                  <label><i class="fa fa-search"></i><span class="sr-only">Search icons</span></label>
                  <input id="search-input" class="form-control input-lg" placeholder="Search for a Businesses" tabindex="1">
                  <button type="submit" class="btn btn-primary btn-lg">Search</button>
                </section>
              </div>
          </div>
        </div>
	</form>
      </div>
    </div>
    <div class="empty-space-40"></div>
  </section>
  <!--  /banner  -->


  <!--  about us  -->
  <section class="text-center">
    <div class="empty-space-70"></div>
    <div class="container">
      <div class="col-md-12">
        <div class="row">
          <div class="top-title">
            <h2 class="font-24 font-w-bold text-upper m-b-15">Welcome to My Visa4Travel</h2>
            <div class="top-title-line"></div>
          </div>
          <div class="about-us-contain">
            <p class="font-16 m-b-20 m-t-20 line-h-24">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
          </div>
        </div>
      </div>
    </div>
    <div class="empty-space-70"></div>
  </section>
  <!--  /about us  -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <input type="text" name="" placeholder="Email Address">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
  <!--  travel less  -->
  <section class="gray-bg">
    <div class="empty-space-70"></div>
    <div class="container">
      <div class="col-md-12">
        <div class="row">
          <div class="top-title text-center">
            <h2 class="font-24 font-w-bold text-upper m-b-15">Ways to make your travel less painful!!</h2>
            <div class="top-title-line"></div>
          </div>
          <div class="travel-less-contain">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/Accomodation.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Accomodation</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/Collage.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Collage</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/country.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Country</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/english.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>English</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/Lawyers.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Lawyers</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/Money Transfer.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Money Transfer</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/passport.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Passport</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/taxi.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Taxi</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/tour.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Tour</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/travel.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Travel</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/visa.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Visa</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="travel-less-item" data-toggle="modal" data-target="#myModal">
                  <div class="travel-less-item-img">
                    <img class="img-responsive" src="<?php echo base_url();?>assests/img/icon/worldwide.png" alt="category icon">
                  </div>
                  <div class="item-contain">
                    <p>Worldwide</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>  
      </div>
    </div>
    <div class="empty-space-70"></div>
  </section>
  <!--  /travel less  -->

<!--  testimonials  -->
<!--<section class="testimonials">
  <div class="empty-space-70"></div>
  <div class="container">
    <div class="row">
      <div class="testimonials-text">
        <h3 class="font-24 text-uppercase font-weight-bold text-center">testimonials</h3>
        <div class="top-title-line"></div>
        <div class="testimonials-1">
          <div class="item ">
            <div class="col-md-8 col-md-offset-2">
              <div class="testimonials-banner-text text-center">
                <div class="testimonials-user-img">
                  <img class="img-responsive" src="<?php echo base_url();?>assests/img/profile-pic.jpg">
                </div>
                <h3 class="font-20 font-weight-bold">Zenith Sprinks</h3>
                <span class="font-18">CEO</span>
                <p class="font-16 line-height-24">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elecktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>  
            </div>
          </div>
          <div class="item ">
            <div class="col-md-8 col-md-offset-2">
              <div class="testimonials-banner-text text-center">
                <div class="testimonials-user-img">
                  <img class="img-responsive" src="<?php echo base_url();?>assests/img/profile-pic.jpg">
                </div>
                <h3 class="font-20 font-weight-bold">Zenith Sprinks</h3>
                <span class="font-18">CEO</span>
                <p class="font-16 line-height-24">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elecktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>  
            </div>
          </div>
          <div class="item ">
            <div class="col-md-8 col-md-offset-2">
              <div class="testimonials-banner-text text-center">
                <div class="testimonials-user-img">
                  <img class="img-responsive" src="<?php echo base_url();?>assests/img/profile-pic.jpg">
                </div>
                <h3 class="font-20 font-weight-bold">Zenith Sprinks</h3>
                <span class="font-18">CEO</span>
                <p class="font-16 line-height-24">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into elecktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>  
  </div> 
  <div class="empty-space-70"></div> 
</section>-->
<!--  /testimonials  -->

  <!--  Looking for Visa  -->
  <section id="mobileapp">
    <div class="empty-space-70"></div>
    <div class="container">
      <div class="col-md-12">
        <div class="row">
          <div class="looking-visa-contain">
            <div class="row">
              <div class="col-md-4 text-center">
                <div class="mobile-screen-box">
                  <img class="img-responsive" src="<?php echo base_url();?>assests/img/mobile-phone.png" alt="mobile phone icon">
		  <!--<img class="img-responsive img-behind" src="<?php echo base_url();?>assests/img/iphone.png" alt="mobile phone icon">-->
                  <div class="mobile-screen">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="<?php echo base_url();?>assests/img/Screen-1.jpg" alt="Screen" width="460" height="345">
                        </div>

                        <div class="item">
                          <img src="<?php echo base_url();?>assests/img/Screen-2.jpg" alt="Screen" width="460" height="345">
                        </div>
                      
                        <div class="item">
                          <img src="<?php echo base_url();?>assests/img/Screen-3.jpg" alt="Screen" width="460" height="345">
                        </div>

                        <div class="item">
                          <img src="<?php echo base_url();?>assests/img/Screen-4.jpg" alt="Screen" width="460" height="345">
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="looking-visa-right">
                  <div class="top-title">
                    <h2 class="font-24 font-w-bold text-upper m-b-15">Looking for Visa or Travel Related Information?</h2>
                    <div class="top-title-line left"></div>
                  </div>
                  <p>Get the App Now</p>
                  <p>We’ll send you a link, open it on your phone to download the app</p>
                  <form class="form-inline">
                    <div class="row">
                      <div class="col-sm-8">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                            <input type="text" class="form-control" placeholder="Emali">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <button type="submit" class="btn btn-primary">E-mali App Link</button>
                      </div>
                    </div>
                  </form>
                  <div class="row m-t-20">
                    <div class="col-sm-3 col-xs-6 text-center mar-bot10">
                      <a href="#"><img class="img-responsive" src="<?php echo base_url();?>assests/img/download_app_icon_1.png" alt="download 0app icon"></a>
                    </div>
                    <div class="col-sm-3 col-xs-6 text-center mar-bot10">
                      <a href="#"><img class="img-responsive" src="<?php echo base_url();?>assests/img/download_app_icon_2.png" alt="download 0app icon"></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="empty-space-70"></div>
  </section>
  
  
  <!--  Know More  -->
  <section class="gray-bg">
    <div class="empty-space-70"></div>
    <div class="container">
      <div class="col-md-12">
        <div class="row">
          <div class="know-more">
            <div class="row">
              
              <div class="col-md-12">
                <div class="">
                  <div class="top-title">
                    <h2 class="font-24 font-w-bold text-upper m-b-15 text-center">Want to know more?</h2>
                    <div class="top-title-line"></div>
                  </div>
                  <p class="text-center">Please leave your contact details below, and<br>we’ll be in touch within 24 hours</p>
                  <form class="text-center">
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="City*" required>
                        </div>
                      </div>
		      <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Restaurant Name*" required>
                        </div>
                      </div>
		      <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Full Name*" required>
                        </div>
                      </div>
		      <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Emali*" required>
                        </div>
                      </div>
		      <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone Number*" required>
                        </div>
                      </div>
                      <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary">Call me back</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="empty-space-70"></div>
  </section>
  
  <!-- Login Form Popup -->
  <div class="modal fade" id="homePageLoginForm" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title text-center" id="myModalLabel">
                    Access Your Business Account
                </h4>
            </div>
            <div class="modal-body">
		<div class="row">
			<div class="col-sm-6">
				<form role="form" method="post" action="<?php echo base_url()."index.php/Home/registerBusiness";?>" id="businessRegisterForm">
					<span class="title-head">Register Now</span>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessMerchantName" id="businessMerchantName" placeholder="Your Name"/>
					</div>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessName" id="businessName" placeholder="Business Name"/>
					</div>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessEmail" id="businessEmail" placeholder="Email Address"/>
					</div>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessPhone" id="businessPhone" placeholder="Mobile Number"/>
					</div>
					<div class="form-group">
					    <input type="checkbox" class="form-control" />
					    <label>I agree to Terms & Conditions</label>
					</div>
					<div class="form-group text-center">
					    <button type="submit" class="btn btn-primary" >Submit</button>
					</div>
					<!--<div class="form-group">
						<div class="col-sm-12">
						    <span class="pull-left">Not a Registered User?Please<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageRegisterForm" > Register Here!</a></span>
						    <span class="pull-right"><a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageForgotPasswordForm" > Can't Access your account?</a></span>
						</div>
					</div>-->
				</form>
			</div>
			<div class="col-sm-6">
				<form role="form" method="post" action="<?php echo base_url()."index.php/Home/businessMerchantLogin";?>" id="businessMerchantLoginForm" >
					<span>Already registered with us?</span>
					<span class="title-head">Login here</span>
					<div class="form-group">
						<input type="text" class="form-control" id="businessEmailLogin" name="businessEmailLogin" placeholder="Email"/>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="passwordLogin" name="passwordLogin" placeholder="Password"/>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-primary" >Submit</button>
					</div>
					<div class="form-group text-center">
						<span><a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageForgotPasswordForm" > Can't Access your account?</a></span>
					</div>
				</form>
			</div>
		</div>
            </div>                        
        </div>
    </div>
</div>


<!-- Register Form Popup -->
  <div class="modal fade" id="homePageRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Register Now
                </h4>
            </div>
            <div class="modal-body">                
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()."index.php/Home/registerBusiness";?>" id="businessRegisterForm">
                  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="businessMerchantName" id="businessMerchantName" placeholder="Your Name"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="businessName" id="businessName" placeholder="Business Name"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="businessEmail" id="businessEmail" placeholder="Email Address"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="number" class="form-control" name="businessPhone" id="businessPhone" placeholder="Mobile Number"/>
                    </div>
                  </div>                  
					<div class="form-group">
						<div class="col-sm-6">														
							<span class="text-center">I Agree to Terms And Conditions </span><input type="checkbox" class="pull-left" name="check" id="check" style="width:20%; position:relative;"/>							                  
						</div>                  
					</div>                  
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button type="submit" class="btn btn-primary" >Register</button>
					  <span class="pull-right">Already A Member? Click Here to <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageLoginForm" >Login</a></span>
                    </div>
                  </div>				   
                </form>
            </div>                        
        </div>
    </div>
</div>



<!-- Forgot password Popup -->
  <div class="modal fade" id="homePageForgotPasswordForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   Can't Access Your Account?
                </h4>				
            </div>
            <div class="modal-body">                
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()."index.php/Home/businessForgotPassword";?>" id="businessForgotPassword" >
                  <div class="form-group">                    
                    <div class="col-sm-12">
                        <span class="text-center">Please enter your registered e-mail address to get a password reset link to acess your account. </span>
                    </div>
                  </div>
                  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="email" class="form-control" id="forgotEmailID" name="forgotEmailID" placeholder="E-mail Address"/>
                    </div>
                  </div>                  
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button type="submit" class="btn btn-primary" >Send</button>
                    </div>
                  </div>				  
                </form>
            </div>                        
        </div>
    </div>
</div>
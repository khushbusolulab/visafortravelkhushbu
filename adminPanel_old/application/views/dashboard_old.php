<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Metronic Frotnend | Prices</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="<?php echo base_url();?>assests/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style_dashboard.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assests/css/jquery.fancybox.css">               
    <link href="<?php echo base_url();?>assests/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url();?>assests/css/prices.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->    
    <link href="<?php echo base_url();?>assests/css/blue.css" rel="stylesheet" type="text/css" id="style_color"/>    
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body>
	<!-- BEGIN STYLE CUSTOMIZER -->
	 
	<!-- END BEGIN STYLE CUSTOMIZER -->    

    <!-- BEGIN HEADER -->
    <div class="front-header">
        <div class="container">
            <div class="navbar">
                <div class="navbar-inner">
                    <!-- BEGIN LOGO (you can use logo image instead of text)-->
                    <a class="brand logo-v1" href="index.html">
                        <img src="<?php echo base_url();?>assests/img/logo.jpg" id="logoimg" alt="" style="margin-top:0px; height:70px;">
                    </a>
                    <!-- END LOGO -->

                      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->

                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="javascript:void(0);">Home</a></li>
                            <li><a href="page_about.html">About Us</a></li>
                            <li><a href="page_services.html">Services</a></li>
                            <li class="active"><a href="javascript:void(0);">DashBoard</a></li>                            
                            <li><a href="page_contacts.html">Contact</a></li>
							<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                                    <i class="icon-user" style=" font-size: 20px; padding:10px 16px 10px;"></i>
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url();?>index.php/Home/addProfile">Add Profile</a></li>
                                    <li><a href="<?php echo base_url();?>index.php/Home/editProfile">Edit Profile</a></li>
                                </ul>
                            </li>
                            <!--<li><a href="<?php echo base_url();?>index.php/Home/addProfile"><i class="icon-user" style=" font-size: 20px; padding:10px 16px 10px;"></i></a></li>-->
                        </ul>
                        <div class="search-box">
                            <div class="input-append">
                                <form>
                                    <input style="background:#fff;" class="m-wrap" type="text" placeholder="Search" />
                                    <button type="submit" class="btn theme-btn">Go</button>
                                </form>
                            </div>
                        </div>                            
                    </div>
                    <!-- BEGIN TOP NAVIGATION MENU -->
                </div>
            </div>
        </div>      
    </div>  
    <!-- END HEADER -->

    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
        <!-- BEGIN PRICING OPTION1 -->
        <div class="row-fluid margin-bottom-40" style="width:100% !important;">
            <div class="span3 pricing-active red">
                <div class="pricing-head">
                    <h3>Free Package</h3>
                    <h4><i>Free</i> <span>Forever</span></h4>
                </div>
                <ul class="pricing-content unstyled">
                    <li><i class="icon-tags"></i> At vero eos</li>
                    <li><i class="icon-asterisk"></i> No Support</li>
                    <li><i class="icon-heart"></i> Fusce condimentum</li>
                    <li><i class="icon-star"></i> Ut non libero</li>
                    <li><i class="icon-shopping-cart"></i> Consecte adiping elit</li>
                </ul>
                <div class="pricing-footer">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .</p>
                    <a href="#" class="btn btn-success">Selected</a>  
                </div>
            </div>
            <div class="span3 pricing-active blue">
                <div class="pricing-head">
                    <h3>Bronze Package</h3>
                    <h4><i>$</i>9<i>.99</i> <span>Per Month</span></h4>
                </div>
                <ul class="pricing-content unstyled">
                    <li><i class="icon-tags"></i> At vero eos</li>
                    <li><i class="icon-asterisk"></i> No Support</li>
                    <li><i class="icon-heart"></i> Fusce condimentum</li>
                    <li><i class="icon-star"></i> Ut non libero</li>
                    <li><i class="icon-shopping-cart"></i> Consecte adiping elit</li>
                </ul>
                <div class="pricing-footer">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .</p>
                    <a href="#" class="btn btn-success">Selected</a>  
                </div>
            </div>
            <div class="span3 pricing-active green">
                <div class="pricing-head pricing-head-active">
                    <h3>Silver Package</h3>
                    <h4><i>$</i>19<i>.99</i> <span>Per Month</span></h4>
                </div>
                <ul class="pricing-content unstyled">
                    <li><i class="icon-tags"></i> At vero eos</li>
                    <li><i class="icon-asterisk"></i> No Support</li>
                    <li><i class="icon-heart"></i> Fusce condimentum</li>
                    <li><i class="icon-star"></i> Ut non libero</li>
                    <li><i class="icon-shopping-cart"></i> Consecte adiping elit</li>
                </ul>
                <div class="pricing-footer">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .</p>
                    <a href="#" class="btn theme-btn">Select</a>  
                </div>
            </div>
            <div class="span3 pricing-active yellow">
                <div class="pricing-head">
                    <h3>Gold Package</h3>
                    <h4><i>$</i>99<i>.00</i> <span>Per Month</span></h4>
                </div>
                <ul class="pricing-content unstyled">
                    <li><i class="icon-tags"></i> At vero eos</li>
                    <li><i class="icon-asterisk"></i> No Support</li>
                    <li><i class="icon-heart"></i> Fusce condimentum</li>
                    <li><i class="icon-star"></i> Ut non libero</li>
                    <li><i class="icon-shopping-cart"></i> Consecte adiping elit</li>
                </ul>
                <div class="pricing-footer">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .</p>
                    <a href="#" class="btn theme-btn">Select</a>  
                </div>
            </div>
        </div>
        <!-- END PRICING OPTION1 -->        
       
			<div class="row">
				<div class="pull-left">
					<span style="padding-right:50px;" >Amount Payable : 20 USD</span>
					<a href="javascript:void(0);" class="btn btn-default" >Pay Now</a>  
				</div>
			</div>
    </div>
    <!-- END CONTAINER --> 
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>         
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script>      
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
                        
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
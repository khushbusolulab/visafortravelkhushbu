<body>
<?php  include("menu.php"); ?>		
    <!-- BEGIN BREADCRUMBS -->   
	<form name="editProfileForm" id="editProfileForm" method="post" action="<?php echo base_url();?>index.php/Home/performEditProfile" enctype="multipart/form-data"  style="background-color: #fff;" >	    
	<div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;"></div>    
	<input type="hidden" name="hidCountryName" id="hidCountryName" value=""> 
	<input type="hidden" name="hidStateName" id="hidStateName" value=""> 
	<input type="hidden" name="hidCityName" id="hidCityName" value=""> 
	<input type="hidden" name="recid" id="recid" value="<?php echo $businessesData['businessId'];?>" />
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Edit Business Profile</h3>
			</div>
		</div> 
		 
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Business Display Name</label>
						<input type="text" class="m-wrap medium form-control"  name="businessName" id="businessName" value="<?php echo $businessesData['businessName'];?>" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Contact Number</label>
						<input type="number" name="businessPhone" id="businessPhone" class="m-wrap medium form-control" value="<?php echo $businessesData['businessPhone'];?>" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Business E-Mail Address</label>
						<input type="text" name="businessEmail" id="businessEmail" class="m-wrap medium form-control"  value="<?php echo $businessesData['businessEmail'];?>" />						
					</div>
				</div>
			</div>
		</div>  
		
		<div class="clearfix margin-bottom-10"></div>
		
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Address Line 1</label>
						<input type="text" name="businessAddress1" id="businessAddress1" class="m-wrap medium form-control" value="<?php echo $businessesData['businessAddress1'];?>" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Address Line 2</label>
						<input type="text" name="businessAddress2" id="businessAddress2" class="m-wrap medium form-control" value="<?php echo $businessesData['businessAddress2'];?>" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Area / Locality</label>
						<input type="text" name="businessArea" id="businessArea" class="m-wrap medium form-control" value="<?php echo $businessesData['businessArea'];?>" />						
					</div>
				</div>
			</div>
		</div>    
		
		
		<div class="clearfix margin-bottom-10"></div>
		
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Country</label>
						<select class="medium m-wrap form-control" name="country" id="country" >
							<option value="">Select Country</option>							
							<?php foreach($countryValues as $country_value){ ?>
								<option value="<?php echo $country_value['countryId'];?>" <?php if($businessesData['countryId'] == $country_value['countryId']){ echo "Selected"; }?>><?php echo $country_value['countryName'];?></option>							
							<?php  } ?>							
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">State</label>
						<select class="medium m-wrap form-control" id="state" name="state" >
							<option value="">Select State</option>
							<?php foreach($StateList as $state_value){ ?>
								<option value="<?php echo $state_value['stateId'];?>" <?php if($businessesData['stateId'] == $state_value['stateId']){ echo "Selected"; }?>><?php echo $state_value['stateName'];?></option>							
							<?php  } ?>							
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">City</label>
						<select class="medium m-wrap form-control" id="city" name="city" >
							<option value="">Select City</option>
							<?php foreach($CityList as $city_value){ ?>
								<option value="<?php echo $city_value['cityId'];?>" <?php if($businessesData['cityId'] == $city_value['cityId']){ echo "Selected"; }?>><?php echo $city_value['cityName'];?></option>							
							<?php  } ?>			
						</select>
					</div>
				</div>
			</div>			
		</div>  
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Select Business Category *<sup>Maximum 3</sup></label>
						<select id="businessCategory" name="businessCategory[]" multiple="multiple" class="medium form-control" >
						<?php foreach($categoryValues as $category_value) { ?>
							<option value="<?php echo $category_value['categoryId']?>" <?php if(in_array($category_value['categoryId'],$selectedCategories)) { echo "Selected";} ?>><?php echo $category_value['categoryName']?></option>
						<?php } ?>								
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Upload Office Picture</label>						
						<input type="file" name="OficeFileUpload" id="OficeFileUpload" class="form-control" accept='image/*' >
					</div>
				</div>
			</div>			 								
		<?php if(!empty($officePictures)) {
			$filename = "./uploads/office_pictures/".$officePictures['images'];
				if(file_exists($filename)){
			?>
			<script type="text/javascript">
				document.getElementById("OficeFileUpload").disabled = true;				
			</script>
			<div class="jFiler-items jFiler-row" id="previewContainer">
				<ul class="jFiler-items-list jFiler-items-grid">
					<li class="jFiler-item" data-jfiler-index="1" style="">
						<div class="jFiler-item-container">
							<div class="jFiler-item-inner">
								<div class="jFiler-item-thumb">
									<div class="jFiler-item-status"></div>                        
									<div class="jFiler-item-thumb-image">
										<img id="previewCroppedImage" src="<?php echo base_url()."uploads/office_pictures/".$officePictures['images'];?>" draggable="false" style="width:100% !important;">
									</div>
								</div>
								<div class="jFiler-item-assets jFiler-row">
									<ul class="list-inline pull-left">
										<li></li>
									</ul>
									<ul class="list-inline pull-right">
										<li><a id="deleteThisPreviewedImage" class="icon-jfi-trash jFiler-item-trash-action" data-imageid="<?php echo $_SESSION['businessId']?>"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>   
				</ul>
			</div>
		<?php 
				}else{ ?>
				<script type="text/javascript">
				document.getElementById("OficeFileUpload").disabled = false;				
			</script>
				<span>File Not Found.Please Upload a new office picture</span>					
		<?php 
				}
		} ?>
		<div class="jFiler-items jFiler-row" id="previewContainer1" style="display:none;">
				<ul class="jFiler-items-list jFiler-items-grid">
					<li class="jFiler-item" data-jfiler-index="1" style="">
						<div class="jFiler-item-container">
							<div class="jFiler-item-inner">
								<div class="jFiler-item-thumb">
									<div class="jFiler-item-status"></div>                        
									<div class="jFiler-item-thumb-image">
										<img id="previewCroppedImage1" src="" draggable="false" style="width:100% !important;">
									</div>
								</div>
								<div class="jFiler-item-assets jFiler-row">
									<ul class="list-inline pull-left">
										<li></li>
									</ul>
									<ul class="list-inline pull-right">
										<li><a id="deleteThisPreviewedImage" class="icon-jfi-trash jFiler-item-trash-action" data-imageid="<?php echo $_SESSION['businessId']?>"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>   
				</ul>
			</div>
		<!-- Modal -->		
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Crop Office Picture</h3>
			</div>
			<div class="modal-body">
				<div>
					<img id="image" src="#">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="saveOfiiceCroppedImage" type="button"> Save changes</button>
			</div>
		</div>
		</div>	
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="control-label">Services <sup>(Mention your best services to find your business in search boxes)</sup></label>
					<input type="text" name="businessServices" id="tokenfield" value="<?php echo $serviceTags;?>"  class="form-control token-example-field m-wrap large" style="border:1px solid #ccc !important;" />
				</div>
			</div>	
		</div>
		
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="from-group">
					<label class="control-label">Upload Service Pictures</label>				
					<!--<div class="dropzone2"></div>-->
					<input type="file" name="ServicesFileUpload" id="ServicesFileUpload" accept='image/*' class="form-control mar-bot10" >
				</div>
			</div>
			<div class="col-sm-12">
			<div class="jFiler-items jFiler-row" id="previewContainerForServiceImages">
				<ul class="jFiler-items-list jFiler-items-grid" id="ulForSserviceImages">
				<?php 
				if(!empty($ServicePictures)) {
					foreach($ServicePictures as $value){	
						$filename = "./uploads/service_pictures/".$value['images'];
						if(file_exists($filename)){	?>
						<li class="jFiler-item" data-jfiler-index="1" style="">
						<li class='jFiler-item' data-jfiler-index='1' id='previewBox_<?php echo $value['businessesImageId'];?>'>
						<div class="jFiler-item-container">
							<div class="jFiler-item-inner">
								<div class="jFiler-item-thumb">
									<div class="jFiler-item-status"></div>                        
									<div class="jFiler-item-thumb-image">
										<img id="previewCroppedImage" src="<?php echo base_url()."uploads/service_pictures/".$value['images'];?>" draggable="false" style="width:100% !important;">
									</div>
								</div>
								<div class="jFiler-item-assets jFiler-row">
									<ul class="list-inline pull-left">
										<li></li>
									</ul>
									<ul class="list-inline pull-right">
										<!--<li><a id="deleteThisPreviewedImage" class="icon-jfi-trash jFiler-item-trash-action" data-imageid="<?php echo $_SESSION['businessId']?>"></a></li>-->
										<li><a class='icon-jfi-trash jFiler-item-trash-action deleteThisPreviewedImageOfServices' data-ServiceId = "<?php echo $value['businessesImageId'];?>"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>   
				<?php 
						}
					}
				} 	?>
				</ul>
			</div>
			</div>
		
		
			<!-- Modal -->		
		<div id="myModalServices" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Crop Services Picture</h3>
			</div>
			<div class="modal-body">
				<div>
					<img id="imageServices" src="#">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="saveOfiiceServicesCroppedImage" type="button"> Save changes</button>
			</div>
		</div>

			
		</div>
		
		
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">
			<div class="col-sm-6">
			<div class="col-xs-4">
				<label class="control-label">Operating Hours </sub></label>				
			</div>	
			<div class="col-xs-4">
				<label class="control-label">From Time </sub></label>				
			</div>	
			<div class="col-xs-4">
				<label class="control-label">To Time </sub></label>				
			</div>
			</div>
		</div>
				<div class="clearfix margin-bottom-10"></div>
		<?php
		
		function openingHoursHtml($index, $day, $data) {
			$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
			//echo "<pre>";print_r($data);echo "</pre>";
			$html = '<div class="row">';
			$html .= '<div class="col-sm-6">';
			$html .= '<div class="col-xs-4">';
			$html .='<div class="form-group">';
			$html .='<div class="controls">';
			$html .='<input type="checkbox" name="weekdays[' . $index . ']" id="weekdays" class="daysCheckBox form-control" ' . (is_array($data) ? 'checked' : '') . ' /> ' . $days[$index];
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='<div class="col-xs-4">';
			$html .='<div class="form-group">';
			$html .='<div class="controls" >';
			$html .='<input type="text" name="fromTime[' . $index . ']" class="m-wrap small pull-left form-control timepickerFromTime" value="'.(is_array($data) ? $data["openingHours"] : '').'" />';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='<div class="col-xs-4">';
			$html .='<div class="form-group">';
			$html .='<div class="controls">';
			$html .='<input type="text" name="toTime[' . $index . ']" class="m-wrap small pull-left timepickerFromTime form-control"  value="'. (is_array($data) ? $data['closingHours'] : '').'" />';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			$html .='<div class="clearfix margin-bottom-10"></div>';
			return $html;
		}
		
		//for($intI = 1; $intI <= 7; $intI++) {
		$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
		foreach($days as $key => $data) {
			if (isset($finalopeninghours[$data])) {
				echo openingHoursHtml($key, $data, $finalopeninghours[$data]);
			} else {
				echo openingHoursHtml($key, $data, '');
			}
		} ?>
		 
		<div class="clearfix margin-bottom-10"></div> 
		 
		 <div class="row text-center">	 	
			<div class="span12">
				<div class="control-group">					
					<div class="controls">						
						<input type="Submit" name="Save" value="Edit Profile" class="btn btn-info">
					</div>
				</div>
			</div>			
		</div>
		<div class="clearfix margin-bottom-10"></div>		
		<div class="row">
			<div class="col-sm-12">
				<label class="control-label">*<sup>Promotion Offers for 3 months to select 3 categories, after that charge for selection for more than 1 category.</sup></label>		
			</div>	
		</div>		
		  
		</div>
	</div>	
</form>       
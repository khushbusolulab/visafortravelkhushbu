<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Visa4Travel</title>
    <!--<link href="<?php echo base_url();?>assests/css/bootstrap_design.css" rel="stylesheet">-->
    <link href="<?php echo base_url();?>assests/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assests/css/icons.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assests/css/owl.theme.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assests/css/style.css" rel="stylesheet">    
    <link href="<?php echo base_url();?>assests/css/bootstrap-dialog.min.css" rel="stylesheet">    
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link href="<?php echo base_url();?>assests/css/custom.css" rel="stylesheet">
</head>
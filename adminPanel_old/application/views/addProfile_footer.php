<script type="text/javascript">
	var base_url = "<?php echo base_url();?>";
	</script>
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-multiselect.js"></script>             
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-tokenfield.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/moment.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/bootstrap-timepicker.min.js"></script>         
	<script src="<?php echo base_url();?>assests/js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/customJs.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/cropper.js"></script>         
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/canvas-toBlob.js"></script>         	
  <script src="<?php echo base_url();?>assests/js/bootstrap-dialog.min.js"></script>
  <script src="<?php echo base_url();?>assests/js/jquery.validate.js"></script>
	
	
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script>   
<script type="text/javascript">	
$(document).ready(function(){	
		//Code for DatePicker 
		var dateNow = new Date();
		$('.timepickerFromTime').timepicker({
            defaultDate:dateNow
        });		 
		 
		 //Code for multi select dropdown
		 App.init(); 
			$('#businessCategory').multiselect({
				buttonWidth: '380px',
				noneSelectedText: 'Select Something (required)',
				minHeight:400,	
				onChange: function(option, checked) {                
					var selectedOptions = $('#businessCategory option:selected');
 
					if (selectedOptions.length >= 3) {						
						var nonSelectedOptions = $('#businessCategory option').filter(function() {
                        return !$(this).is(':selected');
					});
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('#businessCategory option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
				
			});	
			
			
			//Code for Token field
				$('#tokenfield').tokenfield({			
					showAutocompleteOnFocus: true
				});
		

			$('#topProfileButton').on('click', function(){	
				$.ajax({
					url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
					success: function(response){	
						if(response == 1){
							window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
						}else if(response == 0){
							window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
						}						
					}
				});	
			});		
				
});
</script>
<?php	if($msg == "LGD"){		?>
	<script type='text/javascript'>	
		showDialog('Please fill up your profile page.');		    
	</script>        
<?php }elseif($msg == "PTDN"){ ?>
<script type='text/javascript'>	
		showDialog('Payment Done Succesfully.');		    
	</script>        
<?php } ?>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
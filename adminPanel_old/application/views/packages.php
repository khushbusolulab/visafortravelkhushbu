<body>
<?php  include("menu.php"); ?>
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
        <!-- BEGIN PRICING OPTION -->
        <div class="row-fluid margin-bottom-40" style="width:100% !important;">
            <div class="span3 pricing-active red">
                <div class="pricing-head">
                    <h3><?php echo $packageData[3]['packageName'];?> Package</h3>
                    <h4><i>Free</i> <span>Forever</span></h4>
                </div>                
                <div class="pricing-footer">
                    <p><?php echo $packageData[3]['packageDetails'];?></p>
                    <a href="<?php echo base_url();?>index.php/Home/addFreePackage" class="btn btn-success" >Selected</a>  
                </div>
            </div>


            <div class="span3 pricing-active blue">
                <div class="pricing-head">
                    <h3><?php echo $packageData[2]['packageName'];?> Package</h3>
                    <h4><i>$</i><i><?php echo $packageData[2]['packageAmount'];?></i> <span>Per Month</span></h4>
                </div>
                
                <div class="pricing-footer">
                    <p><?php echo $packageData[2]['packageDetails'];?></p>
                    <a href="javascript:void(0);" class="btn theme-btn selectThisOption" data-PackageId = "<?php echo $packageData[2]['packageId'];?>" data-amount="<?php echo $packageData[2]['packageAmount'];?>" >Select</a>  
                </div>
            </div>

            <div class="span3 pricing-active green">
                <div class="pricing-head pricing-head-active">
                    <h3><?php echo $packageData[1]['packageName'];?> Package</h3>
                    <h4><i>$</i><i><?php echo $packageData[1]['packageAmount'];?></i> <span>Per Month</span></h4>
                </div>
               
                <div class="pricing-footer">
                    <p><?php echo $packageData[1]['packageDetails'];?></p>
                    <a href="javascript:void(0);" class="btn theme-btn selectThisOption" data-PackageId = "<?php echo $packageData[1]['packageId'];?>" data-amount="<?php echo $packageData[1]['packageAmount'];?>" >Select</a>  
                </div>
            </div>


            <div class="span3 pricing-active yellow">
                <div class="pricing-head">
                    <h3><?php echo $packageData[0]['packageName'];?> Package</h3>
                    <h4><i>$</i><i><?php echo $packageData[0]['packageAmount'];?></i> <span>Per Month</span></h4>
                </div>
                
                <div class="pricing-footer">
                    <p><?php echo $packageData[0]['packageDetails'];?></p>
                    <a href="javascript:void(0);" class="btn theme-btn selectThisOption" data-PackageId = "<?php echo $packageData[0]['packageId'];?>"  data-amount="<?php echo $packageData[0]['packageAmount'];?>" >Select</a>  
                </div>
            </div>

        </div>
        <!-- END PRICING OPTION -->        
		<!--<form name="paymentForm" id="paymentForm" method="post" action="<?php echo base_url()."index.php/Home/paymentOption";?>">-->
		<form name="paymentForm" id="paymentForm" method="post" action="<?php echo base_url()."index.php/Home/insertOrderMaster";?>">
			<div class="row">
				<div class="span4 pull-left">
					Amount Payable : <span id="amountDiv">0</span> USD 
					<!--<a href="<?php echo base_url()."index.php/Home/paymentOption";?>" class="btn btn-primary" id="showPayNowButton" style="display:none;">Pay Now</a> -->
					<input type="hidden" name="hidAmmountValue" id="hidAmmountValue" value="">
					<input type="hidden" name="hidPackagePurchased" id="hidPackagePurchased" value="">
					<input type="hidden" name="hidbusinessId" id="hidbusinessId" value="<?php echo $_SESSION['businessId'];?>">
					<input type="submit" value="Pay Now" class="btn btn-primary" id="showPayNowButton" style="display:none;" >
				</div>
			</div>
		</form>
    </div>
<body>
	<?php  include("menu.php"); ?>
    <!-- BEGIN BREADCRUMBS -->   
<form name="addProfileForm" id="addProfileForm" method="post" action="<?php echo base_url();?>index.php/Home/performAddProfile" enctype="multipart/form-data"  style="background-color: #fff;">	
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;"></div>    
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Create Business Profile To get started </h3>
			</div>
		</div> 
		 
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Business Display Name</label>
						<input type="text" name="businessName" id="businessName" class="m-wrap medium form-control" value="<?php echo $_SESSION['businessName']?>"/>						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Contact Number</label>
						<input type="number" class="m-wrap medium form-control" name="businessPhone" id="businessPhone" value="<?php echo $_SESSION['businessPhone']?>" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Business E-Mail Address</label>
						<input type="text" class="m-wrap medium form-control"  name="businessEmail" id="businessEmail" value="<?php echo $_SESSION['businessEmail']?>" />						
					</div>
				</div>
			</div>
		</div>  
		
		<div class="clearfix margin-bottom-10"></div>
		
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Address Line 1</label>
						<input type="text" class="m-wrap medium form-control" name="businessAddress1" id="businessAddress1" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Address Line 2</label>
						<input type="text" class="m-wrap medium form-control" name="businessAddress2" id="businessAddress2" />						
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Area / Locality</label>
						<input type="text" class="m-wrap medium form-control" name="businessArea" id="businessArea" />						
					</div>
				</div>
			</div>
		</div>    
		
		
		<div class="clearfix margin-bottom-10"></div>
		
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<select class="medium m-wrap form-control" name="country" id="country" >
							<option value="">Select Country</option>							
							<?php foreach($countryValues as $country_value){ ?>
								<option value="<?php echo $country_value['countryId'];?>"><?php echo $country_value['countryName'];?></option>							
							<?php  } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<select class="medium m-wrap form-control" id="state" name="state" >
							<option value="">Select State</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<select class="medium m-wrap form-control" id="city" name="city" >
							<option value="">Select City</option>							
						</select>
					</div>
				</div>
			</div>			
		</div>  
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">		
			<div class="col-sm-4">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Select Business Category *<sub>Maximum 3</sub></label>
						<select id="businessCategory" name="businessCategory[]" multiple="multiple" class="medium form-control" >
						<?php foreach($categoryValues as $category_value) { ?>
							<option value="<?php echo $category_value['categoryId']?>"><?php echo $category_value['categoryName']?></option>
						<?php } ?>								
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="form-group">					
					<div class="controls">
						<label class="control-label">Upload Office Picture</label>						
						<input type="file" name="OficeFileUpload" id="OficeFileUpload" accept='image/*' class="form-control">						
					</div>
				</div>
			</div>
			<!-- Code For Image Preview -->
			<div class="jFiler-items jFiler-row" id="previewContainer" style="display:none;">
				<ul class="jFiler-items-list jFiler-items-grid">
					<li class="jFiler-item" data-jfiler-index="1" style="">
						<div class="jFiler-item-container">
							<div class="jFiler-item-inner">
								<div class="jFiler-item-thumb">
									<div class="jFiler-item-status"></div>                        
									<div class="jFiler-item-thumb-image">
										<img id="previewCroppedImage" src="" draggable="false" style="width:100% !important;">
									</div>
								</div>
								<div class="jFiler-item-assets jFiler-row">
									<ul class="list-inline pull-left">
										<li></li>
									</ul>
									<ul class="list-inline pull-right">
										<li><a id="deleteThisPreviewedImage" class="icon-jfi-trash jFiler-item-trash-action" data-imageid="<?php echo $_SESSION['businessId']?>"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>   
				</ul>
			</div>
		</div>
		
		<!-- Modal -->		
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Crop Office Picture</h3>
			</div>
			<div class="modal-body">
				<div>
					<img id="image" src="#">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="saveOfiiceCroppedImage" type="button"> Save changes</button>
			</div>
		</div>
		
		
		
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="control-label">Services <sub>(Mention your best services to find your business in search boxes)</sub></label>
					<input type="text" id="tokenfield" name="businessServices"  id="businessServices" value="Visa,Travel Consultancy"  class="form-control token-example-field m-wrap large" style="border:2px solid #ccc !important;" />
				</div>
			</div>	
		</div>
		
		<div class="clearfix margin-bottom-10"></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="control-label">Drag And Drop Service Pictures</label>				
					<input type="file" name="ServicesFileUpload" id="ServicesFileUpload" accept='image/*' class="form-control" >
				</div>
			</div>	
		
			<div class="col-sm-12">
				<div class="jFiler-items jFiler-row" id="previewContainerForServiceImages" style="display:none;">
					<ul class="jFiler-items-list jFiler-items-grid" id="ulForSserviceImages">
				   
					</ul>
				</div>
			</div>
		</div>
		
		
		<!-- Modal -->		
		<div id="myModalServices" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Crop Services Picture</h3>
			</div>
			<div class="modal-body">
				<div>
					<img id="imageServices" src="#">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="saveOfiiceServicesCroppedImage" type="button"> Save changes</button>
			</div>
		</div>
		
		
		<div class="clearfix margin-bottom-10"></div>
		<!--<div class="row">
			<div class="span12">
				<label class="control-label">Operating Hours </sub></label>				
				<div class="clearfix margin-bottom-10"></div>
			</div>	
		</div>-->
		<div class="row">
			<div class="col-sm-6">
				<div class="">
				<div class="col-xs-4">
					<label class="control-label">Operating Hours </sub></label>				
				</div>	
				<div class="col-xs-4">
					<label class="control-label">From Time </sub></label>				
				</div>	
				<div class="col-xs-4">
					<label class="control-label">To Time </sub></label>				
				</div>
				</div>
			</div>
		</div>
		<div class="clearfix margin-bottom-10"></div>
		<?php
		$days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
		for($intI = 1; $intI <= 7; $intI++) { ?>
			
		<div class="row">
			<div class="col-sm-6">
				<div class="">
				<div class="col-xs-4">
					<div class="form-group">					
						<div class="controls">						
							<input type="checkbox" name="weekdays[<?php echo $intI;?>]" id="weekdays" class="daysCheckBox form-control"/><?php echo $days[$intI]; ?>
						</div>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">					
						<div class="controls bootstrap-timepicker timepicker " >						
							<input type="text" name="fromTime[<?php echo $intI;?>]" id="timepickerFromTime_<?php echo $intI;?>"  placeholder="From" class="m-wrap small pull-left form-control timepickerFromTime" >						 
						</div>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">					
						<div class="controls">						
							<input type="text" name="toTime[<?php echo $intI;?>]" id="timepickerToTime_<?php echo $intI;?>" placeholder="To" class="m-wrap small pull-left timepickerFromTime form-control" >
						</div>
					</div>
				</div>
				</div>
			</div>
			</div>		
		<div class="clearfix margin-bottom-10"></div>
	<?php 	} ?>
		 
		 <div class="row text-center">	 	
			<div class="col-sm-12">
				<div class="form-group">					
					<div class="controls">						
						<input type="Submit" name="Save" value="Save And Publish" class="btn btn-info">
					</div>
				</div>
			</div>			
		</div>
		<div class="clearfix margin-bottom-10"></div>		
		<div class="row">
			<div class="col-sm-12">
				<label class="control-label">*<sup>Promotion Offers for 3 months to select 3 categories, after that charge for selection for more than 1 category.</sup></label>		
			</div>	
		</div>		
		  
		</div>	 	
	<input type="hidden" name="hidCountryName" id="hidCountryName" value=""> 
	<input type="hidden" name="hidStateName" id="hidStateName" value=""> 
	<input type="hidden" name="hidCityName" id="hidCityName" value=""> 
</form>
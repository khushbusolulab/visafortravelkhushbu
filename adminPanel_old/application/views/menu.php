<div class="front-header">
	<div class="container">
		<div class="navbar navbar-default">
			<div>
				<div class="navbar-header">                    
					<a class="brand logo-v1" href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assests/img/logo.jpg" id="logoimg" alt="" style="margin-top:0px; height:70px;">
					</a>                    
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li <?php if($this->uri->segment(2) == "dashboard" || $this->uri->segment(2) == "dashBoard") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/dashboard">DashBoard</a></li>                                                        
						<li <?php if($this->uri->segment(2) == "analytics") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/analytics">Analytics</a></li>
						<li <?php if($this->uri->segment(2) == "reports") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/reports">Reports</a></li>
						<li <?php if($this->uri->segment(2) == "subscription") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/subscription">Subscription</a></li>                            
						<li <?php if($this->uri->segment(2) == "paymentHistory") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/paymentHistory">Payment History</a></li>                            
						<li <?php if($this->uri->segment(2) == "aboutUs") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/aboutUs">About Us</a></li>                                                        
						<li <?php if($this->uri->segment(2) == "services") echo "class='active' ";?> ><a href="<?php echo base_url();?>index.php/Home/services">Services</a></li>                                                                                    
						<li><a href="<?php echo base_url()."index.php/Home/logout"?>">Logout</a></li>                           						   
						<li><a href="javascript:void(0);" id="topProfileButton"><i class="icon-user" style=" font-size: 20px; padding:10px 16px 10px;"></i></a></li>
					</ul>                                               
				</div>                    
			</div>
		</div>
	</div>
</div>      
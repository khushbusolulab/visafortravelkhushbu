<!-- END CONTAINER --> 
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/jquery.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/bootstrap-datepicker.min.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/jquery.dataTables.js" type="text/javascript"></script>    
    <!--<script src="<?php echo base_url();?>assests/js/select2.min.js" type="text/javascript"></script>    -->
    <script src="<?php echo base_url();?>assests/js/DT_bootstrap.js" type="text/javascript"></script>    
    <script src="<?php echo base_url();?>assests/js/table-editable.js" type="text/javascript"></script>    
    <!--<script src="<?php echo base_url();?>assests/js/bootstrap-modal-popover.js" type="text/javascript"></script>    -->
    
    
    <script type="text/javascript" src="<?php echo base_url();?>assests/js/hover-dropdown.js"></script>         
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>  
    <![endif]-->   
    <!-- END CORE PLUGINS -->
    <script src="<?php echo base_url();?>assests/js/app.js"></script>      
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
			TableEditable.init();
			$('#tabs').tab();
			$('#topProfileButton').on('click', function(){	
				$.ajax({
					url: '<?php echo base_url();?>index.php/Home/redirectUserProfile',					
					success: function(response){	
						if(response == 1){
							window.location = "<?php echo base_url();?>index.php/Home/editProfile";	
						}else if(response == 0){
							window.location = "<?php echo base_url();?>index.php/Home/addProfile";	
						}						
					}
				});	
			});
			
			
		var date_input=$('input[id="fromDate"]'); 
		var date_input1=$('input[id="toDate"]'); 
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({		
			format: 'yyyy/mm/dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
			widgetPositioning: {
				horizontal: 'right',
				vertical: 'top'
			}
		})
		
		date_input1.datepicker({			
			format: 'yyyy/mm/dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})	


		$("#packageSelection").on("change",function(){			
			  $( "#reportSelectorForm" ).submit();
		});
		
		
		
		//Code to open popup on Hover for clicks
		
		$('.clicks').each(function() {			
			var $this = $(this);
			$this.popover({
				placement: 'bottom',
				html: true,
				content: $('#clicksContent').html()  
			});
			$this.on('show.bs.popover', function () {				
				var currentElementDate = $($this).attr("data-eleDate");	
				var hidpackageSelection = $("#hidpackageSelection").val();	
				$.ajax({
					url:  '<?php echo base_url();?>index.php/Home/getClicksBifurcationWithPackage',
					data: "currentElementDate="+currentElementDate+"&hidpackageSelection="+hidpackageSelection,
					method: "POST",			
					success: function(response){
						if(response.status == "sucess"){
							//Code for Country Counts
							html = "<table border='2' style='width:100%;'><tr><td>Country</td><td>Clicks</td></tr>";
							for (i = 0; i < response.country.length; i++) { 
								html += "<tr>" +
											"<td>"+response.country[i].countryName+"</td>" +
											"<td>"+response.country[i].totClicks+"</td>" +
										"</tr>";
							}		
							html += "</table>";
							$("#countryClicks").append(html);
							
							
							//Code for City Counts
							html_city = "<table border='2' style='width:100%;'><tr><td>City</td><td>Clicks</td></tr>";
							for (i = 0; i < response.city.length; i++) { 
								html_city += "<tr>" +
											"<td>"+response.city[i].cityName+"</td>" +
											"<td>"+response.city[i].totClicks+"</td>" +
										"</tr>";
							}		
							html_city += "</table>";
							$("#cityClicks").append(html_city);						
							$("#loaderDivClicks").hide();	
						}else if(response.status == "error"){
							html = "<table border='2' style='width:100%;'><tr><td>No Data Found</td></tr></table>";
							$("#countryClicks").append(html);
							$("#cityClicks").append(html);	
							$("#loaderDivClicks").hide();			
						}						
					}
				});	
			});
		});
		
		
		//Code to open popup on Hover for bookmarks
		$('.bookmarks').each(function() {
			var $this = $(this);
			$this.popover({				
				placement: 'bottom',
				html: true,
				content: $('#bookmarksContent').html()  
			});
			$this.on('show.bs.popover', function () {				
				var currentElementDate = $($this).attr("data-eleDate");				
				$.ajax({
					url:  '<?php echo base_url();?>index.php/Home/getBookmarksBifurcation',
					data: "currentElementDate="+currentElementDate,
					method: "POST",			
					success: function(response){						 
						if(response.status == "sucess"){
							//Code for Country Counts
							html = "<table border='2' style='width:100%;'><tr><td>Country</td><td>Bookmark(s)</td></tr>";
							for (i = 0; i < response.country.length; i++) { 
								html += "<tr>" +
											"<td>"+response.country[i].countryName+"</td>" +
											"<td>"+response.country[i].totClicks+"</td>" +
										"</tr>";
							}		
							html += "</table>";
							$("#countrybookmark").append(html);
							
							
							//Code for City Counts
							html_city = "<table border='2' style='width:100%;'><tr><td>City</td><td>Bookmark(s)</td></tr>";
							for (i = 0; i < response.city.length; i++) { 
								html_city += "<tr>" +
											"<td>"+response.city[i].cityName+"</td>" +
											"<td>"+response.city[i].totClicks+"</td>" +
										"</tr>";
							}		
							html_city += "</table>";
							$("#citybookmark").append(html_city);						
							$("#loaderDivBookmarks").hide();	
						}else if(response.status == "error"){
							html = "<table border='2' style='width:100%;'><tr><td>No Data Found</td></tr></table>";
							$("#countrybookmark").append(html);
							$("#citybookmark").append(html);	
							$("#loaderDivBookmarks").hide();			
						}						
					}
				});	
			});
		});
		
		
		
		
		//Code to open popup on Hover for reviews
		$('.reviews').each(function() {
			var $this = $(this);
			$this.popover({				
				placement: 'bottom',
				html: true,
				content: $('#reviewsContent').html()  
			});
			$this.on('show.bs.popover', function () {				
				var currentElementDate = $($this).attr("data-eleDate");				
				$.ajax({
					url:  '<?php echo base_url();?>index.php/Home/getReviewsBifurcation',
					data: "currentElementDate="+currentElementDate,
					method: "POST",			
					success: function(response){						 					
						if(response.status == "sucess"){
							//Code for Country Counts
							html = "<table border='2' style='width:100%;'><tr><td>Country</td><td>Review(s)</td></tr>";
							for (i = 0; i < response.country.length; i++) { 
								html += "<tr>" +
											"<td>"+response.country[i].countryName+"</td>" +
											"<td>"+response.country[i].totClicks+"</td>" +
										"</tr>";
							}		
							html += "</table>";
							$("#countryreviwews").append(html);
							
							
							//Code for City Counts
							html_city = "<table border='2' style='width:100%;'><tr><td>City</td><td>Review(s)</td></tr>";
							for (i = 0; i < response.city.length; i++) { 
								html_city += "<tr>" +
											"<td>"+response.city[i].cityName+"</td>" +
											"<td>"+response.city[i].totClicks+"</td>" +
										"</tr>";
							}		
							html_city += "</table>";
							$("#cityreviews").append(html_city);						
							$("#loaderDivReviews").hide();	
						}else if(response.status == "error"){
							html = "<table border='2' style='width:100%;'><tr><td>No Data Found</td></tr></table>";
							$("#countryreviwews").append(html);
							$("#cityreviews").append(html);	
							$("#loaderDivReviews").hide();			
						}						
					}
				});	
			});
		});
		
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
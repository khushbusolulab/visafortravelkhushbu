 <?php	
 
 $businessesDataAll = $businessesData['businessesData']; 
 if($msg == "Succ"){		?>
	<script type="text/javascript">	
		showDialog("Package Has been purchased succesfully");		    
	</script>        
<?php } ?>
<body>
<?php  include("menu.php"); ?>	
    <!-- BEGIN BREADCRUMBS -->   
    <div class="row-fluid breadcrumbs margin-bottom-40" style="width: 100% !important;">
        
    </div>
    <!-- END BREADCRUMBS -->

    <!-- BEGIN CONTAINER -->   
    <div class="container min-hight">
	<div class="row">
		<div class="col-sm-12">
			<h3 class="page-title">Dashboard</h3>
		</div>
	</div>
	<div class="row">
		<?php foreach($packagePurchased['packageInfo'] as $value) { ?>
		<div class="col-sm-4">
			<div class="package mar-bot20">
				<h4 class="margin0"><strong><?php echo $value['packageName'];?> Package</strong></h4>
				<?php if($value['packageId'] == "4") { ?>
					<span class="">Never Expires</span>
				<?php }else{?>
					<span class="">Expires On <?php echo $value['packageExpiryDate'];?></span>				
				<?php }?>
				 <div class="clearfix"></div>
			</div>
		</div>
		<?php } ?>			
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div class="overview text-center mar-bot20">
				<h4 class="text-center">Ads Clicked Today</h4>
					<span class="text-center block mar-bot5" ><?php echo $businessesDataAll['businessesNoOfViews'];?></span>
					<?php if($packagePurchased['packagePurchased'] == "YES"){	?>
					<span class="text-center block" ><a href="<?php echo base_url();?>index.php/Home/analytics">View All</a></span>
					<?php } ?>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="overview text-center mar-bot20">
				<h4 class="text-center">Number of Reviews</h4>
					<span class="text-center block mar-bot5" ><?php echo $businessesDataAll['noOfReviews'];?></span>
					<?php if($packagePurchased['packagePurchased'] == "YES"){	?>
					<span class="text-center block" ><a href="<?php echo base_url();?>index.php/Home/analytics">View All</a></span>
					<?php } ?>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="overview text-center mar-bot20">
				<h4 class="text-center">Number of bookmarks</h4>
					<span class="text-center block mar-bot5" ><?php echo $businessesDataAll['noOfBookmarks'];?></span>
					<?php if($packagePurchased['packagePurchased'] == "YES"){	?>
					<span class="text-center block" ><a href="<?php echo base_url();?>index.php/Home/analytics">View All</a></span>
					<?php } ?>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="overview text-center mar-bot20">
				<h4 class="text-center">Average Ratings</h4>
					<span class="text-center block mar-bot5" ><?php echo $businessesDataAll['avgRating'];?></span>
					<?php if($packagePurchased['packagePurchased'] == "YES"){	?>
					<span class="text-center block" ><a href="<?php echo base_url();?>index.php/Home/analytics">View All</a></span>
					<?php } ?>
			</div>
		</div>
		 
	</div>
	<div class="row">					
        	<div class="col-sm-12">
						<h3>Recent Reviews Received</h3>
						<div class="well">
						<?php 							
						if(!empty($businessesData['RecentReviews'])){
						foreach($businessesData['RecentReviews'] as $value){ 
						?>
							<blockquote>
								<span class="pull-left">
									<?php 
										if(!empty($value['profilePicture'])) {	
										$filename = "./uploads/userImages/".$value['profilePicture'];	
										if(file_exists($filename)){	
										?>
										<img src="<?php echo base_url();?>/uploads/userImages/<?php echo $value["profilePicture"];?>" class="width-100">
									<?php 
										}  } else {?>
											<img src="<?php echo base_url();?>assests/img/User.png" class="width-100">
									<?php } ?>
								</span>
								<span class="pull-left">
									<label class="user-name-label"><?php echo $value['fullName'];?></label>
									<span class="rating-static rating-<?php echo $value['rating'];?>0 class-new-rating"></span>
								</span>
								<span class="pull-right">
									<?php echo $value['createdDate'];?>
								</span>
								<div class="clearfix"></div>
									<p class="margin-top-20"><?php echo $value['rateComments'];?></p>
							</blockquote>		
							<div class="clearfix"></div>
						<?php 
							}
						}else{
						?>	
							<blockquote>
								<p>No Recent Reviews found</p>
							</blockquote>
					<?php 	}
						?>		
						</div>
					</div>
    </div>
    </div>   
<body>
<?php  include("header1.php"); ?>
<div class="row breadcrumbs margin-bottom-40" style="width: 100% !important;"></div>    
<div class="container min-hight">
	<div class="top-title">
	  <h2 class="font-24 font-w-bold m-b-15">FAQs</h2>
	  <div class="top-title-line left"></div>
        </div>
	<div class="">
	  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	</div>
</div>


<!-- Login Form Popup -->
  <div class="modal fade" id="homePageLoginForm" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title text-center" id="myModalLabel">
                    Access Your Business Account
                </h4>
            </div>
            <div class="modal-body">
		<div class="row">
			<div class="col-sm-6">
				<form role="form" method="post" action="<?php echo base_url()."index.php/Home/registerBusiness";?>" id="businessRegisterForm">
					<span class="title-head">Register Now</span>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessMerchantName" id="businessMerchantName" placeholder="Your Name"/>
					</div>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessName" id="businessName" placeholder="Business Name"/>
					</div>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessEmail" id="businessEmail" placeholder="Email Address"/>
					</div>
					<div class="form-group">
					      <input type="text" class="form-control" name="businessPhone" id="businessPhone" placeholder="Mobile Number"/>
					</div>
					<div class="form-group">
					    <input type="checkbox" class="form-control" />
					    <label>I agree to Terms & Conditions</label>
					</div>
					<div class="form-group text-center">
					    <button type="submit" class="btn btn-primary" >Submit</button>
					</div>
					<!--<div class="form-group">
						<div class="col-sm-12">
						    <span class="pull-left">Not a Registered User?Please<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageRegisterForm" > Register Here!</a></span>
						    <span class="pull-right"><a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageForgotPasswordForm" > Can't Access your account?</a></span>
						</div>
					</div>-->
				</form>
			</div>
			<div class="col-sm-6">
				<form role="form" method="post" action="<?php echo base_url()."index.php/Home/businessMerchantLogin";?>" id="businessMerchantLoginForm" >
					<span>Already registered with us?</span>
					<span class="title-head">Login here</span>
					<div class="form-group">
						<input type="text" class="form-control" id="businessEmailLogin" name="businessEmailLogin" placeholder="Email"/>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="passwordLogin" name="passwordLogin" placeholder="Password"/>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-primary" >Submit</button>
					</div>
					<div class="form-group text-center">
						<span><a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageForgotPasswordForm" > Can't Access your account?</a></span>
					</div>
				</form>
			</div>
		</div>
            </div>                        
        </div>
    </div>
</div>


<!-- Register Form Popup -->
  <div class="modal fade" id="homePageRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Register Now
                </h4>
            </div>
            <div class="modal-body">                
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()."index.php/Home/registerBusiness";?>" id="businessRegisterForm">
                  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="businessMerchantName" id="businessMerchantName" placeholder="Your Name"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="businessName" id="businessName" placeholder="Business Name"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="businessEmail" id="businessEmail" placeholder="Email Address"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                    </div>
                  </div>
				  <div class="form-group">                    
                    <div class="col-sm-12">
                        <input type="number" class="form-control" name="businessPhone" id="businessPhone" placeholder="Mobile Number"/>
                    </div>
                  </div>                  
					<div class="form-group">
						<div class="col-sm-6">														
							<span class="text-center">I Agree to Terms And Conditions </span><input type="checkbox" class="pull-left" name="check" id="check" style="width:20%; position:relative;"/>							                  
						</div>                  
					</div>                  
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button type="submit" class="btn btn-primary" >Register</button>
					  <span class="pull-right">Already A Member? Click Here to <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#homePageLoginForm" >Login</a></span>
                    </div>
                  </div>				   
                </form>
            </div>                        
        </div>
    </div>
</div>
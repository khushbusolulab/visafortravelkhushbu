var FormValidation = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#staffProfileForm');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
				errorPlacement: function(error, element){
					if(element.attr('type') == 'file') {
						error.addClass('help-small no-left-padding disp-block');
						element.closest('.fileupload.fileupload-new').append(error);
					}
					else {						
						error.addClass('help-small no-left-padding disp-block').insertAfter(element);
					}
				},
                rules: {
                    staffMembersName: {                        
                        required: true
                    },
					staffMembersPhone: {
                        required: true,
                        number: true,                        
                        noBlankSpace: true,
						maxlength:10,
						minlength:10
                    },
                    staffMembersEmail: {
                        required: true,
                        email: true
                    },
					staffMembersAddress1: {                        
                        required: true
                    },
					staffMembersAddress2: {                        
                        required: true
                    },
					staffMembersArea: {                        
                        required: true
                    },
					countryId: {                        
                        required: true
                    },
					state: {                        
                        required: true
                    },
					city: {                        
                        required: true
                    },
					staffMembersJoiningDate: {                        
                        required: true
                    },
					refferedBy: {                        
                        required: true
                    },
					proofDocument: {                        
                        required: true
                    } 
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                   // success1.show();
				   $("#submitstaffProfileForm").prop('disabled', true);
                    error1.hide();
					form.submit();
                }
            });
    }   



	var handleValidation2 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#embassyDetailsForm');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",	
				errorPlacement: function(error, element){					
					error.addClass('help-small no-left-padding disp-block').insertAfter(element);					
				},				
                rules: {                   
					residentCountry: {                        
                        required: true
                    },
					foreignCountry: {                        
                        required: true,
						notEqualTo: ['#residentCountry']
                    },
					foreignCity: {                        
                        required: true
                    },
					embassyContact: {                        
                        required: true,
                        number: true,                                                
						maxlength:10,
						minlength:10
                    },
					embassyAddress: {                        
                        required: true
                    } 
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //success1.show();
					$("#submitembassyDetailsForm").prop('disabled', true);
                    error1.hide();
					form.submit();
                }
            });
    }
	
	
	
	
	var handleValidation3 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#passportDetailsForm');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",	
				errorPlacement: function(error, element){					
					error.addClass('help-small no-left-padding disp-block').insertAfter(element);					
				},				
                rules: {                   
					countryId: {                        
                        required: true
                    },
					countryDetails: {                        
                        required: true
                    } 
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //success1.show();
					$("#submitpassportDetailsForm").prop('disabled', true);
                    error1.hide();
					form.submit();
                }
            });
    }
	
	
	
	var handleValidation4 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#countryDetailsForm');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",	
				errorPlacement: function(error, element){					
					error.addClass('help-small no-left-padding disp-block').insertAfter(element);					
				},				
                rules: {                   
					countryId: {                        
                        required: true
                    },
					countryDetails: {                        
                        required: true
                    } 
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //success1.show();
					$("#submitcountryDetailsForm").prop('disabled', true);
                    error1.hide();
					form.submit();
                }
            });
    }
	
	
	var handleValidation5 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#businessProfileForm');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",	
				errorPlacement: function(error, element){
					if($(element).hasClass('daysCheckBox')) {
						//var parentElement = $(element).closest('.row-fluid');
						var parentElement = $(element).closest('.row');
						error.addClass('help-small no-left-padding disp-block');
						parentElement.append(error);
					}
					else {
						if($(element).attr("id") == "serviceImageCount"){
							error.addClass('help-small no-left-padding disp-block').insertAfter($("#ServicesFileUpload"));
							//$(element).find('#serviceImageUploadDIv').addClass('error');
							//$("#serviceImageCount").closest('.form-group').addClass('error');
							//$(element).closest('.form-group').addClass('error');
							//var parentElement = $(element).closest('.form-group');
							var parentElement = $(element).closest('.form-group');
							parentElement.addClass('serviceImageEror');
						}else{
							error.addClass('help-small no-left-padding disp-block').insertAfter(element);
						}
					}
				},
                rules: {                   
					businessMerchantName: {                        
                        required: true
                    },
					businessName: {                        
                        required: true
                    },
					businessPhone: {
                        required: true,
                        number: true,                                                
						maxlength:10,
						minlength:10
                    },
					businessEmail: {
                        required: true,
                        email: true
                    },
					businessAddress1: {                        
                        required: true
                    },
					businessAddress2: {                        
                        required: true
                    },
					businessArea: {                        
                        required: true
                    },
					countryId: {                        
                        required: true
                    },
					state: {                        
                        required: true
                    },
					city: {                        
                        required: true
                    },
					'businessCategory[]': {                        
                        needsSelection: true
                    },
					OficeFileUpload: {                        
                        required: true
                    },
					serviceImageCount: {                        
                        required: true,
						checkServiceImage: true	
                    },
					businessServices: {                        
                        required: true
                    },
					monthsDropdown: {                        
                        checkForPackage: true
                    },
					chequeNumber: {                        
                        checkForPackage: true
                    },
					paymentNotes: {                        
                        checkForPackage: true
                    },
					'weekdays[1]': {
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    },
					'weekdays[2]': {                                                 
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    },
					'weekdays[3]': {                                                 
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    },
					'weekdays[4]': {                                                 
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    },
					'weekdays[5]': {                                                 
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    },
					'weekdays[6]': {                                                 
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    },
					'weekdays[7]': {                                                 
						require_from_group:[1, '.daysCheckBox'],
						requireDays:true
                    }
					 					
                },
				messages: {
					'weekdays[1]': {
						require_from_group:"Please select at least one day."
                    },
					'weekdays[2]': {
						require_from_group:"Please select at least one day."
                    },
					'weekdays[3]': {
						require_from_group:"Please select at least one day."
                    },
					'weekdays[4]': {
						require_from_group:"Please select at least one day."
                    },
					'weekdays[5]': {
						require_from_group:"Please select at least one day."
                    },
					'weekdays[6]': {
						require_from_group:"Please select at least one day."
                    },
					'weekdays[7]': {
						require_from_group:"Please select at least one day."
                    }
				},
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs                    
				//console.log($(element).attr("id"));
					if($(element).hasClass('daysCheckBox')) {
						//var parentElement = $(element).closest('.row-fluid');
						var parentElement = $(element).closest('.row');
						parentElement.addClass('weekcalandarerror');
					}
					else if($(element).attr("id") == "serviceImageCount"){						
						//$(element).closest('.form-group').addClass('error');
						$(element).closest('.form-group').addClass('error');
						$(element).closest('.help-inline').removeClass('ok'); // display OK icon
					}else{
						$(element).closest('.help-inline').removeClass('ok'); // display OK icon
						//$(element).closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
						$(element).closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
					}
                },

                unhighlight: function (element) { // revert the change done by hightlight
					console.log($(element));
					if($(element).hasClass('daysCheckBox')) {
						//var parentElement = $(element).closest('.row-fluid');
						var parentElement = $(element).closest('.row');
						parentElement.removeClass('weekcalandarerror');
					}
					//else if($(element).attr("id") == "serviceImageCount"){
					else if($(element).hasClass('classserviceImageCount')){
						//$(element).closest('.form-group').removeClass('error'); // set error class to the control group
					}
					else {
						//$(element).closest('.form-group').removeClass('error'); // set error class to the control group
						$(element).closest('.form-group').removeClass('error'); // set error class to the control group
					}
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
						//.closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
						.closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //success1.show();
					$("#submitcountryDetailsForm").prop('disabled', true);
                    error1.hide();
					form.submit();
                }
            });
			$(document.body).on("change", ".timepickerFromTime.fromTimeTextInput, .timepickerFromTime.toTimeTextInput", function(){
				$(this).closest('.row').find('.daysCheckBox').valid();
			});
    }
   
   var handleValidation6 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#changePasswordForm');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",	
				errorPlacement: function(error, element){					
					error.addClass('help-small no-left-padding disp-block').insertAfter(element);					
				},				
				rules: {
					password: {
						required: true,
						minlength: 6
					},					
					confirmPassword: {
						equalTo: "#password",
						minlength: 6
					}
				},

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.form-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //success1.show();
					$("#submitChangePasswordForm").prop('disabled', true);
                    error1.hide();
					form.submit();
                }
            });
    }
   

    return {
        //main function to initiate the module
        init: function () {   
            handleValidation1();
            handleValidation2();
            handleValidation3();
            handleValidation4();
            handleValidation5();
            handleValidation6();
		}

    };

}();
$(document).ready(function(){	

$.validator.addMethod("noBlankSpace", function (value, element) {
        if (!(value == '' || value.trim().length != 0)) {
            return false;
        } else {
            //$(element).val($(element).val().trim());
            return true;
        }
    }, 'Only Blanks spaces are not allowed for this field');

// $('#businessRegisterForm').submit(function(){
    // $(this).find('button[type=submit]').prop('disabled', true);	
// });

  $.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	//Validations for Register Form
	$("#businessRegisterForm").validate({
		errorPlacement: function (error, element) {
			if (element.attr("type") == "checkbox") {
				element.parent().append( error );
			} else {
				 element.after(error);
			}
		},
		rules: {
			businessMerchantName: "required",
			businessName: "required",
			//businessPhone: "required",				
			businessPhone: {                
                required: true,
				minlength:10,
				maxlength:10
            },
			password: {
				required: true,
				minlength: 6
			},				
			businessEmail: {
				required: true,
				email: true
			},				 
			check: "required"
		},
		messages: {
			businessMerchantName: "Please enter your name",
			businessName: "Please enter business name",
			//businessPhone: "Please enter valid business phone number",				
			businessPhone: {
				required: "Please enter business phone number",
				minlength: "Your phone number must be at least 10 characters long",
				maxlength: "Your phone number must be at max 10 characters long"
			},	
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			},				 
			businessEmail: "Please enter a valid email address",
			check: "Please accept our terms and conditions"				
		}
	});
	
	
	
	//Validations for Login Form
	$("#businessMerchantLoginForm").validate({	 
		rules: {			
			passwordLogin: {
				required: true,
				minlength: 6
			},				
			businessEmailLogin: {
				required: true,
				email: true
			}				 			
		},
		messages: {				
			passwordLogin: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			},				 
			businessEmailLogin: "Please enter a valid email address"			
		}
	});



	//Forgot Password Form
	$("#businessForgotPassword").validate({	 
		rules: {							
			forgotEmailID: {
				required: true,
				email: true
			}				 			
		},
		messages: {							 
			businessEmailLogin: "Please enter a valid email address"			
		}
	});	
	
	
	//Code for Country city state dropdown
	   $('#country').on('change',function(){		  
        var countryID = $(this).val();		
        if(countryID){
			var  CountryName= $('#country option:selected').text();			
			$("#hidCountryName").val(CountryName);	
            $.ajax({
                type:'POST',
                url:base_url+'index.php/Home/getStateList',
                data:'country_id='+countryID,
                success:function(html){					
                    $('#state').html(html);
                    //$('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            //$('#city').html('<option value="">Select state first</option>'); 
        }
	   });
	   
	   $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
			var  StateName= $('#state option:selected').text();			
			$("#hidStateName").val(StateName);
            $.ajax({
                type:'POST',
                url:base_url+'index.php/Home/getCityList',
                data:'state_id='+stateID,
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
	$('#city').on('change',function(){
		var  CityName= $('#city option:selected').text();		
		$("#hidCityName").val(CityName);
	});	
	
	//code for validations for add profile page
	$("#addProfileForm").validate({
		errorPlacement: function (error, element) {
			if (element.attr("type") == "checkbox") {
				element.parent().append( error );
			} else {
				 element.after(error);
			}
		},
		rules: {			
			businessName: {
				noBlankSpace: true
			},			
			businessPhone: {                
                required: true,
				minlength:10,
				maxlength:10
            },
			businessEmail: {
				required: true,
				email: true
			},	
			businessAddress1: {
				noBlankSpace: true
			},	
			businessAddress2: {
				noBlankSpace: true
			},	
			businessArea: {
				noBlankSpace: true
			},	
			businessAddress1: "required",				
			businessAddress2: "required",				
			businessArea: "required",				
			country: "required",				
			state: "required",				
			city: "required",				
			/*businessCategory: "required",	*/
			OficeFileUpload: "required",				
			businessServices: "required",				
			/*ServicesFileUpload: "required",*/
			//weekdays[]: "required",			
			 'weekdays[]': {
                required: true,
                maxlength: 2
            },
			fromTime: "required",			
			toTime: "required",			
			ignore: ':hidden:not("#businessCategory")'	
			
		},
		messages: {
			businessName: "Please enter Business Display Name",			
			businessPhone: {
				required: "Please enter business phone number",
				minlength: "Your phone number must be at least 10 characters long",
				maxlength: "Your phone number must be at max 10 characters long"
			},	
			businessEmail: "Please enter a valid email address",
			businessAddress1: "Please enter Address Line 1",
			businessAddress2: "Please enter Address Line 2",
			businessArea: "Please enter Area / Locality",
			country: "Please Select country",
			state: "Please Select state",
			city: "Please Select city",
			/*businessCategory: "Please Select atleast one Business category",*/
			OficeFileUpload: "Please Upload Office Image",
			businessServices: "Please Add atleast one business service.",
			/*ServicesFileUpload: "Please Upload atleast one Service Image.",*/
			weekdays: "Please Select atleast one working day.",
			fromTime: "Please Select From time.",
			toTime: "Please Select To time."
			
		}
	});
	
	
	
	//code for validations for add profile page
	$("#editProfileForm").validate({
		errorPlacement: function (error, element) {
			if (element.attr("type") == "checkbox") {
				element.parent().append( error );
			} else {
				 element.after(error);
			}
		},
		rules: {
			//businessName: "required",
			//businessPhone: "required",	
			businessName: {
				noBlankSpace: true
			},			
			businessPhone: {                
                required: true,
				minlength:10,
				maxlength:10
            },			
			businessEmail: {
				required: true,
				email: true
			},
			businessAddress1: {
				noBlankSpace: true
			},	
			businessAddress2: {
				noBlankSpace: true
			},	
			businessArea: {
				noBlankSpace: true
			},	
			//businessAddress1: "required",				
			//businessAddress2: "required",				
			//businessArea: "required",				
			country: "required",				
			state: "required",				
			city: "required",				
			/*businessCategory: "required",	*/
			OficeFileUpload: "required",				
			businessServices: "required",				
			/*ServicesFileUpload: "required",*/
			weekdays: "required",			
			fromTime: "required",			
			toTime: "required",			
			ignore: ':hidden:not("#businessCategory")'	
			
		},
		messages: {
			businessName: "Please enter Business Display Name",
			//businessPhone: "Please enter Contact Number",
			businessPhone: {
				required: "Please enter business phone number",
				minlength: "Your phone number must be at least 10 characters long",
				maxlength: "Your phone number must be at max 10 characters long"
			},	
			businessEmail: "Please enter a valid email address",
			businessAddress1: "Please enter Address Line 1",
			businessAddress2: "Please enter Address Line 2",
			businessArea: "Please enter Area / Locality",
			country: "Please Select country",
			state: "Please Select state",
			city: "Please Select city",
			/*businessCategory: "Please Select atleast one Business category",*/
			OficeFileUpload: "Please Upload Office Image",
			businessServices: "Please Add atleast one business service.",
			/*ServicesFileUpload: "Please Upload atleast one Service Image.",*/
			weekdays: "Please Select atleast one working day.",
			fromTime: "Please Select From time.",
			toTime: "Please Select To time."
			
		}
	});

		
	
	
		//Code for single file upload 
			function readURL(input) {				
				var url = $(input).val();				
				var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
				console.log(input.files);
			if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg") ) {
				var reader = new FileReader();
				reader.onload = function (e) {					
					$('#image').attr('src', e.target.result);
					
					$('#myModal').modal('show'); 
					//$("#myModal").css("display","block");
					//$('#myModal').show(); 
						$('#image').cropper({
						aspectRatio: 1 / 1,
						crop: function(e) {		
							console.log(e.x);
							console.log(e.y);
							console.log(e.width);
							console.log(e.height);
							console.log(e.rotate);
							console.log(e.scaleX);
							console.log(e.scaleY);
						},
						minContainerWidth: 500,
						minContainerHeight: 300,
						minCropBoxHeight:100,
						minCropBoxWidth:100,
						viewMode: 1,
						ready: function () {							
							cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
						}
					});	
				}
				reader.readAsDataURL(input.files[0]);
			}else{
				alert("Please Upload Image Only.");
				return false;
			}
		}
		
		
		
		
		function readServicesURL(input) {
			var url = $(input).val();
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg") ) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#imageServices').attr('src', e.target.result);
					
					$('#myModalServices').modal('show'); 
						$('#imageServices').cropper({
						aspectRatio: 1 / 1,
						crop: function(e) {		
							console.log(e.x);
							console.log(e.y);
							console.log(e.width);
							console.log(e.height);
							console.log(e.rotate);
							console.log(e.scaleX);
							console.log(e.scaleY);
						},
						minContainerWidth: 500,
						minContainerHeight: 300,
						minCropBoxHeight:100,
						minCropBoxWidth:100,
						viewMode: 1,
						ready: function () {
							// Strict mode: set crop box data first
							cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
						}
					});	
				}
				reader.readAsDataURL(input.files[0]);
			}
			alert("Please Upload Image Only.");
			return false;
			
		}

$("#OficeFileUpload").change(function(){
    readURL(this);	
});



$("#ServicesFileUpload").change(function(){	
    readServicesURL(this);	
});

$('#saveOfiiceCroppedImage').on('click', function(){	
	var canvasData = $('#image').cropper('getCroppedCanvas');	
	//var dataUrl = canvasData.toDataURL('image/jpeg');				
	canvasData.toBlob(function(blob){
		 var formData = new FormData();
		formData.append('croppedImage', blob);		
		$.ajax({
			url:  base_url+'index.php/Home/addOfficePicture',
			data: formData,
			method: "POST",
			processData: false,
			contentType: false,
			success: function(response){	
				if(response != ""){
					var data = jQuery.parseJSON(response);	
					$("#previewContainer").show();					
					$("#previewCroppedImage").attr('src',data['croppedImage']);					
					$('#myModal').modal('hide');
				}
			}
		});
	});
});




$('#saveOfiiceServicesCroppedImage').on('click', function(){	
	var canvasData = $('#imageServices').cropper('getCroppedCanvas');	
	//var dataUrl = canvasData.toDataURL('image/jpeg');				
	canvasData.toBlob(function(blob){
		 var formData = new FormData();
		formData.append('croppedImage', blob);		
		$.ajax({
			url:  base_url+'index.php/Home/addOfficeServicePicture',
			data: formData,
			method: "POST",
			processData: false,
			contentType: false,
			success: function(response){	
				if(response != ""){
					var data = jQuery.parseJSON(response);						
					$("#previewContainerForServiceImages").show();										
					 
					var html ="<li class='jFiler-item' data-jfiler-index='1' id='previewBox_"+data['serviceImageID']+"'>" +
						"<div class='jFiler-item-container'>" +
							"<div class='jFiler-item-inner'>" +
								"<div class='jFiler-item-thumb'>" +
									"<div class='jFiler-item-status'></div>                        " +
									"<div class='jFiler-item-thumb-image'>" +
										"<img id='previewCroppedServcieImage' src="+data['croppedImage']+" draggable='false' style='width:100% !important;'>" +
									"</div>" +
								"</div>" +
								"<div class='jFiler-item-assets jFiler-row'>" +
									"<ul class='list-inline pull-left'>" +
										"<li></li>" +
									"</ul>" +
									"<ul class='list-inline pull-right'>" +
										"<li><a class='icon-jfi-trash jFiler-item-trash-action deleteThisPreviewedImageOfServices' data-ServiceId = "+data['serviceImageID']+"></a></li>" +
									"</ul>" +
								"</div>" +
							"</div>" +
						"</div>" +
					"</li>";
					$("#ulForSserviceImages").append(html);										
					$('#myModalServices').modal('hide');
					$('#imageServices').cropper('destroy');
					$("#ServicesFileUpload").val("");
					
					
				}
			}
		});
	});
});
$(document.body).delegate('.deleteThisPreviewedImageOfServices', 'click', function (e) {
//$('.deleteThisPreviewedImageOfServices').on('click', function(){	
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataServiceId = $(this).attr("data-ServiceId");
	$.ajax({
			url:  base_url+'index.php/Home/deleteOfficeServicePicture',
			data: "dataid="+dataServiceId,
			method: "POST",			
			success: function(response){					
				if(response == "DELETED"){
					$("#previewBox_"+dataServiceId).hide();
					alert("Image Deleted");	
				}else if(response == "NOTDELETED"){
					alert("Image not deleted. Please try again");
				}				
			}
		});	
    } else {
     
    }
});



$('#deleteThisPreviewedImage').on('click', function(){	
 if (confirm("Are you sure,you want to delete this image?") == true) {
	var dataid = $(this).attr("data-imageid");
	$.ajax({
			url:  base_url+'index.php/Home/deleteOfficePicture',
			data: "dataid="+dataid,
			method: "POST",			
			success: function(response){	
				if(response == "DELETED"){
					$("#previewContainer").hide();
					alert("Image Deleted");	
				}else if(response == "NOTDELETED"){
					$("#previewContainer").hide();
					alert("Image not deleted. Please try again");
				}				
			}
		});	
    } else {
     
    }
});
	

	

	
});
function showDialog(strMessage){
	BootstrapDialog.show({
		title: 'My Visa For Travel',
		message: strMessage
	});	
}
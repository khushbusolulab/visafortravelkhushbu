$(function() {

    $('#side-menu').metisMenu();
	  var $lightbox = $('#lightbox');
        $('[data-target="#lightbox"]').on('click', function(event) {
            var $img = $(this).find('img'), 
                src = $img.attr('src'),
                alt = $img.attr('alt'),
                css = {
                    'maxWidth': $(window).width(),
                    'maxHeight': $(window).height()
                };
        
            $lightbox.find('.close').addClass('hidden');
            $lightbox.find('img').attr('src', src);
            $lightbox.find('img').attr('alt', alt);
            $lightbox.find('img').css(css);
        });
        
        $lightbox.on('shown.bs.modal', function (e) {
            var $img = $lightbox.find('img');
                
            $lightbox.find('.modal-dialog').css({'width': $img.width()});
            $lightbox.find('.close').removeClass('hidden');
        });

});

$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

	 $('#dataTables-user').DataTable({
                responsive: true
        });
		
		// $('#dataTables-ngo').DataTable({
                // responsive: true
        // });
		
		$(document).ready(function() {
			$('#dataTables-ngo').DataTable();
		} );
		
		$('#dataTables-products').DataTable({
                responsive: true
        });
		
		$('#dataTables-feedback').DataTable({
                responsive: true
        });
		 $('#dataTables-example').DataTable({
                responsive: true
        });
		$('#dataTables-Interest').DataTable({
                responsive: true
        });


});
$('#logoutButton').on('click', function(event){		
	BootstrapDialog.confirm('Are You Sure? You want to Exit?', function(result){
		if(result) {
			window.location = url+"logout";
		}
	});	 
});


$('#changeAdminEmailButton').on('click', function(event){		
	event.preventDefault();	
	var admin_id = $("#admin_id").val();
	$.ajax({
		type: "POST",				
		url: url+"getAdminEmail",
		data:'id='+admin_id,
		success: function(response){										
			if(response){				
				$("#email").val(response);
				$("#myModalNorm").show();	
			}
		}
	});		
});

function delteAreaOfInterest(val){
	BootstrapDialog.confirm('Do you want to delete this record?', function(result){
		if(result) {
			// alert(val);
			// return false;
			$.ajax({
				type: "POST",				
				url: url+"deleteInterest",
				data:'id='+val,
				success: function(response){							
					if(response){
						window.location = url+"interest/DELSCC";
					}
				}
			});
		}
	});	 	
}


function deleteCategory(val){
	BootstrapDialog.confirm('Do you want to delete this record?', function(result){
		if(result) {		
			$.ajax({
				type: "POST",				
				url: url+"deleteCategory",
				data:'id='+val,
				success: function(response){							
					if(response){
						window.location = url+"categories/DELSCC";
					}
				}
			});
		}
	});	 	
}


function delteCity(val){
	BootstrapDialog.confirm('Do you want to delete this record?', function(result){
		if(result) {			
			$.ajax({
				type: "POST",				
				url: url+"deleteCity",
				data:'id='+val,
				success: function(response){							
					if(response){
						window.location = url+"cities/DELSCC";
					}
				}
			});
		}
	});	 	
}

function delteBanner(val){	
	BootstrapDialog.confirm('Do you want to delete this record?', function(result){
		if(result) {			
			$.ajax({
				type: "POST",				
				url: url+"delteBanner",
				data:'id='+val,
				success: function(response){							
					if(response){
						window.location = url+"banner/DELSCC";
					}
				}
			});
		}
	});	 	
}

$('#activateButton').change(function () {
	var ngo_id = $("#ngo_id").val();	
	var ngo_email = $("#ngo_email").val();	
	  if($(this).prop('checked')){
			//BootstrapDialog.confirm('Do You Want Activate This NGO?', function(result){				
			//if(result) {
				$.ajax({
					type: "POST",				
					url: url+"activateNGO",
					data:'ngo_id='+ngo_id,
					success: function(response){							
						if(response){
							BootstrapDialog.show({
								title: 'HelpaNGO',
								//message: 'Ngo has been Activated.'
								message: 'You just approved NGO to use this platform.'
							});
						}
					}
				});
			//}
			//});
        }else{
           //BootstrapDialog.confirm('Do You Want to De Activate This NGO?', function(result){				
			//if(result) {
				$.ajax({
					type: "POST",				
					url: url+"deActivateNGO",
					data:'ngo_id='+ngo_id,
					success: function(response){							
						if(response){
							BootstrapDialog.show({
								title: 'HelpaNGO',
								//message: 'Ngo has been De Activated.'
								message: 'You just bared NGO from using this platform'
							});
						}
					}
				});
			//}
			//});
        }
 });
 
 
 $('#experienceActivateButton').change(function () {
	var experienceId = $("#experienceId").val();		
	  if($(this).prop('checked')){
			//BootstrapDialog.confirm('Do You Want Activate This Experinece?', function(result){				
			//if(result) {
				$.ajax({
					type: "POST",				
					url: url+"activateExperience",
					data:'experienceId='+experienceId,
					success: function(response){							
						if(response){
							BootstrapDialog.show({
								title: 'HelpaNGO',
								//message: 'Experince has been Activated.'
								message: "You approved user's experience to display with NGO profile"
							});
						}
					}
				});
			//}
			//});
        }else{
          // BootstrapDialog.confirm('Do You Want to De Activate This Experinece?', function(result){				
			//if(result) {
				$.ajax({
					type: "POST",				
					url: url+"deActivateExperience",
					data:'experienceId='+experienceId,
					success: function(response){							
						if(response){
							BootstrapDialog.show({
								title: 'HelpaNGO',
								//message: 'Experinece has been De Activated.'
								message: "You removed user's exprience to display with NGO profile"
							});
						}
					}
				});
			//}
			//});
        }
 });